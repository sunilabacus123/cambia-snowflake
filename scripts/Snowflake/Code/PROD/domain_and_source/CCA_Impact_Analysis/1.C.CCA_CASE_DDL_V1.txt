CREATE  OR REPLACE TABLE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_cost_unnest_root_BACKUP0423
(
ID varchar,
CASE_ID varchar,
STATUS varchar,
USER_ID varchar,
ENTRY_TYPE varchar,
FROM_DATE DATETIME,
TO_DATE DATETIME,
PROJECTED varchar,
ACTUAL varchar,
SAVE_TYPE_ID varchar,
EXPENSE_TYPE_ID varchar,
QUANTITY varchar,
SAVE_CATEGORY varchar,
META_DATA varchar,
read_only varchar,
COST varchar,
time_type_id varchar,
db_rowversion varchar,
is_deleted varchar,
lineage_input_domain Varchar,
lineage_input_url Varchar,
lineage_input_line_number Numeric,
lineage_ingestion_id Numeric,
lineage_batch_id Numeric,
correlationid VARCHAR,
LDG_REC_CREAT_DT TIMESTAMP, LDG_REC_UPDT_DT TIMESTAMP
);


CREATE  OR REPLACE TABLE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_resource_file_unnest_root_BACKUP0423
(
ID varchar,
PATH varchar,
CONTENTS varchar,
CREATE_DATE DATETIME,
UPDATE_DATE DATETIME,
UPDATED_BY varchar,
GUID varchar,
VARIABLE_FIELD_ID varchar,
VARIABLE_FIELD_NAME varchar,
VARIABLE_FIELD_TYPE varchar,
VARIABLE_FIELD_OPTION_ID varchar,
VARIABLE_FIELD_OPTION_VALUE varchar,
lineage_input_domain Varchar,
lineage_input_url Varchar,
lineage_input_line_number Numeric,
lineage_ingestion_id Numeric,
lineage_batch_id Numeric,
correlationid VARCHAR,
LDG_REC_CREAT_DT TIMESTAMP, LDG_REC_UPDT_DT TIMESTAMP
);



CREATE  OR REPLACE TABLE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_string_locale_unnest_root_BACKUP0423
(
ID varchar,
TYPE_ID varchar,
SUB_ID varchar,
LOCALE_ID varchar,
MG_ID varchar,
STRING varchar,
UPDATE_DATE DATETIME,
MASK varchar,
guid varchar,
protected varchar,
version varchar,
INACTIVE varchar,
db_rowversion varchar,
lineage_input_domain Varchar,
lineage_input_url Varchar,
lineage_input_line_number Numeric,
lineage_ingestion_id Numeric,
lineage_batch_id Numeric,
correlationid VARCHAR,
LDG_REC_CREAT_DT TIMESTAMP, LDG_REC_UPDT_DT TIMESTAMP
);

