DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_CASE_MEMBER_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_CASE_MEMBER_HRA_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_CONCEPT_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_CONCEPT_TYPE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_FORM_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_FORM_QUESTION_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_HRA_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_MEDICAL_CODE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_MEDICAL_CODE_SETS_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_CASE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_CASE_COST_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_CASE_SAVE_TYPE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_CASE_STATUS_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_INFO_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_INFO_NOTEPAD_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_PATIENT_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_PHYSICIAN_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_PN_ANNOTATION_COMMITREPORT_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_UM_AUTH_STATUS_TYPE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_UM_CASE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_UM_LOS_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_UM_LOS_EXTENSION_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_UM_SERVICE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_ORG_UM_SERVICE_CERT_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_RESOURCE_FILE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_CASE_STRING_LOCALE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_CONCEPT_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_CONCEPT_TYPE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_FORM_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_FORM_QUESTION_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_HRA_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEDICAL_CODE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEDICAL_CODE_SETS_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEMBER_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEMBER_COVERAGE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEMBER_EXTERNAL_DATA_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEMBER_HRA_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEMBER_P_CONCEPT_VALUE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEMBER_P_CONCEPT_VALUE_LOG_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEMBER_U_CONCEPT_VALUE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_MEMBER_U_CONCEPT_VALUE_LOG_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_ORG_CASE_SAVE_TYPE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_ORG_CASE_STATUS_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_ORG_UM_AUTH_STATUS_TYPE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_RESOURCE_FILE_UNNEST_ROOT_STREAM_COUNT;
DROP STREAM ABACUS_PRD_FND_DB.SOURCE_DATA.CCA_MEMBER_STRING_LOCALE_UNNEST_ROOT_STREAM_COUNT;
