select distinct
c.ID as case_id
,c.PATIENT_MEMBER_SOURCE_ID as member_id
,ri.ID_VALUE as FORM_ID
,ri2.ID_VALUE as QUESTION_ID
,ast.NAME as CONCEPT_NAME
,ast.ANSWER as Answer
,sl.String as Answer_sl
,ri3.ID_VALUE as CONCEPT_ID
,info.create_DATE as NOTE_CREATE_DATE
,tfq.QUESTION_TEXT as FORM_QUESTION_TEXT
,tfq.TYPE_ID as FORM_QUESTION_TYPE
,tfq.OPTIONS as FORM_QUESTION_OPTIONS
,NULL::BINARY as wh_batch_key
,current_timestamp() as wh_record_create_timestamp
,NULL::TIMESTAMP_NTZ(9) as wh_record_update_timestamp
,current_user() as wh_record_create_user_id
,current_user() as wh_record_update_user_id
FROM UAT_TRST_ENT_DB.Core.CASE_CCA c
left join UAT_TRST_ENT_DB.Core.cca_case_org_info_notepad_CCA ntpd
    on c.id=ntpd.case_id
    and  CURRENT_TIMESTAMP() >= ntpd.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < ntpd.WH_EFFECTIVE_END_TIMESTAMP
LEFT JOIN UAT_TRST_ENT_DB."CORE"."CCA_CASE_ORG_INFO_CCA" info
    on ntpd.Info_id = info.ID
    and  CURRENT_TIMESTAMP() >= info.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < info.WH_EFFECTIVE_END_TIMESTAMP
LEFT JOIN UAT_TRST_ENT_DB.Core.member_stub_CCA ms
    on c.source = ms.source
    --and c.id = split_part(ms.parent_source_id,'-',1) /*was made to restrict the status/type related updates to impact on the join*/ --- and c.sourceid=ms.parentsourceid
    and c.source_id=ms.parent_source_id
    and  CURRENT_TIMESTAMP() >= ms.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < ms.WH_EFFECTIVE_END_TIMESTAMP
LEFT JOIN (select * from (
SELECT *,
case when rn > 1 and option = '' then 'exclude'
else 'include' end as filter_option
FROM
(
SELECT *, SPLIT_PART(ast.source_id,'-',6) as option,
ROW_NUMBER() OVER (PARTITION BY (SPLIT_PART(ast.source_id,'-',1)||SPLIT_PART(ast.source_id,'-',2)||SPLIT_PART(ast.source_id,'-',3)||SPLIT_PART(ast.source_id,'-',4)||SPLIT_PART(ast.source_id,'-',5))
ORDER BY source_id DESC) AS rn
FROM UAT_TRST_ENT_DB.core.ASSESSMENT_STUB_CCA ast
where WH_EFFECTIVE_END_TIMESTAMP = '12/31/9999'::timestamp
)
)
cte
WHERE filter_option <> 'exclude')  ast
   ON ms.source = ast.source
   AND ms.source_id = ast.parent_source_id and ms.parent_source_id= ast.ms_parentsourceid /* we need to keep this join till the issue with assesment stub is resolved in the new Dedup fm we need to split the Wh_parent_source_id to join with parent_source_id of memberstub*/
   AND SPLIT_PART(ast.SOURCE_ID,'-',2) = info.id
  and  CURRENT_TIMESTAMP() >= ast.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < ast.WH_EFFECTIVE_END_TIMESTAMP
LEFT JOIN UAT_TRST_ENT_DB."CORE"."CCA_CASE_ORG_CASE_CCA" org
   on c.ID = org.ID
   and  CURRENT_TIMESTAMP() >= org.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < org.WH_EFFECTIVE_END_TIMESTAMP
LEFT JOIN UAT_TRST_ENT_DB.Core.reference_identifier_CCA ri
   ON ast.source = ri.source
   AND ast.source_id = ri.parent_source_id
   AND UPPER(RI.PARENT_TYPE) = 'ASSESSMENTSTUB'   AND UPPER(RI.ID_TYPE) = 'FORM_ID'
   and  CURRENT_TIMESTAMP() >= ri.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < ri.WH_EFFECTIVE_END_TIMESTAMP
left JOIN UAT_TRST_ENT_DB.Core.reference_identifier_CCA ri2
   ON ast.source = ri2.source
   AND ast.source_id = ri2.parent_source_id
   AND UPPER(RI2.PARENT_TYPE) = 'ASSESSMENTSTUB'   AND UPPER(RI2.ID_TYPE) = 'QUESTION_ID'
   and  CURRENT_TIMESTAMP() >= ri2.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < ri2.WH_EFFECTIVE_END_TIMESTAMP
left JOIN UAT_TRST_ENT_DB.Core.reference_identifier_CCA ri3
   ON ast.source = ri3.source
  AND ast.source_id = ri3.parent_source_id
  and  CURRENT_TIMESTAMP() >= ri3.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < ri3.WH_EFFECTIVE_END_TIMESTAMP
  AND UPPER(RI3.PARENT_TYPE) = 'ASSESSMENTSTUB'   AND UPPER(RI3.ID_TYPE) = 'CONCEPT_ID'
  left JOIN UAT_TRST_ENT_DB.core.cca_case_form_question_CCA tfq
   ON SPLIT_PART(ast.SOURCE_ID,'-',3) = tfq.FORM_ID
  AND SPLIT_PART(ast.SOURCE_ID,'-',4) = tfq.QUESTION_ID
AND ifnull(TRY_CAST(SPLIT_PART(ast.SOURCE_ID,'-',5) as INTEGER),0) = ifnull(tfq.concept_ID,0)
and  CURRENT_TIMESTAMP() >= tfq.WH_EFFECTIVE_START_TIMESTAMP AND CURRENT_TIMESTAMP() < tfq.WH_EFFECTIVE_END_TIMESTAMP
LEFT join "UAT_TRST_ENT_DB_DEDUP_CLONE"."CORE"."CCA_STRING_LOCALE" sl
on ifnull(TRY_CAST(SPLIT_PART(ast.SOURCE_ID,'-',5) as INTEGER),0) = ifnull(sl.id,0)
and SPLIT_PART(ast.SOURCE_ID,'-',6)= sl.sub_id
and sl.mg_id=50 and sl.Type_id=3
WHERE UPPER(C.SOURCE) = 'CCA'
and  CURRENT_TIMESTAMP() >= c.WH_EFFECTIVE_START_TIMESTAMP AND  CURRENT_TIMESTAMP() < c.WH_EFFECTIVE_END_TIMESTAMP
AND UPPER(MS.PARENT_TYPE) = 'CASE'
AND UPPER(AST.PARENT_TYPE) = 'MEMBERSTUB'
AND UPPER(RI.PARENT_TYPE) = 'ASSESSMENTSTUB'
AND Ast.answer is not null
AND org.UM = 0
and upper(c.type) in (select distinct upper(case_type) from UAT_TRST_ENT_DB.CLINICAL.CARE_MANAGEMENT_CASE_TYPE_INCLUSION_LOOKUP where lower(active_indicator)='yes')
AND SPLIT_PART(ast.SOURCE_ID,'-',3) = ri.id_value and ri.id_value=info.template_id
AND SPLIT_PART(ast.SOURCE_ID,'-',4) = ri2.id_value
AND SPLIT_PART(ast.SOURCE_ID,'-',5) = ri3.id_value
  AND  UPPER(AST.TYPE) = 'PROGRESS_NOTE_COMMITMENT_REPORT'
  AND UPPER(RI.PARENT_TYPE) = 'ASSESSMENTSTUB'   AND UPPER(RI.ID_TYPE) = 'FORM_ID'
   AND UPPER(RI2.PARENT_TYPE) = 'ASSESSMENTSTUB'   AND UPPER(RI2.ID_TYPE) = 'QUESTION_ID'
   AND UPPER(RI3.PARENT_TYPE) = 'ASSESSMENTSTUB'   AND UPPER(RI3.ID_TYPE) = 'CONCEPT_ID'
and ifnull(org.create_date,'1753-01-01') >='2019-06-01'
)
