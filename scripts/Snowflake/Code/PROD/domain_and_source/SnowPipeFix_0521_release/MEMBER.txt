CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.LANDING.MEMBER_ASSOCIATEDPERSON_PIPE auto_ingest=true AS
 COPY INTO ABACUS_PRD_FND_DB.LANDING.ASSOCIATEDPERSON
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:associationtype,t.$1:relationship,t.$1:rank,t.$1:contactorg,t.$1:active,t.$1:effdate,t.$1:expdate,current_timestamp(),current_timestamp(),t.$1:partition_3 from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/AssociatedPerson/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');
 

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.LANDING.MEMBER_DECEASED_PIPE auto_ingest=true AS
 COPY INTO ABACUS_PRD_FND_DB.LANDING.DECEASED
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:deceasedind,t.$1:deceasedage,t.$1:deceasedagerange,t.$1:deceaseddate,t.$1:causeofdeath,current_timestamp(),current_timestamp(),t.$1:partition_3 from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Deceased/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');
 

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.LANDING.MEMBER_EMAIL_PIPE auto_ingest=true AS
 COPY INTO ABACUS_PRD_FND_DB.LANDING.EMAIL
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:emailtype,t.$1:rank,t.$1:active,t.$1:email,t.$1:validationstatus,current_timestamp(),current_timestamp(),t.$1:partition_3 from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Email/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');
 

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.LANDING.MEMBER_LANGUAGE_PIPE auto_ingest=true AS
 COPY INTO ABACUS_PRD_FND_DB.LANDING.LANGUAGE
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:languagecode,t.$1:writtenspokencode,t.$1:primary,current_timestamp(),current_timestamp(),t.$1:partition_3 from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Language/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');
 
 
CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.LANDING.MEMBER_PHONE_PIPE auto_ingest=true AS
 COPY INTO ABACUS_PRD_FND_DB.LANDING.PHONE
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:phonetype,t.$1:phonesource,t.$1:number,t.$1:countrycode,t.$1:areacode,t.$1:localnumber,t.$1:extension,t.$1:digitcount,t.$1:formatmask,t.$1:geoarea,t.$1:geocountry,t.$1:linetype,t.$1:active,t.$1:validationstatus,t.$1:priority,current_timestamp(),current_timestamp(),t.$1:partition_3 from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Phone/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');
 