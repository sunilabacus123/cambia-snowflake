use "ABACUS_PRD_FND_DB";

SET var_ingestionid = 5972;

-- Records before the update 
SELECT 'Total Records with incorrect answers in AssessmentStub before the update' as Message,  COUNT(1) AS cnt
FROM (
        SELECT pna.*,question_options_1.form_id as fq_form_id 
        FROM
            (
                      SELECT 
                          split_part(sourceid,'-',3) as form_id,
                          split_part(sourceid,'-',4) as question_id,
                          split_part(sourceid,'-',5) as associatedconcept_id ,
                          split_part(sourceid,'-',6) as option_id,
                          answer ,
                          ingestionid 
                    FROM  LANDING.ASSESSMENTSTUB 
                    WHERE 
                          ingestionid >=$var_ingestionid 
                    AND   type =  'Progress_Note_Commitment_Report'
            ) pna  
        LEFT JOIN (
                    SELECT  *
                    FROM    SOURCE_DATA.CCA_CASE_FORM_QUESTION_UNNEST_ROOT
                    WHERE 
                            COALESCE(UPPER(TRIM(options)),'<OPTIONS/>') = '<OPTIONS/>' 
                    AND     lineage_ingestion_id >=$var_ingestionid        
                  ) question_options ON 
                            pna.form_id = question_options.form_id 
                    AND     pna.question_id = question_options.question_id 
                    AND     pna.ingestionid = question_options.lineage_ingestion_id 
        LEFT JOIN (
                    SELECT  *
                    FROM    SOURCE_DATA.CCA_CASE_FORM_QUESTION_UNNEST_ROOT
                    WHERE 
                            COALESCE(UPPER(TRIM(options)),'<OPTIONS/>') <> '<OPTIONS/>'   
                    AND     lineage_ingestion_id >=$var_ingestionid
                  ) question_options_1 ON 
                            pna.form_id = question_options_1.form_id 
                    AND     pna.question_id = question_options_1.question_id 
                    AND     COALESCE(TRIM(CAST(pna.associatedconcept_id AS VARCHAR)) ,'NA') = COALESCE(TRIM(CAST(question_options_1.concept_id AS VARCHAR)) ,'NA')  
                        AND     pna.ingestionid = question_options_1.lineage_ingestion_id
        WHERE 
                question_options.form_id is null
        ) pna
LEFT JOIN ( 
            SELECT  *
            FROM    SOURCE_DATA.CCA_CASE_STRING_LOCALE_UNNEST_ROOT 
            WHERE   lineage_ingestion_id >=$var_ingestionid 
          ) sl on 
                    pna.ingestionid = sl.lineage_ingestion_id 
            AND     pna.associatedconcept_id = sl.id 
            AND     pna.option_id = sl.sub_id
WHERE 
        fq_form_id is null 
AND     TRIM(answer) <> TRIM(sl.string)

;


SET var_current_timestamp = current_timestamp();

--Update the Identifier table parentsourceid and parenttype for identifier type 'GROUP_ID','GROUP_CK','RESTRICTION_GROUP_ID','EMPLOYER'
UPDATE  
    LANDING.ASSESSMENTSTUB astub
SET 
  astub.answer = expected_answer,
  astub.LDG_REC_UPDT_DT = $var_current_timestamp
FROM 
    (       SELECT      pna.correlationid,
                        pna.source,
                        pna.sourceid,
                        pna.ingestionid,
                        fq_form_id,
                        (
                            CASE 
                                WHEN (fq_form_id IS NOT NULL) THEN answer
                                WHEN (sl.id IS NOT NULL) THEN sl.string 
                                ELSE answer
                            END
                         ) AS  
                        expected_answer
     
            FROM (
            SELECT      pna.*,question_options_1.form_id as fq_form_id 
            FROM
                (
                          SELECT 
                              source,
                              correlationid,
                              sourceid,
                              split_part(sourceid,'-',3) as form_id,
                              split_part(sourceid,'-',4) as question_id,
                              split_part(sourceid,'-',5) as associatedconcept_id ,
                              split_part(sourceid,'-',6) as option_id,
                              answer ,
                              ingestionid 
                        FROM  LANDING.ASSESSMENTSTUB 
                        WHERE 
                              ingestionid >=$var_ingestionid 
                        AND   type =  'Progress_Note_Commitment_Report'
                ) pna  
            LEFT JOIN (
                        SELECT  *
                        FROM    SOURCE_DATA.CCA_CASE_FORM_QUESTION_UNNEST_ROOT
                        WHERE 
                                COALESCE(UPPER(TRIM(options)),'<OPTIONS/>') = '<OPTIONS/>' 
                        AND     lineage_ingestion_id >=$var_ingestionid    
                      ) question_options ON 
                                pna.form_id = question_options.form_id 
                        AND     pna.question_id = question_options.question_id 
                        AND     pna.ingestionid = question_options.lineage_ingestion_id 
            LEFT JOIN (
                        SELECT  *
                        FROM    SOURCE_DATA.CCA_CASE_FORM_QUESTION_UNNEST_ROOT
                        WHERE 
                                COALESCE(UPPER(TRIM(options)),'<OPTIONS/>') <> '<OPTIONS/>'   
                        AND     lineage_ingestion_id >=$var_ingestionid
                      ) question_options_1 ON 
                                pna.form_id = question_options_1.form_id 
                        AND     pna.question_id = question_options_1.question_id 
                        AND     COALESCE(TRIM(CAST(pna.associatedconcept_id AS VARCHAR)) ,'NA') = COALESCE(TRIM(CAST(question_options_1.concept_id AS VARCHAR)) ,'NA')  
                            AND     pna.ingestionid = question_options_1.lineage_ingestion_id
            WHERE 
                    question_options.form_id is null
            ) pna
            LEFT JOIN ( 
                        SELECT  *
                        FROM    SOURCE_DATA.CCA_CASE_STRING_LOCALE_UNNEST_ROOT 
                        WHERE   lineage_ingestion_id >=$var_ingestionid 
                      ) sl on 
                                pna.ingestionid = sl.lineage_ingestion_id 
                        AND     pna.associatedconcept_id = sl.id 
                        AND     pna.option_id = sl.sub_id
           WHERE 
                    fq_form_id is null 
            AND     TRIM(answer) <> TRIM(sl.string)
    
    ) main
WHERE 
        main.source = astub.source
AND     main.correlationid = astub.correlationid
AND     main.sourceid = astub.sourceid
AND     main.ingestionid = astub.ingestionid 


;

-- Records after the update - should be 0

SELECT 'Total Records with incorrect answers in AssessmentStub after the update' as Message,  COUNT(1) AS cnt
FROM (
        SELECT pna.*,question_options_1.form_id as fq_form_id 
        FROM
            (
                      SELECT 
                          split_part(sourceid,'-',3) as form_id,
                          split_part(sourceid,'-',4) as question_id,
                          split_part(sourceid,'-',5) as associatedconcept_id ,
                          split_part(sourceid,'-',6) as option_id,
                          answer ,
                          ingestionid 
                    FROM  LANDING.ASSESSMENTSTUB 
                    WHERE 
                          ingestionid >=$var_ingestionid 
                    AND   type =  'Progress_Note_Commitment_Report'
            ) pna  
        LEFT JOIN (
                    SELECT  *
                    FROM    SOURCE_DATA.CCA_CASE_FORM_QUESTION_UNNEST_ROOT
                    WHERE 
                            COALESCE(UPPER(TRIM(options)),'<OPTIONS/>') = '<OPTIONS/>' 
                    AND     lineage_ingestion_id >=$var_ingestionid         
                  ) question_options ON 
                            pna.form_id = question_options.form_id 
                    AND     pna.question_id = question_options.question_id 
                    AND     pna.ingestionid = question_options.lineage_ingestion_id 
        LEFT JOIN (
                    SELECT  *
                    FROM    SOURCE_DATA.CCA_CASE_FORM_QUESTION_UNNEST_ROOT
                    WHERE 
                            COALESCE(UPPER(TRIM(options)),'<OPTIONS/>') <> '<OPTIONS/>'   
                    AND     lineage_ingestion_id >=$var_ingestionid
                  ) question_options_1 ON 
                            pna.form_id = question_options_1.form_id 
                    AND     pna.question_id = question_options_1.question_id 
                    AND     COALESCE(TRIM(CAST(pna.associatedconcept_id AS VARCHAR)) ,'NA') = COALESCE(TRIM(CAST(question_options_1.concept_id AS VARCHAR)) ,'NA')  
                        AND     pna.ingestionid = question_options_1.lineage_ingestion_id
        WHERE 
                question_options.form_id is null
        ) pna
LEFT JOIN ( 
            SELECT  *
            FROM    SOURCE_DATA.CCA_CASE_STRING_LOCALE_UNNEST_ROOT 
            WHERE   lineage_ingestion_id >=$var_ingestionid 
          ) sl on 
                    pna.ingestionid = sl.lineage_ingestion_id 
            AND     pna.associatedconcept_id = sl.id 
            AND     pna.option_id = sl.sub_id
WHERE 
        fq_form_id is null 
AND     TRIM(answer) <> TRIM(sl.string)

;

-- Records updated in the landing tables
SELECT 
    'Total Affected Records in AssessmentStub table after the update' as Message,  
    COUNT(1) AS cnt
FROM    LANDING.ASSESSMENTSTUB
WHERE
        type =  'Progress_Note_Commitment_Report'
AND     LDG_REC_UPDT_DT >= $var_current_timestamp
