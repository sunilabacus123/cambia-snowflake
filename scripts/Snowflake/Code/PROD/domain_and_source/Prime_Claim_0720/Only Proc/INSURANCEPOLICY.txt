// Create Stage Table


CREATE TABLE ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_STAGE(
CorrelationID varchar(50),
Source varchar(50),
SourceID varchar(50),
ParentSourceID varchar(50),
RelationshipName varchar(50),
SchemaVersion varchar(50),
SourceTimestamp datetime,
ConsumeTimestamp datetime,
HashID varchar(50),
Sequence integer,
PayerID varchar(50),
PolicyID varchar(50),
PolicyName varchar(50),
PolicyHolderID varchar(50),
SubscriberName varchar(50),
SubscriberNumber varchar(50),
GroupID varchar(50),
GroupName varchar(50),
SubGroupID varchar(50),
SubGroupName varchar(50),
SubscriberID varchar(50),
AlternativeIdentifier varchar(50),
AlternativeIdentifierType varchar(50),
PlanID varchar(50),
PlanName varchar(50),
SubPlanID varchar(50),
SubPlanName varchar(50),
PharmacyRxGroup varchar(50),
PharmacyRxBin varchar(50),
PharmacyRxPCN varchar(50),
BeneficiaryRelationshipType varchar(50),
BeneficiaryName varchar(50),
BeneficiaryID varchar(50),
CoordinateWithPolicy varchar(50),
CoverageMonths number,
MemberType varchar(50),
RelationshipType varchar(50),
DependentMemberSequence varchar(50),
Type varchar(50),
ContractType varchar(50),
ContractTypeSourceCode varchar(50),
CarrierCode varchar(50),
Status varchar(50),
COBInd boolean,
COBType boolean,
COBCode boolean,
COBCodeDesc boolean,
EffDate date,
ExpDate date,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP,
CONSTRAINT INSURANCEPOLICY_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);


// Create Final Table

 
CREATE TABLE ABACUS_NONPRD_FND_DB.FOUNDATION.INSURANCEPOLICY(
CorrelationID varchar(50),
Source varchar(50),
SourceID varchar(50),
ParentSourceID varchar(50),
RelationshipName varchar(50),
SchemaVersion varchar(50),
SourceTimestamp datetime,
ConsumeTimestamp datetime,
HashID varchar(50),
Sequence integer,
PayerID varchar(50),
PolicyID varchar(50),
PolicyName varchar(50),
PolicyHolderID varchar(50),
SubscriberName varchar(50),
SubscriberNumber varchar(50),
GroupID varchar(50),
GroupName varchar(50),
SubGroupID varchar(50),
SubGroupName varchar(50),
SubscriberID varchar(50),
AlternativeIdentifier varchar(50),
AlternativeIdentifierType varchar(50),
PlanID varchar(50),
PlanName varchar(50),
SubPlanID varchar(50),
SubPlanName varchar(50),
PharmacyRxGroup varchar(50),
PharmacyRxBin varchar(50),
PharmacyRxPCN varchar(50),
BeneficiaryRelationshipType varchar(50),
BeneficiaryName varchar(50),
BeneficiaryID varchar(50),
CoordinateWithPolicy varchar(50),
CoverageMonths number,
MemberType varchar(50),
RelationshipType varchar(50),
DependentMemberSequence varchar(50),
Type varchar(50),
ContractType varchar(50),
ContractTypeSourceCode varchar(50),
CarrierCode varchar(50),
Status varchar(50),
COBInd boolean,
COBType boolean,
COBCode boolean,
COBCodeDesc boolean,
EffDate date,
ExpDate date,
REC_CREAT_DT TIMESTAMP,
REC_UPDT_DT TIMESTAMP,
FDN_REC_CREAT_DT TIMESTAMP,
FDN_REC_UPDT_DT TIMESTAMP,
CONSTRAINT INSURANCEPOLICY_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);


// Create Stream


CREATE STREAM ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_STREAM ON TABLE ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_STAGE;


// Create PIPE


CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_PIPE auto_ingest=true AS
 COPY INTO ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_STAGE
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:sequence,t.$1:payerid,t.$1:policyid,t.$1:policyname,t.$1:policyholderid,t.$1:subscribername,t.$1:subscribernumber,t.$1:groupid,t.$1:groupname,t.$1:subgroupid,t.$1:subgroupname,t.$1:subscriberid,t.$1:alternativeidentifier,t.$1:alternativeidentifiertype,t.$1:planid,t.$1:planname,t.$1:subplanid,t.$1:subplanname,t.$1:pharmacyrxgroup,t.$1:pharmacyrxbin,t.$1:pharmacyrxpcn,t.$1:beneficiaryrelationshiptype,t.$1:beneficiaryname,t.$1:beneficiaryid,t.$1:coordinatewithpolicy,t.$1:coveragemonths,t.$1:membertype,t.$1:relationshiptype,t.$1:dependentmembersequence,t.$1:type,t.$1:contracttype,t.$1:contracttypesourcecode,t.$1:carriercode,t.$1:status,t.$1:cobind,t.$1:cobtype,t.$1:cobcode,t.$1:cobcodedesc,t.$1:effdate,t.$1:expdate,current_timestamp(),current_timestamp() from @ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/Insurancepolicy/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');
 
 
 
// Create PROC
 

CREATE or replace PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 
  var rs = snowflake.execute( { sqlText:
      `DROP TABLE IF EXISTS ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_TEMP;`
       } );
 
  var rs = snowflake.execute( { sqlText:
      `DROP TABLE IF EXISTS ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_CORRELATION_DELETED;`
       } );
 
  var rs = snowflake.execute( { sqlText:
      `DROP TABLE IF EXISTS ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_CORRELATION_CREATEDT_UPDATE;`
       } );
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_TEMP
         AS
         SELECT S.CORRELATIONID,S.SOURCE,S.SOURCEID,S.PARENTSOURCEID,S.RELATIONSHIPNAME,S.SCHEMAVERSION,S.SOURCETIMESTAMP,S.CONSUMETIMESTAMP,S.HASHID,S.SEQUENCE,S.PAYERID,S.POLICYID,S.POLICYNAME,S.POLICYHOLDERID,S.SUBSCRIBERNAME,S.SUBSCRIBERNUMBER,S.GROUPID,S.GROUPNAME,S.SUBGROUPID,S.SUBGROUPNAME,S.SUBSCRIBERID,S.ALTERNATIVEIDENTIFIER,S.ALTERNATIVEIDENTIFIERTYPE,S.PLANID,S.PLANNAME,S.SUBPLANID,S.SUBPLANNAME,S.PHARMACYRXGROUP,S.PHARMACYRXBIN,S.PHARMACYRXPCN,S.BENEFICIARYRELATIONSHIPTYPE,S.BENEFICIARYNAME,S.BENEFICIARYID,S.COORDINATEWITHPOLICY,S.COVERAGEMONTHS,S.MEMBERTYPE,S.RELATIONSHIPTYPE,S.DEPENDENTMEMBERSEQUENCE,S.TYPE,S.CONTRACTTYPE,S.CONTRACTTYPESOURCECODE,S.CARRIERCODE,S.STATUS,S.COBIND,S.COBTYPE,S.COBCODE,S.COBCODEDESC,S.EFFDATE,S.EXPDATE,S.LDG_REC_CREAT_DT,S.LDG_REC_UPDT_DT
         FROM
         ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_STREAM S JOIN
         ABACUS_NONPRD_FND_DB.LANDING.SUBDOMAIN_RECORDS_PROCESSING B
         ON
         S.CORRELATIONID = B.CORRELATIONID
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_CORRELATION_DELETED
         AS SELECT DISTINCT SOURCE, SOURCEID, CORRELATIONID, PARENTSOURCEID, FDN_REC_CREAT_DT
         FROM  ABACUS_NONPRD_FND_DB.FOUNDATION.INSURANCEPOLICY
         WHERE CORRELATIONID
         IN
         (SELECT PREVIOUSCORRELATIONID FROM ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES)
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_CORRELATION_CREATEDT_UPDATE
         AS SELECT  A.LATESTCORRELATIONID , B.CORRELATIONID,  B.SOURCE, B.SOURCEID , B.PARENTSOURCEID, B.FDN_REC_CREAT_DT FROM
         ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES A JOIN
         ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_CORRELATION_DELETED B
         WHERE A.PREVIOUSCORRELATIONID = B.CORRELATIONID
         GROUP BY A.LATESTCORRELATIONID , B.CORRELATIONID,  B.SOURCE, B.SOURCEID , B.PARENTSOURCEID, B.FDN_REC_CREAT_DT
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText: 
      `DELETE FROM  ABACUS_NONPRD_FND_DB.FOUNDATION.INSURANCEPOLICY
         WHERE CORRELATIONID
         IN
         (SELECT PREVIOUSCORRELATIONID FROM ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES)
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
      `INSERT INTO  ABACUS_NONPRD_FND_DB.FOUNDATION.INSURANCEPOLICY
         SELECT S.CORRELATIONID,S.SOURCE,S.SOURCEID,S.PARENTSOURCEID,S.RELATIONSHIPNAME,S.SCHEMAVERSION,S.SOURCETIMESTAMP,S.CONSUMETIMESTAMP,S.HASHID,S.SEQUENCE,S.PAYERID,S.POLICYID,S.POLICYNAME,S.POLICYHOLDERID,S.SUBSCRIBERNAME,S.SUBSCRIBERNUMBER,S.GROUPID,S.GROUPNAME,S.SUBGROUPID,S.SUBGROUPNAME,S.SUBSCRIBERID,S.ALTERNATIVEIDENTIFIER,S.ALTERNATIVEIDENTIFIERTYPE,S.PLANID,S.PLANNAME,S.SUBPLANID,S.SUBPLANNAME,S.PHARMACYRXGROUP,S.PHARMACYRXBIN,S.PHARMACYRXPCN,S.BENEFICIARYRELATIONSHIPTYPE,S.BENEFICIARYNAME,S.BENEFICIARYID,S.COORDINATEWITHPOLICY,S.COVERAGEMONTHS,S.MEMBERTYPE,S.RELATIONSHIPTYPE,S.DEPENDENTMEMBERSEQUENCE,S.TYPE,S.CONTRACTTYPE,S.CONTRACTTYPESOURCECODE,S.CARRIERCODE,S.STATUS,S.COBIND,S.COBTYPE,S.COBCODE,S.COBCODEDESC,S.EFFDATE,S.EXPDATE,S.LDG_REC_CREAT_DT,S.LDG_REC_UPDT_DT,B.FDN_REC_CREAT_DT,S.LDG_REC_UPDT_DT
         FROM ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_TEMP S JOIN
         ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES B
         ON
         S.CORRELATIONID = B.LATESTCORRELATIONID
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
      `INSERT INTO  ABACUS_NONPRD_FND_DB.FOUNDATION.INSURANCEPOLICY
         SELECT *, CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP()
         FROM ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_TEMP
         WHERE CORRELATIONID NOT IN
         (SELECT LATESTCORRELATIONID FROM ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES)
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
      `UPDATE  ABACUS_NONPRD_FND_DB.FOUNDATION.INSURANCEPOLICY T1
         SET FDN_REC_CREAT_DT  = S.FDN_REC_CREAT_DT
         FROM ABACUS_NONPRD_FND_DB.TRANSIENT.INSURANCEPOLICY_CORRELATION_CREATEDT_UPDATE S
         WHERE
         T1.SOURCE = S.SOURCE
         AND T1.SOURCEID = S.SOURCEID
         AND T1.PARENTSOURCEID = S.PARENTSOURCEID
         AND T1.CORRELATIONID = S.LATESTCORRELATIONID
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );
    return 'Done.';
    $$;
 
 
 
// Create TASK and RESUME TASK
 

CREATE TASK ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_TASK
  warehouse = NONPRD_ABACUS_ELT_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.CLAIM_TASK
WHEN
  SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_STREAM')
as
  call ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_PROC();
 
 
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.INSURANCEPOLICY_TASK RESUME;
 
 
