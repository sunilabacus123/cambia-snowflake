// Create Stage Table


CREATE TABLE ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_STAGE(
CorrelationID varchar(50),
Source varchar(50),
SourceID varchar(50),
ParentSourceID varchar(50),
RelationshipName varchar(50),
SchemaVersion varchar(50),
SourceTimestamp datetime,
ConsumeTimestamp datetime,
HashID varchar(50),
ParentType varchar(50),
Condition varchar(50),
ConditionCause varchar(50),
DiagnosisGroup varchar(50),
DiagnosisCategory varchar(50),
DiagnosisType varchar(50),
DiagnosisClass varchar(50),
Codeset varchar(50),
CodesetType varchar(50),
CodeQualifier varchar(50),
CodeQualifierType varchar(50),
PrimaryInd boolean,
Amount number,
AmountType varchar(50),
DiagnosisGroupAllowedAmount number,
Role varchar(50),
Sequence integer,
SourceDescription varchar(50),
SourceAlternateDescription varchar(50),
EffDate date,
PresentOnAdmissionCode varchar(50),
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP,
CONSTRAINT DIAGNOSIS_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);


// Create Final Table

 
CREATE TABLE ABACUS_NONPRD_FND_DB.FOUNDATION.DIAGNOSIS(
CorrelationID varchar(50),
Source varchar(50),
SourceID varchar(50),
ParentSourceID varchar(50),
RelationshipName varchar(50),
SchemaVersion varchar(50),
SourceTimestamp datetime,
ConsumeTimestamp datetime,
HashID varchar(50),
ParentType varchar(50),
Condition varchar(50),
ConditionCause varchar(50),
DiagnosisGroup varchar(50),
DiagnosisCategory varchar(50),
DiagnosisType varchar(50),
DiagnosisClass varchar(50),
Codeset varchar(50),
CodesetType varchar(50),
CodeQualifier varchar(50),
CodeQualifierType varchar(50),
PrimaryInd boolean,
Amount number,
AmountType varchar(50),
DiagnosisGroupAllowedAmount number,
Role varchar(50),
Sequence integer,
SourceDescription varchar(50),
SourceAlternateDescription varchar(50),
EffDate date,
PresentOnAdmissionCode varchar(50),
REC_CREAT_DT TIMESTAMP,
REC_UPDT_DT TIMESTAMP,
FDN_REC_CREAT_DT TIMESTAMP,
FDN_REC_UPDT_DT TIMESTAMP,
CONSTRAINT DIAGNOSIS_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);


// Create Stream


CREATE STREAM ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_STREAM ON TABLE ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_STAGE;


// Create PIPE


CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_PIPE auto_ingest=true AS
 COPY INTO ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_STAGE
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:condition,t.$1:conditioncause,t.$1:diagnosisgroup,t.$1:diagnosiscategory,t.$1:diagnosistype,t.$1:diagnosisclass,t.$1:codeset,t.$1:codesettype,t.$1:codequalifier,t.$1:codequalifiertype,t.$1:primaryind,t.$1:amount,t.$1:amounttype,t.$1:diagnosisgroupallowedamount,t.$1:role,t.$1:sequence,t.$1:sourcedescription,t.$1:sourcealternatedescription,t.$1:effdate,t.$1:presentonadmissioncode,current_timestamp(),current_timestamp() from @ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/Diagnosis/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');
 
 
 
// Create PROC
 

CREATE or replace PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 
  var rs = snowflake.execute( { sqlText:
      `DROP TABLE IF EXISTS ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_TEMP;`
       } );
 
  var rs = snowflake.execute( { sqlText:
      `DROP TABLE IF EXISTS ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_CORRELATION_DELETED;`
       } );
 
  var rs = snowflake.execute( { sqlText:
      `DROP TABLE IF EXISTS ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_CORRELATION_CREATEDT_UPDATE;`
       } );
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_TEMP
         AS
         SELECT S.CORRELATIONID,S.SOURCE,S.SOURCEID,S.PARENTSOURCEID,S.RELATIONSHIPNAME,S.SCHEMAVERSION,S.SOURCETIMESTAMP,S.CONSUMETIMESTAMP,S.HASHID,S.PARENTTYPE,S.CONDITION,S.CONDITIONCAUSE,S.DIAGNOSISGROUP,S.DIAGNOSISCATEGORY,S.DIAGNOSISTYPE,S.DIAGNOSISCLASS,S.CODESET,S.CODESETTYPE,S.CODEQUALIFIER,S.CODEQUALIFIERTYPE,S.PRIMARYIND,S.AMOUNT,S.AMOUNTTYPE,S.DIAGNOSISGROUPALLOWEDAMOUNT,S.ROLE,S.SEQUENCE,S.SOURCEDESCRIPTION,S.SOURCEALTERNATEDESCRIPTION,S.EFFDATE,S.PRESENTONADMISSIONCODE,S.LDG_REC_CREAT_DT,S.LDG_REC_UPDT_DT
         FROM
         ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_STREAM S JOIN
         ABACUS_NONPRD_FND_DB.LANDING.SUBDOMAIN_RECORDS_PROCESSING B
         ON
         S.CORRELATIONID = B.CORRELATIONID
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_CORRELATION_DELETED
         AS SELECT DISTINCT SOURCE, SOURCEID, CORRELATIONID, PARENTSOURCEID, FDN_REC_CREAT_DT
         FROM  ABACUS_NONPRD_FND_DB.FOUNDATION.DIAGNOSIS
         WHERE CORRELATIONID
         IN
         (SELECT PREVIOUSCORRELATIONID FROM ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES)
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_CORRELATION_CREATEDT_UPDATE
         AS SELECT  A.LATESTCORRELATIONID , B.CORRELATIONID,  B.SOURCE, B.SOURCEID , B.PARENTSOURCEID, B.FDN_REC_CREAT_DT FROM
         ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES A JOIN
         ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_CORRELATION_DELETED B
         WHERE A.PREVIOUSCORRELATIONID = B.CORRELATIONID
         GROUP BY A.LATESTCORRELATIONID , B.CORRELATIONID,  B.SOURCE, B.SOURCEID , B.PARENTSOURCEID, B.FDN_REC_CREAT_DT
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText: 
      `DELETE FROM  ABACUS_NONPRD_FND_DB.FOUNDATION.DIAGNOSIS
         WHERE CORRELATIONID
         IN
         (SELECT PREVIOUSCORRELATIONID FROM ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES)
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
      `INSERT INTO  ABACUS_NONPRD_FND_DB.FOUNDATION.DIAGNOSIS
         SELECT S.CORRELATIONID,S.SOURCE,S.SOURCEID,S.PARENTSOURCEID,S.RELATIONSHIPNAME,S.SCHEMAVERSION,S.SOURCETIMESTAMP,S.CONSUMETIMESTAMP,S.HASHID,S.PARENTTYPE,S.CONDITION,S.CONDITIONCAUSE,S.DIAGNOSISGROUP,S.DIAGNOSISCATEGORY,S.DIAGNOSISTYPE,S.DIAGNOSISCLASS,S.CODESET,S.CODESETTYPE,S.CODEQUALIFIER,S.CODEQUALIFIERTYPE,S.PRIMARYIND,S.AMOUNT,S.AMOUNTTYPE,S.DIAGNOSISGROUPALLOWEDAMOUNT,S.ROLE,S.SEQUENCE,S.SOURCEDESCRIPTION,S.SOURCEALTERNATEDESCRIPTION,S.EFFDATE,S.PRESENTONADMISSIONCODE,S.LDG_REC_CREAT_DT,S.LDG_REC_UPDT_DT,B.FDN_REC_CREAT_DT,S.LDG_REC_UPDT_DT
         FROM ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_TEMP S JOIN
         ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES B
         ON
         S.CORRELATIONID = B.LATESTCORRELATIONID
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
      `INSERT INTO  ABACUS_NONPRD_FND_DB.FOUNDATION.DIAGNOSIS
         SELECT *, CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP()
         FROM ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_TEMP
         WHERE CORRELATIONID NOT IN
         (SELECT LATESTCORRELATIONID FROM ABACUS_NONPRD_FND_DB.LANDING.CLAIM_CORRELATION_UPDATES)
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
      `UPDATE  ABACUS_NONPRD_FND_DB.FOUNDATION.DIAGNOSIS T1
         SET FDN_REC_CREAT_DT  = S.FDN_REC_CREAT_DT
         FROM ABACUS_NONPRD_FND_DB.TRANSIENT.DIAGNOSIS_CORRELATION_CREATEDT_UPDATE S
         WHERE
         T1.SOURCE = S.SOURCE
         AND T1.SOURCEID = S.SOURCEID
         AND T1.PARENTSOURCEID = S.PARENTSOURCEID
         AND T1.CORRELATIONID = S.LATESTCORRELATIONID
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );
    return 'Done.';
    $$;
 
 
 
// Create TASK and RESUME TASK
 

CREATE TASK ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_TASK
  warehouse = NONPRD_ABACUS_ELT_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.CLAIM_TASK
WHEN
  SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_STREAM')
as
  call ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_PROC();
 
 
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.DIAGNOSIS_TASK RESUME;
 
 
