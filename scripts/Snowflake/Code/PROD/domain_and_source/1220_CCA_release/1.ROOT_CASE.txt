// DDL FOR TABLE CASE

// Create Stage Table


CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.CASE(
EntityType varchar,
IngestionID varchar,
BatchID varchar,
CorrelationID varchar,
Source varchar,
SourceID varchar,
ID varchar,
IsAuthoritative boolean,
SchemaVersion varchar,
SourceTimestamp datetime,
ConsumeTimestamp datetime,
PartialUpdate boolean,
LogicalDeleted boolean,
HardDeleted boolean,
Tenant varchar,
HashID varchar,
Username varchar,
Name varchar,
Type varchar,
Status varchar,
OpenReason varchar,
CloseReason varchar,
Level varchar,
OpenedOn date,
ClosedOn date,
SourceType varchar,
Acuity varchar,
Category varchar,
Phase varchar,
InEpisodeInd boolean,
PresentOnAdmissionInd varchar,
HasNotesInd boolean,
CoverageStatusInd boolean,
OnsetDate date,
AssignedDateTime datetime,
Eligibility varchar,
SubscriberMemberSourceID varchar,
PatientMemberSourceID varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP,
CONSTRAINT CASE_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);


// Create FOUNDATION Table

 
CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.FOUNDATION.CASE(
EntityType varchar,
IngestionID varchar,
BatchID varchar,
CorrelationID varchar,
Source varchar,
SourceID varchar,
ID varchar,
IsAuthoritative boolean,
SchemaVersion varchar,
SourceTimestamp datetime,
ConsumeTimestamp datetime,
PartialUpdate boolean,
LogicalDeleted boolean,
HardDeleted boolean,
Tenant varchar,
HashID varchar,
Username varchar,
Name varchar,
Type varchar,
Status varchar,
OpenReason varchar,
CloseReason varchar,
Level varchar,
OpenedOn date,
ClosedOn date,
SourceType varchar,
Acuity varchar,
Category varchar,
Phase varchar,
InEpisodeInd boolean,
PresentOnAdmissionInd varchar,
HasNotesInd boolean,
CoverageStatusInd boolean,
OnsetDate date,
AssignedDateTime datetime,
Eligibility varchar,
SubscriberMemberSourceID varchar,
PatientMemberSourceID varchar,
SRC_LDG_REC_CREAT_DT TIMESTAMP,
SRC_LDG_REC_UPDT_DT TIMESTAMP,
FDN_REC_CREAT_DT TIMESTAMP,
FDN_REC_UPDT_DT TIMESTAMP,
CONSTRAINT CASE_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);


// CREATE OR REPLACE STREAM


CREATE OR REPLACE STREAM ABACUS_PRD_FND_DB.LANDING.CASE_STREAM ON TABLE ABACUS_PRD_FND_DB.LANDING.CASE;


// Create PIPE


CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.LANDING.CASE_PIPE auto_ingest=true AS
 COPY INTO ABACUS_PRD_FND_DB.LANDING.CASE
     FROM
         (SELECT t.$1:entitytype,t.$1:ingestionid,t.$1:batchid,t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:id,t.$1:isauthoritative,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:partialupdate,t.$1:logicaldeleted,t.$1:harddeleted,t.$1:tenant,t.$1:hashid,t.$1:username,t.$1:name,t.$1:type,t.$1:status,t.$1:openreason,t.$1:closereason,t.$1:level,t.$1:openedon,t.$1:closedon,t.$1:sourcetype,t.$1:acuity,t.$1:category,t.$1:phase,t.$1:inepisodeind,t.$1:presentonadmissionind,t.$1:hasnotesind,t.$1:coveragestatusind,t.$1:onsetdate,t.$1:assigneddatetime,t.$1:eligibility,t.$1:subscribermembersourceid,t.$1:patientmembersourceid,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Case/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');
 
 
 
CREATE OR REPLACE PROCEDURE ABACUS_PRD_FND_DB.LANDING.CASE_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 
  var rs = snowflake.execute( { sqlText:
      `DROP TABLE IF EXISTS ABACUS_PRD_FND_DB.LANDING.CASE_STREAM_TEMP;`
       } );
 
 
 
  var rs = snowflake.execute( { sqlText:
      `CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.CONTROL.CASE_TG_VW AS SELECT BATCH_ID FROM ABACUS_PRD_FND_DB.CONTROL.CASE_TRIGGER_STREAM;`
       } );
 

  var stmt = snowflake.createStatement(
      {sqlText: "set VALIDATION_ID = (SELECT BATCH_ID FROM ABACUS_PRD_FND_DB.CONTROL.CASE_TG_VW)"}
      );
  var rs = stmt.execute(); 
 
 
 
  var rs = snowflake.execute( { sqlText:
      `DROP TABLE IF EXISTS ABACUS_PRD_FND_DB.CONTROL.VALIDATION_CASE_INGESTIONID;`
       } );
 
 
 
  var rs = snowflake.execute( { sqlText:
      `CREATE TABLE ABACUS_PRD_FND_DB.LANDING.CASE_STREAM_TEMP AS SELECT * FROM ABACUS_PRD_FND_DB.LANDING.CASE WHERE INGESTIONID = $VALIDATION_ID;`
       } );
 
 
 
  var rs = snowflake.execute( { sqlText:
      `DELETE FROM ABACUS_PRD_FND_DB.LANDING.CASE_SUBDOMAIN_RECORDS_PROCESSING WHERE INGESTIONID = $VALIDATION_ID;`
       } );
 
 
 
  var rs = snowflake.execute( { sqlText:
      `DELETE FROM ABACUS_PRD_FND_DB.LANDING.CASE_CORRELATION_UPDATES WHERE INGESTIONID = $VALIDATION_ID;`
       } );
 
 
 
  var rs = snowflake.execute( { sqlText:
      `CREATE OR REPLACE TABLE  ABACUS_PRD_FND_DB.CONTROL.VALIDATION_CASE_INGESTIONID AS SELECT DISTINCT INGESTIONID, 'CASE'  AS DOMAIN from ABACUS_PRD_FND_DB.LANDING.CASE_STREAM_TEMP;`
       } );
 

  var rs = snowflake.execute( { sqlText:
      `CREATE OR REPLACE TABLE  ABACUS_PRD_FND_DB.CONTROL.VALIDATION_CASE_INGESTIONID AS SELECT DISTINCT BATCH_ID AS INGESTIONID, 'CASE' AS DOMAIN FROM ABACUS_PRD_FND_DB.CONTROL.CASE_TG_VW;`
       } );
 
 
  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_PRD_FND_DB.LANDING.CASE_SUBDOMAIN_RECORDS_PROCESSING
      select SOURCE, SOURCEID, CONSUMETIMESTAMP, SOURCETIMESTAMP, CORRELATIONID, $VALIDATION_ID
      from (
      select
      *,
      row_number() over(partition by SOURCE, SOURCEID order by SOURCETIMESTAMP DESC, CONSUMETIMESTAMP DESC) as rn
      from
      ABACUS_PRD_FND_DB.LANDING.CASE_STREAM_TEMP
      ) t
      where t.rn = 1
      ;`
       } );
 
 
 
  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_PRD_FND_DB.LANDING.CASE_CORRELATION_UPDATES SELECT A.CorrelationID AS LATESTCORRELATIONID, B.CorrelationID AS
      PREVIOUSCORRELATIONID, A.SOURCE, A.SOURCEID, A.CONSUMETIMESTAMP, A.SOURCETIMESTAMP, B.FDN_REC_CREAT_DT, B.FDN_REC_UPDT_DT, $VALIDATION_ID
      FROM
      (SELECT * FROM ABACUS_PRD_FND_DB.LANDING.CASE_SUBDOMAIN_RECORDS_PROCESSING WHERE INGESTIONID = $VALIDATION_ID ) A JOIN
      ABACUS_PRD_FND_DB.FOUNDATION.CASE B
      ON
      A.SOURCE = B.SOURCE AND
      A.SOURCEID = B.SOURCEID
      ;`
       } );
 
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_PRD_FND_DB.FOUNDATION.CASE AS T
         using (
         SELECT *
               from (
                  select 
                   *,
                  row_number() over(partition by SOURCE, SOURCEID order by SOURCETIMESTAMP DESC, CONSUMETIMESTAMP DESC) as rn 
                   from
                   ABACUS_PRD_FND_DB.LANDING.CASE_STREAM_TEMP
                   ) t
                   where t.rn = 1
                   ) AS S 
         ON T.SOURCE = S.SOURCE
         AND T.SOURCEID = S.SOURCEID
         WHEN matched
              THEN
              UPDATE SET
                     T.ENTITYTYPE = S.ENTITYTYPE,
                     T.INGESTIONID = S.INGESTIONID,
                     T.BATCHID = S.BATCHID,
                     T.CORRELATIONID = S.CORRELATIONID,
                     T.SOURCEID = S.SOURCEID,
                     T.ID = S.ID,
                     T.ISAUTHORITATIVE = S.ISAUTHORITATIVE,
                     T.SCHEMAVERSION = S.SCHEMAVERSION,
                     T.SOURCETIMESTAMP = S.SOURCETIMESTAMP,
                     T.CONSUMETIMESTAMP = S.CONSUMETIMESTAMP,
                     T.PARTIALUPDATE = S.PARTIALUPDATE,
                     T.LOGICALDELETED = S.LOGICALDELETED,
                     T.HARDDELETED = S.HARDDELETED,
                     T.TENANT = S.TENANT,
                     T.HASHID = S.HASHID,
                     T.USERNAME = S.USERNAME,
                     T.NAME = S.NAME,
                     T.TYPE = S.TYPE,
                     T.STATUS = S.STATUS,
                     T.OPENREASON = S.OPENREASON,
                     T.CLOSEREASON = S.CLOSEREASON,
                     T.LEVEL = S.LEVEL,
                     T.OPENEDON = S.OPENEDON,
                     T.CLOSEDON = S.CLOSEDON,
                     T.SOURCETYPE = S.SOURCETYPE,
                     T.ACUITY = S.ACUITY,
                     T.CATEGORY = S.CATEGORY,
                     T.PHASE = S.PHASE,
                     T.INEPISODEIND = S.INEPISODEIND,
                     T.PRESENTONADMISSIONIND = S.PRESENTONADMISSIONIND,
                     T.HASNOTESIND = S.HASNOTESIND,
                     T.COVERAGESTATUSIND = S.COVERAGESTATUSIND,
                     T.ONSETDATE = S.ONSETDATE,
                     T.ASSIGNEDDATETIME = S.ASSIGNEDDATETIME,
                     T.ELIGIBILITY = S.ELIGIBILITY,
                     T.SUBSCRIBERMEMBERSOURCEID = S.SUBSCRIBERMEMBERSOURCEID,
                     T.PATIENTMEMBERSOURCEID = S.PATIENTMEMBERSOURCEID,
                     T.SRC_LDG_REC_UPDT_DT = S.LDG_REC_CREAT_DT,
                     T.FDN_REC_UPDT_DT = CURRENT_TIMESTAMP()
         WHEN NOT matched
             THEN
                     INSERT(ENTITYTYPE,INGESTIONID,BATCHID,CORRELATIONID,SOURCE,SOURCEID,ID,ISAUTHORITATIVE,SCHEMAVERSION,SOURCETIMESTAMP,CONSUMETIMESTAMP,PARTIALUPDATE,LOGICALDELETED,HARDDELETED,TENANT,HASHID,USERNAME,NAME,TYPE,STATUS,OPENREASON,CLOSEREASON,LEVEL,OPENEDON,CLOSEDON,SOURCETYPE,ACUITY,CATEGORY,PHASE,INEPISODEIND,PRESENTONADMISSIONIND,HASNOTESIND,COVERAGESTATUSIND,ONSETDATE,ASSIGNEDDATETIME,ELIGIBILITY,SUBSCRIBERMEMBERSOURCEID,PATIENTMEMBERSOURCEID,SRC_LDG_REC_CREAT_DT,SRC_LDG_REC_UPDT_DT,FDN_REC_CREAT_DT,FDN_REC_UPDT_DT)
                     VALUES(S.ENTITYTYPE,S.INGESTIONID,S.BATCHID,S.CORRELATIONID,S.SOURCE,S.SOURCEID,S.ID,S.ISAUTHORITATIVE,S.SCHEMAVERSION,S.SOURCETIMESTAMP,S.CONSUMETIMESTAMP,S.PARTIALUPDATE,S.LOGICALDELETED,S.HARDDELETED,S.TENANT,S.HASHID,S.USERNAME,S.NAME,S.TYPE,S.STATUS,S.OPENREASON,S.CLOSEREASON,S.LEVEL,S.OPENEDON,S.CLOSEDON,S.SOURCETYPE,S.ACUITY,S.CATEGORY,S.PHASE,S.INEPISODEIND,S.PRESENTONADMISSIONIND,S.HASNOTESIND,S.COVERAGESTATUSIND,S.ONSETDATE,S.ASSIGNEDDATETIME,S.ELIGIBILITY,S.SUBSCRIBERMEMBERSOURCEID,S.PATIENTMEMBERSOURCEID,S.LDG_REC_CREAT_DT,S.LDG_REC_UPDT_DT,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );
    return 'Done.';
    $$;
 
 
 
// CREATE OR REPLACE TASK and RESUME TASK
 

CREATE OR REPLACE TASK ABACUS_PRD_FND_DB.LANDING.CASE_TASK
  warehouse = PRD_ABACUS_ELT_WH
  schedule = '1 minute'
WHEN
   SYSTEM$STREAM_HAS_DATA('ABACUS_PRD_FND_DB.CONTROL.CASE_TRIGGER_STREAM')
as
  call ABACUS_PRD_FND_DB.LANDING.CASE_PROC();
 
 
 
CREATE OR REPLACE TASK ABACUS_PRD_FND_DB.LANDING.CASE_VALIDATION_HISTORY
  warehouse = PRD_ABACUS_ELT_WH
  AFTER ABACUS_PRD_FND_DB.LANDING.CASE_TASK
as
  INSERT INTO ABACUS_PRD_FND_DB.CONTROL.CASE_ENTITY_LOAD_LOG_HISTORY
  SELECT A.INGESTIONID, C.TABLE_NAME, B.STAGE_ROW_COUNT, C.FOUNDATION_ROW_COUNT, TO_DATE(CURRENT_TIMESTAMP()) AS RUN_DATE, CURRENT_TIMESTAMP() AS RUN_TIMESTAMP
  FROM ABACUS_PRD_FND_DB.CONTROL.VALIDATION_CASE_INGESTIONID A JOIN
  (
  SELECT 'CASE' AS DOMAIN, TABLE_NAME, ROW_COUNT AS STAGE_ROW_COUNT, TO_DATE(CURRENT_TIMESTAMP()) FROM 
   ABACUS_PRD_FND_DB.INFORMATION_SCHEMA.TABLES
  WHERE TABLE_NAME IN ( 'CASE')
  AND TABLE_SCHEMA = 'LANDING'
  ) B ON A.DOMAIN = B.DOMAIN JOIN (
  SELECT 'CASE' AS DOMAIN, TABLE_NAME, ROW_COUNT AS FOUNDATION_ROW_COUNT, TO_DATE(CURRENT_TIMESTAMP()) FROM 
   ABACUS_PRD_FND_DB.INFORMATION_SCHEMA.TABLES
  WHERE TABLE_NAME IN ( 'CASE')
  AND TABLE_SCHEMA = 'FOUNDATION'
 ) C
ON A.DOMAIN = C.DOMAIN;
 
 
ALTER TASK ABACUS_PRD_FND_DB.LANDING.CASE_VALIDATION_HISTORY RESUME;
 
 
ALTER TASK ABACUS_PRD_FND_DB.LANDING.CASE_TASK RESUME;
 
