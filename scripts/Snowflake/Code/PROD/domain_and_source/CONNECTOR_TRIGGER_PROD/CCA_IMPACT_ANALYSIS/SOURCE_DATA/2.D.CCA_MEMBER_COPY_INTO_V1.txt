COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_p_concept_value_unnest_root_BACKUP0423
FROM (
SELECT
t.$1:CID,
t.$1:concept_id,
t.$1:STR_VALUE,
t.$1:unit_id,
CASE t.$1:update_time
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_time AS DATETIME)
END,
t.$1:obx_id,
t.$1:NUM_VALUE,
CASE t.$1:date_value
 WHEN '' THEN NULL
 ELSE CAST(t.$1:date_value AS DATETIME)
END,
t.$1:concept_type_id,
t.$1:TREND,
CASE t.$1:SYS_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:SYS_DATE AS DATETIME)
END,
t.$1:user_name,
t.$1:source_guid,
t.$1:user_id,
t.$1:source_id,
t.$1:source_type,
t.$1:is_void,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_p_concept_value_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);



COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_u_concept_value_unnest_root_BACKUP0423
FROM (
SELECT
t.$1:CID,
t.$1:concept_id,
t.$1:STR_VALUE,
t.$1:unit_id,
CASE t.$1:update_time
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_time AS DATETIME)
END,
t.$1:obx_id,
t.$1:NUM_VALUE,
CASE t.$1:date_value
 WHEN '' THEN NULL
 ELSE CAST(t.$1:date_value AS DATETIME)
END,
t.$1:concept_type_id,
t.$1:TREND,
CASE t.$1:SYS_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:SYS_DATE AS DATETIME)
END,
t.$1:user_name,
t.$1:source_guid,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_u_concept_value_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_resource_file_unnest_root_BACKUP0423
FROM (
SELECT
t.$1:ID,
t.$1:PATH,
t.$1:CONTENTS,
CASE t.$1:CREATE_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:CREATE_DATE AS DATETIME)
END,
CASE t.$1:UPDATE_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:UPDATE_DATE AS DATETIME)
END,
t.$1:UPDATED_BY,
t.$1:GUID,
t.$1:VARIABLE_FIELD_ID,
t.$1:VARIABLE_FIELD_NAME,
t.$1:VARIABLE_FIELD_TYPE,
t.$1:VARIABLE_FIELD_OPTION_ID,
t.$1:VARIABLE_FIELD_OPTION_VALUE,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_resource_file_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_string_locale_unnest_root_BACKUP0423
FROM (
SELECT
t.$1:ID,
t.$1:TYPE_ID,
t.$1:SUB_ID,
t.$1:LOCALE_ID,
t.$1:MG_ID,
t.$1:STRING,
CASE t.$1:UPDATE_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:UPDATE_DATE AS DATETIME)
END,
t.$1:MASK,
t.$1:guid,
t.$1:protected,
t.$1:version,
t.$1:INACTIVE,
t.$1:db_rowversion,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_string_locale_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);
