--/2020/12/26/1997/
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_concept_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:mg_id,
t.$1:concept_type_id,
t.$1:name,
t.$1:unit_type_id,
t.$1:disable,
t.$1:expire_days,
t.$1:xml_info,
t.$1:folder_id,
t.$1:audit,
t.$1:protected,
t.$1:permission_mask,
t.$1:concept_group_id,
t.$1:list_id,
t.$1:numeric_min,
t.$1:numeric_max,
t.$1:number_of_field,
t.$1:catalog_search,
t.$1:TREND,
t.$1:propagate,
t.$1:options,
t.$1:guid,
t.$1:version,
t.$1:db_rowversion,
t.$1:show_measurement_date::NUMBER,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_concept_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_form_question_unnest_root
FROM (
SELECT
t.$1:form_id,
t.$1:question_id,
t.$1:concept_id,
t.$1:generation,
t.$1:type,
t.$1:default,
t.$1:validation,
t.$1:show_text,
t.$1:"update",
t.$1:populate,
t.$1:"rows",
t.$1:columns,
t.$1:text,
t.$1:description,
t.$1:default_value,
t.$1:calculate,
t.$1:style,
t.$1:options,
t.$1:active,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:create_date AS DATETIME)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_date AS DATETIME)
END,
t.$1:updated_by,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_form_question_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;



COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_form_unnest_root
FROM (
SELECT
t.$1:FORM_ID,
t.$1:FORM_CATEGORY_ID,
t.$1:MEMBER_GROUP_ID,
t.$1:FORM_NAME,
t.$1:FORM_MASK,
t.$1:ACTIVE_IND,
t.$1:SCHEMA_VERSION,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_date AS DATETIME)
END,
t.$1:action_id,
t.$1:USABILITY,
t.$1:USER_NAME,
t.$1:form_xml_deprecated,
t.$1:guid,
t.$1:protected,
t.$1:version,
t.$1:db_rowversion,
t.$1:form_description,
t.$1:input_layout,
t.$1:output_layout,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:create_date AS DATETIME)
END,
t.$1:updated_by,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_form_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;

----------------------------------
-- RUNNING TILL HERE V1
-- TOTAL 3
----------------------------------

COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_medical_code_sets_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:name,
t.$1:description,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_date AS DATETIME)
END,
t.$1:is_loaded,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_medical_code_sets_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_medical_code_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:code,
t.$1:code_set_id,
t.$1:description,
CASE t.$1:effective_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:effective_date AS DATETIME)
END,
CASE t.$1:term_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:term_date AS DATETIME)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_date AS DATETIME)
END,
t.$1:payment_year,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_medical_code_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_coverage_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:cid,
t.$1:subscriber_id,
t.$1:subscriber_suffix,
t.$1:relationship,
t.$1:group_ck,
t.$1:group_id,
t.$1:group_name,
CASE t.$1:orig_effective_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:orig_effective_date AS DATETIME)
END,
CASE t.$1:effective_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:effective_date AS DATETIME)
END,
CASE t.$1:term_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:term_date AS DATETIME)
END,
t.$1:is_primary,
t.$1:is_eligible,
t.$1:source_id,
t.$1:source_detail,
CASE t.$1:source_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:source_date AS DATETIME)
END,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:ssis_recalculate_dates::NUMBER,
t.$1:ssis_insert,
t.$1:ssis_update,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_coverage_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;

-----------------
V2 TILL HERE
----------------

COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_external_data_unnest_root
FROM (
SELECT
t.$1:CID,
t.$1:externalId,
t.$1:externalData,
t.$1:SYSTEM_ID,
t.$1:externalMergedToId,
CASE t.$1:external_merged_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:external_merged_date AS DATETIME)
END,
t.$1:external_merged_to_system_id,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_external_data_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_hra_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:CID,
t.$1:HRA_ID,
t.$1:STATUS,
CASE t.$1:EVENT_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:EVENT_DATE AS DATETIME)
END,
CASE t.$1:SYSTEM_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:SYSTEM_DATE AS DATETIME)
END,
t.$1:VERSION,
t.$1:score,
t.$1:risk_index,
t.$1:user_name,
t.$1:latest,
t.$1:obx_id,
t.$1:is_internal,
t.$1:META_DATA,
t.$1:guid,
t.$1:is_void,
t.$1:void_reason,
t.$1:batch_id,
t.$1:ext_source,
t.$1:file_name,
t.$1:case_id,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:create_date AS DATETIME)
END,
t.$1:user_id,
CASE t.$1:start_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:start_date AS DATETIME)
END,
CASE t.$1:due_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:due_date AS DATETIME)
END,
t.$1:resultcache_id,
t.$1:resultcache_instance_id,
t.$1:resultcache_guid,
t.$1:resultcache_name,
t.$1:resultcache_version,
t.$1:resultcache_registrar,
t.$1:resultcache_risk_index,
t.$1:resultcache_force_start,
t.$1:resultcache_locale_id,
t.$1:resultcache_template_version,
t.$1:resultcache_status,
t.$1:resultcache_score,
t.$1:resultcache_date,
t.$1:resultcache_source_object_type,
t.$1:resultcache_source_object_id,
t.$1:resultcache_calling_hra_id,
t.$1:resultcache_external_page_id,
t.$1:resultcache_in_external_page,
t.$1:resultcache_from_other_hra,
t.$1:resultcache_doctorsession_id,
t.$1:resultcache_obx_id,
t.$1:resultcache_case_id,
t.$1:resultcache_loggedin_userid,
t.$1:resultcache_patient_id,
t.$1:resultcache_patientname,
t.$1:resultcache_allow_void,
t.$1:page_id,
t.$1:page_update_date,
t.$1:page_valid,
t.$1:question_id,
t.$1:question_name,
t.$1:question_score,
t.$1:question_value,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_hra_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_p_concept_value_log_unnest_root
FROM (
SELECT
t.$1:CID,
t.$1:concept_id,
t.$1:str_value,
t.$1:unit_id,
CASE t.$1:update_time
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_time AS DATETIME)
END,
t.$1:obx_id,
t.$1:num_value,
CASE t.$1:date_value
 WHEN '' THEN NULL
 ELSE CAST(t.$1:date_value AS DATETIME)
END,
t.$1:TREND,
CASE t.$1:SYS_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:SYS_DATE AS DATETIME)
END,
t.$1:user_name,
CASE t.$1:system_create_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:system_create_date AS DATETIME)
END,
t.$1:source_guid,
t.$1:ID,
t.$1:user_id,
t.$1:source_id,
t.$1:source_type,
t.$1:is_void,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_p_concept_value_log_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_u_concept_value_log_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:CID,
t.$1:concept_id,
t.$1:str_value,
t.$1:unit_id,
CASE t.$1:update_time
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_time AS DATETIME)
END,
t.$1:obx_id,
t.$1:num_value,
CASE t.$1:date_value
 WHEN '' THEN NULL
 ELSE CAST(t.$1:date_value AS DATETIME)
END,
t.$1:TREND,
CASE t.$1:SYS_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:SYS_DATE AS DATETIME)
END,
t.$1:user_name,
CASE t.$1:system_create_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:system_create_date AS DATETIME)
END,
t.$1:source_guid,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_u_concept_value_log_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_unnest_root
FROM (
SELECT
t.$1:CID,
t.$1:MEMBER_GROUP_ID,
t.$1:ACTIVE_IND,
t.$1:FIRST_NAME,
t.$1:LAST_NAME,
t.$1:GENDER,
CASE t.$1:DATE_OF_BIRTH
 WHEN '' THEN NULL
 ELSE CAST(t.$1:DATE_OF_BIRTH AS DATETIME)
END,
CASE t.$1:OPEN_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:OPEN_DATE AS DATETIME)
END,
CASE t.$1:CAREKEY_EXPIRATION_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:CAREKEY_EXPIRATION_DATE AS DATETIME)
END,
CASE t.$1:LAST_LOGIN_TIME
 WHEN '' THEN NULL
 ELSE CAST(t.$1:LAST_LOGIN_TIME AS DATETIME)
END,
t.$1:WIZARD_STATE,
t.$1:MEMBER_CUSTOMIZATION,
t.$1:XML_FILTER_SORT,
t.$1:PHONE_LOGIN,
t.$1:PHONE_PASSWORD,
t.$1:PHONE_ACTIVE_IND,
t.$1:PRIVATE_MEMBER,
t.$1:LICENSE_ID,
t.$1:DB_VERSION,
t.$1:AUTO_FOCUS_SETTING,
CASE t.$1:PASSWORD_EXPIRE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:PASSWORD_EXPIRE AS DATETIME)
END,
CASE t.$1:last_access_time
 WHEN '' THEN NULL
 ELSE CAST(t.$1:last_access_time AS DATETIME)
END,
t.$1:EMAIL,
t.$1:SMS,
t.$1:ALIAS,
t.$1:allow_qa,
t.$1:failed_time,
t.$1:employer,
t.$1:TITLE,
t.$1:MIDDLE_INITIAL,
CASE t.$1:ALERT_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:ALERT_DATE AS DATETIME)
END,
t.$1:allow_managed,
t.$1:external_unique_id,
CASE t.$1:last_failed_period_start
 WHEN '' THEN NULL
 ELSE CAST(t.$1:last_failed_period_start AS DATETIME)
END,
t.$1:subscriber_id,
t.$1:temp_cid_link,
t.$1:temp_status,
t.$1:merge_user,
CASE t.$1:merge_date
 WHEN '' THEN NULL
 ELSE CAST(t.$1:merge_date AS DATETIME)
END,
CASE t.$1:opt_out_time
 WHEN '' THEN NULL
 ELSE CAST(t.$1:opt_out_time AS DATETIME)
END,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:change_password_required,
t.$1:RESTRICTION_GROUP_ID,
t.$1:SSN,
t.$1:MEDICARE_NO,
t.$1:MEDICAID_NO,
CASE t.$1:CM_IDENTIFIED_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:CM_IDENTIFIED_DATE AS DATETIME)
END,
t.$1:ssis_insert,
t.$1:ssis_update,
t.$1:ssis_external_system_id,
t.$1:ssis_external_id,
t.$1:has_hipaa_privacy,
t.$1:healthcare_id,
t.$1:merge_status,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;



COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_p_concept_value_unnest_root
FROM (
SELECT
t.$1:CID,
t.$1:concept_id,
t.$1:STR_VALUE,
t.$1:unit_id,
CASE t.$1:update_time
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_time AS DATETIME)
END,
t.$1:obx_id,
t.$1:NUM_VALUE,
CASE t.$1:date_value
 WHEN '' THEN NULL
 ELSE CAST(t.$1:date_value AS DATETIME)
END,
t.$1:concept_type_id,
t.$1:TREND,
CASE t.$1:SYS_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:SYS_DATE AS DATETIME)
END,
t.$1:user_name,
t.$1:source_guid,
t.$1:user_id,
t.$1:source_id,
t.$1:source_type,
t.$1:is_void,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_p_concept_value_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;



COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_member_u_concept_value_unnest_root
FROM (
SELECT
t.$1:CID,
t.$1:concept_id,
t.$1:STR_VALUE,
t.$1:unit_id,
CASE t.$1:update_time
 WHEN '' THEN NULL
 ELSE CAST(t.$1:update_time AS DATETIME)
END,
t.$1:obx_id,
t.$1:NUM_VALUE,
CASE t.$1:date_value
 WHEN '' THEN NULL
 ELSE CAST(t.$1:date_value AS DATETIME)
END,
t.$1:concept_type_id,
t.$1:TREND,
CASE t.$1:SYS_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:SYS_DATE AS DATETIME)
END,
t.$1:user_name,
t.$1:source_guid,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_member_u_concept_value_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_resource_file_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:PATH,
t.$1:CONTENTS,
CASE t.$1:CREATE_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:CREATE_DATE AS DATETIME)
END,
CASE t.$1:UPDATE_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:UPDATE_DATE AS DATETIME)
END,
t.$1:UPDATED_BY,
t.$1:GUID,
t.$1:VARIABLE_FIELD_ID,
t.$1:VARIABLE_FIELD_NAME,
t.$1:VARIABLE_FIELD_TYPE,
t.$1:VARIABLE_FIELD_OPTION_ID,
t.$1:VARIABLE_FIELD_OPTION_VALUE,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_resource_file_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;


COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_string_locale_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:TYPE_ID,
t.$1:SUB_ID,
t.$1:LOCALE_ID,
t.$1:MG_ID,
t.$1:STRING,
CASE t.$1:UPDATE_DATE
 WHEN '' THEN NULL
 ELSE CAST(t.$1:UPDATE_DATE AS DATETIME)
END,
t.$1:MASK,
t.$1:guid,
t.$1:protected,
t.$1:version,
t.$1:INACTIVE,
t.$1:db_rowversion,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_string_locale_unnest_root/2020/12/26/1997/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE) PATTERN = '.*1' FORCE = TRUE;
