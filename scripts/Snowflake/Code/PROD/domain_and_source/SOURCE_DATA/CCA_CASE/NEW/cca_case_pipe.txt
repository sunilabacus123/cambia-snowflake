CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_case_member_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_case_member_unnest_root
FROM (
SELECT
t.$1:CID,
t.$1:MEMBER_GROUP_ID,
t.$1:ACTIVE_IND,
t.$1:FIRST_NAME,
t.$1:LAST_NAME,
t.$1:GENDER,
t.$1:DATE_OF_BIRTH,
t.$1:OPEN_DATE,
t.$1:CAREKEY_EXPIRATION_DATE,
t.$1:LAST_LOGIN_TIME,
t.$1:WIZARD_STATE,
t.$1:MEMBER_CUSTOMIZATION,
t.$1:XML_FILTER_SORT,
t.$1:PHONE_LOGIN,
t.$1:PHONE_PASSWORD,
t.$1:PHONE_ACTIVE_IND,
t.$1:PRIVATE_MEMBER,
t.$1:LICENSE_ID,
t.$1:DB_VERSION,
t.$1:AUTO_FOCUS_SETTING,
t.$1:PASSWORD_EXPIRE,
t.$1:last_access_time,
t.$1:EMAIL,
t.$1:SMS,
t.$1:ALIAS,
t.$1:allow_qa,
t.$1:failed_time,
t.$1:employer,
t.$1:TITLE,
t.$1:MIDDLE_INITIAL,
t.$1:ALERT_DATE,
t.$1:allow_managed,
t.$1:external_unique_id,
t.$1:last_failed_period_start,
t.$1:subscriber_id,
t.$1:temp_cid_link,
t.$1:temp_status,
t.$1:merge_user,
CASE t.$1:merge_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:merge_date)
END,
t.$1:opt_out_time,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:change_password_required,
t.$1:db_rowversion,
t.$1:RESTRICTION_GROUP_ID,
t.$1:SSN,
t.$1:MEDICARE_NO,
t.$1:MEDICAID_NO,
t.$1:CM_IDENTIFIED_DATE,
t.$1:ssis_insert,
t.$1:ssis_update,
t.$1:ssis_external_system_id,
t.$1:ssis_external_id,
t.$1:has_hipaa_privacy,
t.$1:healthcare_id,
t.$1:merge_status,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_case_member_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_case_member_hra_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_case_member_hra_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:CID,
t.$1:HRA_ID,
t.$1:STATUS,
t.$1:EVENT_DATE,
t.$1:SYSTEM_DATE,
t.$1:VERSION,
t.$1:score,
t.$1:risk_index,
t.$1:user_name,
t.$1:latest,
t.$1:obx_id,
t.$1:is_internal,
t.$1:META_DATA,
t.$1:guid,
t.$1:is_void,
t.$1:void_reason,
t.$1:batch_id,
t.$1:ext_source,
t.$1:file_name,
t.$1:case_id,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
t.$1:user_id,
CASE t.$1:start_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:start_date)
END,
CASE t.$1:due_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:due_date)
END,
t.$1:resultcache_id,
t.$1:resultcache_instance_id,
t.$1:resultcache_guid,
t.$1:resultcache_name,
t.$1:resultcache_version,
t.$1:resultcache_registrar,
t.$1:resultcache_risk_index,
t.$1:resultcache_force_start,
t.$1:resultcache_locale_id,
t.$1:resultcache_template_version,
t.$1:resultcache_status,
t.$1:resultcache_score,
t.$1:resultcache_date,
t.$1:resultcache_source_object_type,
t.$1:resultcache_source_object_id,
t.$1:resultcache_calling_hra_id,
t.$1:resultcache_external_page_id,
t.$1:resultcache_in_external_page,
t.$1:resultcache_from_other_hra,
t.$1:resultcache_doctorsession_id,
t.$1:resultcache_obx_id,
t.$1:resultcache_case_id,
t.$1:resultcache_loggedin_userid,
t.$1:resultcache_patient_id,
t.$1:resultcache_patientname,
t.$1:resultcache_allow_void,
t.$1:page_id,
t.$1:page_update_date,
t.$1:page_valid,
t.$1:question_id,
t.$1:question_name,
t.$1:question_score,
t.$1:question_value,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_case_member_hra_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_concept_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_concept_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:mg_id,
t.$1:concept_type_id,
t.$1:name,
t.$1:unit_type_id,
t.$1:disable,
t.$1:expire_days,
t.$1:xml_info,
t.$1:folder_id,
t.$1:audit,
t.$1:protected,
t.$1:permission_mask,
t.$1:concept_group_id,
t.$1:list_id,
t.$1:numeric_min,
t.$1:numeric_max,
t.$1:number_of_field,
t.$1:catalog_search,
t.$1:TREND,
t.$1:propagate,
t.$1:options,
t.$1:guid,
t.$1:version,
t.$1:db_rowversion,
CASE t.$1:show_measurement_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:show_measurement_date)
END,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_concept_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_concept_type_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_concept_type_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:type,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_concept_type_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_form_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_form_unnest_root
FROM (
SELECT
t.$1:FORM_ID,
t.$1:FORM_CATEGORY_ID,
t.$1:MEMBER_GROUP_ID,
t.$1:FORM_NAME,
t.$1:FORM_MASK,
t.$1:ACTIVE_IND,
t.$1:SCHEMA_VERSION,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:action_id,
t.$1:USABILITY,
t.$1:USER_NAME,
t.$1:form_xml_deprecated,
t.$1:guid,
t.$1:protected,
t.$1:version,
t.$1:db_rowversion,
t.$1:form_description,
t.$1:input_layout,
t.$1:output_layout,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
t.$1:updated_by,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_form_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_form_question_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_form_question_unnest_root
FROM (
SELECT
t.$1:form_id,
t.$1:question_id,
t.$1:concept_id,
t.$1:generation,
t.$1:type,
t.$1:default,
t.$1:validation,
t.$1:show_text,
t.$1:"update",
t.$1:populate,
t.$1:"rows",
t.$1:columns,
t.$1:text,
t.$1:description,
t.$1:default_value,
t.$1:calculate,
t.$1:style,
t.$1:options,
t.$1:active,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:updated_by,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_form_question_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_hra_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_hra_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:VERSION,
t.$1:QUESTION_ID,
t.$1:OPTION_ID,
t.$1:question_text,
t.$1:OPTION_VALUE,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_hra_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_medical_code_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_medical_code_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:code,
t.$1:code_set_id,
t.$1:description,
CASE t.$1:effective_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:effective_date)
END,
CASE t.$1:term_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:term_date)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:payment_year,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_medical_code_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_medical_code_sets_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_medical_code_sets_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:name,
t.$1:description,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:is_loaded,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_medical_code_sets_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:patient_id,
t.$1:user_id,
t.$1:name,
t.$1:type_id,
t.$1:status,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:tzix_id,
t.$1:diagnosis,
t.$1:level,
CASE t.$1:open_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:open_date)
END,
t.$1:open_reason,
t.$1:close_reason,
CASE t.$1:close_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:close_date)
END,
t.$1:note,
t.$1:source_type,
t.$1:acuity,
t.$1:tzx_type,
t.$1:consent_type,
CASE t.$1:consent_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:consent_date)
END,
t.$1:consent_reason,
t.$1:consent_method,
t.$1:open_reason_type,
t.$1:close_reason_type,
t.$1:phase,
t.$1:um,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:in_episode,
CASE t.$1:next_review_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:next_review_date)
END,
t.$1:db_rowversion,
t.$1:POA_IND,
t.$1:HAS_NOTES,
t.$1:PCP,
t.$1:primary_contact,
t.$1:coverage_status,
t.$1:diagnosis_code_set_id,
t.$1:next_review_action,
CASE t.$1:onset_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:onset_date)
END,
t.$1:STAGE_CASE_ID,
t.$1:eligibility_id,
CASE t.$1:assigned_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:assigned_date)
END,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_case_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_cost_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_cost_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:CASE_ID,
t.$1:STATUS,
t.$1:USER_ID,
t.$1:ENTRY_TYPE,
t.$1:FROM_DATE,
t.$1:TO_DATE,
t.$1:PROJECTED,
t.$1:ACTUAL,
t.$1:SAVE_TYPE_ID,
t.$1:EXPENSE_TYPE_ID,
t.$1:QUANTITY,
t.$1:SAVE_CATEGORY,
t.$1:META_DATA,
t.$1:read_only,
t.$1:COST,
t.$1:time_type_id,
t.$1:db_rowversion,
t.$1:is_deleted,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_case_cost_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_save_type_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_save_type_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:NAME,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_case_save_type_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_status_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_status_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:status,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_case_status_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_info_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_info_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:subject,
t.$1:body,
t.$1:user_id,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
t.$1:template_id,
t.$1:db_rowversion,
t.$1:STAGE_NOTE_ID,
t.$1:parent_id,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_info_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_info_notepad_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_info_notepad_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:PATIENT_ID,
t.$1:type_id,
t.$1:info_id,
t.$1:event_id,
t.$1:object_id,
t.$1:case_id,
t.$1:file_name,
t.$1:final,
t.$1:security_level,
t.$1:user_security_level,
t.$1:db_rowversion,
CASE t.$1:event_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:event_date)
END,
t.$1:category,
t.$1:is_void,
t.$1:void_reason,
CASE t.$1:void_datetime
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:void_datetime)
END,
t.$1:void_user_id,
t.$1:void_parent_id,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_info_notepad_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_patient_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_patient_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:OC_ID,
t.$1:CID,
t.$1:GUID,
t.$1:GUID_TYPE,
t.$1:GUID_EXPIRE,
t.$1:deleted,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_patient_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_physician_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_physician_unnest_root
FROM (
SELECT
t.$1:MG_ID,
CASE t.$1:published_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:published_date)
END,
t.$1:PRCF_MCTR_SPEC,
t.$1:PRCF_MCTR_SPEC_DESC,
t.$1:PRPR_MCTR_LANG,
t.$1:PRPR_MCTR_LANG_DESC,
t.$1:PRPR_MCTR_TYPE,
t.$1:PRPR_MCTR_TYPE_DESC,
t.$1:PRPR_NAME,
t.$1:PRPR_STS,
t.$1:PRPR_TERM_DT,
t.$1:PRPR_ENTITY,
t.$1:PRPR_ENTITY_DESC,
t.$1:PRPR_MCTR_TRSN,
t.$1:PRPR_MCTR_TRSN_DESC,
t.$1:MCTN_ID,
t.$1:PRCP_ID,
t.$1:PRCP_LAST_NAME,
t.$1:PRCP_FIRST_NAME,
t.$1:PRCP_MID_INT,
t.$1:PRCP_SSN,
t.$1:PRCP_BIRTH_DT,
t.$1:PRCP_SEX,
t.$1:PRAD_TYPE_PRIM,
t.$1:is_active,
t.$1:id,
t.$1:user_id,
CASE t.$1:entry_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:entry_date)
END,
t.$1:manual,
t.$1:title,
t.$1:specialty,
t.$1:entity,
t.$1:npi,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:db_rowversion,
t.$1:ssis_insert,
t.$1:ssis_update,
t.$1:ssis_external_system_id,
t.$1:ssis_external_id,
t.$1:ssis_address_id,
t.$1:ssis_specialty_code,
CASE t.$1:effective_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:effective_date)
END,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_physician_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_pn_annotation_commitreport_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_pn_annotation_commitreport_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:progressnote_id,
t.$1:form_id,
t.$1:question_id,
t.$1:question_value,
t.$1:associatedconcept_id,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_pn_annotation_commitreport_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_auth_status_type_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_auth_status_type_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:type,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_um_auth_status_type_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_case_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_case_unnest_root
FROM (
SELECT
t.$1:case_id,
CASE t.$1:notif_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:notif_date)
END,
CASE t.$1:appr_thru_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:appr_thru_date)
END,
CASE t.$1:review_thru_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:review_thru_date)
END,
t.$1:req_prov_id,
t.$1:treating_prov_id,
t.$1:facility_id,
t.$1:case_setting_id,
t.$1:care_type_id,
t.$1:review_type_id,
t.$1:patient_risk_id,
t.$1:caller_type_id,
t.$1:procedure_code,
t.$1:service_place_id,
t.$1:assigned_to_id,
t.$1:is_urgent,
t.$1:case_type,
t.$1:auto_close_is_enabled,
CASE t.$1:auto_close_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:auto_close_date)
END,
t.$1:WFS_auto_approve,
t.$1:auto_close_is_calculated,
t.$1:treating_prov_addr_id,
t.$1:req_prov_addr_id,
t.$1:facility_addr_id,
t.$1:external_id,
t.$1:expedite,
t.$1:exists_in_facets,
t.$1:global_case_ind,
t.$1:accident_ind,
CASE t.$1:accident_datetime
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:accident_datetime)
END,
t.$1:accident_geographic_state,
t.$1:PHASE,
t.$1:procedure_code_set_id,
t.$1:requester_name,
t.$1:requester_phone,
t.$1:contact_name,
t.$1:contact_phone,
t.$1:PROCEDURE_DATE,
t.$1:db_rowversion,
t.$1:requester_id,
t.$1:requester_code_system_id,
t.$1:REQ_PROV_ROLE_ID,
t.$1:case_priority,
t.$1:auth_status_summary,
t.$1:delegate_system,
t.$1:req_prov_network_status,
t.$1:treating_prov_network_status,
t.$1:facility_network_status,
t.$1:pcp_prov_id,
t.$1:pcp_prov_addr_id,
t.$1:pcp_prov_network_status,
CASE t.$1:regulatory_due_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:regulatory_due_date)
END,
CASE t.$1:min_auth_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:min_auth_date)
END,
CASE t.$1:max_auth_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:max_auth_date)
END,
t.$1:regulatory_request_type_id,
CASE t.$1:min_due_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:min_due_date)
END,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_um_case_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_los_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_los_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:case_id,
CASE t.$1:req_adm_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:req_adm_date)
END,
CASE t.$1:auth_adm_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:auth_adm_date)
END,
CASE t.$1:act_adm_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:act_adm_date)
END,
t.$1:treating_prov_id,
t.$1:facility_id,
t.$1:req_prov_id,
t.$1:diag_code,
t.$1:procedure_code,
t.$1:admitting_prov_id,
t.$1:attending_prov_id,
t.$1:surgeon_id,
CASE t.$1:end_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:end_date)
END,
t.$1:tot_req_days,
t.$1:tot_auth_days,
CASE t.$1:exp_disch_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:exp_disch_date)
END,
CASE t.$1:req_disch_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:req_disch_date)
END,
CASE t.$1:act_disch_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:act_disch_date)
END,
CASE t.$1:next_review_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:next_review_date)
END,
t.$1:allow_calculate,
t.$1:user_id,
t.$1:active,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:overall_total_cost,
t.$1:overall_cost_savings,
t.$1:treating_prov_addr_id,
t.$1:req_prov_addr_id,
t.$1:facility_addr_id,
t.$1:admitting_prov_addr_id,
t.$1:attending_prov_addr_id,
t.$1:surgeon_addr_id,
t.$1:external_id,
t.$1:db_rowversion,
t.$1:POA_IND,
t.$1:REC_LOS_TYPE_ID,
t.$1:REC_LOS_ID,
t.$1:deleted,
t.$1:diagnosis_code_set_id,
t.$1:procedure_code_set_id,
t.$1:discharge_disposition,
t.$1:rec_los_value,
t.$1:original_requested_days,
t.$1:service_type_code,
CASE t.$1:onset_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:onset_date)
END,
t.$1:PROCEDURE_DATE,
t.$1:STAGE_LOS_ID,
t.$1:REQ_PROV_ROLE_ID,
t.$1:req_prov_network_status,
t.$1:treating_prov_network_status,
t.$1:facility_network_status,
t.$1:admitting_prov_network_status,
t.$1:attending_prov_network_status,
t.$1:surgeon_network_status,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_um_los_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_los_extension_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_los_extension_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:los_id,
t.$1:prev_extension,
t.$1:req_days,
t.$1:auth_days,
CASE t.$1:start_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:start_date)
END,
CASE t.$1:end_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:end_date)
END,
t.$1:bed_type,
CASE t.$1:next_review_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:next_review_date)
END,
t.$1:status,
t.$1:change_reason,
t.$1:user_id,
t.$1:active,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:rate_type,
t.$1:rate,
t.$1:rate_discount_percent,
t.$1:rate_discount_amount,
t.$1:rate_reason_id,
t.$1:cost_savings,
t.$1:total_cost,
t.$1:penalty_id,
t.$1:penalty_waiver_id,
t.$1:penalty_notes,
t.$1:status_change_reason_code,
t.$1:historical_status_mask,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:reconciled_total_cost,
t.$1:reconciled_cost_savings,
t.$1:reconciled_cost_diff_reason,
t.$1:reconciled_cost_diff_notes,
t.$1:member_notification_time,
t.$1:provider_notification_time,
t.$1:facility_notification_time,
t.$1:external_id,
t.$1:status_changed_by,
t.$1:seq_number,
t.$1:override_ind,
t.$1:override_amount,
t.$1:override_value,
t.$1:override_explanation,
t.$1:db_rowversion,
CASE t.$1:status_change_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:status_change_date)
END,
CASE t.$1:request_created_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:request_created_date)
END,
t.$1:pre_op,
t.$1:leave_of_absence,
t.$1:proxy_user_id,
t.$1:STAGE_EXT_ID,
t.$1:member_notification_type_id,
t.$1:provider_notification_type_id,
t.$1:facility_notification_type_id,
t.$1:facility_notification_letter_time,
t.$1:member_notification_letter_time,
t.$1:provider_notification_letter_time,
t.$1:eligibility_id,
t.$1:is_gap,
t.$1:revenue_code,
CASE t.$1:due_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:due_date)
END,
t.$1:regulatory_request_type_id,
CASE t.$1:stop_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:stop_date)
END,
t.$1:stop_reason_id,
CASE t.$1:stop_expiration_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:stop_expiration_date)
END,
CASE t.$1:restart_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:restart_date)
END,
t.$1:restart_reason_id,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_um_los_extension_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_service_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_service_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:case_id,
t.$1:diag_code,
t.$1:treating_prov_id,
t.$1:facility_id,
t.$1:req_prov_id,
t.$1:procedure_code,
t.$1:tot_req_units,
t.$1:tot_auth_units,
CASE t.$1:next_review_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:next_review_date)
END,
t.$1:allow_calculate,
t.$1:user_id,
t.$1:active,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:service_type_code,
t.$1:deleted,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:overall_total_cost,
t.$1:overall_cost_savings,
t.$1:ndc_LBLCODE,
t.$1:ndc_PRODCODE,
t.$1:ndc_PKGCODE,
t.$1:treating_prov_addr_id,
t.$1:req_prov_addr_id,
t.$1:facility_addr_id,
t.$1:external_id,
t.$1:service_group_id,
t.$1:medispan_ppid,
t.$1:ndc_tradename,
t.$1:db_rowversion,
t.$1:POA_IND,
t.$1:diagnosis_code_set_id,
t.$1:procedure_code_set_id,
t.$1:is_primary,
CASE t.$1:onset_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:onset_date)
END,
t.$1:PROCEDURE_DATE,
t.$1:tos_from_proc_code,
t.$1:tos_desc_lookup_id,
t.$1:STAGE_SERVICE_ID,
t.$1:REQ_PROV_ROLE_ID,
t.$1:req_prov_network_status,
t.$1:treating_prov_network_status,
t.$1:facility_network_status,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_um_service_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_service_cert_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_um_service_cert_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:service_id,
t.$1:prim,
t.$1:req_units,
t.$1:auth_units,
t.$1:duration,
CASE t.$1:from_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:from_date)
END,
CASE t.$1:to_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:to_date)
END,
t.$1:status,
t.$1:change_reason,
t.$1:user_id,
t.$1:active,
CASE t.$1:create_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:create_date)
END,
CASE t.$1:update_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:update_date)
END,
t.$1:rate_type,
t.$1:rate,
t.$1:rate_discount_percent,
t.$1:rate_discount_amount,
t.$1:rate_reason_id,
t.$1:cost_savings,
t.$1:total_cost,
t.$1:penalty_id,
t.$1:penalty_waiver_id,
t.$1:penalty_notes,
t.$1:status_change_reason_code,
t.$1:historical_status_mask,
t.$1:quantity,
t.$1:quantity_unit,
t.$1:period,
t.$1:period_unit,
t.$1:tot_duration,
t.$1:tot_duration_unit,
t.$1:metadata_guid,
t.$1:VF_01,
t.$1:VF_02,
t.$1:VF_03,
t.$1:VF_04,
t.$1:VF_05,
t.$1:VF_06,
t.$1:VF_07,
t.$1:VF_08,
t.$1:VF_09,
t.$1:VF_10,
t.$1:reconciled_total_cost,
t.$1:reconciled_cost_savings,
t.$1:reconciled_cost_diff_reason,
t.$1:reconciled_cost_diff_notes,
t.$1:member_notification_time,
t.$1:provider_notification_time,
t.$1:facility_notification_time,
t.$1:external_id,
t.$1:status_changed_by,
t.$1:seq_number,
t.$1:override_ind,
t.$1:override_amount,
t.$1:override_value,
t.$1:override_explanation,
t.$1:db_rowversion,
CASE t.$1:status_change_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:status_change_date)
END,
t.$1:proxy_user_id,
t.$1:STAGE_CERT_ID,
t.$1:member_notification_type_id,
t.$1:provider_notification_type_id,
t.$1:facility_notification_type_id,
t.$1:facility_notification_letter_time,
t.$1:member_notification_letter_time,
t.$1:provider_notification_letter_time,
t.$1:eligibility_id,
t.$1:revenue_code,
CASE t.$1:request_created_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:request_created_date)
END,
CASE t.$1:due_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:due_date)
END,
t.$1:regulatory_request_type_id,
CASE t.$1:stop_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:stop_date)
END,
t.$1:stop_reason_id,
CASE t.$1:stop_expiration_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:stop_expiration_date)
END,
CASE t.$1:restart_date
 WHEN '' THEN NULL
 ELSE TO_DATE(t.$1:restart_date)
END,
t.$1:restart_reason_id,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_um_service_cert_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_resource_file_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_resource_file_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:PATH,
t.$1:CONTENTS,
t.$1:CREATE_DATE,
t.$1:UPDATE_DATE,
t.$1:UPDATED_BY,
t.$1:GUID,
t.$1:VARIABLE_FIELD_ID,
t.$1:VARIABLE_FIELD_NAME,
t.$1:VARIABLE_FIELD_TYPE,
t.$1:VARIABLE_FIELD_OPTION_ID,
t.$1:VARIABLE_FIELD_OPTION_VALUE,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_resource_file_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_string_locale_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_string_locale_unnest_root
FROM (
SELECT
t.$1:ID,
t.$1:TYPE_ID,
t.$1:SUB_ID,
t.$1:LOCALE_ID,
t.$1:MG_ID,
t.$1:STRING,
t.$1:UPDATE_DATE,
t.$1:MASK,
t.$1:guid,
t.$1:protected,
t.$1:version,
t.$1:INACTIVE,
t.$1:db_rowversion,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_string_locale_unnest_root/ t)
file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET', EMPTY_FIELD_AS_NULL=TRUE);

