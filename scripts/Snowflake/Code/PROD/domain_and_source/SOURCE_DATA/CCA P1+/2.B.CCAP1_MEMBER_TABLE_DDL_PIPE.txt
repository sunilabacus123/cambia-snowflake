CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_string_locale_reference_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_string_locale_reference_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:type_id,
t.$1:group_name,
t.$1:page_name,
t.$1:edit_mode_278_standard,
t.$1:edit_mode_facets,
t.$1:edit_mode_non_278_standard,
t.$1:SORT_ORDER_IND,
t.$1:mask_info,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_string_locale_reference_unnest_root/ t)
file_format = (FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_tcs_send_log_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_tcs_send_log_unnest_root
FROM (
SELECT
t.$1:id,
CASE t.$1:event_datetime
 WHEN '' THEN NULL
 ELSE CAST(t.$1:event_datetime TO DATETIME)
END,
t.$1:host_name,
t.$1:sender_type,
t.$1:sender_id,
t.$1:task_id,
t.$1:member_cid,
t.$1:cae_time_msec,
t.$1:tcs_time_msec,
t.$1:status,
t.$1:tcs_template_id,
t.$1:tcs_document_id,
t.$1:details,
t.$1:db_rowversion,
t.$1:member_first_name,
t.$1:member_last_name,
t.$1:external_case_id,
t.$1:external_system_id,
t.$1:external_member_id,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_tcs_send_log_unnest_root/ t)
file_format = (FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_org_case_type_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_member_org_case_type_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:type,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_member_org_case_type_unnest_root/ t)
file_format = (FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');
