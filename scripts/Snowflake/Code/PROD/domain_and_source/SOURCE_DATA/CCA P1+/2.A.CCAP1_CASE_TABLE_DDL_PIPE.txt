CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_type_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_org_case_type_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:type,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_org_case_type_unnest_root/ t)
file_format = (FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');

CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_string_locale_reference_unnest_root_PIPE auto_ingest=true AS
COPY INTO ABACUS_PRD_FND_DB.SOURCE_DATA.cca_case_string_locale_reference_unnest_root
FROM (
SELECT
t.$1:id,
t.$1:type_id,
t.$1:group_name,
t.$1:page_name,
t.$1:edit_mode_278_standard,
t.$1:edit_mode_facets,
t.$1:edit_mode_non_278_standard,
t.$1:SORT_ORDER_IND,
t.$1:mask_info,
t.$1:__lineage:input_domain,
t.$1:__lineage:input_url,
t.$1:__lineage:input_line_number,
t.$1:__lineage:ingestion_id,
t.$1:__lineage:batch_id,
t.$1:root_id,
current_timestamp(), current_timestamp()
from @ABACUS_PRD_FND_DB.landing.prod_abacus_stg/source_data/cca_case_string_locale_reference_unnest_root/ t)
file_format = (FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');