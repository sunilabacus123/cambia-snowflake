COPY INTO STATEMENTS:

COPY INTO ABACUS_PRD_FND_DB.LANDING.EMPLOYERGROUPSTUB
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:isauthoritative,t.$1:groupstatus,t.$1:grouptype,t.$1:tin,t.$1:groupname,t.$1:groupbilllevel,t.$1:reinstatementdate,t.$1:currentanniversarydate,t.$1:previousanniversarydate,t.$1:nextanniversarydate,t.$1:renewaldate,t.$1:parentgroupid,t.$1:parentgroupname,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/EmployerGroupStub/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 


COPY INTO ABACUS_PRD_FND_DB.LANDING.ENROLLMENT
     FROM
         (SELECT t.$1:entitytype,t.$1:ingestionid,t.$1:batchid,t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:id,t.$1:isauthoritative,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:partialupdate,t.$1:logicaldeleted,t.$1:harddeleted,t.$1:tenant,t.$1:hashid,t.$1:membersourceid,t.$1:groupsourceid,t.$1:parentgroupsourceid,t.$1:subgroupsourceid,t.$1:contracttype,t.$1:enrollmenttype,t.$1:enrollmentcategory,t.$1:enrollmentcategoryid,t.$1:consumerdrivenhealthcareind,t.$1:marketsegmentcode,t.$1:siccode,t.$1:sourceinstitutionalcoveragetype,t.$1:sourceproductid,t.$1:reasoncode,t.$1:effectivedate,t.$1:terminationdate,t.$1:eligibilityeffectiveind,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Enrollment/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 
		 
COPY INTO ABACUS_PRD_FND_DB.LANDING.PRODUCTSTUB
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:payerid,t.$1:payername,t.$1:payertin,t.$1:lineofbusiness,t.$1:lineofbusinessid,t.$1:productcategory,t.$1:productname,t.$1:effectivedate,t.$1:expirydate,t.$1:networkprefix,t.$1:idstock,t.$1:groupid,t.$1:groupname,t.$1:planid,t.$1:planname,t.$1:idcardtype,t.$1:providernetworkprefix,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/ProductStub/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 
		 
COPY INTO ABACUS_PRD_FND_DB.LANDING.QUALIFIEDEVENT
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:type,t.$1:plancode,t.$1:eventtype,t.$1:eventcode,t.$1:eventpbp,t.$1:eventstatecode,t.$1:eventcountycode,t.$1:eventeffdate,t.$1:eventexpdate,t.$1:transactioncode,t.$1:transactioneffdate,t.$1:transactionreason,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/QualifiedEvent/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 
		 
COPY INTO ABACUS_PRD_FND_DB.LANDING.PHONE
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:phonetype,t.$1:phonesource,t.$1:number,t.$1:countrycode,t.$1:areacode,t.$1:localnumber,t.$1:extension,t.$1:digitcount,t.$1:formatmask,t.$1:geoarea,t.$1:geocountry,t.$1:linetype,t.$1:active,t.$1:validationstatus,t.$1:priority,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Phone/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 
		 

COPY INTO ABACUS_PRD_FND_DB.LANDING.ADDRESS
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:addresstype,t.$1:usagetype,t.$1:addresssource,t.$1:attncareof,t.$1:addressline1,t.$1:addressline2,t.$1:addressline3,t.$1:addressline4,t.$1:city,t.$1:county,t.$1:stateprovince,t.$1:postalcode,t.$1:zipcodeextension,t.$1:country,t.$1:rank,t.$1:latitude,t.$1:longitude,t.$1:primary,t.$1:active,t.$1:validationstatus,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Address/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 


COPY INTO ABACUS_PRD_FND_DB.LANDING.BASEDEMOGRAPHIC
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:gender,t.$1:sex,t.$1:race,t.$1:religion,t.$1:ethnicity,t.$1:maritalstatus,t.$1:nationality,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/BaseDemographic/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 
		 
		 
COPY INTO ABACUS_PRD_FND_DB.LANDING.BIRTH
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:dateofbirth,t.$1:yearofbirth,t.$1:multiplebirthsind,t.$1:birthorder,t.$1:birthplace,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Birth/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 
		 

COPY INTO ABACUS_PRD_FND_DB.LANDING.IDENTIFIER
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:identifiertype,t.$1:standardid,t.$1:identifier,t.$1:checkdigit,t.$1:checkdigitschema,t.$1:issuingauthority,t.$1:organization,t.$1:identifiersource,t.$1:lastupdatedate,t.$1:identifierstatus,t.$1:effdate,t.$1:expdate,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/Identifier/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 
		 

COPY INTO ABACUS_PRD_FND_DB.LANDING.MEMBERSTUB
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:isauthoritative,t.$1:membertype,t.$1:membercategory,t.$1:membercategoryid,t.$1:membercategoryprefix,t.$1:subscriberrelationship,t.$1:parentgroupsourceid,t.$1:groupsourceid,t.$1:subgroupsourceid,t.$1:subscriberid,t.$1:planid,t.$1:carrierid,t.$1:hospiceind,t.$1:institutionalind,t.$1:exchangeind,t.$1:stateresidency,t.$1:occupation,t.$1:hiredate,t.$1:medicarehicnumber,t.$1:medicarestartdate,t.$1:medicaidid,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/MemberStub/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;
		 
		 


COPY INTO ABACUS_PRD_FND_DB.LANDING.PERSONNAME
     FROM
         (SELECT t.$1:correlationid,t.$1:source,t.$1:sourceid,t.$1:parentsourceid,t.$1:relationshipname,t.$1:schemaversion,t.$1:sourcetimestamp,t.$1:consumetimestamp,t.$1:hashid,t.$1:parenttype,t.$1:isalternatename,t.$1:firstname,t.$1:middlename,t.$1:lastname,t.$1:prefixname,t.$1:suffixname,t.$1:formerlastname,t.$1:name,t.$1:preferredname,t.$1:nickname,current_timestamp(),current_timestamp() from @ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/PersonName/2020/08/27/254/ t)
         file_format = (ALLOW_DUPLICATE = FALSE, FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET') force=true;