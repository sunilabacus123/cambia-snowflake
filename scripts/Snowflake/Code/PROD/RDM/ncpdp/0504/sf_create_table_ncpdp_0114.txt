
CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_af
(
relationshipid varchar,
relationshiptype varchar,
name varchar,
address1 varchar,
address2 varchar,
city varchar,
statecode varchar,
zipcode varchar,
phonenumber varchar,
extension varchar,
faxnumber varchar,
relationshipnpi varchar,
relationshipfederaltaxid varchar,
contactname varchar,
contacttitle varchar,
e_mailaddress varchar,
contractualcontactname varchar,
contractualcontacttitle varchar,
contractualcontacte_mail varchar,
operationalcontactname varchar,
operationalcontacttitle varchar,
operationalcontacte_mail varchar,
technicalcontactname varchar,
technicalcontacttitle varchar,
technicalcontacte_mail varchar,
auditcontactname varchar,
auditcontacttitle varchar,
auditcontacte_mail varchar,
parentorganizationid varchar,
effectivefromdate varchar,
deletedate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas
(
ncpdpproviderid varchar,
legalbusinessname varchar,
name varchar,
doctor_sname varchar,
storenumber varchar,
physicallocationaddress1 varchar,
physicallocationaddress2 varchar,
physicallocationcity varchar,
physicallocationstatecode varchar,
physicallocationzipcode varchar,
physicallocationphonenumber varchar,
physicallocationextension varchar,
physicallocationfax varchar,
physicallocatione_mailaddress varchar,
physicallocationcrossstreetordirections varchar,
physicallocationcounty_parish varchar,
physicallocationmsa varchar,
physicallocationpmsa varchar,
physicallocation24houroperationflag varchar,
physicallocationproviderhours varchar,
physicallocationcongressionalvotingdistrict varchar,
physicallocationlanguagecode1 varchar,
physicallocationlanguagecode2 varchar,
physicallocationlanguagecode3 varchar,
physicallocationlanguagecode4 varchar,
physicallocationlanguagecode5 varchar,
physicallocationstoreopendate varchar,
physicallocationstoreclosuredate varchar,
mailingaddress1 varchar,
mailingaddress2 varchar,
mailingaddresscity varchar,
mailingaddressstatecode varchar,
mailingaddresszipcode varchar,
contactlastname varchar,
contactfirstname varchar,
contactmiddleinitial varchar,
contacttitle varchar,
contactphonenumber varchar,
contactextension varchar,
contacte_mailaddress varchar,
dispenserclasscode varchar,
primaryprovidertypecode varchar,
secondaryprovidertypecode varchar,
tertiaryprovidertypecode varchar,
medicareprovidersupplierid varchar,
nationalprovideridnpi varchar,
dearegistrationid varchar,
deaexpirationdate varchar,
federaltaxidnumber varchar,
stateincometaxidnumber varchar,
deactivationcode varchar,
reinstatementcode varchar,
reinstatementdate varchar,
transactioncodetransactionfileonly varchar,
transactiondatetransactionfileonly varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_coo
(
ncpdpproviderid varchar,
oldncpdpproviderid varchar,
oldstoreclosedate varchar,
changeofownershipeffectivedate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_fwa
(
ncpdpproviderid varchar,
medicaidpartd varchar,
fwaattestation varchar,
versionnumber varchar,
planyear varchar,
q1 varchar,
q2 varchar,
accreditationdate varchar,
accreditationorganization varchar,
q3 varchar,
q4 varchar,
signatureofresponsibleparty varchar,
signaturedate varchar,
responsibleparty varchar,
participatingpharmacyorpsaoname varchar,
address1 varchar,
address2 varchar,
city varchar,
statecode varchar,
zipcode varchar,
npi varchar,
fax varchar,
email varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_rr
(
ncpdpproviderid varchar,
relationshipid varchar,
paymentcenterid varchar,
remitandreconciliationid varchar,
providertype varchar,
isprimary varchar,
effectivefromdate varchar,
effectivethroughdate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_tx
(
ncpdpproviderid varchar,
taxonomycode varchar,
providertypecode varchar,
deletedate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_svc
(
ncpdpproviderid varchar,
acceptseprescriptionsindicator varchar,
acceptseprescriptionscode varchar,
deliveryserviceindicator varchar,
deliveryservicecode varchar,
compoundingserviceindicator varchar,
compoundingservicecode varchar,
driveupwindowindicator varchar,
driveupwindowcode varchar,
durablemedicalequipmentindicator varchar,
durablemedicalequipmentcode varchar,
walkinclinicindicator varchar,
walkincliniccode varchar,
"24houremergencyserviceindicator" varchar,
"24houremergencyservicecode" varchar,
multidosecompliancepackagingindicator varchar,
multidosecompliancepackagingcode varchar,
immunizationsprovidedindicator varchar,
immunizationsprovidedcode varchar,
handicappedaccessibleindicator varchar,
handicappedaccessiblecode varchar,
"340bstatusindicator" varchar,
"340bstatuscode" varchar,
closeddoorfacilityindicator varchar,
closeddoorfacilitystatuscode varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_stl
(
ncpdpproviderid varchar,
licensestatecode varchar,
statelicensenumber varchar,
statelicenseexpirationdate varchar,
deletedate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_erx
(
ncpdpproviderid varchar,
eprescribingnetworkidentifier varchar,
eprescribingservicelevelcodes varchar,
effectivefromdate varchar,
effectivethroughdate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_pr
(
parentorganizationid varchar,
parentorganizationname varchar,
address1 varchar,
address2 varchar,
city varchar,
statecode varchar,
zipcode varchar,
phonenumber varchar,
extension varchar,
faxnumber varchar,
parentorganizationnpi varchar,
parentorganizationfederaltaxid varchar,
contactname varchar,
contacttitle varchar,
e_mailaddress varchar,
deletedate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_md
(
ncpdpproviderid varchar,
statecode varchar,
medicaidid varchar,
deletedate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_pc
(
paymentcenterid varchar,
paymentcentername varchar,
paymentcenteraddress1 varchar,
paymentcenteraddress2 varchar,
paymentcentercity varchar,
paymentcenterstatecode varchar,
paymentcenterzipcode varchar,
paymentcenterphonenumber varchar,
paymentcenterextension varchar,
paymentcenterfaxnumber varchar,
paymentcenternpi varchar,
paymentcenterfederaltaxid varchar,
paymentcentercontactname varchar,
paymentcentercontacttitle varchar,
paymentcentere_mailaddress varchar,
deletedate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE  TABLE ABACUS_PRD_FND_DB.LANDING.reference_data_ncpdp_mas_rec
(
remitandreconciliationid varchar,
remitandreconciliationname varchar,
remitandreconciliationaddress1 varchar,
remitandreconciliationaddress2 varchar,
remitandreconciliationcity varchar,
remitandreconciliationstatecode varchar,
remitandreconciliationzipcode varchar,
remitandreconciliationphonenumber varchar,
remitandreconciliationextension varchar,
remitandreconciliationfaxnumber varchar,
remitandreconciliationnpi varchar,
remitandreconciliationfederaltaxid varchar,
remitandreconciliationcontactname varchar,
remitandreconciliationcontacttitle varchar,
remitandreconciliatione_mailaddress varchar,
deletedate varchar,
LDG_REC_CREAT_DT TIMESTAMP,
LDG_REC_UPDT_DT TIMESTAMP
);
