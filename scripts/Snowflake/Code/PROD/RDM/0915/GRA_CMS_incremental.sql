CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.GRA_CMS
(state VARCHAR
,rating_area_id VARCHAR
,county_name VARCHAR
,three_digit_zip_code VARCHAR
,last_release_year VARCHAR
,code_set VARCHAR
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
);
	
	
CREATE OR REPLACE PIPE ABACUS_PRD_FND_DB.LANDING.GRA_CMS_PIPE auto_ingest=true AS
COPY INTO  ABACUS_PRD_FND_DB.LANDING.GRA_CMS 
FROM ( SELECT   
t.$1:state
,t.$1:rating_area_id
,t.$1:county_name
,t.$1:three_digit_zip_code
,t.$1:last_release_year
,t.$1:code_set
,current_timestamp()
,current_timestamp()
FROM
@ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/rdm/gra_cms/ t)
file_format = ( FORMAT_NAME='ABACUS_PRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_PRD_FND_DB.LANDING.GRA_CMS_STREAM_COUNT  ON TABLE ABACUS_PRD_FND_DB.LANDING.GRA_CMS;


CREATE OR REPLACE PROCEDURE ABACUS_PRD_FND_DB.LANDING.GRA_CMS_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 

  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_PRD_FND_DB.TRANSIENT.GRA_CMS_TEMP_COUNT
         AS          
         SELECT 'GRA_CMS' AS TABLE_NAME, TO_DATE(LDG_REC_CREAT_DT) AS LOAD_DATE,  COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_PRD_FND_DB.LANDING.GRA_CMS   
         GROUP BY 1, 2
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_PRD_FND_DB.CONTROL.REFERENCE_DATA_LANDING_RECORD_COUNT AS T
        using      ABACUS_PRD_FND_DB.TRANSIENT.GRA_CMS_TEMP_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME    
         AND T.LOAD_DATE = S.LOAD_DATE 
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME,LOAD_DATE,RECORD_COUNT)
                     VALUES(S.TABLE_NAME,S.LOAD_DATE,S.RECORD_COUNT)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_PRD_FND_DB.LANDING.GRA_CMS_COUNT_CHECK_TASK
  warehouse = PRD_ABACUS_ELT_XS_WH
  schedule = '1 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_PRD_FND_DB.LANDING.GRA_CMS_STREAM_COUNT')            
as
  call ABACUS_PRD_FND_DB.LANDING.GRA_CMS_COUNT_CHECK_PROC();
 

ALTER TASK ABACUS_PRD_FND_DB.LANDING.GRA_CMS_COUNT_CHECK_TASK RESUME;   