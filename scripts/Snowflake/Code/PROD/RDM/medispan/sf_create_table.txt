
CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_drg_ndc_wk
(
ndc_upc_hri varchar,
tee_code varchar,
dea_class_code varchar,
desi_code varchar,
rx_otc_indicator_code varchar,
gppc varchar,
old_ndc_upc_hri varchar,
new_ndc_upc_hri varchar,
repackage_code varchar,
id_number_format_code varchar,
third_party_restriction_code varchar,
kdc_flag varchar,
medispan_labeler_identifier varchar,
multi_source_code varchar,
name_type_code varchar,
item_status_flag varchar,
innerpack_code varchar,
clinic_pack_code varchar,
ppg_indicator_code varchar,
hfpg_indicator_code varchar,
dispensing_unit_code varchar,
dollar_rank_code varchar,
rx_rank_code varchar,
storage_condition_code varchar,
limited_distribution_code varchar,
old_effective_date date,
new_effective_date date,
next_smaller_ndc_suffix_number varchar,
next_larger_ndc_suffix_number varchar,
last_change_date date,
concept_type varchar,
country_code varchar,
dose_form_id varchar,
strength varchar,
strength_uom varchar,
name_source varchar,
device_flag varchar,
status varchar,
link_value varchar,
link_date date,
routed_drug_form_id varchar,
drug_dose_form_id varchar,
strength_strength_uom varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_frm_wk
(
concept_type varchar,
country_code varchar,
concept_id varchar,
transaction_cd varchar,
status varchar,
link_value varchar,
link_date date,
reserve varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_prc_wk
(
ndc_upc_hri varchar,
price_code varchar,
price_effective_date date,
unit_price varchar,
extended_unit_price varchar,
package_price varchar,
awp_indicator_code varchar,
transaction_cd varchar,
last_change_date date,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_rtdf_wk
(
concept_type varchar,
country_code varchar,
concept_id varchar,
transaction_cd varchar,
dose_form_id varchar,
status varchar,
link_value varchar,
link_date date,
reserve varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_gppc_wk
(
gppc varchar,
package_size varchar,
package_size_uom varchar,
package_qty varchar,
unit_dose_package_code varchar,
package_desc_code varchar,
reserve varchar,
transaction_cd varchar,
last_change_date date,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_idrg_wk
(
ingredient_drug_id varchar,
transaction_cd varchar,
cas_number varchar,
reserve_1 varchar,
ingredient_drug_name varchar,
generic_id varchar,
reserve_2 varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_ndcm_wk
(
ndc_upc_hri varchar,
modifier_code varchar,
reserve varchar,
transaction_cd varchar,
last_change_date date,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_ndc_wk
(
ndc_upc_hri varchar,
tee_code varchar,
dea_class_code varchar,
desi_code varchar,
rx_otc_indicator_code varchar,
gppc varchar,
old_ndc_upc_hri varchar,
new_ndc_upc_hri varchar,
repackage_code varchar,
id_number_format_code varchar,
third_party_restriction_code varchar,
kdc_flag varchar,
medispan_labeler_identifier varchar,
multi_source_code varchar,
name_type_code varchar,
item_status_flag varchar,
innerpack_code varchar,
clinic_pack_code varchar,
reserve_1 varchar,
ppg_indicator_code varchar,
hfpg_indicator_code varchar,
dispensing_unit_code varchar,
dollar_rank_code varchar,
rx_rank_code varchar,
storage_condition_code varchar,
limited_distribution_code varchar,
old_effective_date date,
new_effective_date date,
next_smaller_ndc_suffix_number varchar,
next_larger_ndc_suffix_number varchar,
reserve_2 varchar,
transaction_cd varchar,
last_change_date date,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_ings_wk
(
ingredient_set_id varchar,
ingredient_id varchar,
ingredient_flag varchar,
transaction_cd varchar,
reserve varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_mod_wk
(
modifier_code varchar,
modifier_description varchar,
reserve varchar,
transaction_cd varchar,
last_change_date date,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_lab_wk
(
labeler_identifier varchar,
manufacturer_name varchar,
manufacturer_abbr_name varchar,
labeler_type_code varchar,
reserve varchar,
transaction_cd varchar,
last_change_date date,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_sec_wk
(
external_drug_id varchar,
external_drug_id_format varchar,
alternate_drug_id varchar,
alternate_drug_id_format_code varchar,
transaction_cd varchar,
reserve varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_name_ndc_wk
(
ndc_upc_hri varchar,
tee_code varchar,
dea_class_code varchar,
desi_code varchar,
rx_otc_indicator_code varchar,
gppc varchar,
old_ndc_upc_hri varchar,
new_ndc_upc_hri varchar,
repackage_code varchar,
id_number_format_code varchar,
third_party_restriction_code varchar,
kdc_flag varchar,
medispan_labeler_identifier varchar,
multi_source_code varchar,
name_type_code varchar,
item_status_flag varchar,
innerpack_code varchar,
clinic_pack_code varchar,
ppg_indicator_code varchar,
hfpg_indicator_code varchar,
dispensing_unit_code varchar,
dollar_rank_code varchar,
rx_rank_code varchar,
storage_condition_code varchar,
limited_distribution_code varchar,
old_effective_date date,
new_effective_date date,
next_smaller_ndc_suffix_number varchar,
next_larger_ndc_suffix_number varchar,
reserve_2 varchar,
transaction_cd varchar,
last_change_date date,
drug_name varchar,
route_of_administration_code varchar,
dosage_form varchar,
strength varchar,
strength_uom varchar,
bioequivalence_code varchar,
controlled_substance_code varchar,
efficacy_code varchar,
legend_indicator_code varchar,
multi_source_summary_code varchar,
brand_name_code varchar,
name_source_code varchar,
screenable_flag varchar,
local_systemic_code varchar,
maintenance_drug_code varchar,
form_type_code varchar,
internal_external_code varchar,
single_combination_code varchar,
representative_gpi_flag varchar,
representative_kdc_flag varchar,
transaction_cd_1 varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_rte_wk
(
concept_type varchar,
country_code varchar,
concept_id varchar,
transaction_cd varchar,
status varchar,
link_value varchar,
link_date date,
reserve varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_stuom_wk
(
concept_type varchar,
country_code varchar,
concept_id varchar,
transaction_cd varchar,
strength varchar,
strength_uom varchar,
status varchar,
link_value varchar,
link_date date,
reserve varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_sum_wk
(
record_type varchar,
reserve_1 varchar,
sequence_number varchar,
reserve_2 varchar,
comment_marker varchar,
data_comment varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_val_wk
(
field_identifier varchar,
field_value varchar,
language_code varchar,
value_description varchar,
value_abbreviation varchar,
reserve varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);

CREATE OR REPLACE TABLE ABACUS_PRD_FND_DB.LANDING.medispan_str_wk
(
ingredient_identifier varchar,
reserve_1 varchar,
transaction_cd varchar,
ingredient_drug_id varchar,
ingredient_strength_value varchar,
combined_ingredient_strength_uom varchar,
individual_ingredient_strength_uom varchar,
volume_value varchar,
volume_uom varchar,
reserve_2 varchar,
code_set varchar,
FND_REC_CREAT_DT TIMESTAMP,
FND_REC_UPDT_DT TIMESTAMP
);
