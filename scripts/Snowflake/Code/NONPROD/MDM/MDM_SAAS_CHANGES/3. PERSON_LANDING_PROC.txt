
CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_LANDING_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER
  AS
  $$

  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 

  var rs = snowflake.execute( { sqlText:
      `CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_UNS_TEMP AS SELECT * FROM ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_UNS_STREAM;`
       } );
	   

  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_NONPRD_FND_DB.CONTROL.MDM_SF_RUNIDS_PERSON SELECT ABACUS_NONPRD_FND_DB.MDM_LANDING.MDMPERSFRUNS_01.nextval, 'PERSON', CURRENT_TIMESTAMP();`
       } );


  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_ADDRESS SELECT DISTINCT	COL1:CorrelationID, 	COL1:MasterRecordID, 	COL1:Source, 	COL1:SourceID, 	COL1:ParentSourceID, 	COL1:RelationShipName,	 	COL1:SchemaVersion, 	COL1:SourceTimestamp, 	COL1:ConsumeTimestamp, 	COL1:HashID, 	COL1:ParentType, 	value:AddressType::String, 	value:UsageType::String, 	value:AddressSource::String, 	value:AttnCareOf::String, 	value:AddressLine1::String, 	value:AddressLine2::String, 	value:AddressLine3::String, 	value:AddressLine4::String, 	value:City::String, 	value:County::String, 	value:StateProvince::String, 	value:PostalCode::String, 	value:ZipCodeExtension::String, 	value:Country::String, 	value:Rank::String, 	value:Latitude::String, 	value:Longitude::String, 	COL1:Primary, 	value:Active::String, 	value:ValidationStatus::String, value:AddressLine5::String, value:POBox::String, value:HandicapAccessInd::String, value:EffDate::String, value:ExpDate::String, value:VerificationStatus::String, value:GeoAccuracy::String, value:DeliveryAddress1::String, value:ExtendedAttributes::String, current_timestamp(), 	current_timestamp()	FROM ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_UNS_TEMP 	, lateral flatten( input => COL1:Addresses );`
       } );


  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_EMAIL SELECT DISTINCT	COL1:CorrelationID, 	COL1:MasterRecordID, 	COL1:Source, 	COL1:SourceID, 	COL1:ParentSourceID, 	COL1:RelationShipName,	 	COL1:SchemaVersion, 	COL1:SourceTimestamp, 	COL1:ConsumeTimestamp, 	COL1:HashID, 	COL1:ParentType, 	value:EmailType::String, 	COL1:Rank, 	value:Active::String, 	value:Email::String, 	COL1:ValidationStatus, value:EffDate::String, value:ExpDate::String,	current_timestamp(), 	current_timestamp()	FROM ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_UNS_TEMP 	, lateral flatten( input => COL1:Emails );`
       } );

	   
  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_IDENTIFIER SELECT DISTINCT	COL1:CorrelationID, 	COL1:MasterRecordID, 	COL1:Source, 	COL1:SourceID, 	COL1:ParentSourceID, 	COL1:RelationShipName,	 	COL1:SchemaVersion, 	COL1:SourceTimestamp, 	COL1:ConsumeTimestamp, 	COL1:HashID, 	COL1:ParentType, 	value:IdentifierType::String, 	COL1:StandardID, 	value:Identifier::String, 	COL1:CheckDigit, 	COL1:CheckDigitSchema, 	value:IssuingAuthority::String, 	value:Organization::String, 	value:IdentifierSource::String, 	value:LastUpdateDate::String, 	value:IdentifierStatus::String, 	value:EffDate::String, 	value:ExpDate::String, 	value:Use::String, current_timestamp(), 	current_timestamp() 	FROM ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_UNS_TEMP 	, lateral flatten( input => COL1:Identifiers );`
       } );


  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_MDMCROSSREFERENCE SELECT DISTINCT	COL1:CorrelationID, 	COL1:MasterRecordID, 	COL1:Source, 	COL1:SourceID, 	COL1:ParentSourceID, 	COL1:RelationShipName,	 	COL1:SchemaVersion, 	COL1:SourceTimestamp, 	COL1:ConsumeTimestamp, 	COL1:HashID, 	COL1:ParentType, 	value:RefEntityType::String, 	COL1:RefEntityCorrelationID, 	value:RefEntitySourceID::String, 	value:RefEntitySource::String, 	current_timestamp(), 	current_timestamp() 	FROM ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_UNS_TEMP 	, lateral flatten( input => COL1:MDMCrossRefs );`
       } );


  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_PHONE SELECT DISTINCT	COL1:CorrelationID, 	COL1:MasterRecordID, 	COL1:Source, 	COL1:SourceID, 	COL1:ParentSourceID, 	COL1:RelationShipName,	 	COL1:SchemaVersion, 	COL1:SourceTimestamp, 	COL1:ConsumeTimestamp, 	COL1:HashID, 	COL1:ParentType, 	value:PhoneType::String, 	value:PhoneSource::String, 	value:Number::String, 	COL1:CountryCode, 	COL1:AreaCode, 	COL1:LocalNumber, 	value:Extension::String, 	COL1:DigitCount, 	COL1:FormatMask, 	COL1:GeoArea, 	COL1:GeoCountry, 	COL1:LineType, 	value:Active::Boolean, 	COL1:ValidationStatus, 	value:Priority::Boolean, 	value:EffDate::String, value:ExpDate::String, value:Rank::String, value:Use::String, value:Availabilities::String, current_timestamp(), 	current_timestamp() 	FROM ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_UNS_TEMP 	, lateral flatten( input => COL1:Phones);`
       } );



  var rs = snowflake.execute( { sqlText:
      `INSERT INTO ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_MASTER SELECT DISTINCT COL1:Tenant::String, COL1:NationalID::String, COL1:Status::String, COL1:BaseDemographic:Sex::String, COL1:BaseDemographic:MaritalStatus::String, COL1:BaseDemographic:Gender::String, COL1:Name:FirstName::String, COL1:Name:MiddleName::String, COL1:Name:LastName::String, COL1:Name:Name::String, COL1:Name:SuffixName::String, COL1:Name:PrefixName::String, COL1:Birth:DateOfBirth::Date, COL1:Birth:YearOfBirth::String, COL1:IsAuthoritative::Boolean, COL1:MasterRecordID::String, COL1:EntityType::String, COL1:EventName::String, COL1:CreatedBy::String, COL1:CreatedTime::DateTime, COL1:UpdatedBy::String, COL1:UpdatedTime::DateTime, COL1:SchemaVersion::String, COL1:Source::String, COL1:SourceID::String, COL1:SourceTimestamp::DateTime, COL1:BatchIDs::String, COL1:IngestionIDs::String, COL1:MasterRecordStatus::String, COL1:CorrelationID::String, COL1:SurvivedRecordID::String, current_timestamp(), current_timestamp() FROM ABACUS_NONPRD_FND_DB.MDM_LANDING.PERSON_UNS_TEMP;`
       } );


	   

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );
    return 'Done.';
    $$;





