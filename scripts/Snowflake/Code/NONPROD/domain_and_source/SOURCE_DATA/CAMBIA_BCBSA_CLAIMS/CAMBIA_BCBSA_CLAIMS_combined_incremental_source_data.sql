CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT
(home_pln_cd VARCHAR
,host_pln_cd VARCHAR
,sccf VARCHAR
,its_sub_id VARCHAR
,pat_relshp_to_sub VARCHAR
,pat_gndr VARCHAR
,pat_fst_name VARCHAR
,pat_lst_name VARCHAR
,pat_mi VARCHAR
,pat_dob VARCHAR
,bcbs_prov_num VARCHAR
,prov_npi VARCHAR
,tot_billd_chrgs VARCHAR
,tot_cov_amt VARCHAR
,tot_alwd_amt VARCHAR
,tot_pd_amt VARCHAR
,clm_typ VARCHAR
,faclty_typ VARCHAR
,typ_of_bill VARCHAR
,adm_dt VARCHAR
,stmt_frm_dt VARCHAR
,stmt_to_dt VARCHAR
,primy_icd9_prcdr_cd VARCHAR
,fst_icd9_prcdr_cd VARCHAR
,secd_icd9_prcdr_cd VARCHAR
,thrd_icd9_prcdr_cd VARCHAR
,fourth_icd9_prcdr_cd VARCHAR
,primy_icd10_prcdr_cd VARCHAR
,fst_icd10_prcdr_cd VARCHAR
,secd_icd10_prcdr_cd VARCHAR
,thrd_icd10_prcdr_cd VARCHAR
,fourth_icd10_prcdr_cd VARCHAR
,icd9_diag_cd_princ VARCHAR
,icd9_diag_cd_secd_1 VARCHAR
,icd9_diag_cd_secd_2 VARCHAR
,icd9_diag_cd_secd_3 VARCHAR
,icd9_diag_cd_secd_4 VARCHAR
,icd9_diag_cd_secd_5 VARCHAR
,icd9_diag_cd_secd_6 VARCHAR
,icd9_diag_cd_secd_7 VARCHAR
,icd9_diag_cd_secd_8 VARCHAR
,icd9_diag_cd_secd_9 VARCHAR
,icd9_diag_cd_secd_10 VARCHAR
,icd9_diag_cd_secd_11 VARCHAR
,icd9_diag_cd_secd_12 VARCHAR
,icd9_diag_cd_secd_13 VARCHAR
,icd9_diag_cd_secd_14 VARCHAR
,icd9_diag_cd_secd_15 VARCHAR
,icd9_diag_cd_secd_16 VARCHAR
,icd9_diag_cd_secd_17 VARCHAR
,icd9_diag_cd_secd_18 VARCHAR
,icd9_diag_cd_secd_19 VARCHAR
,icd9_diag_cd_secd_20 VARCHAR
,icd9_diag_cd_secd_21 VARCHAR
,icd9_diag_cd_secd_22 VARCHAR
,icd9_diag_cd_secd_23 VARCHAR
,icd9_diag_cd_secd_24 VARCHAR
,icd10_diag_cd_princ VARCHAR
,icd10_diag_cd_secd_1 VARCHAR
,icd10_diag_cd_secd_2 VARCHAR
,icd10_diag_cd_secd_3 VARCHAR
,icd10_diag_cd_secd_4 VARCHAR
,icd10_diag_cd_secd_5 VARCHAR
,icd10_diag_cd_secd_6 VARCHAR
,icd10_diag_cd_secd_7 VARCHAR
,icd10_diag_cd_secd_8 VARCHAR
,icd10_diag_cd_secd_9 VARCHAR
,icd10_diag_cd_secd_10 VARCHAR
,icd10_diag_cd_secd_11 VARCHAR
,icd10_diag_cd_secd_12 VARCHAR
,icd10_diag_cd_secd_13 VARCHAR
,icd10_diag_cd_secd_14 VARCHAR
,icd10_diag_cd_secd_15 VARCHAR
,icd10_diag_cd_secd_16 VARCHAR
,icd10_diag_cd_secd_17 VARCHAR
,icd10_diag_cd_secd_18 VARCHAR
,icd10_diag_cd_secd_19 VARCHAR
,icd10_diag_cd_secd_20 VARCHAR
,icd10_diag_cd_secd_21 VARCHAR
,icd10_diag_cd_secd_22 VARCHAR
,icd10_diag_cd_secd_23 VARCHAR
,icd10_diag_cd_secd_24 VARCHAR
,provider_tax_id VARCHAR
,claim_id VARCHAR
,cmi VARCHAR
,mmi VARCHAR
,member_id VARCHAR
,claim_paid_dt VARCHAR
,lineage_input_domain VARCHAR
,lineage_input_url VARCHAR
,lineage_input_line_number VARCHAR 
,lineage_ingestion_id VARCHAR 
,lineage_batch_id VARCHAR 
,correlationid VARCHAR		
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
);
	
	
CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_PIPE auto_ingest=true AS
COPY INTO  ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT 
FROM ( SELECT   
t.$1:home_pln_cd
,t.$1:host_pln_cd
,t.$1:sccf
,t.$1:its_sub_id
,t.$1:pat_relshp_to_sub
,t.$1:pat_gndr
,t.$1:pat_fst_name
,t.$1:pat_lst_name
,t.$1:pat_mi
,t.$1:pat_dob
,t.$1:bcbs_prov_num
,t.$1:prov_npi
,t.$1:tot_billd_chrgs
,t.$1:tot_cov_amt
,t.$1:tot_alwd_amt
,t.$1:tot_pd_amt
,t.$1:clm_typ
,t.$1:faclty_typ
,t.$1:typ_of_bill
,t.$1:adm_dt
,t.$1:stmt_frm_dt
,t.$1:stmt_to_dt
,t.$1:primy_icd9_prcdr_cd
,t.$1:fst_icd9_prcdr_cd
,t.$1:secd_icd9_prcdr_cd
,t.$1:thrd_icd9_prcdr_cd
,t.$1:fourth_icd9_prcdr_cd
,t.$1:primy_icd10_prcdr_cd
,t.$1:fst_icd10_prcdr_cd
,t.$1:secd_icd10_prcdr_cd
,t.$1:thrd_icd10_prcdr_cd
,t.$1:fourth_icd10_prcdr_cd
,t.$1:icd9_diag_cd_princ
,t.$1:icd9_diag_cd_secd_1
,t.$1:icd9_diag_cd_secd_2
,t.$1:icd9_diag_cd_secd_3
,t.$1:icd9_diag_cd_secd_4
,t.$1:icd9_diag_cd_secd_5
,t.$1:icd9_diag_cd_secd_6
,t.$1:icd9_diag_cd_secd_7
,t.$1:icd9_diag_cd_secd_8
,t.$1:icd9_diag_cd_secd_9
,t.$1:icd9_diag_cd_secd_10
,t.$1:icd9_diag_cd_secd_11
,t.$1:icd9_diag_cd_secd_12
,t.$1:icd9_diag_cd_secd_13
,t.$1:icd9_diag_cd_secd_14
,t.$1:icd9_diag_cd_secd_15
,t.$1:icd9_diag_cd_secd_16
,t.$1:icd9_diag_cd_secd_17
,t.$1:icd9_diag_cd_secd_18
,t.$1:icd9_diag_cd_secd_19
,t.$1:icd9_diag_cd_secd_20
,t.$1:icd9_diag_cd_secd_21
,t.$1:icd9_diag_cd_secd_22
,t.$1:icd9_diag_cd_secd_23
,t.$1:icd9_diag_cd_secd_24
,t.$1:icd10_diag_cd_princ
,t.$1:icd10_diag_cd_secd_1
,t.$1:icd10_diag_cd_secd_2
,t.$1:icd10_diag_cd_secd_3
,t.$1:icd10_diag_cd_secd_4
,t.$1:icd10_diag_cd_secd_5
,t.$1:icd10_diag_cd_secd_6
,t.$1:icd10_diag_cd_secd_7
,t.$1:icd10_diag_cd_secd_8
,t.$1:icd10_diag_cd_secd_9
,t.$1:icd10_diag_cd_secd_10
,t.$1:icd10_diag_cd_secd_11
,t.$1:icd10_diag_cd_secd_12
,t.$1:icd10_diag_cd_secd_13
,t.$1:icd10_diag_cd_secd_14
,t.$1:icd10_diag_cd_secd_15
,t.$1:icd10_diag_cd_secd_16
,t.$1:icd10_diag_cd_secd_17
,t.$1:icd10_diag_cd_secd_18
,t.$1:icd10_diag_cd_secd_19
,t.$1:icd10_diag_cd_secd_20
,t.$1:icd10_diag_cd_secd_21
,t.$1:icd10_diag_cd_secd_22
,t.$1:icd10_diag_cd_secd_23
,t.$1:icd10_diag_cd_secd_24
,t.$1:provider_tax_id
,t.$1:claim_id
,t.$1:cmi
,t.$1:mmi
,t.$1:member_id
,t.$1:claim_paid_dt
,t.$1:__lineage:input_domain
,t.$1:__lineage:input_url
,t.$1:__lineage:input_line_number
,t.$1:__lineage:ingestion_id
,t.$1:__lineage:batch_id
,t.$1:root_id
,current_timestamp()
,current_timestamp()
FROM
@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/source_data/cambia_bcbsa_claims_claimheader_unnest_root/ t)
file_format = ( FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_STREAM_COUNT  ON TABLE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT;


CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 

  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_STREAM_COUNT_TEMP AS SELECT * FROM 
	  ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_STREAM_COUNT
         ;`
} );
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT
         AS          
         SELECT 'CAMBIA-BCBSA-CLAIMS' AS CONNECTOR, 'BCBSA-CLAIM' AS SOURCE, 'CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT' AS TABLE_NAME, lineage_ingestion_id as INGESTIONID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT   
         WHERE lineage_ingestion_id  IN (SELECT DISTINCT lineage_ingestion_id  FROM 
		 ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_STREAM_COUNT_TEMP ) GROUP BY 1, 2, 3, 4
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_INGESTIONID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME    
         AND T.INGESTIONID = S.INGESTIONID
         AND T.CONNECTOR = S.CONNECTOR    
         AND T.SOURCE = S.SOURCE    
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(CONNECTOR,SOURCE, TABLE_NAME,INGESTIONID,RECORD_COUNT)
                     VALUES(S.CONNECTOR,S.SOURCE,S.TABLE_NAME, S.INGESTIONID, S.RECORD_COUNT)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT
         AS          
         SELECT 'CAMBIA-BCBSA-CLAIMS' AS CONNECTOR, 'BCBSA-CLAIM' AS SOURCE, 'CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT' AS TABLE_NAME, lineage_batch_id as BATCH_ID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT     
         WHERE lineage_batch_id IN (SELECT DISTINCT lineage_batch_id FROM 
		 ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_STREAM_COUNT_TEMP ) GROUP BY 1, 2, 3, 4
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_BATCH_ID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME    
         AND T.BATCH_ID = S.BATCH_ID
         AND T.CONNECTOR = S.CONNECTOR        
         AND T.SOURCE = S.SOURCE    
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(CONNECTOR,SOURCE, TABLE_NAME,BATCH_ID,RECORD_COUNT)
                     VALUES(S.CONNECTOR,S.SOURCE,S.TABLE_NAME, S.BATCH_ID, S.RECORD_COUNT)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT_CHECK_TASK
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  schedule = '5 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_STREAM_COUNT')            
as
  call ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT_CHECK_PROC();
 

 
CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COPY_HISTORY_1
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT_CHECK_TASK
as
  CREATE  OR REPLACE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_TEMP_COUNT_COPY_HISOTRY
 AS
 SELECT TABLE_NAME, TABLE_SCHEMA_NAME AS SCHEMA_NAME, SPLIT(FILE_NAME,'/')[4]::String AS FILE_NAME, PIPE_RECEIVED_TIME AS LOAD_START_TIME, LAST_LOAD_TIME AS LOAD_END_TIME,
 ROW_COUNT AS RECORD_COUNT,
 FIRST_ERROR_MESSAGE AS ERROR_MESSAGE, FIRST_ERROR_LINE_NUMBER AS ERROR_LINE_NUMBER, FIRST_ERROR_COLUMN_NAME AS ERROR_COLUMN_NAME, ERROR_COUNT, STATUS
 from table(information_schema.copy_history(table_name=>'ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT', start_time=> dateadd(hours, -24, CURRENT_TIMESTAMP()))) WHERE STATUS NOT IN ('LOAD_IN_PROGRESS');
     
	 

CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COPY_HISTORY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 


  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY AS T
         USING  ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_TEMP_COUNT_COPY_HISOTRY  AS S     
         ON T.TABLE_NAME = S.TABLE_NAME     
         AND T.SCHEMA_NAME = S.SCHEMA_NAME
         AND T.FILE_NAME = S.FILE_NAME
         AND T.STATUS = S.STATUS
         AND T.RECORD_COUNT = S.RECORD_COUNT
         AND T.LOAD_START_TIME = S.LOAD_START_TIME
         AND T.LOAD_END_TIME = S.LOAD_END_TIME
         AND T.ERROR_COUNT = S.ERROR_COUNT
         WHEN matched
              THEN
              UPDATE SET  
                     T.ERROR_MESSAGE = S.ERROR_MESSAGE,
                     T.ERROR_LINE_NUMBER = S.ERROR_LINE_NUMBER,
                     T.ERROR_COLUMN_NAME = S.ERROR_COLUMN_NAME,
                     T.ERROR_COUNT = S.ERROR_COUNT    
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME, SCHEMA_NAME, FILE_NAME, LOAD_START_TIME, LOAD_END_TIME, RECORD_COUNT, ERROR_MESSAGE, ERROR_LINE_NUMBER, ERROR_COLUMN_NAME, ERROR_COUNT, STATUS)
                     VALUES(S.TABLE_NAME, S.SCHEMA_NAME, S.FILE_NAME, S.LOAD_START_TIME, S.LOAD_END_TIME, S.RECORD_COUNT, S.ERROR_MESSAGE, S.ERROR_LINE_NUMBER, S.ERROR_COLUMN_NAME, S.ERROR_COUNT, S.STATUS)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;
	
	

CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COPY_HISTORY_2
  warehouse =  NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COPY_HISTORY_1
as
  call ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COPY_HISTORY_PROC();
 
 

ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COPY_HISTORY_1 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COPY_HISTORY_2 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMHEADER_UNNEST_ROOT_COUNT_CHECK_TASK RESUME;   


CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT
(home_pln_cd VARCHAR
,host_pln_cd VARCHAR
,sccf VARCHAR
,clm_ln_num VARCHAR
,perfg_prov_name VARCHAR
,perfg_prov_num VARCHAR
,perfg_prov_npi VARCHAR
,prov_orign_name VARCHAR
,prov_spclty VARCHAR
,prov_typ VARCHAR
,plc_of_svc VARCHAR
,typ_of_svc_cd VARCHAR
,fst_dt_of_svc VARCHAR
,lst_dt_of_svc VARCHAR
,prcdr_cd VARCHAR
,prcdr_modr VARCHAR
,prcdr_modr2 VARCHAR
,prcdr_modr3 VARCHAR
,prcdr_modr4 VARCHAR
,rvnu_cd VARCHAR
,num_of_svc VARCHAR
,ln_icd9_diag_cd_1 VARCHAR
,ln_icd9_diag_cd_2 VARCHAR
,ln_icd9_diag_cd_3 VARCHAR
,ln_icd9_diag_cd_4 VARCHAR
,ln_icd10_diag_cd_1 VARCHAR
,ln_icd10_diag_cd_2 VARCHAR
,ln_icd10_diag_cd_3 VARCHAR
,ln_icd10_diag_cd_4 VARCHAR
,claim_id VARCHAR
,performing_prov_taxonomy_cd VARCHAR
,lineage_input_domain VARCHAR
,lineage_input_url VARCHAR
,lineage_input_line_number VARCHAR 
,lineage_ingestion_id VARCHAR 
,lineage_batch_id VARCHAR 
,correlationid VARCHAR		
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
);
	
	
CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_PIPE auto_ingest=true AS
COPY INTO  ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT 
FROM ( SELECT   
t.$1:home_pln_cd
,t.$1:host_pln_cd
,t.$1:sccf
,t.$1:clm_ln_num
,t.$1:perfg_prov_name
,t.$1:perfg_prov_num
,t.$1:perfg_prov_npi
,t.$1:prov_orign_name
,t.$1:prov_spclty
,t.$1:prov_typ
,t.$1:plc_of_svc
,t.$1:typ_of_svc_cd
,t.$1:fst_dt_of_svc
,t.$1:lst_dt_of_svc
,t.$1:prcdr_cd
,t.$1:prcdr_modr
,t.$1:prcdr_modr2
,t.$1:prcdr_modr3
,t.$1:prcdr_modr4
,t.$1:rvnu_cd
,t.$1:num_of_svc
,t.$1:ln_icd9_diag_cd_1
,t.$1:ln_icd9_diag_cd_2
,t.$1:ln_icd9_diag_cd_3
,t.$1:ln_icd9_diag_cd_4
,t.$1:ln_icd10_diag_cd_1
,t.$1:ln_icd10_diag_cd_2
,t.$1:ln_icd10_diag_cd_3
,t.$1:ln_icd10_diag_cd_4
,t.$1:claim_id
,t.$1:performing_prov_taxonomy_cd
,t.$1:__lineage:input_domain
,t.$1:__lineage:input_url
,t.$1:__lineage:input_line_number
,t.$1:__lineage:ingestion_id
,t.$1:__lineage:batch_id
,t.$1:root_id
,current_timestamp()
,current_timestamp()
FROM
@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/source_data/cambia_bcbsa_claims_claimline_unnest_root/ t)
file_format = ( FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_STREAM_COUNT  ON TABLE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT;


CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 

  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_STREAM_COUNT_TEMP AS SELECT * FROM 
	  ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_STREAM_COUNT
         ;`
} );
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT
         AS          
         SELECT 'CAMBIA-BCBSA-CLAIMS' AS CONNECTOR, 'BCBSA-CLAIM' AS SOURCE, 'CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT' AS TABLE_NAME, lineage_ingestion_id as INGESTIONID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT   
         WHERE lineage_ingestion_id  IN (SELECT DISTINCT lineage_ingestion_id  FROM 
		 ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_STREAM_COUNT_TEMP ) GROUP BY 1, 2, 3, 4
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_INGESTIONID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME    
         AND T.INGESTIONID = S.INGESTIONID
         AND T.CONNECTOR = S.CONNECTOR    
         AND T.SOURCE = S.SOURCE    
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(CONNECTOR,SOURCE, TABLE_NAME,INGESTIONID,RECORD_COUNT)
                     VALUES(S.CONNECTOR,S.SOURCE,S.TABLE_NAME, S.INGESTIONID, S.RECORD_COUNT)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT
         AS          
         SELECT 'CAMBIA-BCBSA-CLAIMS' AS CONNECTOR, 'BCBSA-CLAIM' AS SOURCE, 'CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT' AS TABLE_NAME, lineage_batch_id as BATCH_ID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT     
         WHERE lineage_batch_id IN (SELECT DISTINCT lineage_batch_id FROM 
		 ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_STREAM_COUNT_TEMP ) GROUP BY 1, 2, 3, 4
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_BATCH_ID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME    
         AND T.BATCH_ID = S.BATCH_ID
         AND T.CONNECTOR = S.CONNECTOR        
         AND T.SOURCE = S.SOURCE    
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(CONNECTOR,SOURCE, TABLE_NAME,BATCH_ID,RECORD_COUNT)
                     VALUES(S.CONNECTOR,S.SOURCE,S.TABLE_NAME, S.BATCH_ID, S.RECORD_COUNT)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT_CHECK_TASK
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  schedule = '5 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_STREAM_COUNT')            
as
  call ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT_CHECK_PROC();
 

 
CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COPY_HISTORY_1
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT_CHECK_TASK
as
  CREATE  OR REPLACE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_TEMP_COUNT_COPY_HISOTRY
 AS
 SELECT TABLE_NAME, TABLE_SCHEMA_NAME AS SCHEMA_NAME, SPLIT(FILE_NAME,'/')[4]::String AS FILE_NAME, PIPE_RECEIVED_TIME AS LOAD_START_TIME, LAST_LOAD_TIME AS LOAD_END_TIME,
 ROW_COUNT AS RECORD_COUNT,
 FIRST_ERROR_MESSAGE AS ERROR_MESSAGE, FIRST_ERROR_LINE_NUMBER AS ERROR_LINE_NUMBER, FIRST_ERROR_COLUMN_NAME AS ERROR_COLUMN_NAME, ERROR_COUNT, STATUS
 from table(information_schema.copy_history(table_name=>'ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT', start_time=> dateadd(hours, -24, CURRENT_TIMESTAMP()))) WHERE STATUS NOT IN ('LOAD_IN_PROGRESS');
     
	 

CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COPY_HISTORY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 


  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY AS T
         USING  ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_TEMP_COUNT_COPY_HISOTRY  AS S     
         ON T.TABLE_NAME = S.TABLE_NAME     
         AND T.SCHEMA_NAME = S.SCHEMA_NAME
         AND T.FILE_NAME = S.FILE_NAME
         AND T.STATUS = S.STATUS
         AND T.RECORD_COUNT = S.RECORD_COUNT
         AND T.LOAD_START_TIME = S.LOAD_START_TIME
         AND T.LOAD_END_TIME = S.LOAD_END_TIME
         AND T.ERROR_COUNT = S.ERROR_COUNT
         WHEN matched
              THEN
              UPDATE SET  
                     T.ERROR_MESSAGE = S.ERROR_MESSAGE,
                     T.ERROR_LINE_NUMBER = S.ERROR_LINE_NUMBER,
                     T.ERROR_COLUMN_NAME = S.ERROR_COLUMN_NAME,
                     T.ERROR_COUNT = S.ERROR_COUNT    
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME, SCHEMA_NAME, FILE_NAME, LOAD_START_TIME, LOAD_END_TIME, RECORD_COUNT, ERROR_MESSAGE, ERROR_LINE_NUMBER, ERROR_COLUMN_NAME, ERROR_COUNT, STATUS)
                     VALUES(S.TABLE_NAME, S.SCHEMA_NAME, S.FILE_NAME, S.LOAD_START_TIME, S.LOAD_END_TIME, S.RECORD_COUNT, S.ERROR_MESSAGE, S.ERROR_LINE_NUMBER, S.ERROR_COLUMN_NAME, S.ERROR_COUNT, S.STATUS)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;
	
	

CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COPY_HISTORY_2
  warehouse =  NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COPY_HISTORY_1
as
  call ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COPY_HISTORY_PROC();
 
 

ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COPY_HISTORY_1 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COPY_HISTORY_2 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_CLAIMS_CLAIMLINE_UNNEST_ROOT_COUNT_CHECK_TASK RESUME;   


