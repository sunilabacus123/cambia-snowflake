CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT
(ndw_pln_id VARCHAR
,home_pln_prod_id VARCHAR
,ndw_prod_cat_cd VARCHAR
,mbr_id VARCHAR
,cmi VARCHAR
,mbr_dob VARCHAR
,mbr_gndr VARCHAR
,mbr_confdntlty_cd VARCHAR
,covag_beg_dt VARCHAR
,covag_end_dt VARCHAR
,mbr_relshp VARCHAR
,its_sub_id VARCHAR
,grp_or_indivl_cd VARCHAR
,alph_pfx VARCHAR
,mbr_pfx VARCHAR
,mbr_lst_name VARCHAR
,mbr_fst_name VARCHAR
,mbr_mi VARCHAR
,mbr_sfx VARCHAR
,mbr_primy_str_addr_1 VARCHAR
,mbr_primy_str_addr_2 VARCHAR
,mbr_primy_city VARCHAR
,mbr_primy_st VARCHAR
,mbr_primy_zip_cd VARCHAR
,mbr_primy_zip_cd_4 VARCHAR
,mbr_primy_ph_num VARCHAR
,mbr_primy_email_addr VARCHAR
,mbr_secd_str_addr_1 VARCHAR
,mbr_secd_str_addr_2 VARCHAR
,mbr_secd_city VARCHAR
,mbr_secd_st VARCHAR
,mbr_secd_zip_cd VARCHAR
,mbr_secd_zip_cd_4 VARCHAR
,host_pln_ovrrd VARCHAR
,mbr_parn_ind VARCHAR
,mbr_med_cob_ind VARCHAR
,void_ind VARCHAR
,mmi_id VARCHAR
,host_pln_cd VARCHAR
,home_pln_corp_pln_cd VARCHAR
,host_pln_corp_pln_cd VARCHAR
,pharm_carv_out_submsn_ind VARCHAR
,prior_cmi VARCHAR
,cmi_cycle_change VARCHAR
,prior_mmi VARCHAR
,mmi_cycle_change VARCHAR
,provider_plan_code VARCHAR
,product_id VARCHAR
,provider_number VARCHAR
,provider_number_suffix VARCHAR
,sequence_number VARCHAR
,medicare_beneficiary_id VARCHAR
,medicare_contract_id VARCHAR
,hpn_indicator VARCHAR
,lineage_input_domain VARCHAR
,lineage_input_url VARCHAR
,lineage_input_line_number VARCHAR 
,lineage_ingestion_id VARCHAR 
,lineage_batch_id VARCHAR 
,correlationid VARCHAR		
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
);
	
	
CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_PIPE auto_ingest=true AS
COPY INTO  ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT 
FROM ( SELECT   
t.$1:ndw_pln_id
,t.$1:home_pln_prod_id
,t.$1:ndw_prod_cat_cd
,t.$1:mbr_id
,t.$1:cmi
,t.$1:mbr_dob
,t.$1:mbr_gndr
,t.$1:mbr_confdntlty_cd
,t.$1:covag_beg_dt
,t.$1:covag_end_dt
,t.$1:mbr_relshp
,t.$1:its_sub_id
,t.$1:grp_or_indivl_cd
,t.$1:alph_pfx
,t.$1:mbr_pfx
,t.$1:mbr_lst_name
,t.$1:mbr_fst_name
,t.$1:mbr_mi
,t.$1:mbr_sfx
,t.$1:mbr_primy_str_addr_1
,t.$1:mbr_primy_str_addr_2
,t.$1:mbr_primy_city
,t.$1:mbr_primy_st
,t.$1:mbr_primy_zip_cd
,t.$1:mbr_primy_zip_cd_4
,t.$1:mbr_primy_ph_num
,t.$1:mbr_primy_email_addr
,t.$1:mbr_secd_str_addr_1
,t.$1:mbr_secd_str_addr_2
,t.$1:mbr_secd_city
,t.$1:mbr_secd_st
,t.$1:mbr_secd_zip_cd
,t.$1:mbr_secd_zip_cd_4
,t.$1:host_pln_ovrrd
,t.$1:mbr_parn_ind
,t.$1:mbr_med_cob_ind
,t.$1:void_ind
,t.$1:mmi_id
,t.$1:host_pln_cd
,t.$1:home_pln_corp_pln_cd
,t.$1:host_pln_corp_pln_cd
,t.$1:pharm_carv_out_submsn_ind
,t.$1:prior_cmi
,t.$1:cmi_cycle_change
,t.$1:prior_mmi
,t.$1:mmi_cycle_change
,t.$1:provider_plan_code
,t.$1:product_id
,t.$1:provider_number
,t.$1:provider_number_suffix
,t.$1:sequence_number
,t.$1:medicare_beneficiary_id
,t.$1:medicare_contract_id
,t.$1:hpn_indicator
,t.$1:__lineage:input_domain
,t.$1:__lineage:input_url
,t.$1:__lineage:input_line_number
,t.$1:__lineage:ingestion_id
,t.$1:__lineage:batch_id
,t.$1:root_id
,current_timestamp()
,current_timestamp()
FROM
@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/source_data/cambia_bcbsa_member_member_unnest_root/ t)
file_format = ( FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_STREAM_COUNT  ON TABLE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT;


CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 

  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_STREAM_COUNT_TEMP AS SELECT * FROM 
	  ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_STREAM_COUNT
         ;`
} );
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT
         AS          
         SELECT 'CAMBIA-BCBSA-MEMBER' AS CONNECTOR, 'BCBSA-MEMBER' AS SOURCE, 'CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT' AS TABLE_NAME, lineage_ingestion_id as INGESTIONID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT   
         WHERE lineage_ingestion_id  IN (SELECT DISTINCT lineage_ingestion_id  FROM 
		 ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_STREAM_COUNT_TEMP ) GROUP BY 1, 2, 3, 4
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_INGESTIONID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME    
         AND T.INGESTIONID = S.INGESTIONID
         AND T.CONNECTOR = S.CONNECTOR    
         AND T.SOURCE = S.SOURCE    
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(CONNECTOR,SOURCE, TABLE_NAME,INGESTIONID,RECORD_COUNT)
                     VALUES(S.CONNECTOR,S.SOURCE,S.TABLE_NAME, S.INGESTIONID, S.RECORD_COUNT)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT
         AS          
         SELECT 'CAMBIA-BCBSA-MEMBER' AS CONNECTOR, 'BCBSA-MEMBER' AS SOURCE, 'CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT' AS TABLE_NAME, lineage_batch_id as BATCH_ID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT     
         WHERE lineage_batch_id IN (SELECT DISTINCT lineage_batch_id FROM 
		 ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_STREAM_COUNT_TEMP ) GROUP BY 1, 2, 3, 4
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_BATCH_ID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME    
         AND T.BATCH_ID = S.BATCH_ID
         AND T.CONNECTOR = S.CONNECTOR        
         AND T.SOURCE = S.SOURCE    
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(CONNECTOR,SOURCE, TABLE_NAME,BATCH_ID,RECORD_COUNT)
                     VALUES(S.CONNECTOR,S.SOURCE,S.TABLE_NAME, S.BATCH_ID, S.RECORD_COUNT)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT_CHECK_TASK
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  schedule = '5 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_STREAM_COUNT')            
as
  call ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT_CHECK_PROC();
 

 
CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COPY_HISTORY_1
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT_CHECK_TASK
as
  CREATE  OR REPLACE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_TEMP_COUNT_COPY_HISOTRY
 AS
 SELECT TABLE_NAME, TABLE_SCHEMA_NAME AS SCHEMA_NAME, SPLIT(FILE_NAME,'/')[4]::String AS FILE_NAME, PIPE_RECEIVED_TIME AS LOAD_START_TIME, LAST_LOAD_TIME AS LOAD_END_TIME,
 ROW_COUNT AS RECORD_COUNT,
 FIRST_ERROR_MESSAGE AS ERROR_MESSAGE, FIRST_ERROR_LINE_NUMBER AS ERROR_LINE_NUMBER, FIRST_ERROR_COLUMN_NAME AS ERROR_COLUMN_NAME, ERROR_COUNT, STATUS
 from table(information_schema.copy_history(table_name=>'ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT', start_time=> dateadd(hours, -24, CURRENT_TIMESTAMP()))) WHERE STATUS NOT IN ('LOAD_IN_PROGRESS');
     
	 

CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COPY_HISTORY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 


  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY AS T
         USING  ABACUS_NONPRD_FND_DB.TRANSIENT.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_TEMP_COUNT_COPY_HISOTRY  AS S     
         ON T.TABLE_NAME = S.TABLE_NAME     
         AND T.SCHEMA_NAME = S.SCHEMA_NAME
         AND T.FILE_NAME = S.FILE_NAME
         AND T.STATUS = S.STATUS
         AND T.RECORD_COUNT = S.RECORD_COUNT
         AND T.LOAD_START_TIME = S.LOAD_START_TIME
         AND T.LOAD_END_TIME = S.LOAD_END_TIME
         AND T.ERROR_COUNT = S.ERROR_COUNT
         WHEN matched
              THEN
              UPDATE SET  
                     T.ERROR_MESSAGE = S.ERROR_MESSAGE,
                     T.ERROR_LINE_NUMBER = S.ERROR_LINE_NUMBER,
                     T.ERROR_COLUMN_NAME = S.ERROR_COLUMN_NAME,
                     T.ERROR_COUNT = S.ERROR_COUNT    
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME, SCHEMA_NAME, FILE_NAME, LOAD_START_TIME, LOAD_END_TIME, RECORD_COUNT, ERROR_MESSAGE, ERROR_LINE_NUMBER, ERROR_COLUMN_NAME, ERROR_COUNT, STATUS)
                     VALUES(S.TABLE_NAME, S.SCHEMA_NAME, S.FILE_NAME, S.LOAD_START_TIME, S.LOAD_END_TIME, S.RECORD_COUNT, S.ERROR_MESSAGE, S.ERROR_LINE_NUMBER, S.ERROR_COLUMN_NAME, S.ERROR_COUNT, S.STATUS)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;
	
	

CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COPY_HISTORY_2
  warehouse =  NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COPY_HISTORY_1
as
  call ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COPY_HISTORY_PROC();
 
 

ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COPY_HISTORY_1 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COPY_HISTORY_2 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.SOURCE_DATA.CAMBIA_BCBSA_MEMBER_MEMBER_UNNEST_ROOT_COUNT_CHECK_TASK RESUME;   


