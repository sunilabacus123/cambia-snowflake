
CREATE TABLE ABACUS_NONPRD_FND_DB.CONTROL.RECON_CDC_METADATA
(
cntrl_id NUMBER,
extract_from_dttm TIMESTAMP,
extract_to_dttm TIMESTAMP,
prcs_view_name VARCHAR,
src_db_name VARCHAR,
load_type VARCHAR,
environment VARCHAR,
file_name VARCHAR,
db_object_database VARCHAR,
db_object_schema VARCHAR,
db_object_name VARCHAR,
prcs_key_identifier VARCHAR,
file_records NUMBER,
LOAD_DATE TIMESTAMP
)
;

CREATE TABLE ABACUS_NONPRD_FND_DB.CONTROL.RECON_LAKE_SOURCE_METADATA
(
TABLE_NAME VARCHAR,
BATCH_ID NUMBER,
INGESTION_ID VARCHAR,
RECORD_COUNT VARCHAR,
LOAD_DATE TIMESTAMP
);


CREATE TABLE ABACUS_NONPRD_FND_DB.CONTROL.RECON_LAKE_DOMAIN_METADATA
(
TABLE_NAME VARCHAR,
BATCH_ID NUMBER,
INGESTION_ID VARCHAR,
UNIQUE_SOURCE_ID_COUNT VARCHAR,
LOAD_DATE TIMESTAMP
);


CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.RECON_CDC_METADATA_PIPE auto_ingest=true AS
COPY INTO ABACUS_NONPRD_FND_DB.CONTROL.RECON_CDC_METADATA
FROM (
SELECT 
t.$1:cntrl_id,
t.$1:extract_from_dttm,
t.$1:extract_to_dttm,
t.$1:prcs_view_name,
t.$1:src_db_name,
t.$1:load_type,
t.$1:environment,
t.$1:file_name,
t.$1:db_object_database,
t.$1:db_object_schema,
t.$1:db_object_name,
t.$1:prcs_key_identifier,
t.$1:file_records,
current_timestamp()
from @ABACUS_NONPRD_FND_DB.landing.non_prod_abacus_stg/data/sfreports/source/ t)
file_format = (FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.LAKE_REPORTS auto_ingest=true AS
COPY INTO ABACUS_NONPRD_FND_DB.CONTROL.RECON_LAKE_SOURCE_METADATA
FROM (
SELECT 
t.$1:table_name,
t.$1:batch_id,
t.$1:ingestion_id,
t.$1:record,
current_timestamp()
from @ABACUS_NONPRD_FND_DB.landing.non_prod_abacus_stg/data/sfreports/lake/ t)
file_format = (FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');



CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.LAKE_REPORTS auto_ingest=true AS
COPY INTO ABACUS_NONPRD_FND_DB.CONTROL.RECON_LAKE_DOMAIN_METADATA
FROM (
SELECT 
t.$1:table_name,
t.$1:batch_id,
t.$1:ingestion_id,
t.$1:unique_source_id_count,
current_timestamp()
from @ABACUS_NONPRD_FND_DB.landing.non_prod_abacus_stg/data/sfreports/lake_domain/ t)
file_format = (FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE VIEW ABACUS_NONPRD_FND_DB.CONTROL.RECON_CDC_LAKE_SF_SOURCE_METADATA AS
SELECT C.TABLE_NAME, C.BATCH_ID, A.INGESTION_ID , C.SOURCE_RECORD_COUNT AS CDC_RECORD_COUNT, A.RECORD_COUNT AS LAKE_RECORD_COUNT, B.RECORD_COUNT AS SF_RECORD_COUNT, 
CASE WHEN C.SOURCE_RECORD_COUNT = A.RECORD_COUNT AND C.SOURCE_RECORD_COUNT  = B.RECORD_COUNT  THEN 'SUCCESS'
ELSE 'IN-PROGRESS'
END AS RECON_STATUS,
C.extract_from_dttm, C.extract_to_dttm, C.prcs_view_name, C.src_db_name, C.load_type, C.environment, C.db_object_database, C.db_object_schema, C.db_object_name, C.prcs_key_identifier, B.CONNECTOR, B.SOURCE FROM
(SELECT 'FACETS_UM_'||FILE_NAME||'_UNNEST_ROOT' AS TABLE_NAME, CNTRL_ID AS BATCH_ID, 
 extract_from_dttm, extract_to_dttm, prcs_view_name, src_db_name, load_type, environment, db_object_database, db_object_schema, db_object_name, prcs_key_identifier, file_records AS SOURCE_RECORD_COUNT,
LOAD_DATE FROM  ABACUS_NONPRD_FND_DB.CONTROL.RECON_CDC_METADATA) C
LEFT JOIN
(SELECT * FROM  ABACUS_NONPRD_FND_DB.CONTROL.RECON_LAKE_SOURCE_METADATA) A
ON C.TABLE_NAME = UPPER(A.TABLE_NAME)
AND TO_DATE(C.LOAD_DATE) = TO_DATE(A.LOAD_DATE)
AND C.BATCH_ID = A.BATCH_ID 
LEFT JOIN
(select * from control.source_data_record_count_ingestionid) B
ON A.INGESTION_ID = B.INGESTIONID
AND UPPER(A.TABLE_NAME) = B.TABLE_NAME;



CREATE OR REPLACE VIEW ABACUS_NONPRD_FND_DB.CONTROL.RECON_CDC_LAKE_SF_DOMAIN_METADATA AS
SELECT C.TABLE_NAME, C.BATCH_ID, A.INGESTION_ID , C.SOURCE_RECORD_COUNT,  A.UNIQUE_SOURCE_ID_COUNT AS LAKE_RECORD_COUNT, B.RECORD_COUNT AS SF_RECORD_COUNT,
CASE WHEN C.SOURCE_RECORD_COUNT = A.UNIQUE_SOURCE_ID_COUNT AND C.SOURCE_RECORD_COUNT  = B.RECORD_COUNT  THEN 'SUCCESS'
ELSE 'IN-PROGRESS'
END AS RECON_STATUS,
C.extract_from_dttm, C.extract_to_dttm, C.prcs_view_name, C.src_db_name, C.load_type, C.environment, C.db_object_database, C.db_object_schema, C.db_object_name, C.prcs_key_identifier, B.CONNECTOR, B.SOURCE, C.LOAD_DATE FROM
(SELECT 'FACETS_UM_'||FILE_NAME||'_UNNEST_ROOT' AS TABLE_NAME, CNTRL_ID AS BATCH_ID, 
 extract_from_dttm, extract_to_dttm, prcs_view_name, src_db_name, load_type, environment, db_object_database, db_object_schema, db_object_name, prcs_key_identifier, file_records AS SOURCE_RECORD_COUNT,
LOAD_DATE FROM  ABACUS_NONPRD_FND_DB.CONTROL.RECON_CDC_METADATA) C
LEFT JOIN
(SELECT * FROM  ABACUS_NONPRD_FND_DB.CONTROL.RECON_LAKE_DOMAIN_METADATA) A
ON C.TABLE_NAME = UPPER(A.TABLE_NAME)
AND TO_DATE(C.LOAD_DATE) = TO_DATE(A.LOAD_DATE)
AND C.BATCH_ID = A.BATCH_ID 
LEFT JOIN
(select * from control.source_data_record_count_ingestionid) B
ON A.INGESTION_ID = B.INGESTIONID
AND UPPER(A.TABLE_NAME) = B.TABLE_NAME;