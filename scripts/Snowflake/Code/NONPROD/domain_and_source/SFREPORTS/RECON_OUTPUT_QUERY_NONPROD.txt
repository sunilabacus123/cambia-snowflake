
-- FACETS  MEMBER
SELECT C.TABLE_NAME, C.BATCH_ID, A.INGESTION_ID , C.SOURCE_RECORD_COUNT AS CDC_RECORD_COUNT, A.RECORD_COUNT AS LAKE_RECORD_COUNT, B.RECORD_COUNT AS SF_RECORD_COUNT, 
CASE WHEN C.SOURCE_RECORD_COUNT = A.RECORD_COUNT AND C.SOURCE_RECORD_COUNT  = B.RECORD_COUNT  THEN 'SUCCESS'
ELSE 'IN-PROGRESS'
END AS RECON_LAKE_STATUS,
C.extract_from_dttm, C.extract_to_dttm, C.rec_cr_dttm, C.data_return_start_dttm, C.data_return_end_dttm, C.prcs_fr_dttm, C.prcs_end_dttm, C.prcs_view_name, C.src_db_name, C.load_type, C.environment, C.db_object_database, C.db_object_schema, C.db_object_name, C.prcs_key_identifier, C.btch_exec_id, C.extract_part_key, C.job_attempt,  C.job_status, C.btch_partition_num,  C.base_object_flag, C.full_table_flag, C.file_partition_records, C.keyid_records, C.auto_prcs_conclusion, C.row_filter_flag, C.batch_partition_flag, C.last_partition_flag, C.sftp_flag, C.dynamic_join_w_uk_lst_flag,  B.CONNECTOR, B.SOURCE FROM
(SELECT 'FACETS_MEMBER_V2_'||FILE_NAME||'_UNNEST_ROOT' AS TABLE_NAME, CNTRL_ID AS BATCH_ID, 
 extract_from_dttm, extract_to_dttm, prcs_view_name, src_db_name, load_type, environment, db_object_database, db_object_schema, db_object_name, prcs_key_identifier, file_records AS SOURCE_RECORD_COUNT,
LOAD_DATE, btch_exec_id, extract_part_key, job_attempt, prcs_fr_dttm, prcs_end_dttm, job_status, btch_partition_num, data_return_start_dttm, data_return_end_dttm, base_object_flag, full_table_flag, file_partition_records, keyid_records, auto_prcs_conclusion, row_filter_flag, batch_partition_flag, last_partition_flag, sftp_flag, dynamic_join_w_uk_lst_flag, rec_cr_dttm FROM  ABACUS_NONPRD_FND_DB.CONTROL.RECON_CDC_METADATA
WHERE SRC_DB_NAME = 'FACETS'
 AND PRCS_VIEW_NAME = 'MEMBER'
) C
LEFT JOIN
(SELECT * FROM  ABACUS_NONPRD_FND_DB.CONTROL.RECON_LAKE_SOURCE_METADATA WHERE TABLE_NAME LIKE 'facets_member_v2%') A
ON C.TABLE_NAME = UPPER(A.TABLE_NAME)
AND TO_DATE(C.LOAD_DATE) = TO_DATE(A.LOAD_DATE)
AND C.BATCH_ID = A.BATCH_ID 
LEFT JOIN
(select * from ABACUS_NONPRD_FND_DB.control.source_data_record_count_ingestionid where connector = 'FACETS-MEMBER-V2') B
ON A.INGESTION_ID = B.INGESTIONID
AND UPPER(A.TABLE_NAME) = B.TABLE_NAME
ORDER BY BATCH_ID, INGESTION_ID, TABLE_NAME;




SELECT A.INGESTION_ID, D.BATCHID, B.TABLE_NAME,  A.UNIQUE_SOURCE_ID_COUNT AS LAKE_RECORD_COUNT,
B.RECORD_COUNT AS SNOWFLAKE_RECORD_COUNT,
C.RECORD_COUNT AS SNOWFLAKE_MANIFEST_COUNT,
CASE WHEN A.UNIQUE_SOURCE_ID_COUNT = B.RECORD_COUNT AND B.RECORD_COUNT = C.RECORD_COUNT THEN 'SUCESS'
ELSE 'IN-PROGRESS'
END AS RECON_DOMAIN_STATUS
FROM 
(SELECT * FROM ABACUS_NONPRD_FND_DB.control.RECON_LAKE_DOMAIN_METADATA) A
LEFT JOIN
(SELECT * FROM ABACUS_NONPRD_FND_DB.control.LANDING_RECORD_COUNT_INGESTIONID) B
ON UPPER(A.TABLE_NAME) = B.TABLE_NAME
AND A.INGESTION_ID = B.INGESTIONID
LEFT JOIN
(
SELECT UPPER(OBJ_NAME) AS TABLE_NAME, INGESTION_ID, SUM(RECORD_COUNT) as RECORD_COUNT FROM ABACUS_NONPRD_FND_DB.control.MANIFEST_LANDING GROUP BY 1, 2
) C
ON A.INGESTION_ID = C.INGESTION_ID
AND UPPER(A.TABLE_NAME) = C.TABLE_NAME
AND B.TABLE_NAME = C.TABLE_NAME
LEFT JOIN
(
SELECT DISTINCT BATCHID, INGESTIONID FROM ABACUS_NONPRD_FND_DB.LANDING.MEMBER
) D
ON A.INGESTION_ID = D.INGESTIONID
AND C.INGESTION_ID = D.INGESTIONID;




