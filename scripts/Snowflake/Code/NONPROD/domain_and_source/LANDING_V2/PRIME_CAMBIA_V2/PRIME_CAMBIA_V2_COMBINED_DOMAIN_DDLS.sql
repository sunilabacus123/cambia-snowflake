CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG
(
            CorrelationID string
            ,Source string
            ,SourceID string
            ,ParentSourceID string
            ,RelationshipName string
            ,SchemaVersion string
            ,SourceTimestamp datetime
            ,ConsumeTimestamp datetime
            ,HashID string
            ,ParentType string
            ,MajorTherapeuticClass string
            ,TherapeuticClassQualifier string
            ,DrugMaxQuantity number
            ,DrugAllowedQuantity number
            ,DrugStrength string
            ,DrugUOM string
            ,DrugType string
            ,DrugName string
            ,DrugShortName string
            ,DrugManufacturerID string
            ,DrugManufacturerName string
            ,DrugDescriptorID string
            ,GenericDrugName string
            ,GPINumber string
            ,GenericProductID string
            ,GenericQualifier string
            ,SpecialtyRxQualifier string
            ,SpecialtyProgramID string
            ,SpecialtySchedule string
            ,GenericCodeOverride string
            ,GenericDrugInd boolean
            ,SpecialtyPharmacyInd boolean
            ,MailOrderPharmacyInd boolean
            ,DrugCostReductionInd boolean
            ,CompoundDrugInd boolean
            ,RefillNumber number
            ,FirstFillInd boolean
            ,ChargeAmount number
            ,QuantityPrescribed number
            ,QuantityDispensed number
            ,AuthorizedRefills number
            ,UnitType string
            ,UnitPrice number
            ,ItemCost number
            ,GenericCodeNumber string
            ,CostType string
            ,CostTypeValue string
            ,PriceType string
            ,PriceTypeValue string
            ,BasisOfDeterminationType string
            ,BasisOfDeterminationValue string
            ,NumberOfDaysSupplied number
            ,FormularyType string
            ,FormularyStatus string
            ,ProductID string
            ,ProductIDType string
            ,ProductIDQualifier string
            ,ProductUOM string
            ,ProductPackageAmount string
            ,ProductKey string
            ,DrugGroup string
            ,DrugLabelerCode string
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
,INGESTIONID varchar,
CONSTRAINT  CLAIMLINEDRUG_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);
	
	
CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_PIPE auto_ingest=true AS
COPY INTO  ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG 
FROM ( SELECT
            t.$1:correlationid
            ,t.$1:source
            ,t.$1:sourceid
            ,t.$1:parentsourceid
            ,t.$1:relationshipname
            ,t.$1:schemaversion
            ,t.$1:sourcetimestamp
            ,t.$1:consumetimestamp
            ,t.$1:hashid
            ,t.$1:parenttype
            ,t.$1:majortherapeuticclass
            ,t.$1:therapeuticclassqualifier
            ,t.$1:drugmaxquantity
            ,t.$1:drugallowedquantity
            ,t.$1:drugstrength
            ,t.$1:druguom
            ,t.$1:drugtype
            ,t.$1:drugname
            ,t.$1:drugshortname
            ,t.$1:drugmanufacturerid
            ,t.$1:drugmanufacturername
            ,t.$1:drugdescriptorid
            ,t.$1:genericdrugname
            ,t.$1:gpinumber
            ,t.$1:genericproductid
            ,t.$1:genericqualifier
            ,t.$1:specialtyrxqualifier
            ,t.$1:specialtyprogramid
            ,t.$1:specialtyschedule
            ,t.$1:genericcodeoverride
            ,t.$1:genericdrugind
            ,t.$1:specialtypharmacyind
            ,t.$1:mailorderpharmacyind
            ,t.$1:drugcostreductionind
            ,t.$1:compounddrugind
            ,t.$1:refillnumber
            ,t.$1:firstfillind
            ,t.$1:chargeamount
            ,t.$1:quantityprescribed
            ,t.$1:quantitydispensed
            ,t.$1:authorizedrefills
            ,t.$1:unittype
            ,t.$1:unitprice
            ,t.$1:itemcost
            ,t.$1:genericcodenumber
            ,t.$1:costtype
            ,t.$1:costtypevalue
            ,t.$1:pricetype
            ,t.$1:pricetypevalue
            ,t.$1:basisofdeterminationtype
            ,t.$1:basisofdeterminationvalue
            ,t.$1:numberofdayssupplied
            ,t.$1:formularytype
            ,t.$1:formularystatus
            ,t.$1:productid
            ,t.$1:productidtype
            ,t.$1:productidqualifier
            ,t.$1:productuom
            ,t.$1:productpackageamount
            ,t.$1:productkey
            ,t.$1:druggroup
            ,t.$1:druglabelercode
,current_timestamp()
,current_timestamp()
,t.$1:partition_3
FROM
@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/ClaimLineDrug/ t)
file_format = ( FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_STREAM_COUNT  ON TABLE ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG;


CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.CLAIMLINEDRUG_TEMP_COUNT
         AS
         SELECT UPPER(SOURCE) AS SOURCE, 'CLAIMLINEDRUG' AS TABLE_NAME, INGESTIONID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG 
         WHERE INGESTIONID IN (SELECT DISTINCT INGESTIONID FROM ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_STREAM_COUNT ) GROUP BY 1, 2, 3
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.CLAIMLINEDRUG_TEMP_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME
         AND T.INGESTIONID = S.INGESTIONID
         AND T.SOURCE = S.SOURCE
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(SOURCE, TABLE_NAME,INGESTIONID,RECORD_COUNT)
                     VALUES(S.SOURCE,S.TABLE_NAME, S.INGESTIONID, S.RECORD_COUNT)
         ;`
} );
 
  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COUNT_CHECK_TASK
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  schedule = '1 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_STREAM_COUNT')            
as
  call ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COUNT_CHECK_PROC();
 

 
CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COPY_HISTORY_1
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COUNT_CHECK_TASK
as
  CREATE  OR REPLACE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.CLAIMLINEDRUG_TEMP_COUNT_COPY_HISOTRY
 AS
 SELECT TABLE_NAME, TABLE_SCHEMA_NAME AS SCHEMA_NAME, SPLIT(FILE_NAME,'/')[4]::String AS FILE_NAME, PIPE_RECEIVED_TIME AS LOAD_START_TIME, LAST_LOAD_TIME AS LOAD_END_TIME,
 ROW_COUNT AS RECORD_COUNT,
 FIRST_ERROR_MESSAGE AS ERROR_MESSAGE, FIRST_ERROR_LINE_NUMBER AS ERROR_LINE_NUMBER, FIRST_ERROR_COLUMN_NAME AS ERROR_COLUMN_NAME, ERROR_COUNT, STATUS
 from table(information_schema.copy_history(table_name=>'ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG', start_time=> dateadd(hours, -24, CURRENT_TIMESTAMP()))) WHERE STATUS NOT IN ('LOAD_IN_PROGRESS');
     
	 

CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COPY_HISTORY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 


  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY AS T
         USING  ABACUS_NONPRD_FND_DB.TRANSIENT.CLAIMLINEDRUG_TEMP_COUNT_COPY_HISOTRY  AS S     
         ON T.TABLE_NAME = S.TABLE_NAME     
         AND T.SCHEMA_NAME = S.SCHEMA_NAME
         AND T.FILE_NAME = S.FILE_NAME
         AND T.STATUS = S.STATUS
         AND T.RECORD_COUNT = S.RECORD_COUNT
         AND T.LOAD_START_TIME = S.LOAD_START_TIME
         AND T.LOAD_END_TIME = S.LOAD_END_TIME
         AND T.ERROR_COUNT = S.ERROR_COUNT
         WHEN matched
              THEN
              UPDATE SET  
                     T.ERROR_MESSAGE = S.ERROR_MESSAGE,
                     T.ERROR_LINE_NUMBER = S.ERROR_LINE_NUMBER,
                     T.ERROR_COLUMN_NAME = S.ERROR_COLUMN_NAME,
                     T.ERROR_COUNT = S.ERROR_COUNT    
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME, SCHEMA_NAME, FILE_NAME, LOAD_START_TIME, LOAD_END_TIME, RECORD_COUNT, ERROR_MESSAGE, ERROR_LINE_NUMBER, ERROR_COLUMN_NAME, ERROR_COUNT, STATUS)
                     VALUES(S.TABLE_NAME, S.SCHEMA_NAME, S.FILE_NAME, S.LOAD_START_TIME, S.LOAD_END_TIME, S.RECORD_COUNT, S.ERROR_MESSAGE, S.ERROR_LINE_NUMBER, S.ERROR_COLUMN_NAME, S.ERROR_COUNT, S.STATUS)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;
	
	

CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COPY_HISTORY_2
  warehouse =  NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COPY_HISTORY_1
as
  call ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COPY_HISTORY_PROC();
 
 

ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COPY_HISTORY_1 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COPY_HISTORY_2 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.CLAIMLINEDRUG_COUNT_CHECK_TASK RESUME;    
CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG
(
            CorrelationID string
            ,Source string
            ,SourceID string
            ,ParentSourceID string
            ,RelationshipName string
            ,SchemaVersion string
            ,SourceTimestamp datetime
            ,ConsumeTimestamp datetime
            ,HashID string
            ,ParentType string
            ,SequenceNumber string
            ,IngredientStatus string
            ,IngredientQuantity number
            ,IngredientBiologicsLicenseApplicationInd boolean
            ,IngredientNewDrugApplicationOverrideInd boolean
            ,IngredientBiologicsLicenseApplicationOverrideInd boolean
            ,CostType string
            ,CostTypeDesc string
            ,RateType string
            ,RateCost number
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
,INGESTIONID varchar,
CONSTRAINT  COMPOUNDDRUG_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);
	
	
CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_PIPE auto_ingest=true AS
COPY INTO  ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG 
FROM ( SELECT
            t.$1:correlationid
            ,t.$1:source
            ,t.$1:sourceid
            ,t.$1:parentsourceid
            ,t.$1:relationshipname
            ,t.$1:schemaversion
            ,t.$1:sourcetimestamp
            ,t.$1:consumetimestamp
            ,t.$1:hashid
            ,t.$1:parenttype
            ,t.$1:sequencenumber
            ,t.$1:ingredientstatus
            ,t.$1:ingredientquantity
            ,t.$1:ingredientbiologicslicenseapplicationind
            ,t.$1:ingredientnewdrugapplicationoverrideind
            ,t.$1:ingredientbiologicslicenseapplicationoverrideind
            ,t.$1:costtype
            ,t.$1:costtypedesc
            ,t.$1:ratetype
            ,t.$1:ratecost
,current_timestamp()
,current_timestamp()
,t.$1:partition_3
FROM
@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/CompoundDrug/ t)
file_format = ( FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_STREAM_COUNT  ON TABLE ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG;


CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.COMPOUNDDRUG_TEMP_COUNT
         AS
         SELECT UPPER(SOURCE) AS SOURCE, 'COMPOUNDDRUG' AS TABLE_NAME, INGESTIONID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG 
         WHERE INGESTIONID IN (SELECT DISTINCT INGESTIONID FROM ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_STREAM_COUNT ) GROUP BY 1, 2, 3
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.COMPOUNDDRUG_TEMP_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME
         AND T.INGESTIONID = S.INGESTIONID
         AND T.SOURCE = S.SOURCE
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(SOURCE, TABLE_NAME,INGESTIONID,RECORD_COUNT)
                     VALUES(S.SOURCE,S.TABLE_NAME, S.INGESTIONID, S.RECORD_COUNT)
         ;`
} );
 
  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COUNT_CHECK_TASK
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  schedule = '1 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_STREAM_COUNT')            
as
  call ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COUNT_CHECK_PROC();
 

 
CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COPY_HISTORY_1
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COUNT_CHECK_TASK
as
  CREATE  OR REPLACE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.COMPOUNDDRUG_TEMP_COUNT_COPY_HISOTRY
 AS
 SELECT TABLE_NAME, TABLE_SCHEMA_NAME AS SCHEMA_NAME, SPLIT(FILE_NAME,'/')[4]::String AS FILE_NAME, PIPE_RECEIVED_TIME AS LOAD_START_TIME, LAST_LOAD_TIME AS LOAD_END_TIME,
 ROW_COUNT AS RECORD_COUNT,
 FIRST_ERROR_MESSAGE AS ERROR_MESSAGE, FIRST_ERROR_LINE_NUMBER AS ERROR_LINE_NUMBER, FIRST_ERROR_COLUMN_NAME AS ERROR_COLUMN_NAME, ERROR_COUNT, STATUS
 from table(information_schema.copy_history(table_name=>'ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG', start_time=> dateadd(hours, -24, CURRENT_TIMESTAMP()))) WHERE STATUS NOT IN ('LOAD_IN_PROGRESS');
     
	 

CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COPY_HISTORY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 


  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY AS T
         USING  ABACUS_NONPRD_FND_DB.TRANSIENT.COMPOUNDDRUG_TEMP_COUNT_COPY_HISOTRY  AS S     
         ON T.TABLE_NAME = S.TABLE_NAME     
         AND T.SCHEMA_NAME = S.SCHEMA_NAME
         AND T.FILE_NAME = S.FILE_NAME
         AND T.STATUS = S.STATUS
         AND T.RECORD_COUNT = S.RECORD_COUNT
         AND T.LOAD_START_TIME = S.LOAD_START_TIME
         AND T.LOAD_END_TIME = S.LOAD_END_TIME
         AND T.ERROR_COUNT = S.ERROR_COUNT
         WHEN matched
              THEN
              UPDATE SET  
                     T.ERROR_MESSAGE = S.ERROR_MESSAGE,
                     T.ERROR_LINE_NUMBER = S.ERROR_LINE_NUMBER,
                     T.ERROR_COLUMN_NAME = S.ERROR_COLUMN_NAME,
                     T.ERROR_COUNT = S.ERROR_COUNT    
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME, SCHEMA_NAME, FILE_NAME, LOAD_START_TIME, LOAD_END_TIME, RECORD_COUNT, ERROR_MESSAGE, ERROR_LINE_NUMBER, ERROR_COLUMN_NAME, ERROR_COUNT, STATUS)
                     VALUES(S.TABLE_NAME, S.SCHEMA_NAME, S.FILE_NAME, S.LOAD_START_TIME, S.LOAD_END_TIME, S.RECORD_COUNT, S.ERROR_MESSAGE, S.ERROR_LINE_NUMBER, S.ERROR_COLUMN_NAME, S.ERROR_COUNT, S.STATUS)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;
	
	

CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COPY_HISTORY_2
  warehouse =  NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COPY_HISTORY_1
as
  call ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COPY_HISTORY_PROC();
 
 

ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COPY_HISTORY_1 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COPY_HISTORY_2 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.COMPOUNDDRUG_COUNT_CHECK_TASK RESUME;    
CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK
(
            CorrelationID string
            ,Source string
            ,SourceID string
            ,ParentSourceID string
            ,RelationshipName string
            ,SchemaVersion string
            ,SourceTimestamp datetime
            ,ConsumeTimestamp datetime
            ,HashID string
            ,ParentType string
            ,SuperPharmacyNetworkID string
            ,NetworkID string
            ,Name string
            ,CarrierID string
            ,CarrierIDOverride string
            ,Region string
            ,Type string
            ,PriorityCode string
            ,NDCList string
            ,GPIList string
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
,INGESTIONID varchar,
CONSTRAINT  PHARMACYNETWORK_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);
	
	
CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_PIPE auto_ingest=true AS
COPY INTO  ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK 
FROM ( SELECT
            t.$1:correlationid
            ,t.$1:source
            ,t.$1:sourceid
            ,t.$1:parentsourceid
            ,t.$1:relationshipname
            ,t.$1:schemaversion
            ,t.$1:sourcetimestamp
            ,t.$1:consumetimestamp
            ,t.$1:hashid
            ,t.$1:parenttype
            ,t.$1:superpharmacynetworkid
            ,t.$1:networkid
            ,t.$1:name
            ,t.$1:carrierid
            ,t.$1:carrieridoverride
            ,t.$1:region
            ,t.$1:type
            ,t.$1:prioritycode
            ,t.$1:ndclist
            ,t.$1:gpilist
,current_timestamp()
,current_timestamp()
,t.$1:partition_3
FROM
@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/PharmacyNetwork/ t)
file_format = ( FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_STREAM_COUNT  ON TABLE ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK;


CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.PHARMACYNETWORK_TEMP_COUNT
         AS
         SELECT UPPER(SOURCE) AS SOURCE, 'PHARMACYNETWORK' AS TABLE_NAME, INGESTIONID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK 
         WHERE INGESTIONID IN (SELECT DISTINCT INGESTIONID FROM ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_STREAM_COUNT ) GROUP BY 1, 2, 3
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.PHARMACYNETWORK_TEMP_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME
         AND T.INGESTIONID = S.INGESTIONID
         AND T.SOURCE = S.SOURCE
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(SOURCE, TABLE_NAME,INGESTIONID,RECORD_COUNT)
                     VALUES(S.SOURCE,S.TABLE_NAME, S.INGESTIONID, S.RECORD_COUNT)
         ;`
} );
 
  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COUNT_CHECK_TASK
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  schedule = '1 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_STREAM_COUNT')            
as
  call ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COUNT_CHECK_PROC();
 

 
CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COPY_HISTORY_1
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COUNT_CHECK_TASK
as
  CREATE  OR REPLACE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.PHARMACYNETWORK_TEMP_COUNT_COPY_HISOTRY
 AS
 SELECT TABLE_NAME, TABLE_SCHEMA_NAME AS SCHEMA_NAME, SPLIT(FILE_NAME,'/')[4]::String AS FILE_NAME, PIPE_RECEIVED_TIME AS LOAD_START_TIME, LAST_LOAD_TIME AS LOAD_END_TIME,
 ROW_COUNT AS RECORD_COUNT,
 FIRST_ERROR_MESSAGE AS ERROR_MESSAGE, FIRST_ERROR_LINE_NUMBER AS ERROR_LINE_NUMBER, FIRST_ERROR_COLUMN_NAME AS ERROR_COLUMN_NAME, ERROR_COUNT, STATUS
 from table(information_schema.copy_history(table_name=>'ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK', start_time=> dateadd(hours, -24, CURRENT_TIMESTAMP()))) WHERE STATUS NOT IN ('LOAD_IN_PROGRESS');
     
	 

CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COPY_HISTORY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 


  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY AS T
         USING  ABACUS_NONPRD_FND_DB.TRANSIENT.PHARMACYNETWORK_TEMP_COUNT_COPY_HISOTRY  AS S     
         ON T.TABLE_NAME = S.TABLE_NAME     
         AND T.SCHEMA_NAME = S.SCHEMA_NAME
         AND T.FILE_NAME = S.FILE_NAME
         AND T.STATUS = S.STATUS
         AND T.RECORD_COUNT = S.RECORD_COUNT
         AND T.LOAD_START_TIME = S.LOAD_START_TIME
         AND T.LOAD_END_TIME = S.LOAD_END_TIME
         AND T.ERROR_COUNT = S.ERROR_COUNT
         WHEN matched
              THEN
              UPDATE SET  
                     T.ERROR_MESSAGE = S.ERROR_MESSAGE,
                     T.ERROR_LINE_NUMBER = S.ERROR_LINE_NUMBER,
                     T.ERROR_COLUMN_NAME = S.ERROR_COLUMN_NAME,
                     T.ERROR_COUNT = S.ERROR_COUNT    
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME, SCHEMA_NAME, FILE_NAME, LOAD_START_TIME, LOAD_END_TIME, RECORD_COUNT, ERROR_MESSAGE, ERROR_LINE_NUMBER, ERROR_COLUMN_NAME, ERROR_COUNT, STATUS)
                     VALUES(S.TABLE_NAME, S.SCHEMA_NAME, S.FILE_NAME, S.LOAD_START_TIME, S.LOAD_END_TIME, S.RECORD_COUNT, S.ERROR_MESSAGE, S.ERROR_LINE_NUMBER, S.ERROR_COLUMN_NAME, S.ERROR_COUNT, S.STATUS)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;
	
	

CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COPY_HISTORY_2
  warehouse =  NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COPY_HISTORY_1
as
  call ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COPY_HISTORY_PROC();
 
 

ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COPY_HISTORY_1 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COPY_HISTORY_2 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACYNETWORK_COUNT_CHECK_TASK RESUME;    
CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.LANDING.PHARMACY
(
            CorrelationID string
            ,Source string
            ,SourceID string
            ,ParentSourceID string
            ,RelationshipName string
            ,SchemaVersion string
            ,SourceTimestamp datetime
            ,ConsumeTimestamp datetime
            ,HashID string
            ,ParentType string
            ,PriceScheduleType string
            ,DispenserClass string
            ,DispenserType string
            ,TIN string
            ,NPI string
            ,NCPDPID string
            ,Type string
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
,INGESTIONID varchar,
CONSTRAINT  PHARMACY_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);
	
	
CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_PIPE auto_ingest=true AS
COPY INTO  ABACUS_NONPRD_FND_DB.LANDING.PHARMACY 
FROM ( SELECT
            t.$1:correlationid
            ,t.$1:source
            ,t.$1:sourceid
            ,t.$1:parentsourceid
            ,t.$1:relationshipname
            ,t.$1:schemaversion
            ,t.$1:sourcetimestamp
            ,t.$1:consumetimestamp
            ,t.$1:hashid
            ,t.$1:parenttype
            ,t.$1:pricescheduletype
            ,t.$1:dispenserclass
            ,t.$1:dispensertype
            ,t.$1:tin
            ,t.$1:npi
            ,t.$1:ncpdpid
            ,t.$1:type
,current_timestamp()
,current_timestamp()
,t.$1:partition_3
FROM
@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/Pharmacy/ t)
file_format = ( FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_STREAM_COUNT  ON TABLE ABACUS_NONPRD_FND_DB.LANDING.PHARMACY;


CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.PHARMACY_TEMP_COUNT
         AS
         SELECT UPPER(SOURCE) AS SOURCE, 'PHARMACY' AS TABLE_NAME, INGESTIONID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.LANDING.PHARMACY 
         WHERE INGESTIONID IN (SELECT DISTINCT INGESTIONID FROM ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_STREAM_COUNT ) GROUP BY 1, 2, 3
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.PHARMACY_TEMP_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME
         AND T.INGESTIONID = S.INGESTIONID
         AND T.SOURCE = S.SOURCE
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(SOURCE, TABLE_NAME,INGESTIONID,RECORD_COUNT)
                     VALUES(S.SOURCE,S.TABLE_NAME, S.INGESTIONID, S.RECORD_COUNT)
         ;`
} );
 
  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COUNT_CHECK_TASK
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  schedule = '1 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_STREAM_COUNT')            
as
  call ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COUNT_CHECK_PROC();
 

 
CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COPY_HISTORY_1
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COUNT_CHECK_TASK
as
  CREATE  OR REPLACE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.PHARMACY_TEMP_COUNT_COPY_HISOTRY
 AS
 SELECT TABLE_NAME, TABLE_SCHEMA_NAME AS SCHEMA_NAME, SPLIT(FILE_NAME,'/')[4]::String AS FILE_NAME, PIPE_RECEIVED_TIME AS LOAD_START_TIME, LAST_LOAD_TIME AS LOAD_END_TIME,
 ROW_COUNT AS RECORD_COUNT,
 FIRST_ERROR_MESSAGE AS ERROR_MESSAGE, FIRST_ERROR_LINE_NUMBER AS ERROR_LINE_NUMBER, FIRST_ERROR_COLUMN_NAME AS ERROR_COLUMN_NAME, ERROR_COUNT, STATUS
 from table(information_schema.copy_history(table_name=>'ABACUS_NONPRD_FND_DB.LANDING.PHARMACY', start_time=> dateadd(hours, -24, CURRENT_TIMESTAMP()))) WHERE STATUS NOT IN ('LOAD_IN_PROGRESS');
     
	 

CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COPY_HISTORY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 


  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY AS T
         USING  ABACUS_NONPRD_FND_DB.TRANSIENT.PHARMACY_TEMP_COUNT_COPY_HISOTRY  AS S     
         ON T.TABLE_NAME = S.TABLE_NAME     
         AND T.SCHEMA_NAME = S.SCHEMA_NAME
         AND T.FILE_NAME = S.FILE_NAME
         AND T.STATUS = S.STATUS
         AND T.RECORD_COUNT = S.RECORD_COUNT
         AND T.LOAD_START_TIME = S.LOAD_START_TIME
         AND T.LOAD_END_TIME = S.LOAD_END_TIME
         AND T.ERROR_COUNT = S.ERROR_COUNT
         WHEN matched
              THEN
              UPDATE SET  
                     T.ERROR_MESSAGE = S.ERROR_MESSAGE,
                     T.ERROR_LINE_NUMBER = S.ERROR_LINE_NUMBER,
                     T.ERROR_COLUMN_NAME = S.ERROR_COLUMN_NAME,
                     T.ERROR_COUNT = S.ERROR_COUNT    
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME, SCHEMA_NAME, FILE_NAME, LOAD_START_TIME, LOAD_END_TIME, RECORD_COUNT, ERROR_MESSAGE, ERROR_LINE_NUMBER, ERROR_COLUMN_NAME, ERROR_COUNT, STATUS)
                     VALUES(S.TABLE_NAME, S.SCHEMA_NAME, S.FILE_NAME, S.LOAD_START_TIME, S.LOAD_END_TIME, S.RECORD_COUNT, S.ERROR_MESSAGE, S.ERROR_LINE_NUMBER, S.ERROR_COLUMN_NAME, S.ERROR_COUNT, S.STATUS)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;
	
	

CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COPY_HISTORY_2
  warehouse =  NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COPY_HISTORY_1
as
  call ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COPY_HISTORY_PROC();
 
 

ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COPY_HISTORY_1 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COPY_HISTORY_2 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PHARMACY_COUNT_CHECK_TASK RESUME;    
CREATE OR REPLACE TABLE ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION
(
            CorrelationID string
            ,Source string
            ,SourceID string
            ,ParentSourceID string
            ,RelationshipName string
            ,SchemaVersion string
            ,SourceTimestamp datetime
            ,ConsumeTimestamp datetime
            ,HashID string
            ,ParentType string
            ,WrittenDate date
            ,CreatedDate date
            ,FillDate date
            ,Type string
            ,ReferenceInformation string
            ,OriginalReferenceInformation string
            ,TransmissionType string
            ,AverageWholesalePrice number
,LDG_REC_CREAT_DT TIMESTAMP
,LDG_REC_UPDT_DT TIMESTAMP
,INGESTIONID varchar,
CONSTRAINT  PRESCRIPTION_PKEY_1 primary key (SOURCE, SOURCEID, CORRELATIONID) not enforced
);
	
	
CREATE OR REPLACE PIPE ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_PIPE auto_ingest=true AS
COPY INTO  ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION 
FROM ( SELECT
            t.$1:correlationid
            ,t.$1:source
            ,t.$1:sourceid
            ,t.$1:parentsourceid
            ,t.$1:relationshipname
            ,t.$1:schemaversion
            ,t.$1:sourcetimestamp
            ,t.$1:consumetimestamp
            ,t.$1:hashid
            ,t.$1:parenttype
            ,t.$1:writtendate
            ,t.$1:createddate
            ,t.$1:filldate
            ,t.$1:type
            ,t.$1:referenceinformation
            ,t.$1:originalreferenceinformation
            ,t.$1:transmissiontype
            ,t.$1:averagewholesaleprice
,current_timestamp()
,current_timestamp()
,t.$1:partition_3
FROM
@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/Prescription/ t)
file_format = ( FORMAT_NAME='ABACUS_NONPRD_FND_DB.LANDING.PARQUET');


CREATE OR REPLACE STREAM ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_STREAM_COUNT  ON TABLE ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION;


CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COUNT_CHECK_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 
 
  var rs = snowflake.execute( { sqlText: 
      `CREATE  OR REPLACE TABLE ABACUS_NONPRD_FND_DB.TRANSIENT.PRESCRIPTION_TEMP_COUNT
         AS
         SELECT UPPER(SOURCE) AS SOURCE, 'PRESCRIPTION' AS TABLE_NAME, INGESTIONID, COUNT(*) AS RECORD_COUNT 
         FROM
         ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION 
         WHERE INGESTIONID IN (SELECT DISTINCT INGESTIONID FROM ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_STREAM_COUNT ) GROUP BY 1, 2, 3
         ;`
} );
 
 
  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID AS T
        using      ABACUS_NONPRD_FND_DB.TRANSIENT.PRESCRIPTION_TEMP_COUNT   AS S     
         ON T.TABLE_NAME = S.TABLE_NAME
         AND T.INGESTIONID = S.INGESTIONID
         AND T.SOURCE = S.SOURCE
         WHEN matched
              THEN
              UPDATE SET  
                     T.RECORD_COUNT = S.RECORD_COUNT
         WHEN NOT matched
             THEN
                     INSERT(SOURCE, TABLE_NAME,INGESTIONID,RECORD_COUNT)
                     VALUES(S.SOURCE,S.TABLE_NAME, S.INGESTIONID, S.RECORD_COUNT)
         ;`
} );
 
  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;


CREATE  OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COUNT_CHECK_TASK
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  schedule = '1 minute'
WHEN    
   SYSTEM$STREAM_HAS_DATA('ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_STREAM_COUNT')            
as
  call ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COUNT_CHECK_PROC();
 

 
CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COPY_HISTORY_1
  warehouse = NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COUNT_CHECK_TASK
as
  CREATE  OR REPLACE TABLE  ABACUS_NONPRD_FND_DB.TRANSIENT.PRESCRIPTION_TEMP_COUNT_COPY_HISOTRY
 AS
 SELECT TABLE_NAME, TABLE_SCHEMA_NAME AS SCHEMA_NAME, SPLIT(FILE_NAME,'/')[4]::String AS FILE_NAME, PIPE_RECEIVED_TIME AS LOAD_START_TIME, LAST_LOAD_TIME AS LOAD_END_TIME,
 ROW_COUNT AS RECORD_COUNT,
 FIRST_ERROR_MESSAGE AS ERROR_MESSAGE, FIRST_ERROR_LINE_NUMBER AS ERROR_LINE_NUMBER, FIRST_ERROR_COLUMN_NAME AS ERROR_COLUMN_NAME, ERROR_COUNT, STATUS
 from table(information_schema.copy_history(table_name=>'ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION', start_time=> dateadd(hours, -24, CURRENT_TIMESTAMP()))) WHERE STATUS NOT IN ('LOAD_IN_PROGRESS');
     
	 

CREATE OR REPLACE PROCEDURE ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COPY_HISTORY_PROC()
  RETURNS VARCHAR
  LANGUAGE javascript
  EXECUTE AS CALLER    
  AS
  $$
  var rs = snowflake.execute( { sqlText:
      `BEGIN TRANSACTION;`
       } );
 


  var rs = snowflake.execute( { sqlText:
        `MERGE INTO ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY AS T
         USING  ABACUS_NONPRD_FND_DB.TRANSIENT.PRESCRIPTION_TEMP_COUNT_COPY_HISOTRY  AS S     
         ON T.TABLE_NAME = S.TABLE_NAME     
         AND T.SCHEMA_NAME = S.SCHEMA_NAME
         AND T.FILE_NAME = S.FILE_NAME
         AND T.STATUS = S.STATUS
         AND T.RECORD_COUNT = S.RECORD_COUNT
         AND T.LOAD_START_TIME = S.LOAD_START_TIME
         AND T.LOAD_END_TIME = S.LOAD_END_TIME
         AND T.ERROR_COUNT = S.ERROR_COUNT
         WHEN matched
              THEN
              UPDATE SET  
                     T.ERROR_MESSAGE = S.ERROR_MESSAGE,
                     T.ERROR_LINE_NUMBER = S.ERROR_LINE_NUMBER,
                     T.ERROR_COLUMN_NAME = S.ERROR_COLUMN_NAME,
                     T.ERROR_COUNT = S.ERROR_COUNT    
         WHEN NOT matched
             THEN
                     INSERT(TABLE_NAME, SCHEMA_NAME, FILE_NAME, LOAD_START_TIME, LOAD_END_TIME, RECORD_COUNT, ERROR_MESSAGE, ERROR_LINE_NUMBER, ERROR_COLUMN_NAME, ERROR_COUNT, STATUS)
                     VALUES(S.TABLE_NAME, S.SCHEMA_NAME, S.FILE_NAME, S.LOAD_START_TIME, S.LOAD_END_TIME, S.RECORD_COUNT, S.ERROR_MESSAGE, S.ERROR_LINE_NUMBER, S.ERROR_COLUMN_NAME, S.ERROR_COUNT, S.STATUS)
         ;`
} );
 

  var rs = snowflake.execute( { sqlText:
        `COMMIT;`
         } );       
    return 'Done.';
    $$;
	
	

CREATE OR REPLACE TASK ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COPY_HISTORY_2
  warehouse =  NONPRD_ABACUS_ELT_XS_WH
  AFTER ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COPY_HISTORY_1
as
  call ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COPY_HISTORY_PROC();
 
 

ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COPY_HISTORY_1 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COPY_HISTORY_2 RESUME;
ALTER TASK ABACUS_NONPRD_FND_DB.LANDING.PRESCRIPTION_COUNT_CHECK_TASK RESUME;    
