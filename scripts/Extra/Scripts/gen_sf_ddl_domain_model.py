"""
----------------------------------------------------------------------------------------------------------
Description: This Script will generate Snowflake DDL from Domain Model 

usage: python gen_sf_ddl_domain_model.py stg <Connector_in_Upper_Case> <Source name> <VALIDATION_DOMAIN> <VALIDATION_SOURCE>
If this is domain only connector then VALIDATION_DOMAIN should be passed as Y and VALIDATION_SOURCE as N
If this is source only connector then VALIDATION_SOURCE should be passed as Y and VALIDATION_DOMAIN as N
If this connector loads both domain and source to snowflake then VALIDATION_DOMAIN should be passed as Y and VALIDATION_DOMAIN as Y

Sample Command : 
python gen_sf_ddl_domain_model.py stg CAMBIA_FEP_MEMBER FEP Y Y
python gen_sf_ddl_domain_model.py stg FACETS_RRE FEP N Y


Author  : Sunil Kumar Shivarudraiah
Release : 1

Modification Log:  04 Dec, 2020
-----------------------------------------------------------------------------------------------------------
Date                Author                            Description
-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
"""


import os
import jinja2
import json
import boto3
import sys

env_type = sys.argv[1]
UPPER_CONNECTOR = sys.argv[2]
SOURCE = sys.argv[3]
VALIDATION_DOMAIN = sys.argv[4]
VALIDATION_SOURCE = sys.argv[5]


if not os.path.exists(UPPER_CONNECTOR):
    os.makedirs(UPPER_CONNECTOR)
        
env = jinja2.Environment(loader=jinja2.FileSystemLoader(['jinja2_templates']))

create_incr_table_template = 'domain_ddl_template.j2'
create_incr_table_template_vl = 'domain_ddl_template_vl.j2'
create_incr_table_template_vl1 = 'domain_ddl_template_vl1.j2'
create_incr_table_template_vl2 = 'domain_ddl_template_vl2.j2'

incrtabletemplate = env.get_template(create_incr_table_template)
incrtabletemplate_vl = env.get_template(create_incr_table_template_vl)
incrtabletemplate_vl1 = env.get_template(create_incr_table_template_vl1)
incrtabletemplate_vl2 = env.get_template(create_incr_table_template_vl2)


if env_type == 'stg':
    SF_DATABASE_NAME = 'ABACUS_NONPRD_FND_DB'
    SF_SCHEMA = 'LANDING'
    FILE_FORMAT = 'ABACUS_NONPRD_FND_DB.LANDING.PARQUET'
    s3_data_path = "@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/"
    TEMP_TABLE = 'ABACUS_NONPRD_FND_DB.TRANSIENT.'
    TARGET_INGESTION_TABLE = 'ABACUS_NONPRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID'
    WAREHOUSE = 'NONPRD_ABACUS_ELT_XS_WH'
    COPY_HISTORY_TABLE = "ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY"
    s3_lake_bucket = 'abacus-data-lake-cambiastg'
    
elif env_type == 'prod':
    SF_DATABASE_NAME = 'ABACUS_PRD_FND_DB'
    SF_SCHEMA = 'LANDING'
    FILE_FORMAT = 'ABACUS_PRD_FND_DB.LANDING.PARQUET'    
    s3_data_path = "@ABACUS_PRD_FND_DB.LANDING.PROD_ABACUS_STG/data/"
    TEMP_TABLE =  'ABACUS_PRD_FND_DB.TRANSIENT.'
    TARGET_INGESTION_TABLE = 'ABACUS_PRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID'
    WAREHOUSE = 'PRD_ABACUS_ELT_XS_WH'
    COPY_HISTORY_TABLE = "ABACUS_PRD_FND_DB.CONTROL.COPY_LOAD_HISTORY"
    s3_lake_bucket = 'abacus-data-lake-cambia'



def main():
    if not os.path.exists(UPPER_CONNECTOR):
        os.makedirs(UPPER_CONNECTOR)
    view_list = []

    for root, dirs, files in os.walk(f'21.6.3/resolvedout/tables/'):
        dirs.sort()
        for dirname in dirs:
            for filename in sorted(os.listdir(os.path.join(root, dirname))):
                filepath = os.path.join(root, dirname, filename)
                with open(filepath, 'r') as f:
                    jsonschema = json.load(f)
                    tablename = jsonschema['title']
                    if jsonschema['properties']:

                        if tablename in view_list:
                            with open(f'{UPPER_CONNECTOR}/{tablename}_incremental.sql', 'w') as f1:
                                    print(tablename)
                                    f1.write(incrtabletemplate.render(
                                        tablename=SF_DATABASE_NAME+"."+SF_SCHEMA+"."+filename.split('.')[0].upper(),
                                        s3_path=s3_data_path+filename.split('.')[0]+'/',
                                        file_format="'"+FILE_FORMAT+"'",
                                        empty_line="",
                                        transient_tablename=TEMP_TABLE+filename.split('.')[0].upper(),
                                        TARGET_INGESTION_TABLE = TARGET_INGESTION_TABLE,
                                        WAREHOUSE = WAREHOUSE,
                                        COPY_HISTORY_TABLE = COPY_HISTORY_TABLE,
                                        ONLY_TABLE_NAME = filename.split('.')[0].upper(),
                                        TABLE_NAME = filename.split('.')[0].upper(),
                                        attributes=jsonschema['properties'],
                                    ))    
                        


if __name__ == "__main__":
 #   main()
    
    if VALIDATION_DOMAIN == 'Y' and VALIDATION_SOURCE == 'Y': 
        with open(f'{UPPER_CONNECTOR}/VALIDATION.sql', 'w') as f1:
            f1.write(incrtabletemplate_vl.render(
                UPPER_CONNECTOR=UPPER_CONNECTOR,
                SOURCE = SOURCE
            ))  
    elif VALIDATION_DOMAIN == 'Y': 
        with open(f'{UPPER_CONNECTOR}/VALIDATION.sql', 'w') as f1:
            f1.write(incrtabletemplate_vl1.render(
                UPPER_CONNECTOR=UPPER_CONNECTOR,
                SOURCE = SOURCE
            ))  
    elif VALIDATION_SOURCE == 'Y': 
        with open(f'{UPPER_CONNECTOR}/VALIDATION.sql', 'w') as f1:
            f1.write(incrtabletemplate_vl2.render(
                UPPER_CONNECTOR=UPPER_CONNECTOR,
                SOURCE = SOURCE
            ))