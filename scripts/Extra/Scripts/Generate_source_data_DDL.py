
"""
----------------------------------------------------------------------------------------------------------
Description: Cambia Snowflake SQL Generator

usage:

Author  : Sunil Kumar Shivarudraiah
Release : 1

-----------------------------------------------------------------------------------------------------------
"""

import os
import jinja2
import json
import boto3
import sys

connector = sys.argv[1]
env_type = sys.argv[2]
SOURCE = sys.argv[3]
CONNECTOR_UP = sys.argv[4]
 
env = jinja2.Environment(loader=jinja2.FileSystemLoader(['jinja2_templates']))

create_schema_from_csv_template = 'create_schema_from_csv_template.j2'
create_config_for_athena1 = 'create_athena_config1.j2'
create_config_for_athena2 = 'create_athena_config2.j2'

incrtabletemplate = env.get_template(create_schema_from_csv_template)
athenaconfig1 = env.get_template(create_config_for_athena1)
athenaconfig2 = env.get_template(create_config_for_athena2)


if env_type == 'stg':
    SF_DATABASE_NAME = 'ABACUS_NONPRD_FND_DB'
    SF_SCHEMA = 'SOURCE_DATA'
    FILE_FORMAT = 'ABACUS_NONPRD_FND_DB.LANDING.PARQUET'
    s3_data_path = "@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/source_data/"
    TEMP_TABLE = 'ABACUS_NONPRD_FND_DB.TRANSIENT.'
    TARGET_INGESTION_TABLE = 'ABACUS_NONPRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_INGESTIONID'
    TARGET_BATCH_TABLE = 'ABACUS_NONPRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_BATCH_ID'    
    WAREHOUSE = 'NONPRD_ABACUS_ELT_XS_WH'
    COPY_HISTORY_TABLE = "ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY"
    s3_lake_bucket = 'abacus-data-lake-cambiastg'
    
elif env_type == 'prod':
    SF_DATABASE_NAME = 'ABACUS_PRD_FND_DB'
    SF_SCHEMA = 'SOURCE_DATA'
    FILE_FORMAT = 'ABACUS_PRD_FND_DB.LANDING.PARQUET'    
    s3_data_path = "@ABACUS_PRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/source_data/"
    TEMP_TABLE =  'ABACUS_PRD_FND_DB.TRANSIENT.'
    TARGET_INGESTION_TABLE = 'ABACUS_PRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_INGESTIONID'
    TARGET_BATCH_TABLE = 'ABACUS_PRD_FND_DB.CONTROL.SOURCE_DATA_RECORD_COUNT_BATCH_ID'        
    WAREHOUSE = 'PRD_ABACUS_ELT_XS_WH'
    COPY_HISTORY_TABLE = "ABACUS_PRD_FND_DB.CONTROL.COPY_LOAD_HISTORY"
    s3_lake_bucket = 'abacus-data-lake-cambia'
    
else:
    print("Enter Valid source\n")
    exit()


def main():
    bucket='abacus-data-lake-cambiastg'

    if not os.path.exists(connector):
        os.makedirs(connector)

    if not os.path.exists(connector+"/sql/"):
        os.makedirs(connector+"/sql/")
        
    if not os.path.exists(connector+"/schema/"):
        os.makedirs(connector+"/schema/")

    if not os.path.exists(connector+"/config/"):
        os.makedirs(connector+"/config/")
        
    if os.path.exists(connector+'/sql/combined_incremental.sql'):
        os.remove(connector+'/sql/combined_incremental.sql')
        
    for root, dirs, files in os.walk(connector):
        dirs.sort()
        
        for dirname in dirs:
            print(dirname)
            table_list = ''
            table_list_S3_path = ""
            extractifloop = ""
            data = ""
            data1 = ""
            if dirname == "schema":
                for filename in sorted(os.listdir(os.path.join(root, dirname))):
                    table_list = filename.split('.')[0].lower()+"_unnest_root"+","+table_list
                    table_list_S3_path = filename.split('.')[0].upper()+"_UNNEST_ROOT=data-sources/parsed_unnested/"+connector+"_"+filename.split('.')[0].lower()+"_unnest__root\n"+table_list_S3_path
                    extractifloop = extractifloop+"                elif srcs == '"+filename.split('.')[0].lower()+"_unnest_root':\n"
                    extractifloop = extractifloop+"                    if '"+filename.split('.')[0].upper()+"_UNNEST_ROOT=' in line:\n"
                    extractifloop = extractifloop+"                        src_prefix = line.split('=',1)[1]\n"
                    extractifloop = extractifloop+"                        f_name_2 = '"+connector+"_"+filename.split('.')[0].lower()+"_unnest_root'\n"
                    filepath = os.path.join(root, dirname, filename)
                    file1 = open(filepath, 'r')
                    DDL = ''
                    PIPE = ''
                    COUNT = 0
                    Lines = file1.readlines()
                    for line in Lines:
                        if line.strip() == "":
                            pass
                        else:
                            DDL =  DDL.strip()+line.strip().split(',')[0]+" "+line.strip().split(',')[1]+"\n,"
                            PIPE = PIPE+"t.$1:"+line.strip().split(',')[0]+"\n,"
                    print(DDL[:-1] + ");\n")
                    with open(connector+'/sql/'+filename.split('.')[0]+'_incremental.sql', 'w') as f1:
                        f1.write(incrtabletemplate.render(
                            tablename=SF_DATABASE_NAME+"."+SF_SCHEMA+"."+connector.upper()+"_"+filename.split('.')[0].upper()+"_UNNEST_ROOT",
                            COLUMNS=DDL[:-1].strip(),
                            s3_path=s3_data_path+connector+"_"+filename.split('.')[0].lower()+"_unnest_root/",
                            file_format="'"+FILE_FORMAT+"'",
                            PIPE_COLUMS=PIPE[:-1].strip(),
                            empty_line="",
                            transient_tablename=TEMP_TABLE+connector.upper()+"_"+filename.split('.')[0].upper()+"_UNNEST_ROOT",
                            TARGET_INGESTION_TABLE = TARGET_INGESTION_TABLE,
                            TARGET_BATCH_TABLE = TARGET_BATCH_TABLE,
                            WAREHOUSE = WAREHOUSE,
                            COPY_HISTORY_TABLE = COPY_HISTORY_TABLE,
                            CONNECTOR_UP = CONNECTOR_UP,
                            SOURCE = SOURCE,
                            TABLE_NAME = connector.upper()+"_"+filename.split('.')[0].upper()+"_UNNEST_ROOT"
                        ))    
                        
                        
                    with open(connector+'/sql/'+filename.split('.')[0]+'_incremental.sql') as fp:    
                        data1 = fp.read()
                    data = data1+"\n\n\n"
                    with open (connector+'/sql/combined_incremental.sql', 'a') as fp1:
                        fp1.write(data)                        
                        
                with open(connector+'/config/'+connector+'_source_data.txt', 'w') as f2:
                    f2.write(athenaconfig1.render(
                        table_list = table_list[:-1],
                        s3_lake_bucket = s3_lake_bucket,
                        table_list_S3_path = table_list_S3_path[:-1]
                    ))

                with open(connector+'/config/'+connector+'_extract.txt', 'w') as f2:
                    f2.write(athenaconfig2.render(
                        connector = connector,
                        extractifloop = extractifloop[1:-1]
                    ))
                    

if __name__ == "__main__":
    main()
