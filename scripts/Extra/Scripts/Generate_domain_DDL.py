"""
----------------------------------------------------------------------------------------------------------
Description: Cambia Snowflake SQL Generator

usage:

Author  : Sunil Kumar Shivarudraiah
Release : 1

-----------------------------------------------------------------------------------------------------------
"""

import os
import jinja2
import json
import boto3
import sys

connector = sys.argv[1]
env = jinja2.Environment(loader=jinja2.FileSystemLoader(['jinja2_templates']))
create_incr_table_template = 'domain_ddl_template.j2'
incrtabletemplate = env.get_template(create_incr_table_template)
env_type = 'stg'


if env_type == 'stg':
    SF_DATABASE_NAME = 'ABACUS_NONPRD_FND_DB'
    SF_SCHEMA = 'LANDING'
    FILE_FORMAT = 'ABACUS_NONPRD_FND_DB.LANDING.PARQUET'
    s3_data_path = "@ABACUS_NONPRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/"
    TEMP_TABLE = 'ABACUS_NONPRD_FND_DB.TRANSIENT.'
    TARGET_INGESTION_TABLE = 'ABACUS_NONPRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID'
    WAREHOUSE = 'NONPRD_ABACUS_ELT_XS_WH'
    COPY_HISTORY_TABLE = "ABACUS_NONPRD_FND_DB.CONTROL.COPY_LOAD_HISTORY"
    s3_lake_bucket = 'abacus-data-lake-cambiastg'
    
elif env_type == 'prod':
    SF_DATABASE_NAME = 'ABACUS_PRD_FND_DB'
    SF_SCHEMA = 'LANDING'
    FILE_FORMAT = 'ABACUS_PRD_FND_DB.LANDING.PARQUET'    
    s3_data_path = "@ABACUS_PRD_FND_DB.LANDING.NON_PROD_ABACUS_STG/data/"
    TEMP_TABLE =  'ABACUS_PRD_FND_DB.TRANSIENT.'
    TARGET_INGESTION_TABLE = 'ABACUS_PRD_FND_DB.CONTROL.LANDING_RECORD_COUNT_INGESTIONID'
    WAREHOUSE = 'PRD_ABACUS_ELT_XS_WH'
    COPY_HISTORY_TABLE = "ABACUS_PRD_FND_DB.CONTROL.COPY_LOAD_HISTORY"
    s3_lake_bucket = 'abacus-data-lake-cambia'



def main():

    if not os.path.exists(connector):
        os.makedirs(connector)
    view_list = ['ClaimLineDrug','CompoundDrug','PharmacyNetwork','Pharmacy','Prescription']

    for root, dirs, files in os.walk(f'21.6.3/resolvedout/tables/'):
        dirs.sort()
        for dirname in dirs:
            for filename in sorted(os.listdir(os.path.join(root, dirname))):
                filepath = os.path.join(root, dirname, filename)
                with open(filepath, 'r') as f:
                    jsonschema = json.load(f)
                    tablename = jsonschema['title']
                    if jsonschema['properties']:

                        if tablename in view_list:
                            with open(f'{connector}/{tablename}_incremental.sql', 'w') as f1:
                                    print(tablename)
                                    f1.write(incrtabletemplate.render(
                                        tablename=SF_DATABASE_NAME+"."+SF_SCHEMA+"."+filename.split('.')[0].upper(),
                                        s3_path=s3_data_path+filename.split('.')[0]+'/',
                                        file_format="'"+FILE_FORMAT+"'",
                                        empty_line="",
                                        transient_tablename=TEMP_TABLE+filename.split('.')[0].upper(),
                                        TARGET_INGESTION_TABLE = TARGET_INGESTION_TABLE,
                                        WAREHOUSE = WAREHOUSE,
                                        COPY_HISTORY_TABLE = COPY_HISTORY_TABLE,
                                        ONLY_TABLE_NAME = filename.split('.')[0].upper(),
                                        TABLE_NAME = filename.split('.')[0].upper(),
                                        attributes=jsonschema['properties'],
                                    ))    
                        

    with open(f'{connector}/{connector}_COMBINED_DOMAIN_DDLS.sql', 'w') as outfile:
        for fname in view_list:
            with open(f'{connector}/{fname}_incremental.sql') as infile:
                outfile.write(infile.read())
                outfile.write(" \n")
                
if __name__ == "__main__":
    main()
