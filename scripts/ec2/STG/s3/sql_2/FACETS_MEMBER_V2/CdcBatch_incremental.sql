CREATE TABLE CdcBatch_replacesuffix AS
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE, consumetimestamp, 
BATCHID, INGESTIONID FROM cambiastg_data_main.CONN_UNNESTED_MEMBER
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambiastg_data_main.CONN_UNNESTED_MEMBER WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Facets-Member'
  )
AND SOURCE = 'Facets-Member'
ORDER BY 1,2
;
