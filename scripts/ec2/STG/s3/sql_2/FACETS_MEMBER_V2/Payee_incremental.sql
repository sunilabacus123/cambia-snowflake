CREATE TABLE Payee_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,PayeeType
            ,'null' as PayeeIDCodeType
            ,'null' as PayeeIDCodeDesc
            ,'null' as ContactFunctionCode
            ,'null' as CommunicationType
            ,'null' as CommunicationNumber
            ,MemberSourceID
            ,ServiceProviderSourceID
            ,ServiceOrgSourceID
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Payee
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
