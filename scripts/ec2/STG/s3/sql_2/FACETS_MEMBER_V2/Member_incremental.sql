CREATE TABLE Member_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,MemberLevel
            ,MemberType
            ,MemberCategory
            ,MemberCategoryID
            ,MemberCategoryPrefix
            ,SubscriberRelationship
            ,ParentGroupSourceID
            ,GroupSourceID
            ,SubGroupSourceID
            ,SubscriberID
            ,PlanID
            ,CarrierID
            ,OverrideCarrierID
            ,CoordinationOfBenefitsInd
            ,PersonCode
            ,Status
            ,HasPCP
            ,NationalID
            ,CBSACode
            ,ConsumerDrivenHealthcareInd
            ,PrescriptionCoverageInd
            ,MentalHealthCoverageInd
            ,MarketSegmentCode
            ,SICCode
            ,DualEligibilityInd
            ,EndStageRenalDiseaseInd
            ,HospiceInd
            ,InstitutionalInd
            ,ExchangeInd
            ,StateResidency
            ,IndustryCode
            ,Occupation
            ,EmployeeStatus
            ,HireDate
            ,MedicareHICNumber
            ,MedicareStartDate
            ,MedicareCoverageType
            ,MedicaidID
            ,EffDate
            ,ExpDate
FROM
    (
        SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,CorrelationID
            ,Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,MemberLevel
            ,MemberType
            ,MemberCategory
            ,MemberCategoryID
            ,MemberCategoryPrefix
            ,CASE
				WHEN src.meme_ck IS NULL THEN dm.SubscriberRelationship
				ELSE src.meme_rel
			 END AS SubscriberRelationship
            ,ParentGroupSourceID
            ,GroupSourceID
            ,SubGroupSourceID
            ,SubscriberID
            ,PlanID
            ,CarrierID
            ,OverrideCarrierID
            ,CoordinationOfBenefitsInd
            ,PersonCode
            ,Status
            ,HasPCP
            ,NationalID
            ,CBSACode
            ,ConsumerDrivenHealthcareInd
            ,PrescriptionCoverageInd
            ,MentalHealthCoverageInd
            ,MarketSegmentCode
            ,SICCode
            ,DualEligibilityInd
            ,EndStageRenalDiseaseInd
            ,HospiceInd
            ,InstitutionalInd
            ,ExchangeInd
            ,StateResidency
            ,IndustryCode
            ,Occupation
            ,EmployeeStatus
            ,HireDate
            ,MedicareHICNumber
            ,MedicareStartDate
            ,MedicareCoverageType
            ,MedicaidID
            ,EffDate
            ,ExpDate
            ,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Member
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Facets-Member'
         ) dm
         LEFT JOIN ( 	SELECT DISTINCT trim(meme_ck) as meme_ck, trim(meme_rel) as meme_rel,__lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_member_v2_mbr
						WHERE
						CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
         dm.sourceid = src.meme_ck and
         dm.ingestionid = src.ingestion_id
    )
    WHERE RowNum = 1
);
