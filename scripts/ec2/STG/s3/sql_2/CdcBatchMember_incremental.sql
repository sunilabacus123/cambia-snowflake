CREATE TABLE CdcBatchMember_replacesuffix AS
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE,
BATCHID, INGESTIONID FROM cambiastg_data_main.CONN_UNNESTED_MEMBER
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambiastg_data_main.CONN_UNNESTED_MEMBER WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid
  )ORDER BY 1,2;
