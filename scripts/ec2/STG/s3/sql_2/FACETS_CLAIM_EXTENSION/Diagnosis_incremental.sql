CREATE TABLE Diagnosis_replacesuffix
WITH
(
	bucket_count=10,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Condition
            ,ConditionCause
            ,DiagnosisGroup
            ,DiagnosisCategory
            ,DiagnosisType
            ,DiagnosisClass
            ,Codeset
            ,CodesetType
            ,CodeQualifier
            ,CodeQualifierType
            ,PrimaryInd
            ,Amount
            ,AmountType
            ,DiagnosisGroupAllowedAmount
            ,Role
            ,Sequence
            ,SourceDescription
            ,SourceAlternateDescription
            ,EffDate
            ,PresentOnAdmissionCode
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Condition
            ,ConditionCause
            ,DiagnosisGroup
            ,DiagnosisCategory
			,CASE
				WHEN dm.ParentType = 'Claim' THEN dm.Role
				WHEN dm.ParentType = 'ClaimItem' THEN
					CASE
						WHEN split_part(dm.sourceid,'-',2) = 'Primary' THEN 'Primary'
						WHEN split_part(dm.sourceid,'-',2) = 'Secondary' THEN  'Secondary-' || split_part(dm.sourceid,'-',3)
					END
			 END AS	DiagnosisType
            ,DiagnosisClass
            ,Codeset
            ,CodesetType
            ,CodeQualifier
            ,CodeQualifierType
            ,PrimaryInd
            ,Amount
            ,AmountType
            ,DiagnosisGroupAllowedAmount
            ,CASE
				WHEN dm.ParentType = 'Claim' AND dm.Sequence = 1 THEN 'Primary'
				WHEN dm.ParentType = 'Claim' AND dm.Sequence BETWEEN 2 and 25 THEN 'Secondary'
			 END AS Role
            ,Sequence
            ,SourceDescription
            ,SourceAlternateDescription
            ,EffDate
            ,PresentOnAdmissionCode
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Diagnosis
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         ) dm

    )
    WHERE RowNum = 1
);
