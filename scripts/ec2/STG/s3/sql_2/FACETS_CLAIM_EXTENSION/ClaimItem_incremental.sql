CREATE TABLE ClaimItem_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
  SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,Sequence
            ,ClaimItemStatus
            ,Revenue
            ,RevenueCode
            ,Category
            ,ProductOrService
            ,ProductOrServiceDesc
            ,ProductOrServiceType
            ,CoordinationOfBenefitsCode
            ,CoordinationOfBenefitsDesc
            ,Modifier
            ,ProgramCode
            ,FirstDateOfService
            ,LastDateOfService
            ,PlaceOfService
            ,Quantity
            ,UnitType
            ,UnitPrice
            ,ItemCost
            ,ChargeAmount
            ,BodySite
            ,SubSite
            ,NetworkInd
            ,NetworkID
            ,partition_3
	        ,IncurredDate
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                line.*,
				CASE
				   WHEN LOWER(header.Claimtype) = 'institutional'
				   THEN CASE
							WHEN ( hosp.FacilityBillTypeCode = '02' AND hosp.BillTypeClassificationCode  = '2' OR
								   hosp.FacilityBillTypeCode = '03' AND hosp.BillTypeClassificationCode  = '2'
							)
							THEN
								CASE WHEN CAST(header.StatementFromDate AS DATE) <> DATE '1753-01-01' AND CAST(header.StatementFromDate AS DATE) <=  src_status.clst_sts_dtm THEN CAST(header.StatementFromDate AS DATE)
								ELSE CAST(line.FirstDateOfService AS DATE)
								END
						   ELSE
							   CASE WHEN (
											hosp.FacilityBillTypeCode || hosp.BillTypeClassificationCode IN
										  (
										  '011', '012', '015', '016', '017', '018', '019', '021', '023', '024', '025', '026', '027', '028', '029',
										  '041', '042', '045', '046', '047', '048', '049', '051', '052', '054', '055', '056', '057', '058', '059',
										  '061', '062', '065', '066', '067', '068', '069', '072', '081', '082', '084', '086', '087', '088', '089',
										  '091', '092', '094', '095', '096', '097'

										  )
										)
										AND
										CAST(hosp.AdmissionDate AS DATE) <> Date '1753-01-01' AND
										CAST(hosp.AdmissionDate AS DATE) <= src_status.clst_sts_dtm AND
										CAST(hosp.AdmissionDate AS DATE) >  date_add('year',-2,src_status.clst_sts_dtm)  AND
										(
										  ( CAST(hosp.DischargeDate AS DATE) <> Date '1753-01-01' AND CAST(hosp.AdmissionDate AS DATE) < CAST(hosp.DischargeDate AS DATE) ) OR
										  ( CAST(hosp.DischargeDate AS DATE) = Date '1753-01-01')
										)
									 THEN CAST(hosp.AdmissionDate AS DATE)
									 ELSE CAST(line.FirstDateOfService AS DATE)
								END
						  END
				   WHEN LOWER(header.Claimtype) = 'professional' THEN CAST(line.LastDateOfService AS DATE)
				   WHEN LOWER(header.Claimtype) = 'oral' THEN CAST(line.FirstDateOfService AS DATE)
				END  as IncurredDate
            FROM
            cambiastg_data_main.conn_unnested_ClaimItem line
            join
			(
			  select correlationid, sourceid,source,claimtype,claimcategory, StatementFromDate,BillableStartDate,ID AS ClaimID,claimsequencenumber, partition_3 as header_partition_3
			  from
			  cambiastg_data_main.conn_unnested_claim
			  where source = 'Facets-Claim-Extension' AND CAST(partition_3 AS INTEGER) = replace_ingestionid
			) header on
			  line.correlationid = header.correlationid and
			  line.parentsourceid = header.sourceid and
			  line.source = header.source
			left join ( select
					   correlationid ,
					   sourceid,
					   parentsourceid,
					   source ,
					   DischargeDate,
					   FacilityBillTypeCode,
					   BillTypeClassificationCode,
					   cast(substring( split_part(sourceid,'-',5) || '-' || split_part(sourceid,'-',6) || '-' ||split_part(sourceid,'-',7) , 1,10)  as date) as ClaimStatusDate,
					   cast(case when cardinality(split(sourceid,'-')) = 10 then substring(split_part(sourceid,'-',8) || '-' || split_part(sourceid,'-',9) || '-' || split_part(sourceid,'-',10),1,10)
						else '1753-01-01' END as date)  as AdmissionDate
					   from cambiastg_data_main.conn_unnested_institutional
					   WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid
					  ) hosp on
			  header.correlationid = hosp.correlationid and
			  header.sourceid = hosp.parentsourceid and
			  header.source = hosp.source
			  LEFT JOIN
				(
						SELECT clcl_id,max(cast(substring(cast(clst_sts_dtm as varchar),1,10) as date)) as clst_sts_dtm,__lineage.ingestion_id
						FROM
						cambiastg_data_sources.facets_claim_extension_header_status
						WHERE CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid AND trim(clst_sts) not in ('91','97','99')
						GROUP BY clcl_id,__lineage.ingestion_id
				) src_status ON
				header.sourceid = src_status.clcl_id AND
				LOWER(header.Claimtype) = 'institutional' AND
				header.header_partition_3 = src_status.ingestion_id
    	WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
