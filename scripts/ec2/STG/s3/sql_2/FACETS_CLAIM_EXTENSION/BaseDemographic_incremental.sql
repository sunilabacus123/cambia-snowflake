CREATE TABLE BaseDemographic_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Gender
            ,Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,CAST(NULL AS VARCHAR) AS Gender
            ,CASE
				WHEN src.clcl_id IS NULL THEN dm.sex
			 ELSE src.meme_sex
			 END AS Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_BaseDemographic
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid AND source = 'Facets-Claim-Extension'
         ) dm
		 LEFT JOIN ( 	SELECT sourceid as dm_mbr_sourceid, parentsourceid as dm_mbr_parentsourceid , correlationid as dm_mbr_correlationid, partition_3 as dm_mbr_partition_3
						FROM cambiastg_data_main.conn_unnested_memberstub
						WHERE
						CAST(partition_3 AS INTEGER) = replace_ingestionid  AND source = 'Facets-Claim-Extension' AND relationshipname = 'PatientMember'
					) dm_mbr  ON
         dm.correlationid = dm_mbr.dm_mbr_correlationid AND
         dm.parentsourceid = dm_mbr.dm_mbr_sourceid AND
		 dm.partition_3 = dm_mbr_partition_3
         LEFT JOIN ( 	SELECT clcl_id, trim(meme_sex) as meme_sex,__lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_claim_extension_header
						WHERE
						CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
         dm_mbr.dm_mbr_parentsourceid = src.clcl_id and
         dm_mbr.dm_mbr_partition_3 = src.ingestion_id
    )
    WHERE RowNum = 1
);
