CREATE TABLE InsurancePolicy_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
						,ParentType
            ,Sequence
            ,PayerID
            ,PolicyID
            ,PolicyName
            ,PolicyHolderID
            ,SubscriberName
            ,SubscriberNumber
            ,GroupID
            ,GroupName
            ,SubGroupID
            ,SubGroupName
            ,SubscriberID
            ,AlternativeIdentifier
            ,AlternativeIdentifierType
            ,PlanID
            ,PlanName
            ,SubPlanID
            ,SubPlanName
            ,PharmacyRxGroup
            ,PharmacyRxBin
            ,PharmacyRxPCN
            ,BeneficiaryRelationshipType
            ,BeneficiaryName
            ,BeneficiaryID
            ,CoordinateWithPolicy
            ,CoverageMonths
            ,MemberType
            ,RelationshipType
            ,DependentMemberSequence
            ,Type
            ,ContractType
            ,ContractTypeSourceCode
            ,CarrierCode
            ,Status
            ,COBInd
            ,COBType
            ,COBCode
            ,COBCodeDesc
            ,EffDate
            ,ExpDate
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
			,ParentType
            ,Sequence
            ,PayerID
            ,PolicyID
            ,PolicyName
            ,PolicyHolderID
            ,SubscriberName
            ,SubscriberNumber
            ,GroupID
            ,GroupName
            ,SubGroupID
            ,SubGroupName
            ,SubscriberID
            ,AlternativeIdentifier
            ,AlternativeIdentifierType
            ,PlanID
            ,PlanName
            ,SubPlanID
            ,SubPlanName
            ,PharmacyRxGroup
            ,PharmacyRxBin
            ,PharmacyRxPCN
            ,CASE
				WHEN src.clcl_id IS NULL THEN dm.BeneficiaryRelationshipType
			 ELSE src.meme_rel
			 END AS BeneficiaryRelationshipType
            ,BeneficiaryName
            ,BeneficiaryID
            ,CoordinateWithPolicy
            ,CoverageMonths
            ,MemberType
            ,CASE
				WHEN src.clcl_id IS NULL THEN dm.RelationshipType
			 ELSE src.meme_rel
			 END AS RelationshipType
            ,DependentMemberSequence
            ,CASE
				WHEN src.clcl_id IS NULL THEN dm.Type
			 ELSE src.clcl_cl_type
			 END AS Type
            ,ContractType
            ,ContractTypeSourceCode
            ,CarrierCode
            ,Status
            ,COBInd
            ,COBType
            ,COBCode
            ,COBCodeDesc
            ,EffDate
            ,ExpDate
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_InsurancePolicy
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         ) dm
         LEFT JOIN ( 	SELECT clcl_id, trim(meme_rel) as meme_rel, trim(clcl_cl_type) as clcl_cl_type, __lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_claim_extension_header
						WHERE
						CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
         dm.parentsourceid = src.clcl_id and
         dm.partition_3 = src.ingestion_id

    )
    WHERE RowNum = 1
);
