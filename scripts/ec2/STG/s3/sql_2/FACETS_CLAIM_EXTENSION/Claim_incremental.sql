CREATE TABLE Claim_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,cl.CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,id.claimnumber
            ,CASE
				WHEN src.clcl_id IS NULL THEN cl.ClaimType
			 ELSE src.clcl_cl_sub_type
			 END AS ClaimType
            ,CASE
				WHEN src.clcl_id IS NULL THEN cl.ClaimStatus
			 ELSE src.clcl_cur_sts
			 END AS ClaimStatus
            ,ClaimResponseStatusCode
            ,ClaimCategory
            ,ClaimSubcategory
            ,PrescriptionReferenceID
            ,PrescriptionDateTime
            ,BillableStartDate
            ,BillableEndDate
            ,ClaimSubmittedDateTime
            ,ClaimReceivedDateTime
            ,ClaimIncurredDateTime
            ,ClaimAdjudicationDateTime
            ,ClaimPaidDateTime
            ,ClaimReversalDateTime
            ,ClaimLastActionDate
            ,ClaimAcceptedDate
            ,ClaimNextReviewDate
            ,StatementFromDate
            ,StatementToDate
            ,CapitationInd
            ,CapitationType
            ,CapitationTypeDesc
            ,ClaimSequenceNumber
            ,ClaimOriginationCode
            ,CAST(NULL AS VARCHAR) AS LOB
            ,AccidentInd
            ,ThirdPartyLiabilityInd
            ,XrayEnclosureInd
            ,JobRelatedInd
            ,ReferenceBasedBenefitsInd
            ,ReferenceBasedBenefitsBundledInd
            ,NationalCareNetworkInd
            ,PriceAndShipInd
            ,PricingExclusionInd
            ,RepricingInd
            ,QualifiedHealthExpenseInd
            ,PersonalSavingsAccountNoncoveredInd
            ,MACReduceInd
            ,NetworkInd
            ,NetworkID
            ,PrePriceCode
            ,PatientMemberSourceID
            ,SubscriberMemberSourceID
            ,CoordinationOfBenefitsCode
            ,CoordinationOfBenefitsDesc
FROM
    ( SELECT * FROM
       (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
            FROM
        cambiastg_data_main.conn_unnested_Claim
        WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Facets-Claim-Extension'
       )
    WHERE RowNum = 1) cl
	INNER JOIN
                (
                   SELECT * FROM
                                ( SELECT
                                   correlationid,
                                   parentsourceid,
                                   identifier AS claimnumber,
                                   ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
                                   FROM cambiastg_data_main.conn_unnested_identifier
                                   WHERE

                                    parenttype = 'Claim' AND
                                    relationshipname = 'Identifiers' AND
                                    identifiertype IN ('stnd_rxclaimnbr', 'CLAIM_NUMBER','uc') AND
                                    CAST(partition_3 AS INTEGER) = replace_ingestionid AND
									source = 'Facets-Claim-Extension'
                                ) main
                                WHERE RowNum = 1
                ) id ON
                  cl.correlationid = id.correlationid and
                  cl.sourceid = id.parentsourceid
	LEFT JOIN
			(
					SELECT clcl_id,clcl_cl_sub_type,clcl_cur_sts,__lineage.ingestion_id
					FROM
					cambiastg_data_sources.icfsss_facets_claim_extension_header
					WHERE CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
			) src ON
			cl.sourceid = src.clcl_id AND
			cl.partition_3 = src.ingestion_id
);
