CREATE TABLE Payee_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,PayeeType
            ,PayeeIDCodeType
            ,PayeeIDCodeDesc
            ,ContactFunctionCode
            ,CommunicationType
            ,CommunicationNumber
            ,MemberSourceID
            ,ServiceProviderSourceID
            ,ServiceOrgSourceID
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,CASE
				WHEN src.clcl_id IS NULL THEN dm.PayeeType
			 ELSE src.clcl_pay_pr_ind
			 END AS PayeeType
            ,PayeeIDCodeType
            ,PayeeIDCodeDesc
            ,ContactFunctionCode
            ,CommunicationType
            ,CommunicationNumber
            ,MemberSourceID
            ,ServiceProviderSourceID
            ,ServiceOrgSourceID
            ,partition_3
            ,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Payee
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         ) dm
         LEFT JOIN ( 	SELECT clcl_id, trim(clcl_pay_pr_ind) as clcl_pay_pr_ind, __lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_claim_extension_header
						WHERE
						CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
         dm.parentsourceid = src.clcl_id and
         dm.partition_3 = src.ingestion_id
    )
    WHERE RowNum = 1
);
