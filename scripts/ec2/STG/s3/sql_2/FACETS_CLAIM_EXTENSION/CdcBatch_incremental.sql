CREATE TABLE CdcBatch_replacesuffix AS
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE, consumetimestamp, 
BATCHID, INGESTIONID FROM cambiastg_data_main.CONN_UNNESTED_CLAIM
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambiastg_data_main.CONN_UNNESTED_CLAIM WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid AND SOURCE = 'Facets-Claim-Extension'
  )
AND SOURCE = 'Facets-Claim-Extension'
ORDER BY 1,2;
