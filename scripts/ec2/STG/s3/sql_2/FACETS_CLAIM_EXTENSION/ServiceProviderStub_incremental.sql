CREATE TABLE ServiceProviderStub_replacesuffix
WITH
(
	bucket_count=10,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,IsAuthoritative
            ,Type
            ,NationalID
            ,NPI
            ,DEAID
            ,TIN
            ,Category
            ,SubCategory
            ,PracticeType
            ,ServiceProviderTier
            ,PCPInd
            ,EffDate
            ,ExpDate
            ,NetworkID
            ,BillingProviderInd
            ,NativeTribalAmericanInd
            ,ProviderParticipatingCode
            ,CareTeamInd
            ,ServiceProviderType
            ,ServiceProviderRole
            ,ServiceOrgName
            ,ServiceOrgNameType
            ,ServiceOrgID
            ,ServiceOrgIDType
            ,ServiceOrgNPI
            ,ServiceOrgDEAID
            ,ServiceOrgTIN
            ,FacilityCode
            ,FacilityName
            ,Specialty
			,RedirectedTo
			,RedirectStartDate
			,RedirectEndDate
			,RedirectInd
			,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,IsAuthoritative
            ,CASE
				WHEN UPPER(dm.ParentType) = 'CLAIM' THEN
					CASE
						WHEN LOWER(dm.ServiceProviderRole) =  'billing' AND dm_claim.prepricecode in ('H','T') AND dm_claim.claimcategory  = 'H' THEN 'Organization'
						WHEN LOWER(dm.ServiceProviderRole) =  'billing' AND dm_claim.prepricecode in ('H','T') AND dm_claim.claimcategory  <> 'H' THEN 'Practitioner'
                        WHEN LOWER(dm.ServiceProviderRole) =  'servicing' AND dm_claim.prepricecode in ('H','T') THEN 'ITS-' || COALESCE(src_mctr.mctr_desc,src.prpr_entity)
						WHEN src.prpr_id IS NOT NULL THEN COALESCE(src_mctr.mctr_desc,src.prpr_entity)
						ELSE dm.type

					END
				WHEN UPPER(dm.ParentType) = 'CLAIMITEM' THEN
					CASE
						WHEN src.prpr_id IS NOT NULL THEN COALESCE(src_mctr.mctr_desc,src.prpr_entity)
						ELSE dm.type
                    END
				ELSE dm.type
			 END AS Type
            ,NationalID
            ,NPI
            ,DEAID
            ,TIN
            ,Category
            ,SubCategory
            ,PracticeType
            ,ServiceProviderTier
            ,PCPInd
            ,EffDate
            ,ExpDate
            ,NetworkID
            ,BillingProviderInd
            ,NativeTribalAmericanInd
            ,ProviderParticipatingCode
            ,CareTeamInd
            ,ServiceProviderType
            ,LOWER(ServiceProviderRole) as ServiceProviderRole
            ,ServiceOrgName
            ,ServiceOrgNameType
            ,ServiceOrgID
            ,ServiceOrgIDType
            ,ServiceOrgNPI
            ,ServiceOrgDEAID
            ,ServiceOrgTIN
            ,FacilityCode
            ,FacilityName
            ,Specialty
			,RedirectedTo
			,RedirectStartDate
			,RedirectEndDate
			,RedirectInd
			,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_ServiceProviderStub
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid  and source = 'Facets-Claim-Extension'
         ) dm
		 LEFT JOIN (
						SELECT sourceid as claim_sourceid,trim(prepricecode) as prepricecode, trim(claimcategory) as claimcategory,  partition_3 as claim_partition_3
						FROM cambiastg_data_main.conn_unnested_claim
						WHERE
						CAST(partition_3 AS INTEGER) = replace_ingestionid  and source = 'Facets-Claim-Extension'

		 ) dm_claim ON
		 UPPER(dm.ParentType) = 'CLAIM' AND
		 dm.parentsourceid = dm_claim.claim_sourceid AND
		 dm.partition_3 = dm_claim.claim_partition_3
		 LEFT JOIN ( 	SELECT DISTINCT clcl_id,trim(clcl_payee_pr_id) as clcl_payee_pr_id, trim(prpr_id) as claim_prpr_id,__lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_claim_extension_header

						WHERE
						CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src_claim ON
		 dm.parentsourceid = src_claim.clcl_id AND
		 dm.partition_3 = src_claim.ingestion_id
         LEFT JOIN ( 	SELECT DISTINCT trim(prpr_id) as prpr_id,trim(prpr_entity) as prpr_entity,__lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_claim_extension_provider

						WHERE
						CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
					  (

							(
								LOWER(dm.ServiceProviderRole) =  'billing' AND
                                dm_claim.prepricecode NOT in ('H','T') AND
								SPLIT_PART(dm.sourceid,'-',2) = src.prpr_id

							 )
							 OR
							 (
								LOWER(dm.ServiceProviderRole) IN ('primary','performing','attending','referring','other','otheroperating','servicing') AND
								SPLIT_PART(dm.sourceid,'-',2) = src.prpr_id
							 )
						) and
						dm.partition_3 = src.ingestion_id
		LEFT JOIN (
						SELECT DISTINCT trim(mctr_entity) AS mctr_entity, trim(mctr_type) AS mctr_type, trim(mctr_value) AS mctr_value, trim(mctr_desc) AS mctr_desc,__lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_claim_extension_mctr_code_translations mctr
						WHERE CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid AND trim(mctr_entity) = '^PRP' and trim(mctr_type) = 'ENTY'
				  ) src_mctr ON
			 trim(src.prpr_entity) = trim(src_mctr.mctr_value) AND
			 src.ingestion_id = src_mctr.ingestion_id

    )
    WHERE RowNum = 1
);
