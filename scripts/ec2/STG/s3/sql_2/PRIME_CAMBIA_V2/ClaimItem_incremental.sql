CREATE TABLE ClaimItem_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
  SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,Sequence
            ,ClaimItemStatus
            ,Revenue
            ,RevenueCode
            ,Category
            ,ProductOrService
            ,ProductOrServiceDesc
            ,SPLIT_PART(dm.sourceid,'-',1) AS ProductOrServiceType
            ,CoordinationOfBenefitsCode
            ,CoordinationOfBenefitsDesc
            ,Modifier
            ,ProgramCode
            ,FirstDateOfService
            ,LastDateOfService
            ,PlaceOfService
            ,Quantity
            ,UnitType
            ,UnitPrice
            ,ItemCost
            ,ChargeAmount
            ,BodySite
            ,SubSite
            ,NetworkInd
            ,NetworkID
            ,partition_3
	        ,CAST(NULL AS DATE) AS IncurredDate
FROM
    (
        (
          SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        cambiastg_data_main.conn_unnested_ClaimItem line
        WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Prime-MCEF'
          ) dm
    )
    WHERE RowNum = 1
);
