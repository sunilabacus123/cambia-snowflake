CREATE TABLE Payee_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,PayeeType
            ,PayeeIDCodeType
            ,PayeeIDCodeDesc
            ,ContactFunctionCode
            ,CommunicationType
            ,CommunicationNumber
            ,MemberSourceID
            ,ServiceProviderSourceID
            ,ServiceOrgSourceID
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,CASE
				WHEN src.src_sourceid IS NULL THEN dm.PayeeType
			 ELSE src.reimbursmt
			 END AS PayeeType
            ,PayeeIDCodeType
            ,PayeeIDCodeDesc
            ,ContactFunctionCode
            ,CommunicationType
            ,CommunicationNumber
            ,MemberSourceID
            ,ServiceProviderSourceID
            ,ServiceOrgSourceID
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Payee
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         ) dm
         LEFT JOIN ( 	SELECT DISTINCT (rxclaimnbr || '-' || clmseqnbr || '-' || claimsts || '-' || coalesce(trim(akd9hs))) as src_sourceid,trim(reimbursmt) as reimbursmt, __lineage.ingestion_id
					FROM
					cambiastg_data_sources.prime_cambia_v2_standard
					WHERE CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
         dm.parentsourceid = src.src_sourceid and
         dm.partition_3 = src.ingestion_id
    )
    WHERE RowNum = 1
);
