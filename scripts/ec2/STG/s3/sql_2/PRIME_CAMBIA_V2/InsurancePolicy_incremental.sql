CREATE TABLE InsurancePolicy_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
						,ParentType
            ,Sequence
            ,PayerID
            ,PolicyID
            ,PolicyName
            ,PolicyHolderID
            ,SubscriberName
            ,SubscriberNumber
            ,GroupID
            ,GroupName
            ,SubGroupID
            ,SubGroupName
            ,SubscriberID
            ,AlternativeIdentifier
            ,AlternativeIdentifierType
            ,PlanID
            ,PlanName
            ,SubPlanID
            ,SubPlanName
            ,PharmacyRxGroup
            ,PharmacyRxBin
            ,PharmacyRxPCN
            ,BeneficiaryRelationshipType
            ,BeneficiaryName
            ,BeneficiaryID
            ,CoordinateWithPolicy
            ,CoverageMonths
            ,MemberType
            ,RelationshipType
            ,DependentMemberSequence
            ,Type
            ,ContractType
            ,ContractTypeSourceCode
            ,CarrierCode
            ,Status
            ,COBInd
            ,COBType
            ,COBCode
            ,COBCodeDesc
            ,EffDate
            ,ExpDate
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
			,ParentType
            ,Sequence
            ,PayerID
            ,PolicyID
            ,PolicyName
            ,PolicyHolderID
            ,SubscriberName
            ,SubscriberNumber
            ,GroupID
            ,GroupName
            ,SubGroupID
            ,SubGroupName
            ,SubscriberID
            ,AlternativeIdentifier
            ,AlternativeIdentifierType
            ,PlanID
            ,PlanName
            ,SubPlanID
            ,SubPlanName
            ,PharmacyRxGroup
            ,PharmacyRxBin
            ,PharmacyRxPCN
            ,CASE
				WHEN src.src_sourceid IS NULL THEN dm.BeneficiaryRelationshipType
				ELSE src.mbrrelcde
			 END AS BeneficiaryRelationshipType
            ,BeneficiaryName
            ,BeneficiaryID
            ,CoordinateWithPolicy
            ,CoverageMonths
            ,MemberType
			,CASE
				WHEN src.src_sourceid IS NULL THEN dm.RelationshipType
				ELSE src.mbrrelcde
			 END AS RelationshipType
            ,DependentMemberSequence
            ,Type
            ,ContractType
            ,ContractTypeSourceCode
            ,CarrierCode
            ,Status
            ,COBInd
            ,COBType
            ,COBCode
            ,COBCodeDesc
            ,EffDate
            ,ExpDate
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_InsurancePolicy
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         ) dm
          LEFT JOIN
			(
					SELECT DISTINCT (rxclaimnbr || '-' || clmseqnbr || '-' || claimsts || '-' || coalesce(trim(akd9hs))) as src_sourceid,trim(mbrrelcde) as mbrrelcde, __lineage.ingestion_id
					FROM
					cambiastg_data_sources.prime_cambia_v2_standard
					WHERE CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
			) src ON
			dm.parentsourceid = src.src_sourceid AND
			dm.partition_3 = src.ingestion_id

    )
    WHERE RowNum = 1
);
