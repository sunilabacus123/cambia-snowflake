CREATE TABLE Claim_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(

SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,dm.CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,ID AS claimnumber
            ,ClaimType
            ,SPLIT_PART(SourceID,'-',3) AS ClaimStatus
            ,CASE WHEN src.src_sourceid is not null then trim(clmrespsts)
             ELSE dm.ClaimResponseStatusCode
             END as ClaimResponseStatusCode
            ,ClaimCategory
            ,ClaimSubcategory
            ,PrescriptionReferenceID
            ,PrescriptionDateTime
            ,BillableStartDate
            ,BillableEndDate
            ,ClaimSubmittedDateTime
            ,ClaimReceivedDateTime
            ,ClaimIncurredDateTime
            ,ClaimAdjudicationDateTime
            ,ClaimPaidDateTime
            ,ClaimReversalDateTime
            ,ClaimLastActionDate
            ,ClaimAcceptedDate
            ,ClaimNextReviewDate
            ,StatementFromDate
            ,StatementToDate
            ,CapitationInd
            ,CapitationType
            ,CapitationTypeDesc
            ,ClaimSequenceNumber
            ,ClaimOriginationCode
            ,LOB
            ,AccidentInd
            ,ThirdPartyLiabilityInd
            ,XrayEnclosureInd
            ,JobRelatedInd
            ,ReferenceBasedBenefitsInd
            ,ReferenceBasedBenefitsBundledInd
            ,NationalCareNetworkInd
            ,PriceAndShipInd
            ,PricingExclusionInd
            ,RepricingInd
            ,QualifiedHealthExpenseInd
            ,PersonalSavingsAccountNoncoveredInd
            ,MACReduceInd
            ,NetworkInd
            ,NetworkID
            ,PrePriceCode
            ,PatientMemberSourceID
            ,SubscriberMemberSourceID
            ,CoordinationOfBenefitsCode
            ,CoordinationOfBenefitsDesc
FROM
    ( SELECT * FROM
       (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
            FROM
        cambiastg_data_main.conn_unnested_Claim
        WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Prime-MCEF'
       )
    WHERE RowNum = 1) dm
	INNER JOIN
                (
                   SELECT * FROM
                                ( SELECT
                                   correlationid,
                                   parentsourceid,
                                   identifier AS claimnumber,
                                   ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
                                   FROM cambiastg_data_main.conn_unnested_identifier
                                   WHERE

                                    parenttype = 'Claim' AND
                                    relationshipname = 'Identifiers' AND
                                    identifiertype IN ('stnd_rxclaimnbr', 'CLAIM_NUMBER','uc') AND
                                    CAST(partition_3 AS INTEGER) = replace_ingestionid AND
									source = 'Prime-MCEF'
                                ) main
                                WHERE RowNum = 1
                ) id ON
                  dm.correlationid = id.correlationid and
                  dm.sourceid = id.parentsourceid
	LEFT JOIN
			(
					SELECT DISTINCT (rxclaimnbr || '-' || clmseqnbr || '-' || claimsts || '-' || coalesce(trim(akd9hs))) as src_sourceid,clmrespsts, __lineage.ingestion_id
					FROM
					cambiastg_data_sources.prime_cambia_v2_standard
					WHERE CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
			) src ON
			dm.sourceid = src.src_sourceid AND
			dm.partition_3 = src.ingestion_id
);
