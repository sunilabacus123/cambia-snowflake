CREATE TABLE BaseDemographic_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
        SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Gender
            ,Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,CAST(NULL AS VARCHAR) AS Gender
            ,CASE
				WHEN src.src_sourceid IS NULL THEN dm.sex
				WHEN dm_mbr.dm_mbr_relationshipname = 'PatientMember' and src.src_sourceid IS NOT NULL THEN src.mbrsex
				WHEN dm_mbr.dm_mbr_relationshipname = 'SubscriberMember' and src.src_sourceid IS NOT NULL THEN src.abaast
			 ELSE dm.sex
			 END AS Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_BaseDemographic
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid AND source = 'Prime-MCEF'
         ) dm
		 LEFT JOIN ( 	SELECT sourceid as dm_mbr_sourceid, parentsourceid as dm_mbr_parentsourceid , correlationid as dm_mbr_correlationid, relationshipname as dm_mbr_relationshipname, partition_3 as dm_mbr_partition_3
						FROM cambiastg_data_main.conn_unnested_memberstub
						WHERE
						CAST(partition_3 AS INTEGER) = replace_ingestionid  AND source = 'Prime-MCEF' AND relationshipname in ( 'PatientMember','SubscriberMember')
					) dm_mbr  ON
         dm.correlationid = dm_mbr.dm_mbr_correlationid AND
         dm.parentsourceid = dm_mbr.dm_mbr_sourceid AND
		 dm.partition_3 = dm_mbr_partition_3
         LEFT JOIN ( 	SELECT DISTINCT (rxclaimnbr || '-' || clmseqnbr || '-' || claimsts || '-' || coalesce(trim(akd9hs))) as src_sourceid,trim(mbrsex) as mbrsex,trim(abaast) as abaast, __lineage.ingestion_id
					FROM
					cambiastg_data_sources.prime_cambia_v2_standard
					WHERE CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
         dm_mbr.dm_mbr_parentsourceid = src.src_sourceid and
         dm_mbr.dm_mbr_partition_3 = src.ingestion_id
    )
    WHERE RowNum = 1

);
