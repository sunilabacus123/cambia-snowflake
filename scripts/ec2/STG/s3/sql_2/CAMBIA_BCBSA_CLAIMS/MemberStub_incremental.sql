CREATE TABLE MemberStub_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,IsAuthoritative
            ,MemberType
            ,MemberCategory
            ,MemberCategoryID
            ,MemberCategoryPrefix
            ,dm_suppinfo_codevalue as SubscriberRelationship
            ,ParentGroupSourceID
            ,GroupSourceID
            ,SubGroupSourceID
            ,SubscriberID
            ,PlanID
            ,CarrierID
            ,HospiceInd
            ,InstitutionalInd
            ,ExchangeInd
            ,StateResidency
            ,Occupation
            ,HireDate
            ,MedicareHICNumber
            ,MedicareStartDate
            ,MedicaidID
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_MemberStub
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )dm
		LEFT JOIN (
		             SELECT sourceid as dm_suppinfo_sourceid, correlationid as dm_suppinfo_correlationid, partition_3 as dm_suppinfo_partition_3,
					        relationshipname as dm_suppinfo_relationshipname,codetype as dm_suppinfo_codetype,codevalue as dm_suppinfo_codevalue
						FROM cambiastg_data_main. conn_unnested_codedvalue  
						WHERE 
						CAST(partition_3 AS INTEGER) = replace_ingestionid
		            )dm_suppinfo ON
		  dm.correlationid = dm_suppinfo.dm_suppinfo_correlationid AND 
		  dm.partition_3 = dm_suppinfo.dm_suppinfo_partition_3 AND
		  dm_suppinfo.dm_suppinfo_relationshipname ='AdditionalInformationCode' AND
          dm_suppinfo.dm_suppinfo_codetype='Patient-Subscriber-Relationship'
    )
    WHERE RowNum = 1
);