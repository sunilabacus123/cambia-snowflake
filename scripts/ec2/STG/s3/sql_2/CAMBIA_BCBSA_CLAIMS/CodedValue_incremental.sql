CREATE TABLE CodedValue_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,CodeGroup
            ,CodeType
            ,CodeCategory
            ,CodeValue
            ,CodeValueType
            ,RangeMin
            ,RangeMax
            ,CodeID
            ,Display
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_CodedValue
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);