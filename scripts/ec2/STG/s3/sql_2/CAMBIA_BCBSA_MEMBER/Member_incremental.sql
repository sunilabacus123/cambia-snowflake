CREATE TABLE Member_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,CorrelationID
            ,Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,MemberLevel
            ,MemberType
            ,MemberCategory
            ,MemberCategoryID
            ,MemberCategoryPrefix
            ,dm_varfields_codevalue as SubscriberRelationship
            ,ParentGroupSourceID
            ,GroupSourceID
            ,SubGroupSourceID
            ,SubscriberID
            ,PlanID
            ,CarrierID
            ,OverrideCarrierID
            ,CoordinationOfBenefitsInd
            ,PersonCode
            ,Status
            ,HasPCP
            ,NationalID
            ,CBSACode
            ,ConsumerDrivenHealthcareInd
            ,PrescriptionCoverageInd
            ,MentalHealthCoverageInd
            ,MarketSegmentCode
            ,SICCode
            ,DualEligibilityInd
            ,EndStageRenalDiseaseInd
            ,HospiceInd
            ,InstitutionalInd
            ,ExchangeInd
            ,StateResidency
            ,IndustryCode
            ,Occupation
            ,EmployeeStatus
            ,HireDate
            ,MedicareHICNumber
            ,MedicareStartDate
            ,MedicareCoverageType
            ,MedicaidID
            ,EffDate
            ,ExpDate
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Member
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )dm
		 LEFT JOIN (
		             SELECT sourceid as dm_varfields_sourceid, correlationid as dm_varfields_correlationid, partition_3 as dm_varfields_partition_3,relationshipname as dm_varfields_relationshipname,codetype as dm_varfields_codetype,codevalue as dm_varfields_codevalue
						FROM cambiastg_data_main. conn_unnested_codedvalue  
						WHERE 
						CAST(partition_3 AS INTEGER) = replace_ingestionid
		            )dm_varfields ON
		  dm.correlationid = dm_varfields.dm_varfields_correlationid AND 
		  dm.partition_3 = dm_varfields.dm_varfields_partition_3 AND
		  dm_varfields.dm_varfields_relationshipname ='VariableFields' AND
          dm_varfields.dm_varfields_codetype='SUBSCRIBERRELCD'
    )
    WHERE RowNum = 1
);