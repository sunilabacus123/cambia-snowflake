CREATE TABLE HomeHost_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,InsuranceID
            ,InsuredFullName
            ,InsuredFirstName
            ,InsuredLastName
            ,InsuredMiddleName
            ,InsuredGender
            ,dm_varfields_codevalue as InsuredPatientRelationship
            ,HostPlanName
            ,HostPlanCode
            ,HomePlanName
            ,HomePlanCode
            ,HomeGroup
            ,InsuredBirthDate
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_HomeHost
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )dm
		 LEFT JOIN (
		             SELECT sourceid as dm_varfields_sourceid, correlationid as dm_varfields_correlationid, partition_3 as dm_varfields_partition_3,relationshipname as dm_varfields_relationshipname,codetype as dm_varfields_codetype,codevalue as dm_varfields_codevalue
						FROM cambiastg_data_main. conn_unnested_codedvalue  
						WHERE 
						CAST(partition_3 AS INTEGER) = replace_ingestionid
		            )dm_varfields ON
		  dm.correlationid = dm_varfields.dm_varfields_correlationid AND 
		  dm.partition_3 = dm_varfields.dm_varfields_partition_3 AND
		  dm_varfields.dm_varfields_relationshipname ='VariableFields' AND
          dm_varfields.dm_varfields_codetype='SUBSCRIBERRELCD'
    )
    WHERE RowNum = 1
);