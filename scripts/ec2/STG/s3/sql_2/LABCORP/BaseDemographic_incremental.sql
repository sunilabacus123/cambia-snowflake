CREATE TABLE BaseDemographic_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Gender
            ,Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,CAST(NULL AS VARCHAR) AS Gender
            ,CASE 
				WHEN dm_mbr.dm_mbr_parenttype = 'Observation' THEN
					 CASE WHEN src_obs.src_sourceid IS NULL THEN  NULLIF(dm.Sex,'UNK')
					 ELSE src_obs.sex
					 END
				WHEN dm_mbr.dm_mbr_parenttype = 'Order' THEN
					 CASE WHEN src_order.order_sourceid IS NULL THEN  NULLIF(dm.Sex,'UNK')
					 ELSE src_order.sex
					 END
				WHEN dm_mbr.dm_mbr_parenttype = 'Encounter' THEN
					 CASE WHEN src_encounter.encounter_sourceid IS NULL THEN  NULLIF(dm.Sex,'UNK')
					 ELSE src_encounter.sex
					 END
      
			 END as Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
			cambiastg_data_main.conn_unnested_BaseDemographic
			WHERE
			CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'LabCorp'
        ) dm
		  LEFT JOIN
		  (
			SELECT correlationid as dm_mbr_correlationid, sourceid as dm_mbr_sourceid, parentsourceid as dm_mbr_parentsourceid, parenttype as dm_mbr_parenttype, partition_3 as dm_mbr_partition_3
			FROM 
			cambiastg_data_main.conn_unnested_patientstub
			 WHERE
			CAST(partition_3 AS INTEGER) = replace_ingestionid  and source = 'LabCorp' 
		  ) dm_mbr ON
		  dm.correlationid = dm_mbr.dm_mbr_correlationid and
		  dm.parentsourceid = dm_mbr.dm_mbr_sourceid
		  LEFT JOIN
      (
        SELECT  
			(fillerordernumber || '-' || universalserviceid || '-' || universalcodingsystem || (CASE WHEN observationcode1 = '' OR observationcode1 IS NULL THEN '' ELSE '-' || observationcode1 END) || '-' || observationresultcode2 || '-' || observationcodetype2) as src_sourceid
			,(COALESCE((CASE WHEN LENGTH(trim(internalid)) = 12 AND REGEXP_LIKE (SUBSTRING(trim(internalid),4,9), '^[0-9][0-9]*$') THEN SUBSTRING(trim(internalid),4,9) WHEN LENGTH(trim(internalid)) = 13 AND REGEXP_LIKE (SUBSTRING(trim(internalid),5,9), '^[0-9][0-9]*$') THEN SUBSTRING(trim(internalid),5,9) WHEN LENGTH(trim(internalid)) = 9 AND REGEXP_LIKE (trim(internalid), '^[0-9][0-9]*$') THEN trim(internalid) ELSE trim(internalid) END),'') || '-' || COALESCE(dob,'') || '-' || COALESCE(substring(patient_first_name,1,4),'')) as mbr_sourceid
			,TRIM(sex) as sex
			,ingestion_id
       FROM 
       ( 	
		SELECT 
				DISTINCT obr_unnest.fillerordernumber
				,obr_unnest.universalserviceid.field_0 as universalserviceid
				,obr_unnest.universalserviceid.field_2  as  universalcodingsystem
				,obx_unnest.observationidentifier.field_0  as observationcode1
				,obx_unnest.observationidentifier.field_3  as observationresultcode2
				,obx_unnest.observationidentifier.field_5  as observationcodetype2
				,trim(obx_unnest.status) AS src_observationresult_status, __lineage.ingestion_id
				,pid[1].internalid as internalid
				,pid[1].dob as dob
				,COALESCE(SUBSTRING(pid[1].name.field_1,1,4),'') as patient_first_name
				,pid[1].sex as sex
        FROM cambiastg_data_sources.icfsss_labcorp_lab
        CROSS JOIN unnest (orc[1].obr) as ord(obr_unnest) 
        CROSS JOIN unnest (obr_unnest.obx) as obs(obx_unnest)        
        WHERE
        CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid 
        ) src_inner
       ) src_obs ON
        
			dm_mbr.dm_mbr_parentsourceid = src_obs.src_sourceid and
			trim(dm_mbr.dm_mbr_sourceid) = trim(src_obs.mbr_sourceid) and
			dm_mbr.dm_mbr_partition_3 = src_obs.ingestion_id
	 LEFT JOIN 
	 (
			SELECT 
				DISTINCT orc[1].fillerordernumber as order_sourceid
				, __lineage.ingestion_id
				,pid[1].internalid as internalid
				,pid[1].dob as dob
				,COALESCE(SUBSTRING(pid[1].name.field_1,1,4),'') as patient_first_name
				,pid[1].sex as sex
        FROM cambiastg_data_sources.icfsss_labcorp_lab
        WHERE
        CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid 
	 ) src_order ON
	 dm_mbr.dm_mbr_parentsourceid = src_order.order_sourceid and
	 dm_mbr.dm_mbr_partition_3 = src_order.ingestion_id
	 LEFT JOIN 
	 (
			SELECT 
				( COALESCE(patientclass,'') ||'-'|| COALESCE(fillerordernumber,'') ||'-'|| COALESCE((CASE WHEN provider_npi is null or provider_npi = '' THEN provider_upin else provider_npi end),'')) AS encounter_sourceid
				,ingestion_id
				,internalid
				,dob
				,patient_first_name
				,sex
			FROM 
			(
					SELECT 
						DISTINCT 
								pv1.patientclass
								,CASE WHEN (pv1.referringdoctor.field_12 = 'NPI') THEN pv1.referringdoctor.field_0 END AS provider_npi
								,CASE WHEN (pv1.referringdoctor.field_12 = 'UPIN') THEN pv1.referringdoctor.field_0 END AS provider_upin
								,orc[1].fillerordernumber  as fillerordernumber 
								, __lineage.ingestion_id
								,pid[1].internalid as internalid
								,pid[1].dob as dob
								,COALESCE(SUBSTRING(pid[1].name.field_1,1,4),'') as patient_first_name
								,pid[1].sex as sex
					FROM cambiastg_data_sources.icfsss_labcorp_lab
					WHERE
					CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid 
			) A
	 ) src_encounter ON
	 dm_mbr.dm_mbr_parentsourceid = src_encounter.encounter_sourceid and
	 dm_mbr.dm_mbr_partition_3 = src_encounter.ingestion_id
		
    )
    WHERE RowNum = 1
);