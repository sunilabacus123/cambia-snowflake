CREATE TABLE BaseDemographic_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
     SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Gender
            ,Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,CAST(NULL AS VARCHAR) AS Gender
            ,CASE WHEN trim(Sex) = 'F' THEN 'F'
				WHEN trim(Sex) = 'M' THEN 'M'
				WHEN trim(Sex) = 'U' THEN 'U'
				WHEN trim(Sex) = 'UNK' THEN CAST('U' AS VARCHAR) 
				ELSE Sex
			 END as Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
			cambiastg_data_main.conn_unnested_BaseDemographic
			WHERE
			CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'PAML'
        ) dm
		  
	 
    )
    WHERE RowNum = 1
);