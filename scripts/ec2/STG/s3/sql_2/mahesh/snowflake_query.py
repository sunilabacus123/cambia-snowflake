import json
import datetime
import io
import time
import sys
import os
import boto3
from boto3 import client
import pyarrow.parquet as pq
from pytz import timezone
import pytz
from botocore.exceptions import ClientError

env = 'cambiastg'



src_bucket_name = 'abacus-sandboxes-cambiastg'
dest_bucket_name = 'abacus-data-lake-cambiastg'
database = 'cambiastg_data_ops'
extract_name = "cambia_extract"
extract_loc = "cambia_transfer"
workgroup = 'cambiastg-dataops'

s3_conn   = client('s3')
currentDT = datetime.datetime.now()
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')


ath = boto3.client('athena',region_name='us-east-1')
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
glue = boto3.client('glue',region_name='us-east-1')

src_bucket = s3.Bucket(src_bucket_name)

extract_time = currentDT.strftime("%Y%m%d%H%M")

currentdt = datetime.datetime.now()
run_month = currentdt.strftime("%m")
run_date1 = currentdt.strftime("%d")
run_year = currentdt.strftime("%Y")
run_date2 = currentdt.strftime("%m/%d/%Y")

sql_path = 'data-ops/etl/cambia_extract/sql_2/mahesh/'

def drop_table(view_name):
    # drop table
    print('Dropping table...' + view_name)

    print("====================================\n")
    print(view_name)
    print("====================================\n")

    if view_name == 'Case':
        drop_query = 'drop table if exists ' + database + '.`' + view_name + '`'
    else:
        drop_query = 'drop table if exists ' + database + '.' + view_name
    print(drop_query)
    try:
        response = ath.start_query_execution(QueryString=drop_query, QueryExecutionContext={'Database': database},
                                             WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        print('Drop table query is running...')
        return str(query_execution_id)
    except ClientError as e:
        print('Query failed to submit' % e)

def create_table(view_name, column_name,sql_path,src_bucket_name):
    try:
        file_to_read = sql_path + view_name + '.sql'
        fileobj = s3client.get_object(Bucket=src_bucket_name, Key=file_to_read)
        filedata = fileobj['Body'].read()
        filecontents = filedata.decode("utf-8")

        create_query = filecontents.replace('replace_column',column_name)
    
        print("\n============ Creating table for "+view_name+" ================\n")
        print("Athena Query :"+create_query)

        response = ath.start_query_execution(QueryString=create_query,
                                             QueryExecutionContext={'Database': database}, WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        return query_execution_id
    except ClientError as e:
        print('Query failed to submit' % e)
        return 'null'


def job_status():
    print("Checking job status")
    while len(create_query_ids) > 0:
        response = ath.batch_get_query_execution(QueryExecutionIds=create_query_ids)
        execution_ids = response['QueryExecutions']
        for i in execution_ids:
            query_id = i['QueryExecutionId']
            status = i['Status']['State']
            if status == 'SUCCEEDED':
                create_query_ids.remove(query_id)
            elif status == 'FAILED':
                create_query_ids.remove(query_id)
                failed_create_query_id.append(query_id)
            else:
                print(query_id, status)
        time.sleep(5)


def copy_object(state, src_bucket, src_prefix, view_name, dest_prefix,extract_time):
    if state:
        if state == 'SUCCEEDED':
            time.sleep(5)
            num = 1
            for s3object in src_bucket.objects.filter(Prefix=src_prefix):
                srckey = s3object.key
                if not srckey.endswith('/'):
                    file_num = str(num)
                    fileName = view_name + '_' + extract_time + '_' + file_num + '.gz'
                    print(fileName)
                    destfilekey = dest_prefix + fileName
                    copysource = src_bucket_name + '/' + srckey
                    print('destination_: s3://'+dest_bucket_name+"/" + destfilekey)
                    print('Source: s3://' + copysource)
                    s3.Object(dest_bucket_name, destfilekey).copy_from(CopySource=copysource)
                
                    num = num + 1
            print('Data copy is completed...')
            return 'success'
        else:

            print('Data is not copied...')
            return 'failed'
    else:
        print('Query is not started executing...')
        return 'failed'

def s3_object_rowcount(dest_bucket_name, prefix, view_name, run_date2):

    try:
        response = s3client.list_objects(Bucket=dest_bucket_name, Prefix=prefix, MaxKeys=100)
        outputs = []
        objects = response["Contents"]
        for i in objects:
            file_name = i['Key']
            buffer = io.BytesIO()
            s3object = s3.Object(dest_bucket_name, file_name)
            s3object.download_fileobj(buffer)
            df = pq.ParquetFile(buffer)
            rowcount = df.metadata.num_rows
            print(file_name)
            f_name = file_name.split('/', 8)
            print(f_name)
            f_name1 = f_name[7]
            outputs.append( 'rdm'+ "," +run_date2 + ',' + view_name + ',' + 'refresh' + ',' + f_name1 + ',' + str(rowcount) )

    except KeyError:
        outputs.append('rdm'+ run_date2 + ',' + view_name + ',' + 'refresh' + ',' + 'No_File' + ',' + '0' )
        print('Cannot read s3 file')

    return outputs


def copy_files(view_name):
    if len(failed_create_query_id) > 0:
        print("Create table failed for some view, check athena history page")
        exit()
    else:
        print("\ncopy files.. Started")
        response = glue.get_table(DatabaseName=database, Name=view_name)
        table_dest = response['Table']['StorageDescriptor']['Location']
        f_name = table_dest.split('/', 6)
        table_loc = f_name[6]
        src_prefix = 'data-ops/athena-query-results/tables/' + table_loc 
        dest_prefix = 'data-views/' + extract_loc + '/'
        dest_extract_prefix = dest_prefix + 'rdm/' + view_name.lower() + '/'
        print(dest_extract_prefix+"\n")
        print(src_prefix+"\n")
        copy_state = copy_object('SUCCEEDED', src_bucket, src_prefix, view_name, dest_extract_prefix, extract_time)

reate_query_ids = []
failed_create_query_id = []


changecolumns = open("changecolumns.txt", "r")

for column_name in changecolumns:
    column_name = column_name.strip()
    view_name = 'athenaextract'
    create_query_ids = []
    failed_create_query_id = []
    create_query_ids.append(create_table(view_name,column_name,sql_path,src_bucket_name))
    job_status()
    copy_files(view_name)    
    drop_table(view_name)
    time.sleep(30)
changecolumns.close()
