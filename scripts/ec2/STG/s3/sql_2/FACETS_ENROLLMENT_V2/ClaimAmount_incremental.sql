CREATE TABLE ClaimAmount_replacesuffix
WITH
(
	bucket_count=5,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,ClaimAmountCategory
            ,ClaimAmountType
            ,ClaimAmountCode
            ,ClaimAmountDescription
            ,ClaimAmountDate
            ,ClaimAmount
            ,PartyType
            ,PartyResourceType
            ,PartyID
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_ClaimAmount
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
