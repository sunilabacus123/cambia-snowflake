CREATE TABLE MemberStub_replacesuffix
WITH
(
	bucket_count=3,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,IsAuthoritative
            ,MemberType
            ,MemberCategory
            ,MemberCategoryID
            ,MemberCategoryPrefix
            ,SubscriberRelationship
            ,ParentGroupSourceID
            ,GroupSourceID
            ,SubGroupSourceID
            ,SubscriberID
            ,PlanID
            ,CarrierID
            , HospiceInd
            , InstitutionalInd
            , ExchangeInd
            , StateResidency
            , Occupation
            ,HireDate
            ,MedicareHICNumber
            ,MedicareStartDate
            ,MedicaidID
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,IsAuthoritative
            ,MemberType
            ,MemberCategory
            ,MemberCategoryID
            ,MemberCategoryPrefix
            ,CASE
				WHEN src.meme_ck IS NULL THEN dm.SubscriberRelationship
				ELSE src.meme_rel
			 END AS SubscriberRelationship
            ,ParentGroupSourceID
            ,GroupSourceID
            ,SubGroupSourceID
            ,SubscriberID
            ,PlanID
            ,CarrierID
            , HospiceInd
            , InstitutionalInd
            , ExchangeInd
            , StateResidency
            , Occupation
            ,HireDate
            ,MedicareHICNumber
            ,MedicareStartDate
            ,MedicaidID
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_MemberStub
     WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Facets-Enrollment'
         ) dm
         LEFT JOIN ( 	SELECT DISTINCT trim(meme_ck) as meme_ck, trim(meme_rel) as meme_rel,__lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_enrollment_v2_enrl
						WHERE
						CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
         dm.sourceid = src.meme_ck and
         dm.partition_3 = src.ingestion_id
    )
    WHERE RowNum = 1
);
