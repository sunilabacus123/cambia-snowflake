CREATE TABLE CdcBatch_replacesuffix AS
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE, consumetimestamp, 
BATCHID, INGESTIONID FROM cambiastg_data_main.CONN_UNNESTED_ENROLLMENT
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambiastg_data_main.CONN_UNNESTED_ENROLLMENT WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Facets-Enrollment'
  )
AND SOURCE = 'Facets-Enrollment'
ORDER BY 1,2
;
