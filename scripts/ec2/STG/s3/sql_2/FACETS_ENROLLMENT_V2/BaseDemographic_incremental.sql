CREATE TABLE BaseDemographic_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Gender
            ,Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
FROM
    (
        SELECT
			 CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,CASE
				WHEN src.meme_ck IS NULL THEN dm.gender
				ELSE src.meme_mctr_genp_nvl
			 END AS Gender
            ,CASE
				WHEN src.meme_ck IS NULL THEN dm.sex
				ELSE src.meme_sex
			 END AS Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
            ,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_basedemographic
		WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Facets-Enrollment'
         ) dm
         LEFT JOIN ( 	SELECT DISTINCT trim(meme_ck) as meme_ck, trim(meme_sex) as meme_sex, trim(meme_mctr_genp_nvl) as meme_mctr_genp_nvl ,__lineage.ingestion_id
						FROM cambiastg_data_sources.icfsss_facets_enrollment_v2_enrl
						WHERE
						CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
					) src  ON
         dm.sourceid = src.meme_ck and
         dm.partition_3 = src.ingestion_id


    )
    WHERE RowNum = 1
);
