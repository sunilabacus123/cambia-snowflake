CREATE TABLE DrugUtilizationReview_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ReviewType
            ,Severity
            ,Intervention
            ,Outcome
            ,LevelOfService
            ,OtherPharmacyCode
            ,PrescriptionLastDispenseDate
            ,PreviousQuantity
            ,OtherPrescriberCode
            ,Message
            ,DataSource
            ,Sequence
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_DrugUtilizationReview
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
