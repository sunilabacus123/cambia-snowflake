CREATE TABLE ClaimAmount_replacesuffix
WITH
(
	bucket_count=10,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,ClaimAmountCategory
            ,ClaimAmountType
            ,ClaimAmountCode
            ,ClaimAmountDescription
            ,ClaimAmountDate
            ,ClaimAmount
            ,PartyType
            ,PartyResourceType
            ,PartyID
            ,partition_3
FROM
    (
         select a.*
          FROM (
            SELECT
                *
            FROM
            cambiastg_data_main.conn_unnested_ClaimAmount a
            WHERE
            CAST(partition_3 AS INTEGER) = replace_ingestionid
        )a
        left join 
          (
              select hashid,max(correlationid) as correlationid
              FROM cambiastg_data_main.conn_unnested_ClaimAmount
              WHERE
              CAST(partition_3 AS INTEGER) =  replace_ingestionid 
              group by hashid having count(1)>1  
          ) b on 
          a.correlationid = b.correlationid 
          WHERE b.correlationid  IS NULL
    )
    UNION ALL
  select   a.CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,a.HashID
            ,ParentType
            ,ClaimAmountCategory
            ,ClaimAmountType
            ,ClaimAmountCode
            ,ClaimAmountDescription
            ,ClaimAmountDate
            ,ClaimAmount
            ,PartyType
            ,PartyResourceType
            ,PartyID
            ,partition_3
          FROM (
            SELECT
                *
            FROM
            cambiastg_data_main.conn_unnested_ClaimAmount a
            WHERE
            CAST(partition_3 AS INTEGER) = replace_ingestionid
        )a
        join 
          (
              select hashid,max(correlationid) as correlationid
              FROM cambiastg_data_main.conn_unnested_ClaimAmount
              WHERE
              CAST(partition_3 AS INTEGER) =  replace_ingestionid 
              group by hashid having count(1)>1  
          ) b on 
          a.correlationid = b.correlationid 
          group by   a.CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,a.HashID
            ,ParentType
            ,ClaimAmountCategory
            ,ClaimAmountType
            ,ClaimAmountCode
            ,ClaimAmountDescription
            ,ClaimAmountDate
            ,ClaimAmount
            ,PartyType
            ,PartyResourceType
            ,PartyID
            ,partition_3
)