CREATE TABLE Claim_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,cl.CorrelationID
            ,Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,id.claimnumber
            ,ClaimType
            ,ClaimStatus
            ,ClaimResponseStatusCode
            ,ClaimCategory
            ,ClaimSubcategory
            ,PrescriptionReferenceID
            ,PrescriptionDateTime
            ,BillableStartDate
            ,BillableEndDate
            ,ClaimSubmittedDateTime
            ,ClaimReceivedDateTime
            ,ClaimIncurredDateTime
            ,ClaimAdjudicationDateTime
            ,ClaimPaidDateTime
            ,ClaimReversalDateTime
            ,ClaimLastActionDate
            ,ClaimAcceptedDate
            ,ClaimNextReviewDate
            ,StatementFromDate
            ,StatementToDate
            ,CapitationInd
            ,CapitationType
            ,CapitationTypeDesc
            ,ClaimSequenceNumber
            ,ClaimOriginationCode
            ,LOB
            ,AccidentInd
            ,ThirdPartyLiabilityInd
            ,XrayEnclosureInd
            ,JobRelatedInd
            ,ReferenceBasedBenefitsInd
            ,ReferenceBasedBenefitsBundledInd
            ,NationalCareNetworkInd
            ,PriceAndShipInd
            ,PricingExclusionInd
            ,RepricingInd
            ,QualifiedHealthExpenseInd
            ,PersonalSavingsAccountNoncoveredInd
            ,MACReduceInd
            ,NetworkInd
            ,NetworkID
            ,PrePriceCode
            ,PatientMemberSourceID
            ,SubscriberMemberSourceID
            ,CoordinationOfBenefitsCode
            ,CoordinationOfBenefitsDesc
            ,partition_3
FROM
    ( SELECT * FROM
       (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
            FROM
        cambiastg_data_main.conn_unnested_Claim
        WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
       )
    WHERE RowNum = 1) cl
    INNER JOIN
                (
                   SELECT * FROM
                                ( SELECT
                                   correlationid,
                                   parentsourceid,
                                   identifier AS claimnumber,
                                   ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
                                   FROM cambiastg_data_main.conn_unnested_identifier
                                   WHERE

                                    parenttype = 'Claim' AND
                                    relationshipname = 'Identifiers' AND
                                    identifiertype IN ('stnd_rxclaimnbr', 'CLAIM_NUMBER')
                                    and CAST(partition_3 AS INTEGER) = replace_ingestionid
                                ) main
                                WHERE RowNum = 1
                ) id ON
                  cl.correlationid = id.correlationid and
                  cl.sourceid = id.parentsourceid
);
