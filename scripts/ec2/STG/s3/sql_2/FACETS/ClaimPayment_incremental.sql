CREATE TABLE ClaimPayment_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,PaymentID
            ,AlternatePaymentID
            ,AlternativePaymentIDType
            ,FinancialPlanCode
            ,CustomerPlanCode
            ,AccountID
            ,PaymentDate
            ,PreIssuedCheckNumber
            ,PreIssuedCheckDate
            ,PaymentSequenceNumber
            ,PaymentIssueDate
            ,PreIssuedPaymentCode
            ,CheckStatus
            ,TotalClaimChargeAmount
            ,BilledAmount
            ,AllowedAmount
            ,PaidAmount
            ,TotalClaimRemittanceAmount
            ,PatientResponsibilityAmount
            ,IRSWithholdingAmount
            ,HeldAmount
            ,TotalInterestAmount
            ,AdditionalPaymentAmount
            ,CheckAmount
            ,CheckAmountAppliedToClaim
            ,CashControlNumber
            ,PaymentPostingDate
            ,PaymentStatusCode
            ,PaymentStatusDate
            ,PaymentCycleDate
            ,PaymentType
            ,IRSWithholdingPercent
            ,CapitationInd
            ,PurgeInd
            ,SupersetCode
            ,GroupSetCode
            ,FundingSetCode
            ,BankName
            ,ClassControlNumber
            ,PayeeDirection
            ,TransactionCode
            ,TransactionDesc
            ,PaymentMethodCode
            ,PaymentMethodCodeDesc
            ,AccountNumber
            ,ReferenceIDType
            ,ReferenceID
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_ClaimPayment
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
