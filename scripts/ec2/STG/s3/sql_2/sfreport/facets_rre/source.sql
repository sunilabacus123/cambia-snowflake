CREATE TABLE source_replacesuffix
as
select
cntrl_id,
extract_from_dttm,
extract_to_dttm,
prcs_view_name,
src_db_name,
load_type,
environment,
file_name,
db_object_database,
db_object_schema,
db_object_name,
prcs_key_identifier,
file_records,
btch_exec_id,
extract_part_key,
job_attempt,
prcs_fr_dttm,
prcs_end_dttm,
job_status,
btch_partition_num,
data_return_start_dttm,
data_return_end_dttm,
base_object_flag,
full_table_flag,
file_partition_records,
keyid_records,
auto_prcs_conclusion,
row_filter_flag,
batch_partition_flag,
last_partition_flag,
sftp_flag,
dynamic_join_w_uk_lst_flag,
rec_cr_dttm
from cambiastg_data_ops.abc
where
file_name not like
'CDC%'
and src_db_name = 'FACETS'
and prcs_view_name = 'RRE'
and cntrl_id in
(
select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_cmc_cscs_class where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid       union select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_cmc_grgr_group where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid union select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_cmc_pagr_parent_gr where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid union select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_cmc_plds_plan_desc where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid union  select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_cmc_sgsg_sub_group where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid union  select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_trgt_ede_cspi_ext where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid union  select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_trgt_ede_devl1 where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid union select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_trgt_ede_grgr_ext where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid union  select distinct __lineage.batch_id from cambiastg_data_sources.facets_rre_trgt_ede_sbsb_ext where CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
);
