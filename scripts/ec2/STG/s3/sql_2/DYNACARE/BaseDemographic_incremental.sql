CREATE TABLE BaseDemographic_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Gender
            ,Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
FROM
    (
        SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,CAST(NULL AS VARCHAR) AS Gender
            ,CASE WHEN src_obs.src_sourceid IS NULL THEN  NULLIF(dm.Sex,'UNK')
					 ELSE src_obs.sex
			END AS Sex
            ,Race
            ,Religion
            ,Ethnicity
            ,MaritalStatus
            ,Nationality
            ,partition_3
			,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_BaseDemographic
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Dynacare'
         ) dm
		   LEFT JOIN
		  (
			SELECT correlationid as dm_mbr_correlationid, sourceid as dm_mbr_sourceid, parentsourceid as dm_mbr_parentsourceid, parenttype as dm_mbr_parenttype, partition_3 as dm_mbr_partition_3
			FROM 
			cambiastg_data_main.conn_unnested_patientstub
			 WHERE
			CAST(partition_3 AS INTEGER) = replace_ingestionid  and source = 'Dynacare' 
		  ) dm_mbr ON
		  dm.correlationid = dm_mbr.dm_mbr_correlationid and
		  dm.parentsourceid = dm_mbr.dm_mbr_sourceid
		  LEFT JOIN
      (
        SELECT
					(trim(coalesce(CAST(pid AS VARCHAR),'')||'-'||coalesce(CAST(dos AS VARCHAR),'')||'-'||coalesce(CAST(accession AS VARCHAR),'')||'-'||coalesce(CAST(tcnum AS VARCHAR),'')||'-'||coalesce(CAST(oucnum AS VARCHAR),'')||'-'||coalesce(CAST(uccnum AS VARCHAR),'')||'-'||coalesce(CAST(policy_number AS VARCHAR),'')||'-'||sex)) AS src_sourceid,
					(COALESCE((CASE WHEN LENGTH(policy_number) = 12 AND REGEXP_LIKE (SUBSTRING(policy_number,4,9), '^[0-9][0-9]*$') THEN SUBSTRING(policy_number,4,9) WHEN LENGTH(policy_number) = 13 AND REGEXP_LIKE (SUBSTRING(policy_number,5,9), '^[0-9][0-9]*$') THEN SUBSTRING(policy_number,5,9) WHEN LENGTH(policy_number) = 9 AND REGEXP_LIKE (policy_number, '^[0-9][0-9]*$') THEN policy_number ELSE policy_number END),'')||'-'||COALESCE(CAST(to_date((Substring(split_part(dob,',',2),1,10)),'mm/dd/yyyy') AS VARCHAR),'')||'-'||COALESCE(SUBSTRING((trim(split_part(split_part(patientname,',',2),' ',1))),1,4),'')) AS mbr_sourceid,
					sex,
					ingestion_id
				FROM
				(
				
						SELECT DISTINCT
							  "dos"
							, "accession"
							, "patientname"
							, "pid"
							,  (CASE WHEN mrn = 'NOT GIVEN' or mrn = 'CKOJNE' or lower(mrn) = 'unknown'
										 or mrn='000-00-0000' or mrn='0000000'  or mrn='000000000' THEN ''
							ELSE mrn END) as mrn
							, "policy_number"
							, (CASE WHEN ssn = 'NOT GIVEN' or ssn = 'CKOJNE' or lower(ssn) = 'unknown'
										 or ssn='000-00-0000' or ssn='0000000'  or ssn='000000000' THEN ''
							ELSE ssn END) as ssn
							, "dob"
							, "sex"
							, "oucnum"
							, "oucnam"
							, "uccnum"
							, "uccnam"
							, "tcnum"
							, "tcnam"
							, "cptcodes"
							, "icd9"
							, "results"
							, "unit_measure"
							, "flags"
							, "canned_msg"
							, "clientname"
							, "clientnum"
							, "phone"
							, "adr1"
							, "adr2"
							, "adr3"
							, "adr4"
							, __lineage.ingestion_id as ingestion_id


							
							FROM
							cambiastg_data_sources.icfsss_dynacare_lab
							 WHERE
							CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
							GROUP BY "dos", "accession", "patientname", "pid", "mrn", "policy_number", "ssn", "dob", "sex", "oucnum", "oucnam", "uccnum", "uccnam", "tcnum", "tcnam", "cptcodes", "icd9", "results", "unit_measure", "flags", "canned_msg", "clientname", "clientnum", "phone", "adr1", "adr2", "adr3", "adr4", __lineage.ingestion_id
					) src_inner
       ) src_obs ON
        
			dm_mbr.dm_mbr_parentsourceid = src_obs.src_sourceid and
			dm_mbr.dm_mbr_sourceid = src_obs.mbr_sourceid and
			dm_mbr.dm_mbr_partition_3 = src_obs.ingestion_id
    )
    WHERE RowNum = 1
);