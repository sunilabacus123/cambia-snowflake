CREATE TABLE CdcBatch_replacesuffix AS
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE, consumetimestamp, 
BATCHID, INGESTIONID FROM cambiastg_data_main.CONN_UNNESTED_ORDER
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambiastg_data_main.CONN_UNNESTED_ORDER WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid AND SOURCE = 'Quest-Lab'
  )
AND SOURCE = 'Quest-Lab'
ORDER BY 1,2;
