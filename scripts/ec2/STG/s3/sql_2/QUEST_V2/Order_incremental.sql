CREATE TABLE Order_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,ParentOrderID
            ,Type
            ,Status
            ,OrderDateTime
            ,OrderEffDateTime
            ,OrderControlID
            ,OrderControlCodesetType
            ,OrderControlCodeset
            ,AlternateControlID
            ,AlternateCodesetType
            ,AlternateControlCodeset
            ,OrderControlCode
            ,EnteringDevice
            ,PlacerGroupNumber
            ,ResponseFlag
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Order
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
