CREATE TABLE Observation_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(


SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,CorrelationID
            ,'replace_source_value' as Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,ObservationSubID
            ,Sequence
            ,Reason
            ,ReasonID
            ,Context
            ,ContextID
            ,Category
            ,Type
            ,Status
            ,UniversalServiceID
            ,UniversalServiceText
            ,UniversalCodingSystem
            ,AlternateServiceID
            ,AlternateServiceText
            ,AlternateCodingSystem
            ,Focus
            ,ObservationUnits
            ,ObservationResultStatus
            ,ObservationScheduledDateTime
            ,ObservationStartDateTime
            ,ObservationEndDateTime
            ,ObservationIssuedDateTime
            ,ObservationMethod
            ,ObservationEquipment
            ,NumberSampleContainers
            ,CollectionVolume
            ,CollectionVolumeUnitType
            ,CollectionIdentifier
            ,CollectorName
            ,SourceTable
            ,AssigningAuthority
            ,NameTypeCode
            ,CheckDigit
            ,CheckDigitScheme
            ,IDTypeCode
            ,AssigningFacility
            ,DataAbsentReason
            ,AbnormalInd
            ,AbnormalLevel
            ,Probability
            ,NatureOfAbnormalTest
            ,UserDefinedAccessChecks
            ,ProducerID
            ,partition_3
FROM
    (
        SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,CorrelationID
            ,Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,ObservationSubID
            ,Sequence
            ,Reason
            ,ReasonID
            ,Context
            ,ContextID
            ,Category
            ,Type
            ,Status
            ,UniversalServiceID
            ,UniversalServiceText
            ,UniversalCodingSystem
            ,AlternateServiceID
            ,AlternateServiceText
            ,AlternateCodingSystem
            ,Focus
            ,ObservationUnits
            ,CASE WHEN src.src_sourceid is NOT NULL THEN src.src_observationresult_status
            ELSE dm.ObservationResultStatus
            END AS ObservationResultStatus
            ,ObservationScheduledDateTime
            ,ObservationStartDateTime
            ,ObservationEndDateTime
            ,ObservationIssuedDateTime
            ,ObservationMethod
            ,ObservationEquipment
            ,NumberSampleContainers
            ,CollectionVolume
            ,CollectionVolumeUnitType
            ,CollectionIdentifier
            ,CollectorName
            ,SourceTable
            ,AssigningAuthority
            ,NameTypeCode
            ,CheckDigit
            ,CheckDigitScheme
            ,IDTypeCode
            ,AssigningFacility
            ,DataAbsentReason
            ,AbnormalInd
            ,AbnormalLevel
            ,Probability
            ,NatureOfAbnormalTest
            ,UserDefinedAccessChecks
            ,ProducerID
            ,partition_3
      ,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Observation
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'Quest-Lab'
         ) dm
      LEFT JOIN
      (
        SELECT  (fillerordernumber || '-' || universalserviceid || '-' || universalcodingsystem || (CASE WHEN observationcode1 = '' OR observationcode1 IS NULL THEN '' ELSE '-' || observationcode1 END) || '-' || observationresultcode2 || '-' || observationcodetype2) as src_sourceid,
				src_observationresult_status,
				ingestion_id
        FROM
       (
			SELECT
				 obr_unnest.fillerordernumber
				,obr_unnest.universalserviceid.field_0 as universalserviceid
				, obr_unnest.universalserviceid.field_2  as  universalcodingsystem
				, obx_unnest.observationidentifier.field_0  as observationcode1
				, obx_unnest.observationidentifier.field_3  as observationresultcode2
				, obx_unnest.observationidentifier.field_5  as observationcodetype2
				, trim(obx_unnest.status) AS src_observationresult_status,
				__lineage.ingestion_id
			FROM cambiastg_data_sources.icfsss_quest_v2_lab
			CROSS JOIN unnest (orc[1].obr) as ord(obr_unnest)
			CROSS JOIN unnest (obr_unnest.obx) as obs(obx_unnest)
			WHERE
			CAST(__lineage.ingestion_id AS INTEGER) = replace_ingestionid
        )

      ) src ON
      dm.sourceid = src.src_sourceid and
      dm.partition_3 = src.ingestion_id
    )
    WHERE RowNum = 1
);
