CREATE TABLE cambiastg_data_ops.county_gaf_cms_replace_date as
SELECT
"carrier" as carrier
,"state" as state
,"locality number" as locality_number
,"medicare locality" as medicare_locality
,"2019 gaf" as "2019_gaf"
,"2020 gaf-with 1.0 work floor" as "2020_gaf_with_1_work_floor"
,"2019-2020 percent change-with 1.0 work floor" as "2019_2020_percent_change_with_1_work_floor"
,"2020 gaf-without  1.0 work floor" as "2020_gaf_without__1_work_floor"
,"2019-2020 percent change-without 1.0 work floor" as "2019_2020_percent_change_without_1_work_floor"
,"2021 gaf" as "2021_gaf"
,"2019-2021 percent change" as "2019_2021_percent_change"
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.county_gaf_cms;
