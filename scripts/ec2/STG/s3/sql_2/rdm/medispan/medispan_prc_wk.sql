CREATE TABLE cambiastg_data_ops.medispan_prc_wk_replace_date as
SELECT
"ndc-upc-hri" as ndc_upc_hri
,"price code" as price_code
,"price effective date" as price_effective_date
,"unit price" as unit_price
,"extended unit price" as extended_unit_price
,"package price" as package_price
,"awp indicator code" as awp_indicator_code
,"transaction cd" as transaction_cd
,"last change date" as last_change_date
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.medispan_prc_wk;
