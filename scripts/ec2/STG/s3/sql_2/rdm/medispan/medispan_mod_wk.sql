CREATE TABLE cambiastg_data_ops.medispan_mod_wk_replace_date as
SELECT
"modifier code" as modifier_code
,"modifier description" as modifier_description
,"transaction cd" as transaction_cd
,"last change date" as last_change_date
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.medispan_mod_wk;
