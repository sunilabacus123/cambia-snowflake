CREATE TABLE cambiastg_data_ops.medispan_ndcm_wk_replace_date as
SELECT
"ndc-upc-hri" as ndc_upc_hri
,"modifier code" as modifier_code
,"transaction cd" as transaction_cd
,"last change date" as last_change_date
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.medispan_ndcm_wk;
