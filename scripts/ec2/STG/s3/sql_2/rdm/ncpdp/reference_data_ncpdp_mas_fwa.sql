CREATE TABLE cambiastg_data_ops.reference_data_ncpdp_mas_fwa_replace_date as
SELECT
"1_ncpdpproviderid" as ncpdpproviderid
,"2_medicaidpartd" as medicaidpartd
,"3_fwaattestation" as fwaattestation
,"4_versionnumber" as versionnumber
,"5_planyear" as planyear
,"6_q1" as q1
,"7_q2" as q2
,"8_accreditationdate" as accreditationdate
,"9_accreditationorganization" as accreditationorganization
,"10_q3" as q3
,"11_q4" as q4
,"12_signatureofresponsibleparty" as signatureofresponsibleparty
,"13_signaturedate" as signaturedate
,"14_responsibleparty" as responsibleparty
,"15_participatingpharmacyorpsaoname" as participatingpharmacyorpsaoname
,"16_address1" as address1
,"17_address2" as address2
,"18_city" as city
,"19_statecode" as statecode
,"20_zipcode" as zipcode
,"21_npi" as npi
,"22_fax" as fax
,"23_email" as email
from  cambiastg_data_sources.reference_data_ncpdp_mas_fwa
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
