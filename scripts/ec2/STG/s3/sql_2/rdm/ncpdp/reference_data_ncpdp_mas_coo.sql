CREATE TABLE cambiastg_data_ops.reference_data_ncpdp_mas_coo_replace_date as
SELECT
"1_ncpdpproviderid" as ncpdpproviderid
,"2_oldncpdpproviderid" as oldncpdpproviderid
,"3_oldstoreclosedate" as oldstoreclosedate
,"4_changeofownershipeffectivedate" as changeofownershipeffectivedate
from  cambiastg_data_sources.reference_data_ncpdp_mas_coo
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
