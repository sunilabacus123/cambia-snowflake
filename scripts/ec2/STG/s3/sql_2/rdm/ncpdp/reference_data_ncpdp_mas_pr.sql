CREATE TABLE cambiastg_data_ops.reference_data_ncpdp_mas_pr_replace_date as
SELECT
"1_parentorganizationid" as parentorganizationid
,"2_parentorganizationname" as parentorganizationname
,"3_address1" as address1
,"4_address2" as address2
,"5_city" as city
,"6_statecode" as statecode
,"7_zipcode" as zipcode
,"8_phonenumber" as phonenumber
,"9_extension" as extension
,"10_faxnumber" as faxnumber
,"11_parentorganizationnpi" as parentorganizationnpi
,"12_parentorganizationfederaltaxid" as parentorganizationfederaltaxid
,"13_contactname" as contactname
,"14_contacttitle" as contacttitle
,"15_e_mailaddress" as e_mailaddress
,"16_deletedate" as deletedate
from  cambiastg_data_sources.reference_data_ncpdp_mas_pr
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
