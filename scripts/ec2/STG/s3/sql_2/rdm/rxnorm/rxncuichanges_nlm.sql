CREATE TABLE cambiastg_data_ops.rxncuichanges_nlm_replace_date as
SELECT
"atom_id" as atom_id
,"code" as code
,"concept_source_abb" as concept_source_abb
,"term_type" as term_type
,"string" as string
,"old_concept_id" as old_concept_id
,"new_concept_id" as new_concept_id
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.rxncuichanges_nlm;
