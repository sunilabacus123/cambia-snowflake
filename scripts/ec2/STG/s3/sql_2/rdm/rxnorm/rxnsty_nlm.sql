CREATE TABLE cambiastg_data_ops.rxnsty_nlm_replace_date as
SELECT
"concept_id" as concept_id
,"semantic_id" as semantic_id
,"semantic_tree_num" as semantic_tree_num
,"semantic_type" as semantic_type
,"content_view_flag" as content_view_flag
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.rxnsty_nlm;
