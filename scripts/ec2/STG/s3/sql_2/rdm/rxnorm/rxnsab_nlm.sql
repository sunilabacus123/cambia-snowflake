CREATE TABLE cambiastg_data_ops.rxnsab_nlm_replace_date as
SELECT
"version_concept_id" as version_concept_id
,"root_concept_id" as root_concept_id
,"version_source_abb" as version_source_abb
,"root_source_abb" as root_source_abb
,"source_name" as source_name
,"source_family" as source_family
,"source_version" as source_version
,"efft_dt" as efft_dt
,"insert_version" as insert_version
,"source_lic_content" as source_lic_content
,"source_content_contact" as source_content_contact
,"source_restrict_lvl" as source_restrict_lvl
,"contxt_type" as contxt_type
,"term_type_list" as term_type_list
,"attribute_name_list" as attribute_name_list
,"language" as language
,"encode" as encode
,"version_flag" as version_flag
,"subset_flag" as subset_flag
,"source_short_name" as source_short_name
,"source_citation" as source_citation
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.rxnsab_nlm;
