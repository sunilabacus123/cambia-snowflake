CREATE TABLE ServiceOrganizationStub_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,IsAuthoritative
            ,NPI
            ,DEAID
            ,TIN
            ,GroupPracticeInd
            ,NetworkInd
            ,Active
            ,OrgRelationshipType
            ,EffDate
            ,ExpDate
            ,RedirectedTo
            ,RedirectStartDate
            ,RedirectEndDate
            ,RedirectInd
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_ServiceOrganizationStub
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);