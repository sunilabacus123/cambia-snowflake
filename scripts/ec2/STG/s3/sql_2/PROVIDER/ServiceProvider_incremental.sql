CREATE TABLE ServiceProvider_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,CorrelationID
            ,Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,Type
            ,NationalID
            ,NPI
            ,DEAID
            ,TIN
            ,Category
            ,SubCategory
            ,PracticeType
            ,HCPInd
            ,Active
            ,AdministratorInd
            ,ResearchInd
            ,ClinicalTrialsInd
            ,Website
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_ServiceProvider
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);