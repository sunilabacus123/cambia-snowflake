CREATE TABLE Identifier_replacesuffix
WITH
(
	bucket_count=15,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,IdentifierType
            ,StandardID
            ,Identifier
            ,CheckDigit
            ,CheckDigitSchema
            ,IssuingAuthority
            ,Organization
            ,IdentifierSource
            ,LastUpdateDate
            ,IdentifierStatus
            ,EffDate
            ,ExpDate
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Identifier
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
