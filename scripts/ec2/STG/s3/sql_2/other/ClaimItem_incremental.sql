CREATE TABLE ClaimItem_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,Sequence
            ,ClaimItemStatus
            ,Revenue
            ,RevenueCode
            ,Category
            ,ProductOrService
            ,ProductOrServiceDesc
            ,ProductOrServiceType
            ,CoordinationOfBenefitsCode
            ,CoordinationOfBenefitsDesc
            ,Modifier
            ,ProgramCode
            ,FirstDateOfService
            ,LastDateOfService
            ,PlaceOfService
            ,Quantity
            ,UnitType
            ,UnitPrice
            ,ItemCost
            ,ChargeAmount
            ,BodySite
            ,SubSite
            ,NetworkInd
            ,NetworkID
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_ClaimItem
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);