CREATE TABLE MemberStub_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,IsAuthoritative
            ,MemberType
            ,MemberCategory
            ,MemberCategoryID
            ,MemberCategoryPrefix
            ,SubscriberRelationship
            ,ParentGroupSourceID
            ,GroupSourceID
            ,SubGroupSourceID
            ,SubscriberID
            ,PlanID
            ,CarrierID
            ,HospiceInd
            ,InstitutionalInd
            ,ExchangeInd
            ,StateResidency
            ,Occupation
            ,HireDate
            ,MedicareHICNumber
            ,MedicareStartDate
            ,MedicaidID
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_MemberStub
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);