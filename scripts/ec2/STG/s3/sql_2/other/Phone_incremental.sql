CREATE TABLE Phone_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,PhoneType
            ,PhoneSource
            ,Number
            ,CountryCode
            ,AreaCode
            ,LocalNumber
            ,Extension
            ,DigitCount
            ,FormatMask
            ,GeoArea
            ,GeoCountry
            ,LineType
            ,Active
            ,ValidationStatus
            ,Priority
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Phone
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);