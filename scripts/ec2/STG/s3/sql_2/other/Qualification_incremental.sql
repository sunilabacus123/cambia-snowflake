CREATE TABLE Qualification_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,QualificationType
            ,QualificationNumber
            ,QualificationCode
            ,LicenseCategory
            ,GroupID
            ,BoardID
            ,LicenseNumber
            ,CertificateID
            ,Jurisdiction
            ,Issuer
            ,IssueDate
            ,EffDate
            ,ExpDate
            ,Status
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_Qualification
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);