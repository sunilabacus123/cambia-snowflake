CREATE TABLE ProductStub_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,PayerID
            ,PayerName
            ,PayerTIN
            ,LineOfBusiness
            ,LineOfBusinessID
            ,ProductCategory
            ,ProductName
            ,EffectiveDate
            ,ExpiryDate
            ,NetworkPrefix
            ,IDStock
            ,GroupID
            ,GroupName
            ,PlanID
            ,PlanName
            ,IDCardType
            ,ProviderNetworkPrefix
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambiastg_data_main.conn_unnested_ProductStub
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);