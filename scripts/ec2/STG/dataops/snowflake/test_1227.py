"""
----------------------------------------------------------------------------------------------------------
Description: Data Extract

usage:

Author  : Bala Muthukrishnan
Release : 1

Modification Log:  04 Dec, 2020
-----------------------------------------------------------------------------------------------------------
Date                Author                            Description
-----------------------------------------------------------------------------------------------------------
05/26/2020       Bala M                    First Version.
07/14/2020       Bala M                    Added Auto Extract Job id Increment, Skipping manifest generation
                                           Fix on Update Job status, Job Status database switch is added.
09/03/2020       Bala M                    Added Trigger file creation, SQL_PATH change based on Source check
11/10/2020       Sunil Kumar S             Adding CASE
12/04/2020       Sunil Kumar S             Adding Provider & Quest
1/24/2021        Naveen Gollapudi          Adding ability to run in paraelle options
02/23/2021       Sunil Kumar S             Foundation Decomission
03/01/2021       Sunil Kumar S             Changes for Dynacare
03/02/2021       Sunil Kumar S             Changes for LabCorp
03/26/2021       Sunil Kumar S             Changes for PAML
08/29/2021       Sunil Kumar S             Development for Faets Member & Enrollment V2
09/01/2021       Sunil Kumar S             Adding Logging 
10/10/2021       Sunil Kumar S             Adding BCBSA
11/18/2021       Mucahit Yildiz            Add update DynamoDB table + increment all sys.argv by 1 since we added new parameter at beginning of call
11/19/2021       Mucahit Yildiz            Revoke changes from 11/18/2021
-----------------------------------------------------------------------------------------------------------
"""

import datetime
import io
import time
import sys
import json
import boto3
import botocore
import pandas as pd
import pyarrow.parquet as pq
from botocore.exceptions import ClientError
import os
from pytz import timezone
import pytz
import logging

# Setup date variables
date_format_sf='%m/%d/%Y %H:%M:%S %Z'
date_sf = datetime.datetime.now(tz=pytz.utc)
date_sf = date_sf.astimezone(timezone('US/Pacific'))

# Environment Setup
env = 'cambiastg'
extract_name = 'cambia_extract'
#extract_loc = 'cambia_transfer'  ##changed for testing
extract_loc = 'cambia_extract'
job_status_table_name = 'cambia_extract_job_status'   ##changed for testing
CONNECTOR_TYPE = 'LANDING_V2_COPY'

LANDING_V2_COPY = ['FACETS-ENROLLMENT-V2','FACETS-CLAIM-EXTENSION','FACETS-MEMBER-V2','QUEST-V2','PRIME-CAMBIA-V2']
COPY_CONNECTORS = ['FACETS-MEMBER-V2','FACETS-CLAIM-EXTENSION','QUEST-V2','FACETS-ENROLLMENT-V2','PRIME-CAMBIA-V2']

# Read parameter
source = sys.argv[1]
root_domain = sys.argv[2]
ingestion_ids=[]
new_ingestion = str(json.loads(sys.argv[3]))

#-------------------------------------------------------------------
# Changes to support script developed by dataops for osshore trigger
#-------------------------------------------------------------------

for i in range(3,len(sys.argv)):
    ingestion_ids.append(sys.argv[i])
if root_domain == 'Claim' or root_domain == 'CCA' or root_domain == 'DYNACARE' or root_domain == 'LABCORP' or root_domain == 'PAML' or root_domain == 'FACETS-MEMBER-V2' or root_domain == 'FACETS-ENROLLMENT-V2' or root_domain == 'FACETS-CLAIM-EXTENSION' or root_domain == 'QUEST-V2' or root_domain == 'CAMBIA-BCBSA-CLAIMS' or root_domain == 'CAMBIA-BCBSA-MEMBER' or root_domain =='PRIME-CAMBIA-V2':
    source = sys.argv[1]
else:
    source=''

predefined_root_list =['Enrollment','Member','Claim','QUEST','PROVIDER','CCA','DYNACARE','LABCORP','PAML','MEMBER','FACETS','FACETS-MEMBER-V2','FACETS-ENROLLMENT-V2','FACETS-CLAIM-EXTENSION','QUEST-V2','CAMBIA-BCBSA-CLAIMS','CAMBIA-BCBSA-MEMBER','PRIME-CAMBIA-V2']
predefined_source_list=['VSP','Prime','FACETS','','MEMBER','CCA','DYNACARE','LABCORP','PAML','FACETS-CLAIM-EXTENSION','QUEST-LAB','BCBSA-CLAIM','BCBSA-MEMBER','PRIME','FACETS-ENROLLMENT','FACETS-MEMBER']

NO_SOURCE = ['DYNACARE','LABCORP','VSP','FACETS-CLAIM','QUEST','PROVIDER-FOUNDATION','FACETS-MEMBER','FACETS-ENROLLMENT','PAML']
Y_SOURCE = ['PRIME-CAMBIA','CCA-MEMBER','CCA-CASE','FACETS-MEMBER-V2','FACETS-ENROLLMENT-V2','FACETS-CLAIM-EXTENSION','QUEST-LAB','BCBSA-CLAIM','CAMBIA-BCBSA-CLAIMS','CAMBIA-BCBSA-MEMBER','BCBSA-MEMBER','PRIME-CAMBIA-V2']


if  source not in predefined_source_list or root_domain not in predefined_root_list :
    print ('invalid argugements, terminating')
    exit()
else:
    print('arguments passed are in defined metadata list, proceeding')


# Setting up Session
ath = boto3.client('athena',region_name='us-east-1')
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
glue = boto3.client('glue',region_name='us-east-1')


# buckets source and destinations
src_bucket_name = 'abacus-sandboxes-' + env
dest_bucket_name = 'abacus-data-lake-' + env
database = env + '_data_ops'
source_database = env + ''
workgroup = env + '-dataops'
CONFIG_BUCKET_NAME = 'abacus-sandboxes-' + env
REPORT_KEY = 'data-ops/etl/'+extract_loc+'/config/extract_job_report/job_report.txt'


# Download job report file from s3 to store job execution report
try:
    s3.Bucket(CONFIG_BUCKET_NAME).download_file(REPORT_KEY, 'job_report.txt')
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise


# Create control folders if not exists
if not os.path.exists('manifest'):
    os.makedirs('manifest')
if not os.path.exists('domain_logs'):
    os.makedirs('domain_logs')
if not os.path.exists('connector'):
    os.makedirs('connector')
if not os.path.exists('entitylog'):
    os.makedirs('entitylog')
if not os.path.exists('success'):
    os.makedirs('success')


# Setting SQL Path
if root_domain == 'Claim' and source == 'VSP':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/' + source + '/'
elif root_domain == 'Claim' and source == 'FACETS':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/' + source + '/'
elif root_domain == 'DYNACARE' and source == 'DYNACARE':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/DYNACARE/'
elif root_domain == 'LABCORP' and source == 'LABCORP':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/LABCORP/'
elif root_domain == 'CCA' and source == 'MEMBER':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/CCA_MEMBER/'
elif root_domain == 'CCA' and source == 'CCA':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/CCA_CCA/'
elif root_domain == 'QUEST':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/QUEST/'
elif root_domain == 'PROVIDER':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/PROVIDER/'
elif root_domain == 'PAML' and source == 'PAML':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/PAML/'

elif root_domain == 'FACETS-MEMBER-V2' and source == 'FACETS-MEMBER':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/FACETS_MEMBER_V2/'

elif root_domain == 'QUEST-V2' and source == 'QUEST-LAB':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/QUEST_V2/'

elif root_domain == 'FACETS-CLAIM-EXTENSION' and source == 'FACETS-CLAIM-EXTENSION':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/FACETS_CLAIM_EXTENSION/'

elif root_domain == 'FACETS-ENROLLMENT-V2' and source == 'FACETS-ENROLLMENT':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/FACETS_ENROLLMENT_V2/'

elif root_domain == 'CAMBIA-BCBSA-CLAIMS' and source == 'BCBSA-CLAIM':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/CAMBIA_BCBSA_CLAIMS/'

elif root_domain == 'CAMBIA-BCBSA-MEMBER' and source == 'BCBSA-MEMBER':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/CAMBIA_BCBSA_MEMBER/'

elif root_domain == 'PRIME-CAMBIA-V2' and source == 'PRIME':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/PRIME_CAMBIA_V2/'

else:
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/'


par_suffix=''

# Setting parameters required for Snowflake CONTROL tables and CONFIG File for extrat
if root_domain == 'Claim' and source == 'Prime':
    config_file = 'data-ops/etl/' + extract_name + '/config/claim_view_list.txt'
    CONNECTOR = 'CLAIM'
    OP_DOMAIN = 'PRIME'
    C_CONNECTOR = 'PRIME-CAMBIA'
    C_SOURCE = 'PRIME'
    CONNECTOR_s3 = 'PRIME_CAMBIA/PRIME'

elif root_domain == 'Claim' and source == 'VSP':
    config_file = 'data-ops/etl/' + extract_name + '/config/vsp_view_list.txt'
    CONNECTOR = 'CLAIM'
    OP_DOMAIN = 'VSP'
    C_CONNECTOR = 'VSP'
    C_SOURCE = 'VSP'
    CONNECTOR_s3 = 'VSP/VSP'

elif root_domain == 'Claim' and source == 'FACETS':
    config_file = 'data-ops/etl/' + extract_name + '/config/facets_view_list.txt'
    CONNECTOR = 'CLAIM'
    OP_DOMAIN = 'FACETS'
    C_CONNECTOR = 'FACETS-CLAIM'
    C_SOURCE = 'FACETS'
    CONNECTOR_s3 = 'FACETS_CLAIM/FACETS'


elif root_domain == 'DYNACARE' and source == 'DYNACARE':
    config_file = 'data-ops/etl/' + extract_name + '/config/dynacare_dynacare_view_list.txt'
    CONNECTOR = 'DYNACARE'
    OP_DOMAIN = 'DYNACARE'
    C_CONNECTOR = 'DYNACARE'
    C_SOURCE = 'DYNACARE'
    CONNECTOR_s3 = 'DYNACARE/DYNACARE'


elif root_domain == 'LABCORP' and source == 'LABCORP':
    config_file = 'data-ops/etl/' + extract_name + '/config/labcorp_labcorp_view_list.txt'
    CONNECTOR = 'LABCORP'
    OP_DOMAIN = 'LABCORP'
    C_CONNECTOR = 'LABCORP'
    C_SOURCE = 'LABCORP'
    CONNECTOR_s3 = 'LABCORP/LABCORP'


elif root_domain == 'CCA' and source == 'MEMBER':
    config_file = 'data-ops/etl/' + extract_name + '/config/cca_member_view_list.txt'
    CONNECTOR = 'CCA'
    OP_DOMAIN = 'MEMBER'
    C_CONNECTOR = 'CCA-MEMBER'
    C_SOURCE = 'CCA'
    CONNECTOR_s3 = 'CCA_MEMBER/CCA'

elif root_domain == 'CCA' and source == 'CCA':
    config_file = 'data-ops/etl/' + extract_name + '/config/cca_cca_view_list.txt'
    CONNECTOR = 'CCA'
    OP_DOMAIN = 'CCA'
    C_CONNECTOR = 'CCA-CASE'
    C_SOURCE = 'CCA'
    CONNECTOR_s3 = 'CCA_CASE/CCA'

elif root_domain == 'QUEST':
    config_file = 'data-ops/etl/' + extract_name + '/config/quest_view_list.txt'
    CONNECTOR = 'QUEST'
    OP_DOMAIN = 'QUEST'
    C_CONNECTOR = 'QUEST'
    C_SOURCE = 'QUEST'
    CONNECTOR_s3 = 'QUEST/QUEST'

elif root_domain == 'PROVIDER':
    config_file = 'data-ops/etl/' + extract_name + '/config/provider_view_list.txt'
    CONNECTOR = 'PROVIDER'
    OP_DOMAIN = 'PROVIDER'
    C_CONNECTOR = 'PROVIDER-FOUNDATION'
    C_SOURCE = 'PROVIDER-FOUNDATION'
    CONNECTOR_s3 = 'PROVIDER_FOUNDATION/PROVIDER_FOUNDATION'

elif root_domain == 'Member':
    config_file = 'data-ops/etl/' + extract_name + '/config/member_view_list.txt'
    CONNECTOR = 'FACETS'
    OP_DOMAIN = 'MEMBER'
    C_CONNECTOR = 'FACETS-MEMBER'
    C_SOURCE = 'FACETS'
    CONNECTOR_s3 = 'FACETS_MEMBER/FACETS'

elif root_domain == 'Enrollment':
    config_file = 'data-ops/etl/' + extract_name + '/config/enrollment_view_list.txt'
    CONNECTOR = 'FACETS'
    OP_DOMAIN = 'ENROLLMENT'
    C_CONNECTOR = 'FACETS-ENROLLMENT'
    C_SOURCE = 'FACETS'
    CONNECTOR_s3 = 'FACETS_ENROLLMENT/FACETS'

elif root_domain == 'PAML' and source == 'PAML':
    config_file = 'data-ops/etl/' + extract_name + '/config/paml_paml_view_list.txt'
    CONNECTOR = 'PAML'
    OP_DOMAIN = 'PAML'
    C_CONNECTOR = 'PAML'
    C_SOURCE = 'PAML'
    CONNECTOR_s3 = 'PAML/PAML'

elif root_domain == 'FACETS-MEMBER-V2' and source == 'FACETS-MEMBER':
    config_file = 'data-ops/etl/' + extract_name + '/config/facets_member_v2_view_list.txt'
    CONNECTOR = 'FACETS_MEMBER_V2'
    OP_DOMAIN = 'FACETS_MEMBER_V2'
    C_CONNECTOR = 'FACETS-MEMBER-V2'
    C_SOURCE = 'FACETS'
    CONNECTOR_s3 = 'FACETS_MEMBER/FACETS_MEMBER_V2'
    OLD_SOURCE = 'Facets'

elif root_domain == 'FACETS-CLAIM-EXTENSION' and source == 'FACETS-CLAIM-EXTENSION':
    config_file = 'data-ops/etl/' + extract_name + '/config/facets_claim_extension_view_list.txt'
    CONNECTOR = 'FACETS_CLAIM_EXTENSION'
    OP_DOMAIN = 'FACETS_CLAIM_EXTENSION'
    C_CONNECTOR = 'FACETS-CLAIM-EXTENSION'
    C_SOURCE = 'FACETS'
    CONNECTOR_s3 = 'FACETS_CLAIM_EXTENSION/FACETS_CLAIM_EXTENSION'
    OLD_SOURCE = 'Facets'

elif root_domain == 'QUEST-V2' and source == 'QUEST-LAB':
    config_file = 'data-ops/etl/' + extract_name + '/config/quest_v2_view_list.txt'
    CONNECTOR = 'QUEST_V2'
    OP_DOMAIN = 'QUEST_LAB'
    C_CONNECTOR = 'QUEST-V2'
    C_SOURCE = 'QUEST'
    CONNECTOR_s3 = 'QUEST_V2/QUEST_LAB'
    OLD_SOURCE = 'Quest'

elif root_domain == 'FACETS-ENROLLMENT-V2' and source == 'FACETS-ENROLLMENT':
    config_file = 'data-ops/etl/' + extract_name + '/config/facets_enrollment_v2_view_list.txt'
    CONNECTOR = 'FACETS_ENROLLMENT_V2'
    OP_DOMAIN = 'FACETS_ENROLLMENT_V2'
    C_CONNECTOR = 'FACETS-ENROLLMENT-V2'
    C_SOURCE = 'FACETS'
    CONNECTOR_s3 = 'FACETS_ENROLLMENT/FACETS_ENROLLMENT_V2'
    OLD_SOURCE = 'Facets'

elif root_domain == 'CAMBIA-BCBSA-CLAIMS' and source == 'BCBSA-CLAIM':
    config_file = 'data-ops/etl/' + extract_name + '/config/bcbsa_claims_view_list.txt'
    CONNECTOR = 'CAMBIA_BCBSA_CLAIMS'
    OP_DOMAIN = 'CAMBIA_BCBSA_CLAIMS'
    C_CONNECTOR = 'CAMBIA-BCBSA-CLAIMS'
    C_SOURCE = 'BCBSA-CLAIM'
    CONNECTOR_s3 = 'BCBSA_CLAIM/CAMBIA_BCBSA_CLAIMS'


elif root_domain == 'CAMBIA-BCBSA-MEMBER' and source == 'BCBSA-MEMBER':
    config_file = 'data-ops/etl/' + extract_name + '/config/bcbsa_member_view_list.txt'
    CONNECTOR = 'CAMBIA_BCBSA_MEMBER'
    OP_DOMAIN = 'CAMBIA_BCBSA_MEMBER'
    C_CONNECTOR = 'CAMBIA-BCBSA-MEMBER'
    C_SOURCE = 'BCBSA-MEMBER'
    CONNECTOR_s3 = 'BCBSA_MEMBER/CAMBIA_BCBSA_MEMBER'
    OLD_SOURCE = 'BCBSA-MEMBER'

elif root_domain == 'PRIME-CAMBIA-V2' and source == 'PRIME':
    config_file = 'data-ops/etl/' + extract_name + '/config/prime_cambia_v2_view_list.txt'
    CONNECTOR = 'PRIME_CAMBIA_V2'
    OP_DOMAIN = 'PRIME_CAMBIA_V2'
    C_CONNECTOR = 'PRIME-CAMBIA-V2'
    C_SOURCE = 'PRIME-MCEF'
    C_SOURCE = 'PRIME'
    CONNECTOR_s3 = 'PRIME/PRIME_CAMBIA_V2'
    OLD_SOURCE = 'Prime'

else:
    print("Root domain :"+root_domain+" and source :"+source+" is not correct")
    exit()

refresh_type = 'INC'
par_suffix ='_'+CONNECTOR+'_'+OP_DOMAIN


def drop_table(view_name):
    # Function to DROP tables created by previous execution
    print('Dropping table...' + view_name)

    if view_name == 'Case':
        drop_query = 'drop table if exists ' + database + '.`' + view_name + '`'
    else:
        drop_query = 'drop table if exists ' + database + '.' + view_name

    try:
        response = ath.start_query_execution(QueryString=drop_query, QueryExecutionContext={'Database': database},
                                             WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        print('Drop table query is running...')
        return str(query_execution_id)
    except ClientError as e:
        print('Query failed to submit' % e)


def read_parquet(dest_bucket_name, file_name):
    buffer = io.BytesIO()
    s3object = s3.Object(dest_bucket_name, file_name)
    s3object.download_fileobj(buffer)
    df = pd.read_parquet(buffer)
    print(df.count())


def delete_file(stage_prefix, stage_bucket_name):
    try:
        bucket = s3.Bucket(stage_bucket_name)
        for obj in bucket.objects.filter(Prefix=stage_prefix):
            s3.Object(bucket.name, obj.key).delete()
            print('File Delete Successful ' + obj.key)
    except ClientError as e:
        print('s3 File Delete Failed' % e)


def delete_stage(view_name, stage_bucket):
    # Find Table

    try:
        response = glue.get_table(DatabaseName=database, Name=view_name)
        table_dest = response['Table']['StorageDescriptor']['Location']
    except:
        table_dest = 'NULL'
    if table_dest == 'NULL':
        print(view_name + " View does not exits")
        print("No need of stage deletion")
    else:
        print('Stage Deletion in progress...' + view_name)

        stage_prefix = table_dest[33:]
        for obj in stage_bucket.objects.filter(Prefix=stage_prefix):
            s3.Object(stage_bucket.name, obj.key).delete()

        print('Stage s3 object deletion completed for ...' + view_name)


def create_table(view_name, refresh_type, ingestion_id):
    print('==========================')
    print('Submitting create table query for...' + view_name)
    try:
        if refresh_type == 'FULL':
            file_to_read = sql_path + view_name + '.sql'
        elif refresh_type == 'INC':
            file_to_read = sql_path + view_name + '_incremental.sql'
        print(file_to_read)
        fileobj = s3client.get_object(Bucket=src_bucket_name, Key=file_to_read)
        filedata = fileobj['Body'].read()
        filecontents = filedata.decode("utf-8")
        filecontents_temp1 = filecontents.replace('replace_ingestionid',ingestion_id)
        
        if C_CONNECTOR in COPY_CONNECTORS:
            filecontents_temp = filecontents_temp1.replace('replace_source_value',OLD_SOURCE)
        else:
            filecontents_temp = filecontents_temp1

        create_query = filecontents_temp.replace('_replacesuffix',par_suffix)

        if view_name == 'CdcBatch' or view_name == 'CdcBatchMember' or view_name == 'CdcBatchEnrollment' or view_name == 'CdcBatchPrime':
            create_query = create_query.replace('replace_connector',C_CONNECTOR)
                    

        response = ath.start_query_execution(QueryString=create_query,
                                             QueryExecutionContext={'Database': database}, WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        print('Athena table Creation is running for :' + view_name)
        return query_execution_id
    except ClientError as e:
        print('Query failed to submit' % e)
        return 'null'


# Get query status
def get_query_state(query_execution_id, max_execution=50):
    print('Checking query Status...')
    # print('query_execution_id...:'+query_execution_id)
    response = ath.get_query_execution(QueryExecutionId=query_execution_id)
    state = response['QueryExecution']['Status']['State']
    print('Query State:' + state)
    while max_execution > 0 and state in ['RUNNING'] or state in ['QUEUED']:
        max_execution = max_execution - 1
        # print('execution:'+ str(max_execution))
        response = ath.get_query_execution(QueryExecutionId=query_execution_id)
        if 'QueryExecution' in response and 'Status' in response['QueryExecution'] and 'State' in \
                response['QueryExecution']['Status']:
            state = response['QueryExecution']['Status']['State']
            if state == 'FAILED':
                print('query failed, check the logs for details...')
                status_string = response['QueryExecution']['Status']['StateChangeReason']
                print(status_string)
                return state
            elif state == 'SUCCEEDED':

                print('query execution succeeded...')
                print('==========================')
                return state
            time.sleep(5)


def copy_object(state, src_bucket, src_prefix, view_name, dest_prefix,extract_time):
    if state:
        if state == 'SUCCEEDED':
            time.sleep(5)
            num = 1
            # s3objects = src_bucket.objects.filter(Prefix=src_prefix)
            # print(s3objects)
            # if len(s3objects) > 0:
            for s3object in src_bucket.objects.filter(Prefix=src_prefix):
                srckey = s3object.key
                # print("srckey" + srckey)
                if not srckey.endswith('/'):
                    # fileName = srcKey.split('/')[-1]
                    file_num = str(num)
                    fileName = view_name + '_' + extract_time + '_' + file_num + '.gz'
                    print(fileName)
                    destfilekey = dest_prefix + fileName
                    copysource = src_bucket_name + '/' + srckey
                    print('destination_bucket_name: ' + dest_bucket_name)
                    print('destination_file_key: ' + destfilekey)
                    print('copy Source:' + copysource)
                    s3.Object(dest_bucket_name, destfilekey).copy_from(CopySource=copysource)
                    num = num + 1
            print('Data copy is completed...')
            return 'success'
        else:

            print('Data is not copied...')
            return 'failed'
    else:
        print('Query is not started executing...')
        return 'failed'


def run_query(view_name):
    print('==========================')
    print('Submitting query...' + view_name)
    try:
        file_to_read = sql_path + view_name + '.sql'
        print(file_to_read)
        fileobj = s3client.get_object(Bucket=src_bucket_name, Key=file_to_read)
        filedata = fileobj['Body'].read()
        filecontents = filedata.decode("utf-8")

        # QueryString = sql.read()QueryExecutionContext={'Database': database}
        response = ath.start_query_execution(QueryString=filecontents, QueryExecutionContext={'Database': database},
                                             WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        print('Query submitted to Athena...' + view_name)
        return query_execution_id
    except ClientError as e:
        print('Query failed to submit' % e)


def read_result(src_bucket_name, src_prefix):
    print('Getting csv files results of the query')

    try:
        # response = s3client.get_object(Bucket=src_bucket_name, Key=src_prefix)
        response = s3.Bucket(src_bucket_name).Object(key=src_prefix).get()
        print('read csv results...')

        t = pd.read_csv(io.BytesIO(response['Body'].read()), encoding='utf8')
        return t.iat[0, 0]

    except ClientError as e:
        print('Cannot read csv file' % e)


def read_config(src_bucket_name, config_file):
    try:
        s3.Object(src_bucket_name, config_file).load()
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print('config file not  exists')
            exit()
        else:
            raise
    else:
        obj = s3.Object(src_bucket_name, config_file)
        config_data = obj.get()['Body'].read()
        content = config_data.decode('utf-8')
        # print(content)
        view_list = []
        for line in content.splitlines():
            if line.startswith("#"):
                print("Skipping view " + line)
            else:
                view_list.append(line)
        return view_list


def s3_object_rowcount(dest_bucket_name, prefix, view_name, ingestion_id, root_domain,C_CONNECTOR,C_SOURCE):

    try:
        response = s3client.list_objects(Bucket=dest_bucket_name, Prefix=prefix, MaxKeys=100)

        outputs = []
        objects = response["Contents"]
        for i in objects:
            file_name = i['Key']
            buffer = io.BytesIO()
            s3object = s3.Object(dest_bucket_name, file_name)
            s3object.download_fileobj(buffer)
            df = pq.ParquetFile(buffer)
            rowcount = df.metadata.num_rows
            print(file_name)
            f_name = file_name.split('/', 8)
            f_name1 = f_name[8]
            outputs.append( C_CONNECTOR +',' + C_SOURCE + ',' + ingestion_id + ',' + view_name + ',' + 'Insert_Update' + ',' + f_name1 + ',' + str(rowcount) + ',' + root_domain)

    except KeyError:
        outputs.append(
             C_CONNECTOR + ',' +  C_SOURCE + ',' + ingestion_id + ',' + view_name + ',' + 'Insert_Update' + ',' + 'No_File' + ',' + '0' + ',' + root_domain)
        print('Cannot read s3 file')

    return outputs


def update_job_status(extract_job_time,view_name,ingestion_id,copy_state):
    # Update Job Status
    print('Update Job Status for table...' + view_name)
    if refresh_type == 'FULL':
        update_query = 'insert into ' + database + '.' + job_status_table_name + ' values (\'' + extract_job_time \
                       + '\' ,\'' + view_name + '\' ,' + ingestion_id + '\' ,\'' \
                       + copy_state + '\'); '
        print(update_query)
        try:
            response = ath.start_query_execution(QueryString=update_query, QueryExecutionContext={'Database': database},
                                                 WorkGroup=workgroup)
            query_execution_id = response["QueryExecutionId"]
            print('Update job status query is running...')
            return str(query_execution_id)
        except ClientError as e:
            print('Update job status query failed to submit' % e)
    elif refresh_type == 'INC':
        update_query = 'insert into ' + database + '.' + job_status_table_name + ' values ( \'' + extract_job_time \
                       + '\' ,\'' + view_name + '\' ,\'' + ingestion_id + '\' ,\'' \
                       + copy_state + '\');'
        print(update_query)
        try:
            response = ath.start_query_execution(QueryString=update_query, QueryExecutionContext={'Database': database},
                                                 WorkGroup=workgroup)
            query_execution_id = response["QueryExecutionId"]
            print('Update job status query is running...')
            return str(query_execution_id)
        except ClientError as e:
            print('Update job status query failed to submit' % e)


def check_ingestion_query(ingestion_id):
    ingestion_count = 'select count(*) from ' + database + '.' + job_status_table_name + ' where ' \
                                 'ingestionid=\'' + ingestion_id + '\''
    print(ingestion_count)
    try:
        response = ath.start_query_execution(QueryString=ingestion_count,
                                         QueryExecutionContext={'Database': database}, WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        print('Ingestion check query is running...')
        return query_execution_id

    except ClientError as e:
        print('Ingestion check Query failed to submit' % e)


def main():
    for ingestion_id in ingestion_ids:
        currentdt = datetime.datetime.now()
        extract_date = currentdt.strftime("%Y/%m/%d")
        extract_time = currentdt.strftime("%Y%m%d%H%M")
        logging_file_name = "domain_logs/"+CONNECTOR+"_"+OP_DOMAIN+"_"+extract_time+"_"+ingestion_id+".log"
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        logging.basicConfig(filename=logging_file_name, level=logging.INFO)
        #CCYYMMDD_Date = currentdt.strftime("%Y%m%d")
        extract_job_time = currentdt.strftime("%Y-%m-%d-%H-%M")
        date_time = currentdt.strftime("%m/%d/%Y, %H:%M:%S")

        logging.info("Extract Start Time:- " + date_time +" : "+ ingestion_id)
        logging.info("=============================================================")

        print("Extract Start Time:- " + date_time +" : "+ ingestion_id)
        print("============================================")
        print("Started Processing Ingestion:" + ingestion_id)
        job_start_time_ts = date_time
        view_list = read_config(src_bucket_name, config_file)
        src_bucket = s3.Bucket(src_bucket_name)
        # dest_bucket = s3.Bucket(dest_bucket_name)
        query_id = check_ingestion_query(ingestion_id)
        state = get_query_state(query_id, max_execution=1000)
        if state == 'SUCCEEDED':
            print("Ingestion id processed check query executed Successfully: ")
        else:
            print("Ingestion id to be processed query Failed: ")

        csv_file = 'data-ops/athena-query-results/' + query_id + '.csv'
        ingestion_count = read_result(src_bucket_name, csv_file)
        print("Check ingestion status, processed or not :")
        if ingestion_count > 0:
            print('Ingestion_id: ' + ingestion_id + ' is already processed, skipping this ingestion...')
            logging.info('Ingestion_id: ' + ingestion_id + ' is already processed, skipping this ingestion...')
            print("==============================================")
            continue
        else:
            drop_query_ids = []

            for view_name in view_list:
                delete_stage(view_name+par_suffix, src_bucket)
                drop_query_ids.append(drop_table(view_name+par_suffix))
            print(drop_query_ids)

            max_execution = 500
            failed_query_ids = []
            while len(drop_query_ids) > 0:
                max_execution = max_execution - 1
                response = ath.batch_get_query_execution(QueryExecutionIds=drop_query_ids)
                execution_ids = response['QueryExecutions']
                # query_status=response['QueryExecutions']['Status']['State']
                for i in execution_ids:
                    query_id = i['QueryExecutionId']
                    status = i['Status']['State']
                    print(query_id, status)
                    logging.info(query_id+" : "+status)
                    if status == 'SUCCEEDED':
                        drop_query_ids.remove(query_id)
                    elif status == 'FAILED':
                        drop_query_ids.remove(query_id)
                        failed_query_ids.append(query_id)
                time.sleep(5)

            create_query_ids = []
            if len(failed_query_ids) > 0:
                print('Failed Query ids ...')
                print(failed_query_ids)
                print("Table Drop failed for some view check athena history tab ")
                logging.info("Table Drop failed for some view check athena history tab ")
                out_str = " "

                logging.info(out_str.join(failed_query_ids))
                logging.info("============================================")
                exit()
            else:
                for view_name in view_list:
                    create_query_ids.append(create_table(view_name, refresh_type,ingestion_id))
                    print(view_name+par_suffix)
                    logging.info("Creating View in Athena "+view_name+par_suffix)
                    print(create_query_ids)
            max_execution = 500
            failed_create_query_id = []
            while len(create_query_ids) > 0:
                max_execution = max_execution - 1
                response = ath.batch_get_query_execution(QueryExecutionIds=create_query_ids)
                execution_ids = response['QueryExecutions']
                for i in execution_ids:
                    query_id = i['QueryExecutionId']
                    # print(query_id)
                    status = i['Status']['State']
                    # print(status)
                    if status == 'SUCCEEDED':
                        create_query_ids.remove(query_id)
                    elif status == 'FAILED':
                        create_query_ids.remove(query_id)
                        failed_create_query_id.append(query_id)
                    else:
                        print(query_id, status)
                time.sleep(5)

            if len(failed_create_query_id) > 0:
                print("Create table failed for some view, check athena history page")
                logging.error("Create table failed for some view, check athena history page")
                logging.error(failed_create_query_id)
                logging.info("============================================")
                exit()
            else:
                print("copy files.. Started")
                logging.info("Started Copying Files")
                logging.info("============================================")
                print("===========")
                manifest_view_list = []
                for view_name in view_list:

                    response = glue.get_table(DatabaseName=database, Name=view_name+par_suffix)
                    table_dest = response['Table']['StorageDescriptor']['Location']
                    f_name = table_dest.split('/', 6)
                    table_loc = f_name[6]

                    if table_loc.endswith('/'):
                    
                        src_prefix = 'data-ops/athena-query-results/tables/' + table_loc 

                    else:

                        src_prefix = 'data-ops/athena-query-results/tables/' + table_loc + '/'

                    print(src_prefix)
                    dest_prefix = 'data-views/' + extract_loc + '/'
                    
                    if C_CONNECTOR in LANDING_V2_COPY:
                        dest_extract_prefix = dest_prefix + 'data/landing_v2/' + view_name + '/' + extract_date + '/' + ingestion_id + '/'
                    else:
                        dest_extract_prefix = dest_prefix + 'data/' + view_name + '/' + extract_date + '/' + ingestion_id + '/'
                    print(dest_extract_prefix)
                    # if view_name == 'Claim':
                    #     print("For claim sleep for 4 minutes")
                    #     time.sleep(240)
                    # elif view_name == 'Enrollment':
                    #     print("For Enrollment sleep for 7 minutes")
                    #     #time.sleep(420)
                    # elif view_name == 'Member':
                    #     print("For Member sleep for 6 minutes")
                    #     time.sleep(360)

                    copy_state = copy_object('SUCCEEDED', src_bucket, src_prefix, view_name, dest_extract_prefix, extract_time)

                    if view_name == 'CdcBatch':
                        print("Batch ID Table so Not generating Manifest \n")
                    else:
                        manifest_view_list.append(view_name)

                    logging.info("Updating Job status in Athena")
                    print('Updating job status' + view_name)
                    update_job_status(extract_job_time, view_name, ingestion_id, copy_state)
                    print("==============================================")
                    print("Extract Generation completed for " + view_name + "!!")
                    print("==============================================")
                    logging.info("============================================")
                    logging.info("Extract Generation completed for " + view_name + "!!")
                    logging.info("============================================")

            manifest = []
            if len(manifest_view_list) > 0:
                for view_name in manifest_view_list:
                    print("==============================================")
                    print("Manifest Generation Started for " + view_name)
                    print("==============================================")

                    logging.info("============================================")
                    logging.info("Manifest Generation Started for " + view_name)
                    logging.info("============================================")

                    dest_prefix = 'data-views/' + extract_loc + '/'
                    dest_manifest_prefix = dest_prefix + 'manifest_landing/' + extract_date + '/' + ingestion_id + '/'

                    if C_CONNECTOR in LANDING_V2_COPY:
                        prefix = dest_prefix + 'data/landing_v2/' + view_name + '/' + extract_date + '/' + ingestion_id + '/'
                    else:
                        prefix = dest_prefix + 'data/' + view_name + '/' + extract_date + '/' + ingestion_id + '/'

                    manifest.append(s3_object_rowcount(dest_bucket_name, prefix, view_name, ingestion_id,root_domain,C_CONNECTOR,C_SOURCE))

                manifest1 = ['\n'.join(ele) for ele in manifest]
                manifest_file_name = 'manifest/manifest_' + extract_time + '.csv'
                with open(manifest_file_name, 'w') as filehandle:
                    filehandle.write('domain,batch_id,obj_name,operation_type,file_name,record_count\n')
                    for listitem in manifest1:
                        filehandle.write('%s\n' % listitem)
                    print("==============================================")
                    print("Manifest Generation Completed for " + view_name)
                    print("==============================================")

                    logging.info("============================================")
                    logging.info("Manifest Generation Completed for " + view_name)
                    logging.info("============================================")

                manifest_file_name = 'manifest/manifest_' + extract_time + '.csv'
                dest_file_name = dest_manifest_prefix + manifest_file_name
                print("Destination bucket :" + dest_bucket_name + '/' + manifest_file_name)
                s3client.upload_file(manifest_file_name, dest_bucket_name, dest_file_name)

            else:
                print('Manifest file generation is not needed...')
                logging.info("============================================")
                logging.info("Manifest file generation is not needed..")
                logging.info("============================================")
            
        success_dest_prefix = 'data-views/' + extract_loc + '/trigger/Connector/' + ingestion_id + '/'
        success_file_name = 'success/success_' + root_domain + extract_time + '.csv'


        with open(success_file_name, 'w') as filehandle:
           filehandle.write('root_source,batch_id,status,connector,op_domain\n')
           status = 'NEW'
           if root_domain == 'CCA' and source == 'CCA':
                success = 'CASE' + ',' + ingestion_id + ',' + status + ',' + 'CCA' + ',' + 'CASE'
                filehandle.write(success)
                filehandle.write('\n')
                success = 'UTILIZATIONMANAGEMENTEVENT' + ',' + ingestion_id + ',' + status + ',' + 'CCA' + ',' + 'UTILIZATIONMANAGEMENTEVENT'
                filehandle.write(success)

           elif root_domain == 'CCA' and source == 'MEMBER':
                root_source = 'MEMBER'
                success = root_source + ',' + ingestion_id + ',' + status + ',' + 'CCA' + ',' + 'MEMBER'
                filehandle.write(success)



           elif root_domain == 'QUEST':
                success = 'ORDER' + ',' + ingestion_id + ',' + status + ',' + 'QUEST' + ',' + 'ORDER'
                filehandle.write(success)
                filehandle.write('\n')
                success = 'OBSERVATION' + ',' + ingestion_id + ',' + status + ',' + 'QUEST' + ',' + 'OBSERVATION'
                filehandle.write(success)


           elif root_domain == 'PROVIDER':
                success = 'PROVIDERFACILITYRELATIONSHIP' + ',' + ingestion_id + ',' + status + ',' + 'PROVIDER' + ',' + 'PROVIDERFACILITYRELATIONSHIP'
                filehandle.write(success)
                filehandle.write('\n')
                success = 'SERVICEORGANIZATION' + ',' + ingestion_id + ',' + status + ',' + 'PROVIDER' + ',' + 'SERVICEORGANIZATION'
                filehandle.write(success)
                filehandle.write('\n')
                success = 'SERVICEPROVIDER' + ',' + ingestion_id + ',' + status + ',' + 'PROVIDER' + ',' + 'SERVICEPROVIDER'
                filehandle.write(success)

           else:
                root_source = root_domain
                success = root_source + ',' + ingestion_id + ',' + status + ',' + CONNECTOR + ',' + OP_DOMAIN
                filehandle.write(success)

        print("sleep for 2 minutes for Trigger file upload")
        time.sleep(5)
        dest_success_file_name = success_dest_prefix + success_file_name
        s3client.upload_file(success_file_name, dest_bucket_name, dest_success_file_name)

        now = datetime.datetime.now()
        date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
        job_end_time_ts = date_time

        run_cmp = open("job_report.txt", "a")
        run_cmp.write(root_domain + "|" + source + "|" + ingestion_id + "|" + job_start_time_ts + "|" + job_end_time_ts + "\n")
        run_cmp.close()
        s3client.upload_file('job_report.txt', CONFIG_BUCKET_NAME , REPORT_KEY)

        connector_file_name = 'connector/connector_' + C_CONNECTOR +'_'+C_SOURCE +'_'+ extract_time + '.csv'
        run_conn = open(connector_file_name, "w")
        run_conn.write(C_CONNECTOR + "," + C_SOURCE + "," + ingestion_id + "," + "NEW\n")
        run_conn.close()

        entity_file_name = 'entitylog/entity_' + C_CONNECTOR +'_'+C_SOURCE +'_'+ extract_time + '.csv'
        run_entity = open(entity_file_name, "w")

        if C_CONNECTOR in Y_SOURCE:
            run_entity.write(C_CONNECTOR + "," + C_SOURCE + "," + ingestion_id + "," + "STARTED,STARTED,"+str(date_sf)+"\n")
        else:
            run_entity.write(C_CONNECTOR + "," + C_SOURCE + "," + ingestion_id + "," + "STARTED,NA,"+str(date_sf)+"\n")

        run_entity.close()

        entity_dest_prefix = 'data-views/' + extract_loc + '/entity_load_log_landing/'+CONNECTOR_s3+'/' + ingestion_id + '/'
        
        if C_CONNECTOR in LANDING_V2_COPY:
            conn_dest_prefix = 'data-views/' + extract_loc + '/data/landing_v2/connector_trigger/'+CONNECTOR_s3+'/' + ingestion_id + '/'
        else:
            conn_dest_prefix = 'data-views/' + extract_loc + '/connector_trigger/'+CONNECTOR_s3+'/' + ingestion_id + '/'

        dest_entity_file_name = entity_dest_prefix + entity_file_name
        s3client.upload_file(entity_file_name, dest_bucket_name, dest_entity_file_name)
        dest_conn_file_name = conn_dest_prefix + connector_file_name
        s3client.upload_file(connector_file_name, dest_bucket_name, dest_conn_file_name)

        print("Extract End Time:- " + date_time + " : "+ ingestion_id)
        print("============================================")
        print("Sleep for 10 Mins before next ingestion extract\n")


        logging.info("============================================")
        logging.info("Extract End Time:- " + date_time + " : "+ ingestion_id)
        logging.info("Data Extract for ingestion completed for : "+ ingestion_id)
        logging.info("============================================")

        logs_prefix = 'data-views/' + extract_loc + '/snowflake_logs/domain_logs/'
        logging_file_name_s3 = CONNECTOR+"_"+OP_DOMAIN+"_"+extract_time+"_"+ingestion_id+".log"
        dest_log_file_name = logs_prefix + logging_file_name_s3
        s3client.upload_file(logging_file_name, dest_bucket_name, dest_log_file_name)

        time.sleep(5)
        print("Data Extract for ingestion completed\n")

if __name__ == '__main__':
    main()
