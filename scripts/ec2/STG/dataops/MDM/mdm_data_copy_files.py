import json
import datetime
import io
import time
import sys  
import os
import boto3
import subprocess
from boto3 import client
import pyarrow.parquet as pq
s3_conn   = client('s3')

currentDT = datetime.datetime.now()
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
ingestionid = list(sys.argv[2].split(",")) 
extract_time = currentDT.strftime("%Y%m%d%H%M")
run_count = 1

MANIFEST_CHECK = False
print(sys.argv[2])
print(type(sys.argv[2]))
if len(sys.argv) != 3:
    print("Usage: mdm_data_copy.py <source>  <DATE>")
    exit()
elif type(ingestionid) is not list:
    print("Please pass list for DATE\n")
    exit()
else:    
    env = sys.argv[1]


source = sys.argv[1]
if source == 'MEMBER':
    source_file = "mdm_member_data.txt"
elif source == 'PERSON':
    source_file = "mdm_person_data.txt"
else:
    print("please enter correct source details\n")
    exit()


CONFIG_BUCKET_NAME = 'abacus-sandboxes-cambiastg'
CONFIG_KEY = 'data-ops/etl/cambia_extract/config/mdm/MDM_run_completed.txt'


file = open(source_file, "r")
read = file.read()

for line in read.splitlines():
    if 'SOURCE_BUCKET=' in line:
        src_bucket_name = line.split('=',1)[1]
    elif 'TARGET_BUCKET=' in line:
        trg_bucket_name =  line.split('=',1)[1]
    elif 'TARGET_PREFIX=' in line:
        trg_prefix = line.split('=',1)[1]
    elif 'SOURCE_PREFIX=' in line:
        src_prefix = line.split('=',1)[1]


print("-------------------------------------------------------------------------------")
print("| Data Copy from Stage to distibution for source data is in progress          |")
print("| This sript copy data to s3 bucket abacus-data-lake-cambiastg                |")
print("| s3 replication is set up on this bucket to copy data to dist bcuket         |")
print("| Data Copy in Progess for DATE's  "+sys.argv[2]+"                            |")
print("| For MDM "+sys.argv[1]+"                                                     |")
print("-------------------------------------------------------------------------------")

print(src_prefix)
print(trg_prefix)
print(trg_bucket_name)
print(src_bucket_name)
print(source)
print(source_file)
print(ingestionid)


try:
    s3.Bucket(CONFIG_BUCKET_NAME).download_file(CONFIG_KEY, 'MDM_run_completed.txt')
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise



def copy_object(table_name, ingest, src_bucket, src_prefix, dest_bucket, dest_prefix, sleep_check):
    bucketobj = s3.Bucket(src_bucket)
    num = 1
    num1 = 1
    trg_prefix_count = ""
    if sleep_check:
        time.sleep(5)
    print("-----------------------------------------------------------------------")
    print("Data copy is running for table "+table_name +" Ingestion ID "+ingest)
    print("-----------------------------------------------------------------------")
    print(table_name)
    src_prefix = src_prefix+"/"+ingest
    for s3object in bucketobj.objects.filter(Prefix=src_prefix):
        srckey = s3object.key
        print(srckey)
        if not srckey.endswith('/') and not srckey.endswith('SUCCESS'):
            file_num = str(num)
            fileName = table_name + '_' + extract_time + '_' + file_num
            destfilekey = dest_prefix+"/"+table_name.lower()+"/"+ingest+"/"+fileName
            copysource = src_bucket+"/"+srckey
            file_name_json = list(copysource.split("/"))
            count_file = open("MDM_count_check.sh", "w")
            count_file.write("aws s3 cp s3://"+dest_bucket+"/"+destfilekey+" - | wc -l")
            count_file.close()
            s3.Object(dest_bucket, destfilekey).copy_from(CopySource=copysource)
            result = subprocess.run(['chmod', '+x', 'MDM_count_check.sh'], stdout=subprocess.PIPE)
            out = os.popen('./MDM_count_check.sh').read()
            manifest = "mdm_"+table_name+"_manifest_"+extract_time+".csv"
            mf_cmp = open(manifest, "a")
            mf_cmp.write("source_data,"+table_name+","+file_name_json[7]+","+out.strip()+"\n")
            mf_cmp.close()

            MANIFEST_CHECK = True
            num1 = num1 + 1
            num = num + 1
    run_cmp = open("MDM_run_completed.txt", "a")
    run_cmp.write(table_name+":"+ingest+"\n")
    run_cmp.close()
#            print("")
#            print("Number of files copied : "+str(num1))
#            print("")
#            s3obj = s3.Object( src_bucket, srckey)
#            filedata= s3obj.get()["Body"].read()
#            print(filedata.count)
#            s3objcount = s3.Object( dest_bucket, destfilekey)
#            s3objcount = s3.list_objects_v2(Bucket=src_bucket,Prefix=srckey)
#            fileCount = s3objcount['KeyCount']
#            print(src_bucket+"/"+srckey)
#            objs = boto3.client('s3').list_objects_v2(Bucket=src_bucket,Prefix=srckey)
#            fileCount = objs['KeyCount']
#            print(fileCount)
#            filedata= s3objcount.get()["Body"].read()
#            print(filedata.decode('utf8').count('\n')-1)
#    else:
#        print("----------------------Warning-----------------------------")
#        print("No data for SOurce "+source+" Ingestion ID "+i)
#        print("----------------------------------------------------------")
#        run_cmp = open("MDM_run_completed.txt", "a") 
#        run_cmp.write(table_name+":"+ingest+"|IngestionNotPresentForthisConnector\n")
#        run_cmp.close()


for i in ingestionid:
        print(" ")
        sleep_check = False
        if run_count > 1:
            sleep_check = True
            
        table_name = source
    
        table_ingestion = table_name+":"+i
        logfile = open('MDM_run_completed.txt', 'r')
        loglist = logfile.readlines()
        logfile.close()
        found = False
        manifest = "mdm_"+table_name+"_manifest_"+extract_time+".csv"

        for line1 in loglist:
            if str(table_ingestion) in line1:
                print("----------------------------------------------------------------------------------------------------")
                print("Data Copy for table "+table_name+" ingestion id "+i+" is already done")
                print("Delete the row which needs re run from s3://"+CONFIG_BUCKET_NAME+"/"+CONFIG_KEY+" & run again")
                print("----------------------------------------------------------------------------------------------------")
                found = True

        if not found:
                copy_object(table_name, i, src_bucket_name, src_prefix, trg_bucket_name, trg_prefix, sleep_check)
                print("Running data load for ingestion "+source+ i)
                MANIFEST_CHECK = True
        run_count = run_count + 1
        print(" ")
              

manifest = "mdm_"+table_name+"_manifest_"+extract_time+".csv"
s3client.upload_file('MDM_run_completed.txt', CONFIG_BUCKET_NAME , CONFIG_KEY)
#s3client.upload_file('MDM_run_completed.txt', CONFIG_BUCKET_NAME , CONFIG_KEY+"_"+extract_time)
if MANIFEST_CHECK:
        s3client.upload_file(manifest, trg_bucket_name, trg_prefix+"/manifest/"+manifest)
else:
        print("No data copied today")
exit()

s3_result =  s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter = "/")

if 'Contents' not in s3_result:
        print(s3_result)
