import json
import datetime
import io
import time
import sys  
import os
import boto3
from boto3 import client
import pyarrow.parquet as pq
s3_conn   = client('s3')

currentDT = datetime.datetime.now()
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
ingestionid = list(sys.argv[3].split(",")) 
extract_time = currentDT.strftime("%Y%m%d%H%M")
run_count = 1

if len(sys.argv) != 4:
    print("Usage: copy_source_data.py <source> <sub_source> <ingestion_id>")
    exit()
elif type(ingestionid) is not list:
    print("Please pass list for ingestionid\n")
    exit()
else:    
    env = sys.argv[1]

source = sys.argv[1]
if source == 'prime':
    source_file = "prime_source_data.txt"
elif source == 'vsp':
    source_file = "vsp_source_data.txt"
elif source == 'facets':
    source_file = "facets_source_data.txt"
else:
    print("please enter correct source details\n")
    exit()


CONFIG_BUCKET_NAME = 'abacus-sandboxes-cambiastg'
CONFIG_KEY = 'data-ops/etl/cambia_transfer/config/source_data/run_completed.txt'


file = open(source_file, "r")
read = file.read()

for line in read.splitlines():
    if 'SOURCES=' in line:
        SOURCES = line.split('=',1)[1]
    elif 'SOURCE_BUCKET=' in line:
        src_bucket_name = line.split('=',1)[1]
    elif 'TARGET_BUCKET=' in line:
        trg_bucket_name =  line.split('=',1)[1]
    elif 'TARGET_PREFIX=' in line:
        trg_prefix = line.split('=',1)[1]


print("-------------------------------------------------------------------------------")
print("| Data Copy from Stage to distibution for source data is in progress          |")
print("| This sript copy data to s3 bucket abacus-data-lake-cambiastg                |")
print("| s3 replication is set up on this bucket to copy data to dist bcuket         |")
print("| Data Copy in Progess for Ingestion ID's "+sys.argv[3]+"                         |")
print("| For "+sys.argv[1]+" "+SOURCES+" sources                            |")
print("-------------------------------------------------------------------------------")

try:
    s3.Bucket(CONFIG_BUCKET_NAME).download_file(CONFIG_KEY, 'run_completed.txt')
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise


SOURCE = list(SOURCES.split(","))

def s3_object_rowcount(source,dest_bucket_name, prefix, table_name, job_id):
    print(dest_bucket_name+":"+prefix)
    try:
        response = s3client.list_objects(Bucket=dest_bucket_name, Prefix=prefix, MaxKeys=100)
        objects = response["Contents"]
        for i in objects:
            file_name = i['Key']
            buffer = io.BytesIO()
            s3object = s3.Object(dest_bucket_name, file_name)
#            print(s3object)
            s3object.download_fileobj(buffer)
            df = pq.ParquetFile(buffer)
            rowcount = df.metadata.num_rows
#            print(file_name)
            f_name = file_name.split('/', 8)
            f_name1 = f_name[8]
            manifest = source+"_manifest_"+extract_time+".csv"
            mf_cmp = open(manifest, "a")
            mf_cmp.write(source + ',' + job_id + ',' + table_name + ',' + 'source_data' + ',' + f_name1 + ',' + str(rowcount) +"\n")
            mf_cmp.close()
#            print(source + ',' + job_id + ',' + table_name + ',' + 'source_data' + ',' + f_name1 + ',' + str(rowcount))

    except KeyError:
        print('Cannot read s3 file')

def copy_object(table_name, ingest, src_bucket, src_prefix, dest_bucket, dest_prefix, sleep_check, f_name_2):
    bucketobj = s3.Bucket(src_bucket)
    num = 1
    num1 = 1
    trg_prefix_count = ""
    if sleep_check:
        time.sleep(20)
    print("-----------------------------------------------------------------------")
    print("Data copy is running for table "+table_name +" Ingestion ID "+ingest)
    print("-----------------------------------------------------------------------")
#    print(table_name)
#    print(ingest)
#    print(src_bucket)
#    print(src_prefix)
    for s3object in bucketobj.objects.filter(Prefix=src_prefix):
        srckey = s3object.key
        if not srckey.endswith('/') and not srckey.endswith('SUCCESS'):
            file_num = str(num)
            fileName = table_name + '_' + extract_time + '_' + file_num
            ingestion = list(srckey.split("/"))
            fileprefix = f_name_2+"/"+ingestion[3]+"/"+ingestion[4]+"/"+ingestion[5]+"/"+ingest+"/"+fileName
            destfilekey = dest_prefix+"/"+fileprefix
            copysource = dest_bucket+"/"+srckey
            if ingestion[6] == ingest:
#                print(src_bucket+":"+srckey)
#                print(dest_bucket+":"+dest_prefix+"/"+fileprefix)
                trg_prefix_count = dest_prefix+"/"+f_name_2+"/"+ingestion[3]+"/"+ingestion[4]+"/"+ingestion[5]+"/"+ingest                
                s3.Object(dest_bucket, destfilekey).copy_from(CopySource=copysource)
                num1 = num1 + 1
            num = num + 1
    time.sleep(10)       
    
    if trg_prefix_count:
        s3_object_rowcount(source,dest_bucket,trg_prefix_count,table_name, i)
        run_cmp = open("run_completed.txt", "a")
        run_cmp.write(table_name+":"+ingest+"\n")
        run_cmp.close()
        print("")
        print("Number of files copied : "+str(num1))
        print("")
    else:
        print("----------------------Warning-----------------------------")
        print("No data for SOurce "+source+" Ingestion ID "+i)
        print("----------------------------------------------------------")
        run_cmp = open("run_completed.txt", "a") 
        run_cmp.write(table_name+":"+ingest+"|IngestionNotPresentForthisConnector\n")
        run_cmp.close()


for i in ingestionid:
    for srcs in SOURCE:
        if source == 'prime':        
            for line in read.splitlines():
                if srcs == 'standard':
                    if 'STANDARD=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_standard'
                elif srcs == 'supplemental':
                    if 'SUPPLEMENTAL=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_supplemental'
                elif srcs == 'compound':
                    if 'COMPOUND=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_compound'
                else:
                    print("prime source is invalid check the config file \n")
                    exit()
        elif source == 'vsp':
            for line in read.splitlines():
                if srcs == 'common_common':
                    if 'COMMON_COMMON=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'vsp_common'
                elif srcs == 'medicare_mcare':
                    if 'MEDICARE_MCARE=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'vsp_medicare'
                else:
                    print(srcs)
                    print("vsp source is invalid check the config file \n")
                    exit()
        elif source == 'facets':
            for line in read.splitlines():
                if srcs == 'member_addr':
                    if 'MEMBER_ADDR=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_member_addr'
                elif srcs == 'member':
                    if 'MEMBER=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_member'
                elif srcs == 'enrollment':
                    if 'ENROLLMENT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_enrollment'
                else:
                    print(srcs)
                    print("vsp source is invalid check the config file \n")
                    exit()                        


        print(" ")
        sleep_check = False
        if run_count > 1:
            sleep_check = True
            
        table_name = source + "_" + srcs
    
        table_ingestion = table_name+":"+i
        logfile = open('run_completed.txt', 'r')
        loglist = logfile.readlines()
        logfile.close()
        found = False
        for line1 in loglist:
            if str(table_ingestion) in line1:
                print("----------------------------------------------------------------------------------------------------")
                print("Data Copy for table "+table_name+" ingestion id "+i+" is already done")
                print("Delete the row which needs re run from s3://"+CONFIG_BUCKET_NAME+"/"+CONFIG_KEY+" & run again")
                print("----------------------------------------------------------------------------------------------------")
                found = True

        if not found:
                copy_object(table_name, i, src_bucket_name, src_prefix, trg_bucket_name, trg_prefix, sleep_check, f_name_2)
#                s3_object_rowcount(src_bucket_name,trg_prefix,table_name, i)
        run_count = run_count + 1
        print(" ")
              

manifest = source+"_manifest_"+extract_time+".csv"
s3client.upload_file('run_completed.txt', CONFIG_BUCKET_NAME , CONFIG_KEY)
s3client.upload_file('run_completed.txt', CONFIG_BUCKET_NAME , CONFIG_KEY+"_"+extract_time)
s3client.upload_file(manifest, trg_bucket_name, trg_prefix+"/manifest/"+manifest)
exit()

s3_result =  s3_conn.list_objects_v2(Bucket=bucket_name, Prefix=prefix, Delimiter = "/")

if 'Contents' not in s3_result:
        print(s3_result)
