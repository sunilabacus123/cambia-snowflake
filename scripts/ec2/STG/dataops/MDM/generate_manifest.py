import json
import datetime
import io
import time
import sys
import os
import boto3
import subprocess
from boto3 import client
import pyarrow.parquet as pq
s3_conn   = client('s3')

currentDT = datetime.datetime.now()
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
extract_time = currentDT.strftime("%Y%m%d%H%M")
ingestionid = list(sys.argv[3].split(","))
srcbk = sys.argv[2]
source = sys.argv[1]

def copy_object(table_name, ingest, src_bucket, src_prefix, dest_bucket, dest_prefix):
    bucketobj = s3.Bucket(src_bucket)
    print("-----------------------------------------------------------------------")
    print(table_name)
    print(src_bucket+"/"+src_prefix)
    for s3object in bucketobj.objects.filter(Prefix=src_prefix):
        srckey = s3object.key
        print(srckey)
        if not srckey.endswith('/') and not srckey.endswith('SUCCESS'):
            destfilekey = srckey
            file_name_json = srckey.split("/")
            count_file = open("MDM_count_check.sh", "w")
            count_file.write("aws s3 cp s3://"+dest_bucket+"/"+destfilekey+" - | gunzip | wc -l")
            count_file.close()
            result = subprocess.run(['chmod', '+x', 'MDM_count_check.sh'], stdout=subprocess.PIPE)
            out = os.popen('./MDM_count_check.sh').read()
            manifest = "mdm_"+table_name+"_manifest_"+extract_time+".csv"
            mf_cmp = open(manifest, "a")
            mf_cmp.write("source_data,"+table_name+","+file_name_json[7]+","+out.strip()+"\n")
            mf_cmp.close()

            MANIFEST_CHECK = True
    run_cmp = open("MDM_run_completed.txt", "a")
    run_cmp.write(table_name+":"+ingest+"\n")
    run_cmp.close()

for i in ingestionid:
    print(srcbk+"/"+i)
    file = srcbk+"/"+i
    copy_object(source, '2020/11/06', 'abacus-stage-distro-cambia', file, 'abacus-stage-distro-cambia', file)
