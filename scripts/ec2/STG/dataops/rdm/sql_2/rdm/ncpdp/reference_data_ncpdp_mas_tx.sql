CREATE TABLE cambiastg_data_ops.reference_data_ncpdp_mas_tx_replace_date as
SELECT
"1_ncpdpproviderid" as ncpdpproviderid
,"2_taxonomycode" as taxonomycode
,"3_providertypecode" as providertypecode
,"4_deletedate" as deletedate
from  cambiastg_data_sources.reference_data_ncpdp_mas_tx
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
