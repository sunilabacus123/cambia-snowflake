CREATE TABLE cambiastg_data_ops.reference_data_ncpdp_mas_af_replace_date as
SELECT
"1_relationshipid" as relationshipid
,"2_relationshiptype" as relationshiptype
,"3_name" as name
,"4_address1" as address1
,"5_address2" as address2
,"6_city" as city
,"7_statecode" as statecode
,"8_zipcode" as zipcode
,"9_phonenumber" as phonenumber
,"10_extension" as extension
,"11_faxnumber" as faxnumber
,"12_relationshipnpi" as relationshipnpi
,"13_relationshipfederaltaxid" as relationshipfederaltaxid
,"14_contactname" as contactname
,"15_contacttitle" as contacttitle
,"16_e_mailaddress" as e_mailaddress
,"17_contractualcontactname" as contractualcontactname
,"18_contractualcontacttitle" as contractualcontacttitle
,"19_contractualcontacte_mail" as contractualcontacte_mail
,"20_operationalcontactname" as operationalcontactname
,"21_operationalcontacttitle" as operationalcontacttitle
,"22_operationalcontacte_mail" as operationalcontacte_mail
,"23_technicalcontactname" as technicalcontactname
,"24_technicalcontacttitle" as technicalcontacttitle
,"25_technicalcontacte_mail" as technicalcontacte_mail
,"26_auditcontactname" as auditcontactname
,"27_auditcontacttitle" as auditcontacttitle
,"28_auditcontacte_mail" as auditcontacte_mail
,"29_parentorganizationid" as parentorganizationid
,"30_effectivefromdate" as effectivefromdate
,"31_deletedate" as deletedate
from  cambiastg_data_sources.reference_data_ncpdp_mas_af
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
