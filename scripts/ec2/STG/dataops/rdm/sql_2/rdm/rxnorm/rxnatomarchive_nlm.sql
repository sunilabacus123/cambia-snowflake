CREATE TABLE cambiastg_data_ops.rxnatomarchive_nlm_replace_date as
SELECT
"atom_id" as atom_id
,"meta_atom_id" as meta_atom_id
,"string" as string
,"archive_date" as archive_date
,"create_date" as create_date
,"update_date" as update_date
,"code" as code
,"language" as language
,"rxnorm_term_dt" as rxnorm_term_dt
,"version_source_abb" as version_source_abb
,"concept_id" as concept_id
,"concept_source_abb" as concept_source_abb
,"term_type" as term_type
,"new_rxcui" as new_rxcui
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.rxnatomarchive_nlm;
