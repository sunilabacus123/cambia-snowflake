CREATE TABLE cambiastg_data_ops.medispan_dfdrg_wk_replace_date as
SELECT
"concept type" as concept_type
,"country code" as country_code
,"concept id" as concept_id
,"transaction cd" as transaction_cd
,"dose form id" as dose_form_id
,"status" as status
,"link value" as link_value
,"link date" as link_date
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.medispan_dfdrg_wk;
