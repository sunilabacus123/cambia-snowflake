CREATE TABLE cambiastg_data_ops.medispan_ings_wk_replace_date as
SELECT
"ingredient set id" as ingredient_set_id
,"ingredient id" as ingredient_id
,"ingredient flag" as ingredient_flag
,"transaction cd" as transaction_cd
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.medispan_ings_wk;
