CREATE TABLE cambiastg_data_ops.medispan_idrg_wk_replace_date as
SELECT
"ingredient drug id" as ingredient_drug_id
,"transaction cd" as transaction_cd
,"cas number" as cas_number
,"ingredient drug name" as ingredient_drug_name
,"generic id" as generic_id
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.medispan_idrg_wk;
