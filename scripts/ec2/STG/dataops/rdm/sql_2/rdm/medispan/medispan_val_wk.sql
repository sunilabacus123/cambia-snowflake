CREATE TABLE cambiastg_data_ops.medispan_val_wk_replace_date as
SELECT
"field identifier" as field_identifier
,"field value" as field_value
,"language code" as language_code
,"value description" as value_description
,"value abbreviation" as value_abbreviation
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.medispan_val_wk;
