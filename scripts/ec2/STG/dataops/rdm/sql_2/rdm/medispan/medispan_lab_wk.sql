CREATE TABLE cambiastg_data_ops.medispan_lab_wk_replace_date as
SELECT
"labeler identifier" as labeler_identifier
,"manufacturer name" as manufacturer_name
,"manufacturer abbr name" as manufacturer_abbr_name
,"labeler type code" as labeler_type_code
,"transaction cd" as transaction_cd
,"last change date" as last_change_date
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.medispan_lab_wk;
