CREATE TABLE cambiastg_data_ops.county_gpci_cms_replace_date as
SELECT
"carrier" as carrier
,"state" as state
,"locality number" as locality_number
,"locality name" as locality_name
,"2019 pw gpci-with 1.0 floor" as "2019_pw_gpci_with_1_floor"
,"2019 pe gpci" as "2019_pe_gpci"
,"2019 mp gpci" as "2019_mp_gpci"
,"2020 pw gpci-with 1.0 floor" as "2020_pw_gpci_with_1_floor"
,"2020 pw gpci-without 1.0 floor" as "2020_pw_gpci_without_1_floor"
,"2020 pe gpci" as "2020_pe_gpci"
,"2020 mp gpci" as "2020_mp_gpci"
,"2021 pw gpci" as "2021_pw_gpci"
,"2021 pe gpci" as "2021_pe_gpci"
,"2021 mp gpci" as "2021_mp_gpci"
,"code_set" as code_set
,"version_month" as version_month
from  cambiastg_data_reference_codes.county_gpci_cms;
