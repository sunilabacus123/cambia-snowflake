CREATE TABLE cambia_data_ops.zip_code_crosswalk_dartmouth_atlas_replace_date as
SELECT
"zip_code" as zip_code
,"hsa_num" as hsa_num
,"hsa_city" as hsa_city
,"has_state" as has_state
,"hrr_num" as hrr_num
,"hrr_city" as hrr_city
,"hrr_state" as hrr_state
,"code_set" as code_set
,"last_release_year" as last_release_year
from  cambia_data_reference_codes.zip_code_crosswalk_dartmouth_atlas;
