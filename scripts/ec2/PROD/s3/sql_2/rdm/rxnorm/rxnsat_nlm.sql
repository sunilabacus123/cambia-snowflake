CREATE TABLE cambia_data_ops.rxnsat_nlm_replace_date as
SELECT
"concept_id" as concept_id
,"atom_id" as atom_id
,"id_type" as id_type
,"code" as code
,"attribute_id" as attribute_id
,"attribute_name" as attribute_name
,"attribute_source_abb" as attribute_source_abb
,"attribute_value" as attribute_value
,"suppress" as suppress
,"content_view_flag" as content_view_flag
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.rxnsat_nlm;
