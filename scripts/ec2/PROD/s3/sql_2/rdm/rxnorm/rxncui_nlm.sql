CREATE TABLE cambia_data_ops.rxncui_nlm_replace_date as
SELECT
"term_concept_id" as term_concept_id
,"version_start" as version_start
,"version_term" as version_term
,"cardinality" as cardinality
,"synonym_concept_id" as synonym_concept_id
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.rxncui_nlm;
