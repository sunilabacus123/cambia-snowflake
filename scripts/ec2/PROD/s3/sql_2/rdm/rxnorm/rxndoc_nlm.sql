CREATE TABLE cambia_data_ops.rxndoc_nlm_replace_date as
SELECT
"attribute" as attribute
,"abb_value" as abb_value
,"description_type" as description_type
,"description" as description
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.rxndoc_nlm;
