CREATE TABLE cambia_data_ops.rxnconso_nlm_replace_date as
SELECT
"concept_id" as concept_id
,"language" as language
,"atom_id" as atom_id
,"source_atom_id" as source_atom_id
,"source_concept_id" as source_concept_id
,"source_descriptor_id" as source_descriptor_id
,"concept_source_abb" as concept_source_abb
,"term_type" as term_type
,"code" as code
,"string" as string
,"suppress" as suppress
,"content_view_flag" as content_view_flag
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.rxnconso_nlm;
