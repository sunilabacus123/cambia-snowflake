CREATE TABLE cambia_data_ops.rxnrel_nlm_replace_date as
SELECT
"concept_id1" as concept_id1
,"atom_id1" as atom_id1
,"id_type1" as id_type1
,"relation1" as relation1
,"concept_id2" as concept_id2
,"atom_id2" as atom_id2
,"id_type2" as id_type2
,"relation2" as relation2
,"relation_id" as relation_id
,"rel_source_abbr" as rel_source_abbr
,"rg_indicator" as rg_indicator
,"content_view_flag" as content_view_flag
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.rxnrel_nlm;
