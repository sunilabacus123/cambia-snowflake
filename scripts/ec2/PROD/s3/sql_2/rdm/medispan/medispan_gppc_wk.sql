CREATE TABLE cambia_data_ops.medispan_gppc_wk_replace_date as
SELECT
"gppc" as gppc
,"package size" as package_size
,"package size uom" as package_size_uom
,"package qty" as package_qty
,"unit dose package code" as unit_dose_package_code
,"package desc code" as package_desc_code
,"transaction cd" as transaction_cd
,"last change date" as last_change_date
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.medispan_gppc_wk;
