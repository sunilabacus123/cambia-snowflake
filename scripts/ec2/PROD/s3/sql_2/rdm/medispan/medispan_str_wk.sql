CREATE TABLE cambia_data_ops.medispan_str_wk_replace_date as
SELECT
"ingredient identifier" as ingredient_identifier
,"transaction cd" as transaction_cd
,"ingredient drug id" as ingredient_drug_id
,"ingredient strength value" as ingredient_strength_value
,"combined ingredient strength uom" as combined_ingredient_strength_uom
,"individual ingredient strength uom" as individual_ingredient_strength_uom
,"volume value" as volume_value
,"volume uom" as volume_uom
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.medispan_str_wk;
