CREATE TABLE cambia_data_ops.medispan_sum_wk_replace_date as
SELECT
"record type" as record_type
,"sequence number" as sequence_number
,"comment marker" as comment_marker
,"data/comment" as data_comment
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.medispan_sum_wk;
