CREATE TABLE cambia_data_ops.medispan_cph_wk_replace_date as
SELECT
"external drug id" as external_drug_id
,"id number format code" as id_number_format_code
,"price code" as price_code
,"price effective date" as price_effective_date
,"unit price" as unit_price
,"package price" as package_price
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.medispan_cph_wk;
