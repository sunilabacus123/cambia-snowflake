CREATE TABLE cambia_data_ops.medispan_dict_wk_replace_date as
SELECT
"field identifier" as field_identifier
,"field description" as field_description
,"field type" as field_type
,"field length" as field_length
,"implied decimal flag" as implied_decimal_flag
,"decimal places" as decimal_places
,"field validation flag" as field_validation_flag
,"field abbreviation flag" as field_abbreviation_flag
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.medispan_dict_wk;
