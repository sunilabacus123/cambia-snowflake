CREATE TABLE cambia_data_ops.medispan_name_ndc_wk_replace_date as
SELECT
"ndc-upc-hri" as ndc_upc_hri
,"tee code" as tee_code
,"dea class code" as dea_class_code
,"desi code" as desi_code
,"rx-otc indicator code" as rx_otc_indicator_code
,"gppc" as gppc
,"old ndc-upc-hri" as old_ndc_upc_hri
,"new ndc-upc-hri" as new_ndc_upc_hri
,"repackage code" as repackage_code
,"id number format code" as id_number_format_code
,"third party restriction code" as third_party_restriction_code
,"kdc flag" as kdc_flag
,"medispan labeler identifier" as medispan_labeler_identifier
,"multi source code" as multi_source_code
,"name type code" as name_type_code
,"item status flag" as item_status_flag
,"innerpack code" as innerpack_code
,"clinic pack code" as clinic_pack_code
,"ppg indicator code" as ppg_indicator_code
,"hfpg indicator code" as hfpg_indicator_code
,"dispensing unit code" as dispensing_unit_code
,"dollar rank code" as dollar_rank_code
,"rx rank code" as rx_rank_code
,"storage condition code" as storage_condition_code
,"limited distribution code" as limited_distribution_code
,"old effective date" as old_effective_date
,"new effective date" as new_effective_date
,"next smaller ndc suffix number" as next_smaller_ndc_suffix_number
,"next larger ndc suffix number" as next_larger_ndc_suffix_number
,"transaction cd" as transaction_cd
,"last change date" as last_change_date
,"drug name" as drug_name
,"route of administration code" as route_of_administration_code
,"dosage form" as dosage_form
,"strength" as strength
,"strength uom" as strength_uom
,"bioequivalence code" as bioequivalence_code
,"controlled substance code" as controlled_substance_code
,"efficacy code" as efficacy_code
,"legend indicator code" as legend_indicator_code
,"multi-source summary code" as multi_source_summary_code
,"brand name code" as brand_name_code
,"name source code" as name_source_code
,"screenable flag" as screenable_flag
,"local/systemic code" as systemic_code
,"maintenance drug code" as maintenance_drug_code
,"form type code" as form_type_code
,"internal-external code" as internal_external_code
,"single combination code" as single_combination_code
,"representative gpi flag" as representative_gpi_flag
,"representative kdc flag" as representative_kdc_flag
,"transaction cd 1" as transaction_cd_1
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.medispan_name_ndc_wk;
