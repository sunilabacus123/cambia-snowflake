CREATE TABLE cambia_data_ops.medispan_sec_wk_replace_date as
SELECT
"external drug id" as external_drug_id
,"external drug id format" as external_drug_id_format
,"alternate drug id" as alternate_drug_id
,"alternate drug id format code" as alternate_drug_id_format_code
,"transaction cd" as transaction_cd
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.medispan_sec_wk;
