CREATE TABLE cambia_data_ops.medispan_dfdrg_drgnm_wk_replace_date as
SELECT
"concept type" as concept_type
,"country code" as country_code
,"concept id" as concept_id
,"transaction cd" as transaction_cd
,"dose form id" as dose_form_id
,"status" as status
,"link value" as link_value
,"link date" as link_date
,"name type" as name_type
,"code_set" as code_set
,"version_month" as version_month
from  cambia_data_reference_codes.medispan_dfdrg_drgnm_wk;
