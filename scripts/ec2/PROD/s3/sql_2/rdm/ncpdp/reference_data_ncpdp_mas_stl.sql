CREATE TABLE cambia_data_ops.reference_data_ncpdp_mas_stl_replace_date as
SELECT
"1_ncpdpproviderid" as ncpdpproviderid
,"2_licensestatecode" as licensestatecode
,"3_statelicensenumber" as statelicensenumber
,"4_statelicenseexpirationdate" as statelicenseexpirationdate
,"5_deletedate" as deletedate
from  cambia_data_sources.reference_data_ncpdp_mas_stl
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
