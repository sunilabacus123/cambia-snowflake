CREATE TABLE cambia_data_ops.reference_data_ncpdp_mas_svc_replace_date as
SELECT
"1_ncpdpproviderid" as ncpdpproviderid
,"2_acceptseprescriptionsindicator" as acceptseprescriptionsindicator
,"3_acceptseprescriptionscode" as acceptseprescriptionscode
,"4_deliveryserviceindicator" as deliveryserviceindicator
,"5_deliveryservicecode" as deliveryservicecode
,"6_compoundingserviceindicator" as compoundingserviceindicator
,"7_compoundingservicecode" as compoundingservicecode
,"8_driveupwindowindicator" as driveupwindowindicator
,"9_driveupwindowcode" as driveupwindowcode
,"10_durablemedicalequipmentindicator" as durablemedicalequipmentindicator
,"11_durablemedicalequipmentcode" as durablemedicalequipmentcode
,"12_walkinclinicindicator" as walkinclinicindicator
,"13_walkincliniccode" as walkincliniccode
,"14_24houremergencyserviceindicator" as "24houremergencyserviceindicator"
,"15_24houremergencyservicecode" as "24houremergencyservicecode"
,"16_multidosecompliancepackagingindicator" as multidosecompliancepackagingindicator
,"17_multidosecompliancepackagingcode" as multidosecompliancepackagingcode
,"18_immunizationsprovidedindicator" as immunizationsprovidedindicator
,"19_immunizationsprovidedcode" as immunizationsprovidedcode
,"20_handicappedaccessibleindicator" as handicappedaccessibleindicator
,"21_handicappedaccessiblecode" as handicappedaccessiblecode
,"22_340bstatusindicator" as "340bstatusindicator"
,"23_340bstatuscode" as "340bstatuscode"
,"24_closeddoorfacilityindicator" as closeddoorfacilityindicator
,"25_closeddoorfacilitystatuscode" as closeddoorfacilitystatuscode
from  cambia_data_sources.reference_data_ncpdp_mas_svc
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
