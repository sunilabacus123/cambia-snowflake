CREATE TABLE cambia_data_ops.reference_data_ncpdp_mas_rr_replace_date as
SELECT
"1_ncpdpproviderid" as ncpdpproviderid
,"2_relationshipid" as relationshipid
,"3_paymentcenterid" as paymentcenterid
,"4_remitandreconciliationid" as remitandreconciliationid
,"5_providertype" as providertype
,"6_isprimary" as isprimary
,"7_effectivefromdate" as effectivefromdate
,"8_effectivethroughdate" as effectivethroughdate
from  cambia_data_sources.reference_data_ncpdp_mas_rr
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
