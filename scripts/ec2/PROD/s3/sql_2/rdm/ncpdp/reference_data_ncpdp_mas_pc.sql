CREATE TABLE cambia_data_ops.reference_data_ncpdp_mas_pc_replace_date as
SELECT
"1_paymentcenterid" as paymentcenterid
,"2_paymentcentername" as paymentcentername
,"3_paymentcenteraddress1" as paymentcenteraddress1
,"4_paymentcenteraddress2" as paymentcenteraddress2
,"5_paymentcentercity" as paymentcentercity
,"6_paymentcenterstatecode" as paymentcenterstatecode
,"7_paymentcenterzipcode" as paymentcenterzipcode
,"8_paymentcenterphonenumber" as paymentcenterphonenumber
,"9_paymentcenterextension" as paymentcenterextension
,"10_paymentcenterfaxnumber" as paymentcenterfaxnumber
,"11_paymentcenternpi" as paymentcenternpi
,"12_paymentcenterfederaltaxid" as paymentcenterfederaltaxid
,"13_paymentcentercontactname" as paymentcentercontactname
,"14_paymentcentercontacttitle" as paymentcentercontacttitle
,"15_paymentcentere_mailaddress" as paymentcentere_mailaddress
,"16_deletedate" as deletedate
from  cambia_data_sources.reference_data_ncpdp_mas_pc
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
