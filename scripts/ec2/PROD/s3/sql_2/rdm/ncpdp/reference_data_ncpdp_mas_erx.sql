CREATE TABLE cambia_data_ops.reference_data_ncpdp_mas_erx_replace_date as
SELECT
"1_ncpdpproviderid" as ncpdpproviderid
,"2_eprescribingnetworkidentifier" as eprescribingnetworkidentifier
,"3_eprescribingservicelevelcodes" as eprescribingservicelevelcodes
,"4_effectivefromdate" as effectivefromdate
,"5_effectivethroughdate" as effectivethroughdate
from  cambia_data_sources.reference_data_ncpdp_mas_erx
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
