CREATE TABLE cambia_data_ops.reference_data_ncpdp_mas_md_replace_date as
SELECT
"1_ncpdpproviderid" as ncpdpproviderid
,"2_statecode" as statecode
,"3_medicaidid" as medicaidid
,"4_deletedate" as deletedate
from  cambia_data_sources.reference_data_ncpdp_mas_md
where CAST(partition_3 AS INTEGER) = replace_ingestionid;
