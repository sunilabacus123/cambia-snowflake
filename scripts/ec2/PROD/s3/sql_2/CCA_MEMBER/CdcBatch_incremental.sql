CREATE TABLE CdcBatch_replacesuffix AS
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE, consumetimestamp, 
BATCHID, INGESTIONID FROM cambia_data_main.CONN_UNNESTED_MEMBER
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambia_data_main.CONN_UNNESTED_MEMBER WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid and source = 'CCA'
  )
AND SOURCE = 'CCA'
ORDER BY 1,2
;
