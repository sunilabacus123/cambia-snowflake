CREATE TABLE CodedValue_replacesuffix
WITH
(
	bucket_count=60,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,CodeGroup
            ,CodeType
            ,CodeCategory
            ,CodeValue
            ,CodeValueType
            ,RangeMin
            ,RangeMax
            ,CodeID
            ,partition_3
FROM
        cambia_data_main.conn_unnested_CodedValue
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
);
