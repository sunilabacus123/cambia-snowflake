CREATE TABLE CdcBatch_replacesuffix AS
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE,
BATCHID, INGESTIONID FROM cambia_data_main.CONN_UNNESTED_Observation
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambia_data_main.CONN_UNNESTED_Observation WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid
  )ORDER BY 1,2;
