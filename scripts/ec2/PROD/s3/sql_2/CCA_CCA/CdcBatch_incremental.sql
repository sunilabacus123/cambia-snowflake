CREATE TABLE CdcBatch_replacesuffix AS
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE,consumetimestamp,
BATCHID, INGESTIONID FROM cambia_data_main.CONN_UNNESTED_CASE
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambia_data_main.CONN_UNNESTED_CASE WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid AND SOURCE = 'CCA'
  )
AND SOURCE = 'CCA'
UNION
SELECT  DISTINCT 'replace_connector' AS CONNECTOR,
SOURCE,consumetimestamp,
BATCHID, INGESTIONID FROM cambia_data_main.CONN_UNNESTED_UtilizationManagementEvent
WHERE BATCHID =
(
  SELECT DISTINCT BATCHID FROM cambia_data_main.CONN_UNNESTED_UtilizationManagementEvent WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid AND SOURCE = 'CCA'
  )
AND SOURCE = 'CCA'
  ORDER BY 1,2
