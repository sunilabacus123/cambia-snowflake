CREATE TABLE Inpatient_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,RequestedAdmissionDate
            ,AuthorizedAdmissionDate
            ,ActualAdmissionDate
            ,RequestedEndDate
            ,TotalRequestedDays
            ,TotalAuthorizedDays
            ,ExpectedDischargeDate
            ,RequestedDischargeDate
            ,NextReviewDateTime
            ,Username
            ,ActiveInd
            ,OverallTotalCost
            ,OverallCostSavings
            ,PresentOnAdmissionInd
            ,DischargeReason
            ,OriginalRequestedDays
            ,ServiceType
            ,OnsetDate
            ,AutoCalculateInd
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambia_data_main.conn_unnested_Inpatient
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
