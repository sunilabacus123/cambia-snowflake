CREATE TABLE Diagnosis_replacesuffix
WITH
(
	bucket_count=5,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Condition
            ,ConditionCause
            ,DiagnosisGroup
            ,DiagnosisCategory
            ,DiagnosisType
            ,DiagnosisClass
            ,Codeset
            ,CodesetType
            ,CodeQualifier
            ,CodeQualifierType
            ,PrimaryInd
            ,Amount
            ,AmountType
            ,DiagnosisGroupAllowedAmount
            ,Role
            ,Sequence
            ,SourceDescription
            ,SourceAlternateDescription
            ,EffDate
            ,PresentOnAdmissionCode
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambia_data_main.conn_unnested_Diagnosis
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
