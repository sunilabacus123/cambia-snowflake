CREATE TABLE ClaimItem_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
  SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,Sequence
            ,ClaimItemStatus
            ,Revenue
            ,RevenueCode
            ,Category
            ,ProductOrService
            ,ProductOrServiceDesc
            ,ProductOrServiceType
            ,CoordinationOfBenefitsCode
            ,CoordinationOfBenefitsDesc
            ,Modifier
            ,ProgramCode
            ,FirstDateOfService
            ,LastDateOfService
            ,PlaceOfService
            ,Quantity
            ,UnitType
            ,UnitPrice
            ,ItemCost
            ,ChargeAmount
            ,BodySite
            ,SubSite
            ,NetworkInd
            ,NetworkID
            ,partition_3
	        ,IncurredDate
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                line.*,
				CASE 
				   WHEN header.Claimtype = 'Institutional' 
				   THEN CASE
							WHEN ( hosp.FacilityBillTypeCode = '02' AND hosp.BillTypeClassificationCode  = '2' OR
								   hosp.FacilityBillTypeCode = '03' AND hosp.BillTypeClassificationCode  = '2' 
							)
							THEN 
								CASE WHEN CAST(header.StatementFromDate AS DATE) <> DATE '1753-01-01' AND CAST(header.StatementFromDate AS DATE) <=  hosp.ClaimStatusDate THEN CAST(header.StatementFromDate AS DATE)
								ELSE CAST(line.FirstDateOfService AS DATE)
								END
						   ELSE
							   CASE WHEN (
											hosp.FacilityBillTypeCode || hosp.BillTypeClassificationCode IN
										  (
										  '011', '012', '015', '016', '017', '018', '019', '021', '023', '024', '025', '026', '027', '028', '029',
										  '041', '042', '045', '046', '047', '048', '049', '051', '052', '054', '055', '056', '057', '058', '059',
										  '061', '062', '065', '066', '067', '068', '069', '072', '081', '082', '084', '086', '087', '088', '089', 
										  '091', '092', '094', '095', '096', '097'
										  )
										)
										AND
										CAST(hosp.AdmissionDate AS DATE) <> Date '1753-01-01' AND
										CAST(hosp.AdmissionDate AS DATE) <= hosp.ClaimStatusDate AND
										CAST(hosp.AdmissionDate AS DATE) >  date_add('year',-2,hosp.ClaimStatusDate)  AND
										(
										  ( CAST(hosp.DischargeDate AS DATE) <> Date '1753-01-01' AND CAST(hosp.AdmissionDate AS DATE) <= CAST(hosp.DischargeDate AS DATE) ) OR
										  ( CAST(hosp.DischargeDate AS DATE) = Date '1753-01-01')
										) 
									 THEN CAST(hosp.AdmissionDate AS DATE)
									 ELSE CAST(line.FirstDateOfService AS DATE)
								END
						  END
				   WHEN header.Claimtype = 'Professional' THEN CAST(line.LastDateOfService AS DATE)
				   WHEN header.Claimtype = 'Oral' THEN CAST(line.FirstDateOfService AS DATE)
				END  as IncurredDate
            FROM
            cambia_data_main.conn_unnested_ClaimItem line
            join 
			( 
			  select correlationid, sourceid,source,claimtype,claimcategory, StatementFromDate,BillableStartDate,ID AS ClaimID
			  from
			  cambia_data_main.conn_unnested_claim 
			  where source = 'Facets' AND CAST(partition_3 AS INTEGER) = replace_ingestionid
			) header on 
			  line.correlationid = header.correlationid and 
			  line.parentsourceid = header.sourceid and
			  line.source = header.source
			left join ( select 
					   correlationid , 
					   sourceid, 
					   parentsourceid,
					   source , 
					   AdmissionDate,
					   DischargeDate,
					   FacilityBillTypeCode,
					   BillTypeClassificationCode,
					   cast(substring( SPLIT(sourceid,'-')[5] || '-' || SPLIT(sourceid,'-')[6] || '-' ||SPLIT(sourceid,'-')[7] , 1,10)  as date) as ClaimStatusDate
					   from cambia_data_main.conn_unnested_institutional
					   WHERE CAST(partition_3 AS INTEGER) = replace_ingestionid
					  ) hosp on
			  header.correlationid = hosp.correlationid and 
			  header.sourceid = hosp.parentsourceid and
			  header.source = hosp.source
    	WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);