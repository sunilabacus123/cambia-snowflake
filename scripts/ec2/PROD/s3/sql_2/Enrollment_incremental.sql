CREATE TABLE Enrollment_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            EntityType
            ,IngestionID
            ,BatchID
            ,CorrelationID
            ,Source
            ,SourceID
            ,ID
            ,IsAuthoritative
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,PartialUpdate
            ,LogicalDeleted
            ,HardDeleted
            ,Tenant
            ,HashID
            ,MemberSourceID
            ,GroupSourceID
            ,ParentGroupSourceID
            ,SubGroupSourceID
            ,ContractType
            ,EnrollmentType
            ,EnrollmentCategory
            ,EnrollmentCategoryID
            ,ConsumerDrivenHealthcareInd
            ,MarketSegmentCode
            ,SICCode
            ,SourceInstitutionalCoverageType
            ,SourceProductID
            ,ReasonCode
            ,EffectiveDate
            ,TerminationDate
            ,EligibilityEffectiveInd
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambia_data_main.conn_unnested_Enrollment
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
