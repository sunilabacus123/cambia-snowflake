CREATE TABLE Network_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,NetworkName
            ,DerivedInd
            ,ParentNetworkID
            ,NetworkPrefix
            ,Status
            ,NetworkCode
            ,NetworkCodeDescription
            ,FiscalStartDate
            ,FiscalEndDate
            ,EffDate
            ,ExpDate
            ,MaxPatients
            ,Gender
            ,MinAge
            ,MaxAge
            ,DirectoryListInd
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambia_data_main.conn_unnested_Network
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
