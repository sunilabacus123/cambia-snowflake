CREATE TABLE Review_replacesuffix
WITH
(
	bucket_count=1,
	bucketed_by = ARRAY['CorrelationID']
)
AS
(
    SELECT
            CorrelationID
            ,Source
            ,SourceID
            ,ParentSourceID
            ,RelationshipName
            ,SchemaVersion
            ,SourceTimestamp
            ,ConsumeTimestamp
            ,HashID
            ,ParentType
            ,Type
            ,Action
            ,DateAndTimeOfReview
            ,DateAndTimeOfReviewEnd
            ,Description
            ,ReviewReceivedDate
            ,ReviewRestrictionType
            ,RestrictionStartDate
            ,RestrictionEndDate
            ,partition_3
FROM
    (
        SELECT
            *,ROW_NUMBER()OVER(PARTITION BY Partition_3,HashId ORDER BY HashId) RowNum
        FROM
        (
            SELECT
                *
            FROM
        cambia_data_main.conn_unnested_Review
    WHERE
        CAST(partition_3 AS INTEGER) = replace_ingestionid
         )
    )
    WHERE RowNum = 1
);
