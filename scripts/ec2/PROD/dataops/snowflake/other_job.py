import datetime
import io
import time
import sys
import re
import boto3
import botocore
import pandas as pd
import os
import csv
import json
import pandas as pd
import os
# import requests
import time
# from aws_requests_auth.boto_utils import BotoAWSRequestsAuthimport argparse
from pathlib import Path
import json
# from requests.exceptions import HTTPError
import re
from datetime import date, datetime



# import pyarrow.parquet as pq
from botocore.exceptions import ClientError


def athena_query(client, param_q):
    response = client.start_query_execution(

        QueryString=param_q['query'],
        QueryExecutionContext={
            'Database': param_q['database']

        },
        WorkGroup='cambia-dataops'
    )
    return response


ath = boto3.client('athena', region_name='us-east-1')
glue = boto3.client('glue', region_name='us-east-1')

bucket_name = 'abacus-sandboxes-cambia'
database_main = 'cambia_data_main'

# start client & session


# print("Extract Start Time:- " + date_time)

parent_view_list = ['member']

bucket_name = 'abacus-sandboxes-cambia'
database_main = 'cambia_data_main'

nh = 1
while nh == 1:



    cmdd ='aws s3 cp s3://abacus-sandboxes-cambia/teams/dataops/other_job_status.txt .'
    os.system(cmdd)

    fp = open('other_job_status.txt')
    prime_ingestion_trigger_1 = 0
    ingestion_id = 0
    for file_control in fp:
        print(file_control)
        #print(file_control.split(",")[2])
        ing_str=file_control

        if 'VSP' in file_control or 'PROVIDER' in file_control or 'FACETS' in file_control or 'Prime' in file_control or 'CCA' or 'Facets' in file_control or 'QUEST-LAB' in file_control or 'SOURCE' in file_control or 'CAMBIA-BCBSA-CLAIMS' in file_control or 'CAMBIA-BCBSA-MEMBER' in file_control or 'FACETS-MEMBER-V2' in file_control or 'FACETS-ENROLLMENT-V2' in file_control or 'FACETS-CLAIM-EXTENSION' in file_control:
          prime_ingestion_trigger_1 = 1


			
			#print('loop 2')
            #ingestion_id = file_control.split(",")[1].strip()
            #print(ingestion_id)

          now_1 = datetime.now()
          file_name = 'other_job_status_'+str(now_1).replace(" ","_")[:19]+'.txt'

          cmdd = 'cat other_job_status.txt dummy_file.txt >> daily_trigger_offshore.txt'
          os.system(cmdd)

          cmdd ='mv other_job_status.txt archive_job_status/'+file_name
          os.system(cmdd)


          cmdd = 'touch temp_11.txt'
          os.system(cmdd)
          cmdd = 'aws s3 cp daily_trigger_offshore.txt s3://abacus-sandboxes-cambia/teams/dataops/daily_trigger_offshore.txt'
          os.system(cmdd)
          cmdd = 'aws s3 cp temp_11.txt s3://abacus-sandboxes-cambia/teams/dataops/other_job_status.txt'
          os.system(cmdd)


  
            ###############################################################




          fw = open('message_to_karteek.txt','w')



          if prime_ingestion_trigger_1 == 1:



                cmdd = "ps -ef|grep test_1227 > o3.out"
                os.system(cmdd)
                print(cmdd)
                prime_ingestion_trigger_3 = 0
                fp = open('o3.out')
                prime_ingestion_trigger_3 = 0
                source_str = ing_str.replace("Prime Claim ","")
                source_str = source_str.replace("Prime Reject ","")
                source_str = source_str.replace("Facets Um ","")
                source_str = source_str.replace("Facets rre ","")
                source_str = source_str.replace("CCA SOURCE ","")
                source_str = source_str.replace("CCA CCA ","")
                source_str = source_str.replace("MEMBER CCA ","")
                source_str = source_str.replace("FACETS-MEMBER FACETS-MEMBER-V2 ","")
                source_str = source_str.replace("FACETS-ENROLLMENT FACETS-ENROLLMENT-V2 ","")
                source_str = source_str.replace("FACETS-CLAIM-EXTENSION FACETS-CLAIM-EXTENSION ","")
                source_str = source_str.replace("BCBSA-CLAIM CAMBIA-BCBSA-CLAIMS ","")
                source_str = source_str.replace("BCBSA-MEMBER CAMBIA-BCBSA-MEMBER ","")
                source_str = source_str.replace("FACETS Claim ","")
                source_str = source_str.replace("LABCORP LABCORP ","")
                source_str = source_str.replace("PROVIDER PROVIDER ","")
                source_str = source_str.replace("PAML PAML ","")
                source_str = source_str.replace("DYNACARE DYNACARE ","")
                source_str = source_str.replace("QUEST-LAB QUEST-V2 ","")
                source_str = source_str.replace("Facets Recon Um ","")
                source_str = source_str.replace("MEMBER Recon CCA ","")
                source_str = source_str.replace("CCA Recon CCA ","")
                source_str = source_str.replace("Facets Recon rre ","")
                source_str = source_str.replace("\n","")
                source_str = source_str.strip().replace(' ',',')

                for file_control_1 in fp:
                    if "test_1227.py" in file_control_1 or "ntest.py" in file_control_1 or "file_extract.py" in file_control_1:
                        prime_ingestion_trigger_3 = 0
                        msg_st='already one snowflake run in progress. try uploading  file again after some time'
                        print('in progress, cannot initiate one more')

                if prime_ingestion_trigger_3 == 0:
                    print("other run going to kick off " )
                    if 'Prime Claim' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py prime ' + source_str + ' >> /data/nohup/prime_source.out & '
                      os.system(cmd)
                      time.sleep(300)

                    elif 'CCA CCA' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py cca_case ' + source_str + ' >> /data/nohup/cca_case_source.out & '
                      os.system(cmd)
                      print(cmd)
                      time.sleep(300)

                    elif 'FACETS Claim' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py facets_claim ' + source_str + ' >> /data/nohup/facets_claim_source.out & '
                      os.system(cmd)
                      print(cmd)
                      time.sleep(300)


                    elif 'MEMBER CCA' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py cca_member ' + source_str + ' >>  /data/nohup/cca_member_source.out & '
                      os.system(cmd)
                      #msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)  
					  
					  
                    elif 'Prime Reject' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py prime_reject ' + source_str + ' >> /data/nohup/prime_reject_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)    

                    elif 'LABCORP LABCORP' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py labcorp ' + source_str + ' >> /data/nohup/labcorp_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    elif 'PAML PAML' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py paml ' + source_str + ' >> /data/nohup/paml_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    elif 'DYNACARE DYNACARE' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py dynacare ' + source_str + ' >> /data/nohup/dynacare_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)
                      
                    elif 'PROVIDER PROVIDER' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py provider_foundation ' + source_str + ' >> /data/nohup/provider_foundation_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    elif 'QUEST-LAB QUEST-V2' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py quest_v2 ' + source_str + ' >> /data/nohup/quest_v2_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    elif 'BCBSA-CLAIM CAMBIA-BCBSA-CLAIMS' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py cambia_bcbsa_claims ' + source_str + ' >> /data/nohup/cambia_bcbsa_claims_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    elif 'BCBSA-MEMBER CAMBIA-BCBSA-MEMBER' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd ='nohup python copy_source_data.py cambia_bcbsa_member ' + source_str + ' >> /data/nohup/cambia_bcbsa_member_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    elif 'Facets Recon Um' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd = 'sh ./execute_abc.sh >> /data/nohup/abc_facets.out'
                      os.system(cmd)
                      print(cmd)

                      cmd ='nohup python snowflake_sfreports.py facets_um ' + source_str  + ' >> /data/nohup/abc_recon_sf_um.out  & '
                      os.system(cmd)
                      print(cmd)

                      time.sleep(300)

                    elif 'Facets Recon rre' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd = 'sh ./execute_abc.sh >> /data/nohup/abc_facets_rre.out'
                      os.system(cmd)
                      print(cmd)

                      cmd ='nohup python snowflake_sfreports.py facets_rre ' + source_str + ' >> /data/nohup/abc_recon_sf_rre.out  & '
                      os.system(cmd)
                      print(cmd)

                      time.sleep(300)

                    elif 'MEMBER Recon CCA' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd = 'sh ./execute_abc.sh >> /data/nohup/abc_abc.out'
                      os.system(cmd)
                      print(cmd)
                      time.sleep(300)
                      cmd ='nohup python snowflake_sfreports.py cca_member ' + source_str  + ' >> /data/nohup/abc_recon_sf_cca_member.out  & '
                      os.system(cmd)
                      print(cmd)
                      time.sleep(300)

                    elif 'CCA Recon CCA' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()
                      cmd = 'sh ./execute_abc.sh >> /data/nohup/abc_abc.out'
                      os.system(cmd)
                      print(cmd)
                      time.sleep(300)
                      cmd ='nohup python snowflake_sfreports.py cca_case ' + source_str  + ' >> /data/nohup/abc_recon_sf_cca_case.out  & '
                      os.system(cmd)
                      print(cmd)
                      time.sleep(300)

                    elif 'Facets Um' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()

                      cmd ='nohup python copy_source_data.py facets_um ' + source_str + ' >> /data/nohup/facets_um_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                                            
                      time.sleep(300)


                    elif 'Facets rre' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()

                      cmd ='nohup python copy_source_data.py facets_rre ' + source_str + ' >> /data/nohup/facets_rre_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)


                    elif 'CCA SOURCE' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()

                      cmd ='nohup python copy_source_data.py cca_source ' + source_str + ' >> /data/nohup/cca_source_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)


                    elif 'FACETS-ENROLLMENT FACETS-ENROLLMENT-V2' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()

                      cmd ='nohup python copy_source_data.py facets_enrollment_v2 ' + source_str + ' >> /data/nohup/facets_enrollment_v2_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    elif 'FACETS-MEMBER FACETS-MEMBER-V2' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()

                      cmd ='nohup python copy_source_data.py facets_member_v2  ' + source_str + ' >> /data/nohup/facets_member_v2_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    elif 'FACETS-CLAIM-EXTENSION FACETS-CLAIM-EXTENSION' in file_control :
                      ingestion_id = file_control.split(" ")[1].strip()

                      cmd ='nohup python copy_source_data.py facets_claim_extension  ' + source_str + ' >> /data/nohup/facets_claim_extension_source.out & '
                      os.system(cmd)
                      print(cmd)
                      msg_st='succesfully triggered load for string: '+ ing_str
                      time.sleep(300)

                    abc=1
                    ing_str=ing_str.replace("\n","") 
                    if 'Prime Reject' in file_control or 'Facets Um' in file_control or 'Facets rre' in file_control or 'CCA SOURCE' in file_control or 'FACETS-ENROLLMENT FACETS-ENROLLMENT-V2' in file_control or 'FACETS-MEMBER FACETS-MEMBER-V2' in file_control or 'FACETS-CLAIM-EXTENSION FACETS-CLAIM-EXTENSION' in file_control:
                      print("Domain Load not required")
                    else:
                      cmdd="nohup python test_1227.py "+ing_str +" >> /data/nohup/sf_daily_load.out &"
                      print(cmdd)
                      os.system(cmdd)
                      msg_st='succesfully triggered load for string: '+ ing_str


          else:

                msg_st=' One of the tables have zero count and snowflake load is not triggered, contact onshore: Facet Member for ingestionid'+ ingestion_id

          fw.write(msg_st)
          fw.close()
          cmdd = 'aws s3 cp message_to_karteek.txt s3://abacus-sandboxes-cambia/teams/dataops/'
          os.system(cmdd)


    time.sleep(300)
