"""
----------------------------------------------------------------------------------------------------------
Description: Snowflake Reports PIPELINE

usage:

Author  : Sunil Kumar
Release : 1

Modification Log:  09 June, 2021
-----------------------------------------------------------------------------------------------------------
Date                Author                            Description
-----------------------------------------------------------------------------------------------------------
06/09/2021       Sunil Kumar S             Version 1
-----------------------------------------------------------------------------------------------------------
"""
import json
import datetime
import io
import time
import sys
import os
import boto3
from boto3 import client
import pyarrow.parquet as pq
from pytz import timezone
import pytz
from botocore.exceptions import ClientError

##### Change here ####
env = 'cambia'


if not os.path.exists('manifest'):
    os.makedirs('manifest')

src_bucket_name = 'abacus-sandboxes-' + env
dest_bucket_name = 'abacus-data-lake-' + env
database = env + '_data_ops'
extract_name = "cambia_extract"
#extract_loc = "cambia_transfer"
extract_loc = "cambia_extract"
workgroup = env + '-dataops'
#### Change here ####

s3_conn   = client('s3')
currentDT = datetime.datetime.now()
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')


ath = boto3.client('athena',region_name='us-east-1')
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
glue = boto3.client('glue',region_name='us-east-1')

src_bucket = s3.Bucket(src_bucket_name)

extract_time = currentDT.strftime("%Y%m%d%H%M")

currentdt = datetime.datetime.now()
run_date = currentdt.strftime("%m_%d_%Y")
run_month = currentdt.strftime("%m")
run_date1 = currentdt.strftime("%d")
run_year = currentdt.strftime("%Y")
run_date2 = currentdt.strftime("%m/%d/%Y")

source =sys.argv[1]
ingestionid = sys.argv[2]

if source == 'facets_um':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/sfreport/'+source+'/'
    table_list = 'source,lake'
elif source == 'facets_member_v2':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/sfreport/'+source+'/'
    table_list = 'source,lake,lake_domain'
elif source == 'facets_enrollment_v2':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/sfreport/'+source+'/'
    table_list = 'source,lake,lake_domain'
elif source == 'facets_claim_extension':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/sfreport/'+source+'/'
    table_list = 'source,lake,lake_domain'	    
elif source == 'cca_member':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/sfreport/'+source+'/'
    table_list = 'source,lake,lake_domain'
elif source == 'cca_case':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/sfreport/'+source+'/'
    table_list = 'source,lake,lake_domain'
elif source == 'facets_rre':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/sfreport/'+source+'/'
    table_list = 'source,lake'
elif source == 'facets_claim_v2':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/sfreport/'+source+'/'
    table_list = 'source,lake'    
else:
    print("Enter correct rdm code set")
    exit(0)
def drop_table(view_name,source):
    print('Dropping table...' + view_name)

    print("====================================\n")
    print(view_name)
    print("====================================\n")

    drop_query = 'drop table if exists ' + database + '.' + view_name + '_' + source

    print(drop_query)
    try:
        response = ath.start_query_execution(QueryString=drop_query, QueryExecutionContext={'Database': database},
                                             WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        print('Drop table query is running...')
        return str(query_execution_id)
    except ClientError as e:
        print('Query failed to submit' % e)


def create_table(view_name, run_date,sql_path,src_bucket_name,source):
    try:
        file_to_read = sql_path + view_name + '.sql'
        fileobj = s3client.get_object(Bucket=src_bucket_name, Key=file_to_read)
        filedata = fileobj['Body'].read()
        filecontents = filedata.decode("utf-8")

        create_query = filecontents.replace('replace_ingestionid',ingestionid)
        create_query = create_query.replace('replacesuffix',source)

        print("\n============ Creating table for "+view_name+" ================\n")
        print("Athena Query :"+create_query)

        response = ath.start_query_execution(QueryString=create_query,
                                             QueryExecutionContext={'Database': database}, WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        return query_execution_id
    except ClientError as e:
        print('Query failed to submit' % e)
        return 'null'


def job_status():
    print("Checking job status")
    while len(create_query_ids) > 0:
        response = ath.batch_get_query_execution(QueryExecutionIds=create_query_ids)
        execution_ids = response['QueryExecutions']
        for i in execution_ids:
            query_id = i['QueryExecutionId']
            status = i['Status']['State']
            if status == 'SUCCEEDED':
                create_query_ids.remove(query_id)
            elif status == 'FAILED':
                create_query_ids.remove(query_id)
                failed_create_query_id.append(query_id)
            else:
                print(query_id, status)
        time.sleep(5)


def copy_object(state, src_bucket, src_prefix, view_name, dest_prefix,extract_time):
    if state:
        if state == 'SUCCEEDED':
            time.sleep(5)
            num = 1
            for s3object in src_bucket.objects.filter(Prefix=src_prefix):
                srckey = s3object.key
                if not srckey.endswith('/'):
                    file_num = str(num)
                    fileName = view_name + '_' + extract_time + '_' + file_num + '.gz'
                    print(fileName)
                    destfilekey = dest_prefix + fileName
                    copysource = src_bucket_name + '/' + srckey
                    print('destination_: s3://'+dest_bucket_name+"/" + destfilekey)
                    print('Source: s3://' + copysource)
                    s3.Object(dest_bucket_name, destfilekey).copy_from(CopySource=copysource)
                
                    num = num + 1
            print('Data copy is completed...')
            return 'success'
        else:

            print('Data is not copied...')
            return 'failed'
    else:
        print('Query is not started executing...')
        return 'failed'

def s3_object_rowcount(dest_bucket_name, prefix, view_name, run_date2):

    print(prefix)
    try:
        response = s3client.list_objects(Bucket=dest_bucket_name, Prefix=prefix, MaxKeys=100)
        outputs = []
        objects = response["Contents"]
        for i in objects:
            file_name = i['Key']
            buffer = io.BytesIO()
            s3object = s3.Object(dest_bucket_name, file_name)
            s3object.download_fileobj(buffer)
            df = pq.ParquetFile(buffer)
            rowcount = df.metadata.num_rows
            print(file_name)
            f_name = file_name.split('/', 8)
            print(f_name)
            f_name1 = f_name[7]
            outputs.append( 'rdm'+ "," +run_date2 + ',' + view_name + ',' + 'refresh' + ',' + f_name1 + ',' + str(rowcount) )

    except KeyError:
        outputs.append('rdm'+ run_date2 + ',' + view_name + ',' + 'refresh' + ',' + 'No_File' + ',' + '0' )
        print('Cannot read s3 file')

    return outputs


def copy_files(view_name,source):
    if len(failed_create_query_id) > 0:
        print("Create table failed for some view, check athena history page")
        exit()
    else:
        print("\ncopy files.. Started")
        view_name_s3 = view_name + '_' + source
        response = glue.get_table(DatabaseName=database, Name=view_name_s3)
        table_dest = response['Table']['StorageDescriptor']['Location']
        f_name = table_dest.split('/', 6)
        table_loc = f_name[6]
        src_prefix = 'data-ops/athena-query-results/tables/' + table_loc
        dest_prefix = 'data-views/' + extract_loc + '/'
        dest_extract_prefix = dest_prefix + 'data/sfreports/' + view_name.lower() + '/' + run_year + '/' + run_month + '/' + run_date1 + '/' + ingestionid + '/'
        copy_state = copy_object('SUCCEEDED', src_bucket, src_prefix, view_name, dest_extract_prefix, extract_time)


reate_query_ids = []
failed_create_query_id = []

manifest_file_name = 'manifest/manifest_' + extract_time + '.csv'
with open(manifest_file_name, 'w') as filehandle:
    print("Starting Manifest generation")
filehandle.close()


if source in ['facets_um','facets_member_v2','facets_enrollment_v2','cca_member','cca_case','facets_rre','facets_claim_extension']:
    for view_name in table_list.split(','):
        drop_table(view_name,source)
        time.sleep(10)        
        create_query_ids = []
        failed_create_query_id = []
        create_query_ids.append(create_table(view_name,run_date,sql_path,src_bucket_name,source))
        job_status()
        copy_files(view_name,source)
else:
    print("Invalid source, enter the correct source \n")

