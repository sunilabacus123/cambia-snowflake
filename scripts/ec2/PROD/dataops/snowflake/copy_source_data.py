import json
import datetime
import io
import time
import sys
import os
import boto3
from boto3 import client
import pyarrow.parquet as pq
from pytz import timezone
import pytz
from pathlib import Path
import logging
import pandas as pd
from botocore.exceptions import ClientError
import botocore

s3_conn   = client('s3')
currentDT = datetime.datetime.now()
s3 = boto3.resource('s3',region_name='us-east-1')
ath = boto3.client('athena',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
glue = boto3.client('glue',region_name='us-east-1')
ingestionid = list(sys.argv[2].split(","))

extract_time = currentDT.strftime("%Y%m%d%H%M")
currentdt = datetime.datetime.now()
extract_date = currentdt.strftime("%Y/%m/%d")
extract_time = currentdt.strftime("%Y%m%d%H%M")

env = 'cambia'
#extract_loc = 'cambia_transfer'
extract_loc = 'cambia_extract'
database = env+'_data_ops'
job_status_table_name = 'cambia_extract_source_data_job_status'
workgroup = env + '-dataops'
src_bucket_name_check = 'abacus-sandboxes-' + env
date_format_sf='%m/%d/%Y %H:%M:%S %Z'
date_sf = datetime.datetime.now(tz=pytz.utc)
date_sf = date_sf.astimezone(timezone('US/Pacific'))
dest_bucket_name = 'abacus-data-lake-' + env


if not os.path.exists('entitylog'):
    os.makedirs('entitylog')
if not os.path.exists('manifest'):
    os.makedirs('manifest')
if not os.path.exists('connector'):
    os.makedirs('connector')
if not os.path.exists('entitylog'):
    os.makedirs('entitylog')
if not os.path.exists('success'):
    os.makedirs('success')
if not os.path.exists('manifest_source'):
    os.makedirs('manifest_source')
if not os.path.exists('source_logs'):
    os.makedirs('source_logs')

run_count = 1
print(len(sys.argv))
if len(sys.argv) != 3:
    print("Usage: copy_source_data.py <source> <sub_source> <ingestion_id>")
    exit()
elif type(ingestionid) is not list:
    print("Please pass list for ingestionid\n")
    exit()
else:
    source = sys.argv[1]

source = sys.argv[1]
print(source)
if source == 'prime':
    source_file = "prime_source_data.txt"
    CONNECTOR = "PRIME-CAMBIA"
    SOURCE_NAME = "prime"
elif source == 'prime_reject':
    source_file = "prime_cambia_reject_source_data.txt"
    CONNECTOR = "PRIME-CAMBIA-REJECT"
    SOURCE_NAME = "prime"
    ENT_CONNECTOR='PRIME'
    SRC_CONNECTOR='REJECT'
elif source == 'vsp':
    source_file = "vsp_source_data.txt"
    CONNECTOR = "VPS"
    SOURCE_NAME = "VSP"
elif source == 'facets':
    source_file = "facets_source_data.txt"
    CONNECTOR = "FACETS-CLAIM"
    SOURCE_NAME = "FACETS"
elif source == 'cca_case':
    source_file = "cca_case_source_data.txt"
    CONNECTOR = "CCA-CASE"
    SOURCE_NAME = "CCA"
elif source == 'cca_member':
    source_file = "cca_member_source_data.txt"
    CONNECTOR = "CCA-MEMBER"
    SOURCE_NAME = "CCA"
elif source == 'provider_foundation':
    source_file = "provider_foundation_source_data.txt"
    CONNECTOR = "PROVIDER-FOUNDATION"
    SOURCE_NAME = "PROVIDER-FOUNDATION"
elif source == 'quest_lab':
    source_file = "quest_lab_source_data.txt"
    CONNECTOR = "QUEST"
    SOURCE_NAME = "QUEST"
elif source == 'facets_um':
    source_file = "facets_um_source_data.txt"
    CONNECTOR = "FACETS-UM"
    SOURCE_NAME = "FACETS"
    ENT_CONNECTOR='FACETS_UM'
    SRC_CONNECTOR='FACETS'
elif source == 'facets_claim':
    source_file = "facets_claim_source_data.txt"
    CONNECTOR = "FACETS-CLAIM"
    SOURCE_NAME = "FACETS"
elif source == 'facets_rre':
    source_file = "facets_rre_source_data.txt"
    CONNECTOR = "FACETS-RRE"
    SOURCE_NAME = "FACETS"
    ENT_CONNECTOR='FACETS_RRE'
    SRC_CONNECTOR='FACETS'
elif source == 'facets_enrollment':
    source_file = "facets_enrollment_source_data.txt"
    CONNECTOR = "FACETS-ENROLLMENT"
    SOURCE_NAME = "FACETS"
elif source == 'facets_member':
    source_file = "facets_member_source_data.txt"
    CONNECTOR = "FACETS-MEMBER"
    SOURCE_NAME = "FACETS"
elif source == 'facets_enrollment_v2':
    source_file = "facets_enrollment_v2_source_data.txt"
    CONNECTOR = "FACETS-ENROLLMENT-V2"
    SOURCE_NAME = "FACETS"
elif source == 'facets_member_v2':
    source_file = "facets_member_v2_source_data.txt"
    CONNECTOR = "FACETS-MEMBER-V2"
    SOURCE_NAME = "FACETS"
elif source == 'quest_v2':
    source_file = "quest_v2_source_data.txt"
    CONNECTOR = "FACETS-V2-LAB"
    SOURCE_NAME = "QUEST-LAB"
elif source == 'labcorp':
    source_file = "labcorp_source_data.txt"
    CONNECTOR = "LABCORP"
    SOURCE_NAME = "LABCORP"
elif source == 'paml':
    source_file = "paml_source_data.txt"
    CONNECTOR = "PAML"
    SOURCE_NAME = "PAML"
elif source == 'dynacare':
    source_file = "dynacare_source_data.txt"
    CONNECTOR = "DYNACARE"
    SOURCE_NAME = "DYNACARE"
elif source == 'facets_claim_extension':
    source_file = "facets_claim_extension_source_data.txt"
    CONNECTOR = "FACETS-CLAIM-EXTENSION"
    SOURCE_NAME = "FACETS"
elif source == 'cambia_bcbsa_claims':
    source_file = "cambia_bcbsa_claims_source_data.txt"
    CONNECTOR = "CAMBIA-BCBSA-CLAIMS"
    SOURCE_NAME = "BCBSA-CLAIM"
elif source == 'cambia_bcbsa_member':
    source_file = "cambia_bcbsa_member_source_data.txt"
    CONNECTOR = "CAMBIA-BCBSA-MEMBER"
    SOURCE_NAME = "BCBSA-MEMBER"
elif source == 'prime_cambia_v2':
    source_file = "prime_cambia_v2_source_data.txt"
    CONNECTOR = "PRIME-CAMBIA-V2"
    SOURCE_NAME = "PRIME"
elif source == 'cca_source':
    source_file = "cca_source_source_data.txt"
    CONNECTOR = "CCA-SOURCE"
    SOURCE_NAME = "CCA-SOURCE"
    ENT_CONNECTOR='CCA_SOURCE'
    SRC_CONNECTOR='CCA_SOURCE'
else:
    print("please enter correct source details\n")
    exit()


CONFIG_BUCKET_NAME = 'abacus-sandboxes-' + env
CONFIG_KEY = 'data-ops/etl/'+extract_loc+'/config/source_data/run_completed.txt'
REPORT_KEY = 'data-ops/etl/'+extract_loc+'/config/extract_job_report_source_data/job_report_source_data.txt'
SCHEMA_NAME = "SOURCE_DATA"


file = open(source_file, "r")
read = file.read()

for line in read.splitlines():
    if 'INPUTSOURCES=' in line:
        SOURCES = line.split('=',1)[1]
    elif 'SOURCE_BUCKET=' in line:
        src_bucket_name = line.split('=',1)[1]
    elif 'TARGET_BUCKET=' in line:
        trg_bucket_name =  line.split('=',1)[1]
    elif 'TARGET_PREFIX=' in line:
        trg_prefix = line.split('=',1)[1]


print("-------------------------------------------------------------------------------")
print("| Data Copy from Stage to distibution for source data is in progress          |")
print("| This sript copy data to s3 bucket abacus-data-lake-cambia                   |")
print("| s3 replication is set up on this bucket to copy data to dist bcuket         |")
print("| Data Copy in Progess for Ingestion ID's "+sys.argv[2]+"                         |")
print("| For "+sys.argv[1]+" "+SOURCES+" sources                            |")
print("-------------------------------------------------------------------------------")

try:
    s3.Bucket(CONFIG_BUCKET_NAME).download_file(CONFIG_KEY, 'run_completed.txt')
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise


SOURCE = list(SOURCES.split(","))


def check_ingestion_query(ingestion_id):
    ingestion_count = 'select count(*) from ' + database + '.' + job_status_table_name + ' where ' \
                                 'ingestionid=\'' + ingestion_id + '\''
    print(ingestion_count)
    try:
        response = ath.start_query_execution(QueryString=ingestion_count,
                                         QueryExecutionContext={'Database': database}, WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        print('Ingestion check query is running...')
        return query_execution_id
    except ClientError as e:
        print('Ingestion check Query failed to submit' % e)


def get_query_state(query_execution_id, max_execution=50):
    print('Checking query Status...')
    # print('query_execution_id...:'+query_execution_id)
    response = ath.get_query_execution(QueryExecutionId=query_execution_id)
    state = response['QueryExecution']['Status']['State']
    print('Query State:' + state)
    while max_execution > 0 and state in ['RUNNING'] or state in ['QUEUED']:
        max_execution = max_execution - 1
        # print('execution:'+ str(max_execution))
        response = ath.get_query_execution(QueryExecutionId=query_execution_id)
        if 'QueryExecution' in response and 'Status' in response['QueryExecution'] and 'State' in \
                response['QueryExecution']['Status']:
            state = response['QueryExecution']['Status']['State']
            if state == 'FAILED':
                print('query failed, check the logs for details...')
                status_string = response['QueryExecution']['Status']['StateChangeReason']
                print(status_string)
                return state
            elif state == 'SUCCEEDED':

                print('query execution succeeded...')
                print('==========================')
                return state
            time.sleep(5)


def read_result(src_bucket_name_check, src_prefix):
    print('Getting csv files results of the query')

    try:
        # response = s3client.get_object(Bucket=src_bucket_name_check, Key=src_prefix)
        response = s3.Bucket(src_bucket_name_check).Object(key=src_prefix).get()
        print('read csv results...')

        t = pd.read_csv(io.BytesIO(response['Body'].read()), encoding='utf8')
        return t.iat[0, 0]

    except ClientError as e:
        print('Cannot read csv file' % e)


def update_job_status(connector,extract_job_time,ingestion_id,copy_state):
    # Update Job Status
    refresh_type = 'INC'
    if refresh_type == 'INC':
        update_query = 'insert into ' + database + '.' + job_status_table_name + ' values ( \'' + connector \
                       + '\' ,\'' + extract_job_time + '\' ,\'' + ingestion_id + '\' ,\'' \
                       + copy_state + '\');'
        print(update_query)
        try:
            response = ath.start_query_execution(QueryString=update_query, QueryExecutionContext={'Database': database},
                                                 WorkGroup=workgroup)
            query_execution_id = response["QueryExecutionId"]
            print('Update job status query is running...')
            return str(query_execution_id)
        except ClientError as e:
            print('Update job status query failed to submit' % e)

def s3_object_rowcount(source,dest_bucket_name, prefix, table_name, job_id):
    print(dest_bucket_name+":"+prefix)
    try:
        response = s3client.list_objects(Bucket=dest_bucket_name, Prefix=prefix, MaxKeys=100)
        objects = response["Contents"]
        for i in objects:
            file_name = i['Key']
            buffer = io.BytesIO()
            s3object = s3.Object(dest_bucket_name, file_name)
            s3object.download_fileobj(buffer)
            df = pq.ParquetFile(buffer)
            rowcount = df.metadata.num_rows
            f_name = file_name.split('/', 8)
            f_name1 = f_name[8]
            manifest = 'manifest_source/'+source+"_manifest_"+extract_time+"_"+job_id+".csv"
            mf_cmp = open(manifest, "a")
#            mf_cmp.write(CONNECTOR+ ","+ SOURCE_NAME + ',' + job_id + ',' + table_name + ',' + SCHEMA_NAME + "," + f_name1 + ',' + str(rowcount) + "," + str(extract_time)+"\n")
            mf_cmp.write(CONNECTOR+ ","+ SOURCE_NAME + ',' + job_id + ',' + table_name + ',' + SCHEMA_NAME + "," + f_name1 + ',' + str(rowcount) + "," + str(date_sf)+"\n")
            mf_cmp.close()

    except KeyError:
        print('Cannot read s3 file')

def copy_object(table_name, ingest, src_bucket, src_prefix, dest_bucket, dest_prefix, sleep_check, f_name_2):
#    logging.info(table_name +"||"+ src_bucket+"||"+ src_prefix+"||"+ dest_bucket+"||"+ dest_prefix+"||"+ f_name_2)
    bucketobj = s3.Bucket(src_bucket)
    num = 1
    num1 = 1
    trg_prefix_count = ""
    if sleep_check:
        time.sleep(5)
    print("-----------------------------------------------------------------------")
    print("Data copy is running for table "+table_name +" Ingestion ID "+ingest)
    print("-----------------------------------------------------------------------")
    for s3object in bucketobj.objects.filter(Prefix=src_prefix):
        srckey = s3object.key
        if not srckey.endswith('/') and not srckey.endswith('SUCCESS'):
            file_num = str(num)
            fileName = table_name + '_' + extract_time + '_' + file_num
            ingestion = list(srckey.split("/"))
            fileprefix = f_name_2+"/"+ingestion[3]+"/"+ingestion[4]+"/"+ingestion[5]+"/"+ingest+"/"+ingestion[8]
            destfilekey = dest_prefix+"/"+fileprefix
            copysource = dest_bucket+"/"+srckey
            if ingestion[6] == ingest:
                trg_prefix_count = dest_prefix+"/"+f_name_2+"/"+ingestion[3]+"/"+ingestion[4]+"/"+ingestion[5]+"/"+ingest
                s3.Object(dest_bucket, destfilekey).copy_from(CopySource=copysource)
#                logging.info("SS: "+copysource)
#                logging.info("TT: "+destfilekey)
                num1 = num1 + 1
            num = num + 1
    time.sleep(5)
#    logging.info(" ")
#    logging.info(" ")
#    logging.info(" ")
#    logging.info(" ")
    if trg_prefix_count:
        s3_object_rowcount(source,dest_bucket,trg_prefix_count,table_name, i)
        run_cmp = open("run_completed.txt", "a")
        run_cmp.write(table_name+":"+ingest+"\n")
        run_cmp.close()
        print("")
        num1 = num1 - 1
        print("Number of files copied : "+str(num1))
        logging.info("Total Number of files ocpied : "+str(num1))
        print("")
    else:
        print("----------------------Warning-----------------------------")
        print("No data for SOurce "+source+" Ingestion ID "+i)
        print("----------------------------------------------------------")
        run_cmp = open("run_completed.txt", "a")
        run_cmp.write(table_name+":"+ingest+"|IngestionNotPresentForthisConnector\n")
        run_cmp.close()


for i in ingestionid:
 date_time = currentdt.strftime("%m/%d/%Y, %H:%M:%S")
 ingestion_id = i
 logging_file_name = "source_logs/"+source+"_"+ingestion_id+"_"+extract_time+".log"
 print(logging_file_name)
 for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
 logging.basicConfig(filename=logging_file_name, level=logging.INFO)
 logging.info("Extract Start Time:- " + date_time +" : "+ ingestion_id)
 currentdt = datetime.datetime.now()
 logging.info("Strart time :" + str(currentdt))
 extract_job_time = currentdt.strftime("%Y-%m-%d-%H-%M")
 logging.info("Check Ingestion Status in Athena")
 query_id = check_ingestion_query(ingestion_id)
 state = get_query_state(query_id, max_execution=1000)
 if state == 'SUCCEEDED':
    print("Ingestion id processed check query executed Successfully: ")
 else:
    print("Ingestion id to be processed query Failed: ")

 csv_file = 'data-ops/athena-query-results/' + query_id + '.csv'
 ingestion_count = read_result(src_bucket_name_check, csv_file)
 logging.info("Total number of entries in log table: "+ str(job_status_table_name) +"  is "+ str(ingestion_count))
 print("Check ingestion status, processed or not :")
 if ingestion_count > 0:
     print('Ingestion_id: ' + ingestion_id + ' is already processed, skipping this ingestion...')
     print(ingestion_count)
     logging.info("Ingestion_id: " + ingestion_id + " is already processed, skipping this ingestion...")
     continue
 else:
#    ingestion_id = i
#    currentdt = datetime.datetime.now()
    try:
        s3.Bucket(CONFIG_BUCKET_NAME).download_file(REPORT_KEY, 'job_report_source_data.txt')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise

    now1 = datetime.datetime.now()
    date_time1 = now1.strftime("%m/%d/%Y, %H:%M:%S")
    job_start_time_ts = date_time1

    logging.info("Started data copy for Ingestion "+ str(ingestion_id))
    copy_state = 'STARTED'
    logging.info("Insert entry to log table as STARTED" )
    update_job_status(CONNECTOR, extract_job_time, ingestion_id, copy_state)
#    date_time = currentdt.strftime("%m/%d/%Y, %H:%M:%S")
#    logging_file_name = "source_logs/"+source+"_"+ingestion_id+"_"+extract_time+".log"
#    logging.basicConfig(filename=logging_file_name, level=logging.INFO)
#    logging.info("Extract Start Time:- " + date_time +" : "+ ingestion_id)
    for srcs in SOURCE:

        if source == 'prime':
            for line in read.splitlines():
                if srcs == 'standard':
                    if 'STANDARD=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_standard'
                elif srcs == 'supplemental':
                    if 'SUPPLEMENTAL=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_supplemental'
                elif srcs == 'compound':
                    if 'COMPOUND=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_compound'
                else:
                    print("prime source is invalid check the config file \n")
                    exit()

        elif source == 'prime_reject':
            for line in read.splitlines():
                if srcs == 'reject':
                    if 'REJECT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_reject'
                else:
                    print("prime reject source is invalid check the config file \n")
                    exit()

        elif source == 'vsp':
            for line in read.splitlines():
                if srcs == 'common_common':
                    if 'COMMON_COMMON=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'vsp_common'
                elif srcs == 'medicare_mcare':
                    if 'MEDICARE_MCARE=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'vsp_medicare'
                else:
                    print(srcs)
                    print("vsp source is invalid check the config file \n")
                    exit()

        elif source == 'facets':
            for line in read.splitlines():
                if srcs == 'member_addr':
                    if 'MEMBER_ADDR=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_member_addr'
                elif srcs == 'member':
                    if 'MEMBER=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_member'
                elif srcs == 'enrollment':
                    if 'ENROLLMENT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_enrollment'
                else:
                    print(srcs)
                    print("vsp source is invalid check the config file \n")
                    exit()

        elif source == 'cca_case':
            for line in read.splitlines():
                if srcs == 'case_member_hra_unnest_root':
                    if 'CASE_MEMBER_HRA_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_case_member_hra_unnest_root'
                elif srcs == 'case_member_unnest_root':
                    if 'CASE_MEMBER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_case_member_unnest_root'
                elif srcs == 'concept_type_unnest_root':
                    if 'CONCEPT_TYPE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_concept_type_unnest_root'
                elif srcs == 'concept_unnest_root':
                    if 'CONCEPT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_concept_unnest_root'
                elif srcs == 'form_question_unnest_root':
                    if 'FORM_QUESTION_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_form_question_unnest_root'
                elif srcs == 'form_unnest_root':
                    if 'FORM_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_form_unnest_root'
                elif srcs == 'hra_unnest_root':
                    if 'HRA_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_hra_unnest_root'
                elif srcs == 'medical_code_sets_unnest_root':
                    if 'MEDICAL_CODE_SETS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_medical_code_sets_unnest_root'
                elif srcs == 'medical_code_unnest_root':
                    if 'MEDICAL_CODE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_medical_code_unnest_root'
                elif srcs == 'case_member_hra_unnest_root':
                    if 'CASE_MEMBER_HRA_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_case_member_hra_unnest_root'
                elif srcs == 'case_member_unnest_root':
                    if 'CASE_MEMBER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_case_member_unnest_root'
                elif srcs == 'org_case_cost_unnest_root':
                    if 'ORG_CASE_COST_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_case_cost_unnest_root'
                elif srcs == 'org_case_save_type_unnest_root':
                    if 'ORG_CASE_SAVE_TYPE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_case_save_type_unnest_root'
                elif srcs == 'org_case_status_unnest_root':
                    if 'ORG_CASE_STATUS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_case_status_unnest_root'
                elif srcs == 'org_case_unnest_root':
                    if 'ORG_CASE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_case_unnest_root'
                elif srcs == 'org_info_notepad_unnest_root':
                    if 'ORG_INFO_NOTEPAD_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_info_notepad_unnest_root'
                elif srcs == 'org_info_unnest_root':
                    if 'ORG_INFO_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_info_unnest_root'
                elif srcs == 'org_patient_unnest_root':
                    if 'ORG_PATIENT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_patient_unnest_root'
                elif srcs == 'org_physician_unnest_root':
                    if 'ORG_PHYSICIAN_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_physician_unnest_root'
                elif srcs == 'org_pn_annotation_commitreport_unnest_root':
                    if 'ORG_PN_ANNOTATION_COMMITREPORT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_pn_annotation_commitreport_unnest_root'
                elif srcs == 'org_um_auth_status_type_unnest_root':
                    if 'ORG_UM_AUTH_STATUS_TYPE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_um_auth_status_type_unnest_root'
                elif srcs == 'org_um_case_unnest_root':
                    if 'ORG_UM_CASE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_um_case_unnest_root'
                elif srcs == 'org_um_los_extension_unnest_root':
                    if 'ORG_UM_LOS_EXTENSION_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_um_los_extension_unnest_root'
                elif srcs == 'org_um_los_unnest_root':
                    if 'ORG_UM_LOS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_um_los_unnest_root'
                elif srcs == 'org_um_service_cert_unnest_root':
                    if 'ORG_UM_SERVICE_CERT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_um_service_cert_unnest_root'
                elif srcs == 'org_um_service_unnest_root':
                    if 'ORG_UM_SERVICE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_um_service_unnest_root'
                elif srcs == 'resource_file_unnest_root':
                    if 'RESOURCE_FILE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_resource_file_unnest_root'
                elif srcs == 'string_locale_unnest_root':
                    if 'STRING_LOCALE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_string_locale_unnest_root'
                elif srcs == 'org_case_type_unnest_root':
                    if 'ORG_CASE_TYPE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_org_case_type_unnest_root'
                elif srcs == 'string_locale_reference_unnest_root':
                    if 'STRING_LOCALE_REFERENCE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_case_string_locale_reference_unnest_root'
                else:
                    print(srcs)
                    print(" source is invalid check the config file \n")
                    exit()

        elif source == 'cca_member':
            for line in read.splitlines():
                if srcs == 'concept_unnest_root':
                    if 'CONCEPT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_concept_unnest_root'
                elif srcs == 'concept_type_unnest_root':
                    if 'CONCEPT_TYPE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_concept_type_unnest_root'
                elif srcs == 'form_unnest_root':
                    if 'FORM_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_form_unnest_root'
                elif srcs == 'form_question_unnest_root':
                    if 'FORM_QUESTION_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_form_question_unnest_root'
                elif srcs == 'hra_unnest_root':
                    if 'HRA_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_hra_unnest_root'
                elif srcs == 'medical_code_unnest_root':
                    if 'MEDICAL_CODE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_medical_code_unnest_root'
                elif srcs == 'medical_code_sets_unnest_root':
                    if 'MEDICAL_CODE_SETS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_medical_code_sets_unnest_root'
                elif srcs == 'member_unnest_root':
                    if 'MEMBER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_member_unnest_root'
                elif srcs == 'member_coverage_unnest_root':
                    if 'MEMBER_COVERAGE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_member_coverage_unnest_root'
                elif srcs == 'member_external_data_unnest_root':
                    if 'MEMBER_EXTERNAL_DATA_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_member_external_data_unnest_root'
                elif srcs == 'member_hra_unnest_root':
                    if 'MEMBER_HRA_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_member_hra_unnest_root'
                elif srcs == 'member_p_concept_value_unnest_root':
                    if 'MEMBER_P_CONCEPT_VALUE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_member_p_concept_value_unnest_root'
                elif srcs == 'member_p_concept_value_log_unnest_root':
                    if 'MEMBER_P_CONCEPT_VALUE_LOG_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_member_p_concept_value_log_unnest_root'
                elif srcs == 'member_u_concept_value_unnest_root':
                    if 'MEMBER_U_CONCEPT_VALUE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_member_u_concept_value_unnest_root'
                elif srcs == 'member_u_concept_value_log_unnest_root':
                    if 'MEMBER_U_CONCEPT_VALUE_LOG_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_member_u_concept_value_log_unnest_root'
                elif srcs == 'org_case_save_type_unnest_root':
                    if 'ORG_CASE_SAVE_TYPE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_org_case_save_type_unnest_root'
                elif srcs == 'org_case_status_unnest_root':
                    if 'ORG_CASE_STATUS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_org_case_status_unnest_root'
                elif srcs == 'org_um_auth_status_type_unnest_root':
                    if 'ORG_UM_AUTH_STATUS_TYPE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_org_um_auth_status_type_unnest_root'
                elif srcs == 'resource_file_unnest_root':
                    if 'RESOURCE_FILE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_resource_file_unnest_root'
                elif srcs == 'string_locale_unnest_root':
                    if 'STRING_LOCALE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_string_locale_unnest_root'
                elif srcs == 'org_case_type_unnest_root':
                    if 'ORG_CASE_TYPE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_org_case_type_unnest_root'
                elif srcs == 'string_locale_reference_unnest_root':
                    if 'STRING_LOCALE_REFERENCE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_string_locale_reference_unnest_root'
                elif srcs == 'tcs_send_log_unnest_root':
                    if 'TCS_SEND_LOG_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_member_tcs_send_log_unnest_root'
                else:
                    print(srcs)
                    print(" source is invalid check the config file \n")
                    exit()

        elif source == 'provider_foundation':
            for line in read.splitlines():
                if srcs == 'provider_unnest_root':
                    if 'PROVIDER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root'
                elif srcs == 'provider_unnest_root_aois':
                    if 'PROVIDER_UNNEST_ROOT_AOIS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_aois'
                elif srcs == 'provider_unnest_root_appointments':
                    if 'PROVIDER_UNNEST_ROOT_APPOINTMENTS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_appointments'
                elif srcs == 'provider_unnest_root_certifications':
                    if 'PROVIDER_UNNEST_ROOT_CERTIFICATIONS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_certifications'
                elif srcs == 'provider_unnest_root_degrees':
                    if 'PROVIDER_UNNEST_ROOT_DEGREES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_degrees'
                elif srcs == 'provider_unnest_root_educations':
                    if 'PROVIDER_UNNEST_ROOT_EDUCATIONS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_educations'
                elif srcs == 'provider_unnest_root_hospital_affiliations':
                    if 'PROVIDER_UNNEST_ROOT_HOSPITAL_AFFILIATIONS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_hospital_affiliations'
                elif srcs == 'provider_unnest_root_insurance':
                    if 'PROVIDER_UNNEST_ROOT_INSURANCE=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_insurance'
                elif srcs == 'provider_unnest_root_languages':
                    if 'PROVIDER_UNNEST_ROOT_LANGUAGES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_languages'
                elif srcs == 'provider_unnest_root_licenses':
                    if 'PROVIDER_UNNEST_ROOT_LICENSES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_licenses'
                elif srcs == 'provider_unnest_root_locations':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations'
                elif srcs == 'provider_unnest_root_locations__billing_address___fax_list':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS__BILLING_ADDRESS___FAX_LIST=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations__billing_address___fax_list'
                elif srcs == 'provider_unnest_root_locations__billing_address___phone_list':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS__BILLING_ADDRESS___PHONE_LIST=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations__billing_address___phone_list'
                elif srcs == 'provider_unnest_root_locations__mailing_address___fax_list':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS__MAILING_ADDRESS___FAX_LIST=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations__mailing_address___fax_list'
                elif srcs == 'provider_unnest_root_locations__mailing_address___phone_list':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS__MAILING_ADDRESS___PHONE_LIST=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations__mailing_address___phone_list'
                elif srcs == 'provider_unnest_root_locations__physical_address___fax_list':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS__PHYSICAL_ADDRESS___FAX_LIST=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations__physical_address___fax_list'
                elif srcs == 'provider_unnest_root_locations__physical_address___phone_list':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS__PHYSICAL_ADDRESS___PHONE_LIST=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations__physical_address___phone_list'
                elif srcs == 'provider_unnest_root_locations___beds':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___BEDS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___beds'
                elif srcs == 'provider_unnest_root_locations___certifications':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___CERTIFICATIONS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___certifications'
                elif srcs == 'provider_unnest_root_locations___disclaimers':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___DISCLAIMERS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___disclaimers'
                elif srcs == 'provider_unnest_root_locations___mdcares':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___MDCARES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___mdcares'
                elif srcs == 'provider_unnest_root_locations___networks':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___NETWORKS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___networks'
                elif srcs == 'provider_unnest_root_locations___networks___contracts':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___NETWORKS___CONTRACTS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___networks___contracts'
                elif srcs == 'provider_unnest_root_locations___networks___derived_networks':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___NETWORKS___DERIVED_NETWORKS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___networks___derived_networks'
                elif srcs == 'provider_unnest_root_locations___networks___infosources':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___NETWORKS___INFOSOURCES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___networks___infosources'
                elif srcs == 'provider_unnest_root_locations___networks___reimbursements':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___NETWORKS___REIMBURSEMENTS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___networks___reimbursements'
                elif srcs == 'provider_unnest_root_locations___networks___specialties':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___NETWORKS___SPECIALTIES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___networks___specialties'
                elif srcs == 'provider_unnest_root_locations___npis':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___NPIS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___npis'
                elif srcs == 'provider_unnest_root_locations___probation_reviews':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___PROBATION_REVIEWS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___probation_reviews'
                elif srcs == 'provider_unnest_root_locations___prprs':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___PRPRS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___prprs'
                elif srcs == 'provider_unnest_root_locations___redirects':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___REDIRECTS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___redirects'
                elif srcs == 'provider_unnest_root_locations___specialties':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___SPECIALTIES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___specialties'
                elif srcs == 'provider_unnest_root_locations___tax_ids':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___TAX_IDS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___tax_ids'
                elif srcs == 'provider_unnest_root_locations___tax_ids___medical_coalitions':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___TAX_IDS___MEDICAL_COALITIONS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___tax_ids___medical_coalitions'
                elif srcs == 'provider_unnest_root_locations___triwests':
                    if 'PROVIDER_UNNEST_ROOT_LOCATIONS___TRIWESTS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_locations___triwests'
                elif srcs == 'provider_unnest_root_mdcares':
                    if 'PROVIDER_UNNEST_ROOT_MDCARES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_mdcares'
                elif srcs == 'provider_unnest_root_modalities':
                    if 'PROVIDER_UNNEST_ROOT_MODALITIES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_modalities'
                elif srcs == 'provider_unnest_root_npis':
                    if 'PROVIDER_UNNEST_ROOT_NPIS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_npis'
                elif srcs == 'provider_unnest_root_npis___infosources':
                    if 'PROVIDER_UNNEST_ROOT_NPIS___INFOSOURCES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_npis___infosources'
                elif srcs == 'provider_unnest_root_populations':
                    if 'PROVIDER_UNNEST_ROOT_POPULATIONS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_populations'
                elif srcs == 'provider_unnest_root_prcps':
                    if 'PROVIDER_UNNEST_ROOT_PRCPS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_prcps'
                elif srcs == 'provider_unnest_root_probation_reviews':
                    if 'PROVIDER_UNNEST_ROOT_PROBATION_REVIEWS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_probation_reviews'
                elif srcs == 'provider_unnest_root_specialties':
                    if 'PROVIDER_UNNEST_ROOT_SPECIALTIES=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_specialties'
                elif srcs == 'provider_unnest_root_ssns':
                    if 'PROVIDER_UNNEST_ROOT_SSNS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_ssns'
                elif srcs == 'provider_unnest_root_upins':
                    if 'PROVIDER_UNNEST_ROOT_UPINS=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'provider_foundation_provider_unnest_root_upins'
                else:
                    print(srcs)
                    print(" source is invalid check the config file "+srcs +" \n")
                    exit()

        elif source == 'quest_lab':
            for line in read.splitlines():
                if srcs == 'quest_lab_unnest_root':
                    if 'QUEST_LAB_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_lab_unnest_root'
                elif srcs == 'quest_lab_unnest_root_orc':
                    if 'QUEST_LAB_UNNEST_ROOT_ORC=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_lab_unnest_root_orc'
                elif srcs == 'quest_lab_unnest_root_orc___dg1':
                    if 'QUEST_LAB_UNNEST_ROOT_ORC___DG1=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_lab_unnest_root_orc___dg1'
                elif srcs == 'quest_lab_unnest_root_orc___obr':
                    if 'QUEST_LAB_UNNEST_ROOT_ORC___OBR=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_lab_unnest_root_orc___obr'
                elif srcs == 'quest_lab_unnest_root_orc___obr___obx':
                    if 'QUEST_LAB_UNNEST_ROOT_ORC___OBR___OBX=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_lab_unnest_root_orc___obr___obx'
                elif srcs == 'quest_lab_unnest_root_orc___obr___obx___nte':
                    if 'QUEST_LAB_UNNEST_ROOT_ORC___OBR___OBX___NTE=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_lab_unnest_root_orc___obr___obx___nte'
                elif srcs == 'quest_lab_unnest_root_orc___orderingprovider':
                    if 'QUEST_LAB_UNNEST_ROOT_ORC___ORDERINGPROVIDER=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_lab_unnest_root_orc___orderingprovider'
                elif srcs == 'quest_lab_unnest_root_pid':
                    if 'QUEST_LAB_UNNEST_ROOT_PID=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_lab_unnest_root_pid'
                else:
                    print(srcs)
                    print(" source is invalid check the config file "+srcs +" \n")
                    exit()

        elif source == 'quest_v2':
            for line in read.splitlines():
                if srcs == 'lab_unnest_root':
                    if 'LAB_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root'
                elif srcs == 'lab_unnest_root___orc':
                    if 'LAB_UNNEST_ROOT___ORC=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root___orc'
                elif srcs == 'lab_unnest_root___orc___dg1':
                    if 'LAB_UNNEST_ROOT___ORC___DG1=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root___orc___dg1'
                elif srcs == 'lab_unnest_root___orc___obr':
                    if 'LAB_UNNEST_ROOT___ORC___OBR=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root___orc___obr'
                elif srcs == 'lab_unnest_root___orc___obr___obx':
                    if 'LAB_UNNEST_ROOT___ORC___OBR___OBX=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root___orc___obr___obx'
                elif srcs == 'lab_unnest_root___orc___obr___obx___nte':
                    if 'LAB_UNNEST_ROOT___ORC___OBR___OBX___NTE=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root___orc___obr___obx___nte'
                elif srcs == 'lab_unnest_root___orc___obr___orderingprovider':
                    if 'LAB_UNNEST_ROOT___ORC___OBR___ORDERINGPROVIDER=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root___orc___obr___orderingprovider'

                elif srcs == 'lab_unnest_root___orc___orderingprovider':
                    if 'LAB_UNNEST_ROOT___ORC___ORDERINGPROVIDER=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root___orc___orderingprovider'

                elif srcs == 'lab_unnest_root___pid':
                    if 'LAB_UNNEST_ROOT___PID=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'quest_v2_lab_unnest_root___pid'
                else:
                    print("quest_v2 source is invalid check the config file \n")
                    exit()


        elif source == 'facets_um':
            for line in read.splitlines():
                if srcs == 'activity_unnest_root':
                    if 'ACTIVITY_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_um_activity_unnest_root'
                elif srcs == 'base_unnest_root':
                    if 'BASE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_um_base_unnest_root'
                elif srcs == 'diagnosis_codeset_unnest_root':
                    if 'DIAGNOSIS_CODESET_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_um_diagnosis_codeset_unnest_root'
                elif srcs == 'inpatient_unnest_root':
                    if 'INPATIENT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_um_inpatient_unnest_root'
                elif srcs == 'procedure_codeset_unnest_root':
                    if 'PROCEDURE_CODESET_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_um_procedure_codeset_unnest_root'
                elif srcs == 'provider_unnest_root':
                    if 'PROVIDER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_um_provider_unnest_root'
                elif srcs == 'services_unnest_root':
                    if 'SERVICES_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_um_services_unnest_root'
                else:
                    print("facets um source is invalid check the config file \n")
                    exit()

        elif source == 'facets_claim':
            for line in read.splitlines():
                if srcs == 'dental_lineitem_override_unnest_root':
                    if 'DENTAL_LINEITEM_OVERRIDE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_dental_lineitem_override_unnest_root'
                elif srcs == 'header_over_payment_unnest_root':
                    if 'HEADER_OVER_PAYMENT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_header_over_payment_unnest_root'
                elif srcs == 'mctr_code_translations_unnest_root':
                    if 'MCTR_CODE_TRANSLATIONS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_mctr_code_translations_unnest_root'
                elif srcs == 'dental_lineitem_disallow_amt_unnest_root':
                    if 'DENTAL_LINEITEM_DISALLOW_AMT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_dental_lineitem_disallow_amt_unnest_root'
                elif srcs == 'dental_lineitem_unnest_root':
                    if 'DENTAL_LINEITEM_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_dental_lineitem_unnest_root'
                elif srcs == 'payment_reduction_hist_unnest_root':
                    if 'PAYMENT_REDUCTION_HIST_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_payment_reduction_hist_unnest_root'
                elif srcs == 'member_cob_unnest_root':
                    if 'MEMBER_COB_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_member_cob_unnest_root'
                elif srcs == 'member_pcp_rel_hist_unnest_root':
                    if 'MEMBER_PCP_REL_HIST_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_member_pcp_rel_hist_unnest_root'
                elif srcs == 'payment_check_unnest_root':
                    if 'PAYMENT_CHECK_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_payment_check_unnest_root'
                elif srcs == 'procedure_code_unnest_root':
                    if 'PROCEDURE_CODE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_procedure_code_unnest_root'
                elif srcs == 'product_unnest_root':
                    if 'PRODUCT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_product_unnest_root'
                elif srcs == 'payment_check_status_unnest_root':
                    if 'PAYMENT_CHECK_STATUS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_payment_check_status_unnest_root'
                elif srcs == 'payment_summary_unnest_root':
                    if 'PAYMENT_SUMMARY_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_payment_summary_unnest_root'
                elif srcs == 'header_override_unnest_root':
                    if 'HEADER_OVERRIDE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_header_override_unnest_root'
                elif srcs == 'plan_product_linking_data_unnest_root':
                    if 'PLAN_PRODUCT_LINKING_DATA_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_plan_product_linking_data_unnest_root'
                elif srcs == 'subscriber_address_unnest_root':
                    if 'SUBSCRIBER_ADDRESS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_subscriber_address_unnest_root'
                elif srcs == 'medical_lineitem_disallow_amt_unnest_root':
                    if 'MEDICAL_LINEITEM_DISALLOW_AMT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_medical_lineitem_disallow_amt_unnest_root'
                elif srcs == 'payment_unnest_root':
                    if 'PAYMENT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_payment_unnest_root'
                elif srcs == 'diagnosis_unnest_root':
                    if 'DIAGNOSIS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_diagnosis_unnest_root'
                elif srcs == 'header_status_unnest_root':
                    if 'HEADER_STATUS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_header_status_unnest_root'
                elif srcs == 'medical_lineitem_overrides_unnest_root':
                    if 'MEDICAL_LINEITEM_OVERRIDES_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_medical_lineitem_overrides_unnest_root'
                elif srcs == 'ppl_unnest_root':
                    if 'PPL_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_ppl_unnest_root'
                elif srcs == 'header_unnest_root':
                    if 'HEADER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_header_unnest_root'
                elif srcs == 'medical_lineitem_unnest_root':
                    if 'MEDICAL_LINEITEM_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_medical_lineitem_unnest_root'
                else:
                    print(srcs)
                    print(" source is invalid check the config file \n")
                    exit()

        elif source == 'facets_rre':
            for line in read.splitlines():
                if srcs == 'trgt_ede_cspi_ext_unnest_root':
                    if 'TRGT_EDE_CSPI_EXT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_trgt_ede_cspi_ext_unnest_root'
                elif srcs == 'trgt_ede_devl1_unnest_root':
                    if 'TRGT_EDE_DEVL1_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_trgt_ede_devl1_unnest_root'
                elif srcs == 'trgt_ede_grgr_ext_unnest_root':
                    if 'TRGT_EDE_GRGR_EXT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_trgt_ede_grgr_ext_unnest_root'
                elif srcs == 'trgt_ede_sbsb_ext_unnest_root':
                    if 'TRGT_EDE_SBSB_EXT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_trgt_ede_sbsb_ext_unnest_root'
                elif srcs == 'cmc_cscs_class_unnest_root':
                    if 'CMC_CSCS_CLASS_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_cscs_class_unnest_root'
                elif srcs == 'cmc_grgr_group_unnest_root':
                    if 'CMC_GRGR_GROUP_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_grgr_group_unnest_root'
                elif srcs == 'cmc_pagr_parent_gr_unnest_root':
                    if 'CMC_PAGR_PARENT_GR_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_pagr_parent_gr_unnest_root'
                elif srcs == 'cmc_plds_plan_desc_unnest_root':
                    if 'CMC_PLDS_PLAN_DESC_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_plds_plan_desc_unnest_root'
                elif srcs == 'cmc_sgsg_sub_group_unnest_root':
                    if 'CMC_SGSG_SUB_GROUP_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_sgsg_sub_group_unnest_root'
                elif srcs == 'cmc_ntnb_note_base_unnest_root':
                    if 'CMC_NTNB_NOTE_BASE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_ntnb_note_base_unnest_root'
                elif srcs == 'cmc_nttx_note_text_unnest_root':
                    if 'CMC_NTTX_NOTE_TEXT_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_nttx_note_text_unnest_root'
                elif srcs == 'cmc_memd_mecr_detl_unnest_root':
                    if 'CMC_MEMD_MECR_DETL_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_memd_mecr_detl_unnest_root'
                elif srcs == 'cmc_meme_member_unnest_root':
                    if 'CMC_MEME_MEMBER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_meme_member_unnest_root'
                elif srcs == 'cmc_mepe_prcs_elig_unnest_root':
                    if 'CMC_MEPE_PRCS_ELIG_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_mepe_prcs_elig_unnest_root'
                elif srcs == 'cmc_sbsb_subsc_unnest_root':
                    if 'CMC_SBSB_SUBSC_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_rre_cmc_sbsb_subsc_unnest_root'
                else:
                    print("facets_rre source is invalid check the config file \n")
                    exit()
        elif source == 'facets_enrollment':
            for line in read.splitlines():
                if srcs == 'enrl_unnest_root':
                    if 'ENRL_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_enrollment_enrl_unnest_root'
                else:
                    print("facets_enrollment source is invalid check the config file \n")
                    exit()
        elif source == 'facets_member':
            for line in read.splitlines():
                if srcs == 'addr_unnest_root':
                    if 'ADDR_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_member_addr_unnest_root'
                elif srcs == 'mbr_unnest_root':
                    if 'MBR_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_member_mbr_unnest_root'
                else:
                    print("facets_member source is invalid check the config file \n")
                    exit()

        elif source == 'facets_member_v2':
            for line in read.splitlines():
                if srcs == 'addr_unnest_root':
                    if 'ADDR_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_member_v2_addr_unnest_root'
                elif srcs == 'mbr_unnest_root':
                    if 'MBR_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_member_v2_mbr_unnest_root'
                else:
                    print("facets_member_v2 source is invalid check the config file \n")
                    exit()
        elif source == 'facets_enrollment_v2':
            for line in read.splitlines():
                if srcs == 'enrl_unnest_root':
                    if 'ENRL_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_enrollment_v2_enrl_unnest_root'
                else:
                    print("facets_enrollment_v2 source is invalid check the config file \n")
                    exit()

        elif source == 'facets_claim_extension':
            for line in read.splitlines():
                logging.info("srcs"+srcs)
                if srcs == 'cer_atnd_note_c_unnest_root':
                    if 'CER_ATND_NOTE_C_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_cer_atnd_note_c_unnest_root'
                elif srcs == 'cer_atxr_attach_u_unnest_root':
                    if 'CER_ATXR_ATTACH_U_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_cer_atxr_attach_u_unnest_root'
                elif srcs == 'dental_lineitem_unnest_root':
                    if 'DENTAL_LINEITEM_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_dental_lineitem_unnest_root'
                elif srcs == 'dental_lineitem_cob_unnest_root':
                    if 'DENTAL_LINEITEM_COB_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_dental_lineitem_cob_unnest_root'
                elif srcs == 'dental_lineitem_disallow_amt_unnest_root':
                    if 'DENTAL_LINEITEM_DISALLOW_AMT_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_dental_lineitem_disallow_amt_unnest_root'
                elif srcs == 'dental_lineitem_override_unnest_root':
                    if 'DENTAL_LINEITEM_OVERRIDE_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_dental_lineitem_override_unnest_root'
                elif srcs == 'dental_procedure_desc_unnest_root':
                    if 'DENTAL_PROCEDURE_DESC_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_dental_procedure_desc_unnest_root'
                elif srcs == 'diagnosis_unnest_root':
                    if 'DIAGNOSIS_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_diagnosis_unnest_root'
                elif srcs == 'diagnosis_codeset_unnest_root':
                    if 'DIAGNOSIS_CODESET_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_diagnosis_codeset_unnest_root'
                elif srcs == 'eligibility_unnest_root':
                    if 'ELIGIBILITY_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_eligibility_unnest_root'
                elif srcs == 'explanation_codeset_unnest_root':
                    if 'EXPLANATION_CODESET_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_explanation_codeset_unnest_root'
                elif srcs == 'group_unnest_root':
                    if 'GROUP_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_group_unnest_root'
                elif srcs == 'header_unnest_root':
                    if 'HEADER_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_unnest_root'
                elif srcs == 'header_cob_unnest_root':
                    if 'HEADER_COB_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_cob_unnest_root'
                elif srcs == 'header_its_patient_unnest_root':
                    if 'HEADER_ITS_PATIENT_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_its_patient_unnest_root'
                elif srcs == 'header_its_provider_unnest_root':
                    if 'HEADER_ITS_PROVIDER_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_its_provider_unnest_root'
                elif srcs == 'header_its_subscriber_unnest_root':
                    if 'HEADER_ITS_SUBSCRIBER_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_its_subscriber_unnest_root'
                elif srcs == 'header_misc_unnest_root':
                    if 'HEADER_MISC_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_misc_unnest_root'
                elif srcs == 'header_multi_func_unnest_root':
                    if 'HEADER_MULTI_FUNC_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_multi_func_unnest_root'
                elif srcs == 'header_override_unnest_root':
                    if 'HEADER_OVERRIDE_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_override_unnest_root'
                elif srcs == 'header_over_payment_unnest_root':
                    if 'HEADER_OVER_PAYMENT_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_over_payment_unnest_root'
                elif srcs == 'header_status_unnest_root':
                    if 'HEADER_STATUS_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_header_status_unnest_root'
                elif srcs == 'inpatient_procedure_unnest_root':
                    if 'INPATIENT_PROCEDURE_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_inpatient_procedure_unnest_root'
                elif srcs == 'institutional_unnest_root':
                    if 'INSTITUTIONAL_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_institutional_unnest_root'
                elif srcs == 'lob_unnest_root':
                    if 'LOB_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_lob_unnest_root'
                elif srcs == 'mctr_code_translations_unnest_root':
                    if 'MCTR_CODE_TRANSLATIONS_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_mctr_code_translations_unnest_root'
                elif srcs == 'medical_lineitem_unnest_root':
                    if 'MEDICAL_LINEITEM_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_medical_lineitem_unnest_root'
                elif srcs == 'medical_lineitem_cob_unnest_root':
                    if 'MEDICAL_LINEITEM_COB_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_medical_lineitem_cob_unnest_root'
                elif srcs == 'medical_lineitem_disallow_amt_unnest_root':
                    if 'MEDICAL_LINEITEM_DISALLOW_AMT_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_medical_lineitem_disallow_amt_unnest_root'
                elif srcs == 'medical_lineitem_its_provider_unnest_root':
                    if 'MEDICAL_LINEITEM_ITS_PROVIDER_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_medical_lineitem_its_provider_unnest_root'
                elif srcs == 'medical_lineitem_overrides_unnest_root':
                    if 'MEDICAL_LINEITEM_OVERRIDES_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_medical_lineitem_overrides_unnest_root'
                elif srcs == 'member_unnest_root':
                    if 'MEMBER_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_member_unnest_root'
                elif srcs == 'member_cob_unnest_root':
                    if 'MEMBER_COB_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_member_cob_unnest_root'
                elif srcs == 'member_pcp_rel_hist_unnest_root':
                    if 'MEMBER_PCP_REL_HIST_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_member_pcp_rel_hist_unnest_root'
                elif srcs == 'payment_unnest_root':
                    if 'PAYMENT_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_payment_unnest_root'
                elif srcs == 'payment_check_unnest_root':
                    if 'PAYMENT_CHECK_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_payment_check_unnest_root'
                elif srcs == 'payment_check_status_unnest_root':
                    if 'PAYMENT_CHECK_STATUS_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_payment_check_status_unnest_root'
                elif srcs == 'payment_reduction_hist_unnest_root':
                    if 'PAYMENT_REDUCTION_HIST_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_payment_reduction_hist_unnest_root'
                elif srcs == 'payment_summary_unnest_root':
                    if 'PAYMENT_SUMMARY_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_payment_summary_unnest_root'
                elif srcs == 'payor_unnest_root':
                    if 'PAYOR_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_payor_unnest_root'
                elif srcs == 'plan_desc_unnest_root':
                    if 'PLAN_DESC_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_plan_desc_unnest_root'
                elif srcs == 'plan_product_linking_data_unnest_root':
                    if 'PLAN_PRODUCT_LINKING_DATA_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_plan_product_linking_data_unnest_root'
                elif srcs == 'ppl_unnest_root':
                    if 'PPL_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_ppl_unnest_root'
                elif srcs == 'procedure_codeset_unnest_root':
                    if 'PROCEDURE_CODESET_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_procedure_codeset_unnest_root'
                elif srcs == 'product_unnest_root':
                    if 'PRODUCT_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_product_unnest_root'
                elif srcs == 'product_desc_unnest_root':
                    if 'PRODUCT_DESC_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_product_desc_unnest_root'
                elif srcs == 'provider_unnest_root':
                    if 'PROVIDER_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_provider_unnest_root'
                elif srcs == 'provider_address_unnest_root':
                    if 'PROVIDER_ADDRESS_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_provider_address_unnest_root'
                elif srcs == 'related_entity_unnest_root':
                    if 'RELATED_ENTITY_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_related_entity_unnest_root'
                elif srcs == 'revenue_codeset_unnest_root':
                    if 'REVENUE_CODESET_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_revenue_codeset_unnest_root'
                elif srcs == 'service_description_unnest_root':
                    if 'SERVICE_DESCRIPTION_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_service_description_unnest_root'
                elif srcs == 'service_rule_unnest_root':
                    if 'SERVICE_RULE_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_service_rule_unnest_root'
                elif srcs == 'subscriber_unnest_root':
                    if 'SUBSCRIBER_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_subscriber_unnest_root'
                elif srcs == 'subscriber_address_unnest_root':
                    if 'SUBSCRIBER_ADDRESS_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_subscriber_address_unnest_root'
                elif srcs == 'sub_group_unnest_root':
                    if 'SUB_GROUP_UNNEST_ROOT' == line.split("=")[0]:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'facets_claim_extension_sub_group_unnest_root'
                else:
                    print("facets_claim_extension source is invalid check the config file \n")
                    exit()

        elif source == 'cambia_bcbsa_claims':
            for line in read.splitlines():
                if srcs == 'claimheader_unnest_root':
                    if 'CLAIMHEADER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cambia_bcbsa_claims_claimheader_unnest_root'
                elif srcs == 'claimline_unnest_root':
                    if 'CLAIMLINE_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cambia_bcbsa_claims_claimline_unnest_root'
                else:
                    print("cambia_bcbsa_claims source is invalid check the config file \n")
                    exit()

        elif source == 'cambia_bcbsa_member':
            for line in read.splitlines():
                if srcs == 'member_unnest_root':
                    if 'MEMBER_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cambia_bcbsa_member_member_unnest_root'
                else:
                    print("cambia_bcbsa_member source is invalid check the config file \n")
                    exit()

        elif source == 'labcorp':
            for line in read.splitlines():
                if srcs == 'lab_unnest_root':
                    if 'LAB_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'labcorp_lab_unnest_root'
                elif srcs == 'lab_unnest_root___dg1':
                    if 'LAB_UNNEST_ROOT___DG1=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'labcorp_lab_unnest_root___dg1'
                elif srcs == 'lab_unnest_root___orc':
                    if 'LAB_UNNEST_ROOT___ORC=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'labcorp_lab_unnest_root___orc'
                elif srcs == 'lab_unnest_root___orc___obr':
                    if 'LAB_UNNEST_ROOT___ORC___OBR=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'labcorp_lab_unnest_root___orc___obr'
                elif srcs == 'lab_unnest_root___orc___obr___obx':
                    if 'LAB_UNNEST_ROOT___ORC___OBR___OBX=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'labcorp_lab_unnest_root___orc___obr___obx'
                elif srcs == 'lab_unnest_root___orc___obr___obx___nte':
                    if 'LAB_UNNEST_ROOT___ORC___OBR___OBX___NTE=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'labcorp_lab_unnest_root___orc___obr___obx___nte'
                elif srcs == 'lab_unnest_root___orc___orderingprovider':
                    if 'LAB_UNNEST_ROOT___ORC___ORDERINGPROVIDER=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'labcorp_lab_unnest_root___orc___orderingprovider'
                elif srcs == 'lab_unnest_root___pid':
                    if 'LAB_UNNEST_ROOT___PID=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'labcorp_lab_unnest_root___pid'
                else:
                    print("labcorp source is invalid check the config file \n")
                    exit()


        elif source == 'paml':
            for line in read.splitlines():
                if srcs == 'lab_unnest_root':
                    if 'LAB_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'paml_lab_unnest_root'
                elif srcs == 'lab_unnest_root___orc':
                    if 'LAB_UNNEST_ROOT___ORC=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'paml_lab_unnest_root___orc'
                elif srcs == 'lab_unnest_root___orc___obr':
                    if 'LAB_UNNEST_ROOT___ORC___OBR=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'paml_lab_unnest_root___orc___obr'
                elif srcs == 'lab_unnest_root___orc___obr___nte':
                    if 'LAB_UNNEST_ROOT___ORC___OBR___NTE=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'paml_lab_unnest_root___orc___obr___nte'
                elif srcs == 'lab_unnest_root___orc___obr___obx':
                    if 'LAB_UNNEST_ROOT___ORC___OBR___OBX=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'paml_lab_unnest_root___orc___obr___obx'
                elif srcs == 'lab_unnest_root___orc___obr___obx___nte':
                    if 'LAB_UNNEST_ROOT___ORC___OBR___OBX___NTE=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'paml_lab_unnest_root___orc___obr___obx___nte'
                elif srcs == 'lab_unnest_root___pid':
                    if 'LAB_UNNEST_ROOT___PID=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'paml_lab_unnest_root___pid'
                else:
                    print("paml source is invalid check the config file \n")
                    exit()


        elif source == 'dynacare':
            for line in read.splitlines():
                if srcs == 'lab_unnest_root':
                    if 'LAB_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'dynacare_lab_unnest_root'
                else:
                    print("dynacare source is invalid check the config file \n")
                    exit()

        elif source == 'prime_cambia_v2':
            for line in read.splitlines():
                if srcs == 'compound_unnest_root':
                    if 'COMPOUND_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_cambia_v2_compound_unnest_root'
                elif srcs == 'standard_unnest_root':
                    if 'STANDARD_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_cambia_v2_standard_unnest_root'
                elif srcs == 'supplemental_unnest_root':
                    if 'SUPPLEMENTAL_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'prime_cambia_v2_supplemental_unnest_root'
                else:
                    print("prime_cambia_v2 source is invalid check the config file \n")
                    exit()

        elif source == 'cca_source':
            for line in read.splitlines():
                if srcs == 'external_system_unnest_root':
                    if 'EXTERNAL_SYSTEM_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_source_external_system_unnest_root'
                elif srcs == 'merge_member_control_unnest_root':
                    if 'MERGE_MEMBER_CONTROL_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_source_merge_member_control_unnest_root'
                elif srcs == 'task_unnest_root':
                    if 'TASK_UNNEST_ROOT=' in line:
                        src_prefix = line.split('=',1)[1]
                        f_name_2 = 'cca_source_task_unnest_root'
                else:
                    print("cca_source source is invalid check the config file \n")
                    exit()


            logging.info("srss"+f_name_2)

        else:
            print("Please enter the correct source")
            exit()

        print(" ")
        sleep_check = False
        if run_count > 1:
            sleep_check = True

        table_name = source + "_" + srcs

        table_ingestion = table_name+":"+i
        logfile = open('run_completed.txt', 'r')
        loglist = logfile.readlines()
        logfile.close()
        found = False
        for line1 in loglist:
            if str(table_ingestion) in line1:
                print("----------------------------------------------------------------------------------------------------")
                print("Data Copy for table "+table_name+" ingestion id "+i+" is already done")
                print("Delete the row which needs re run from s3://"+CONFIG_BUCKET_NAME+"/"+CONFIG_KEY+" & run again")
                print("----------------------------------------------------------------------------------------------------")
                found = True

        if not found:
                logging.info("Starting copying data for source view "+table_name)
                logging.info("Source:"+src_prefix)
                logging.info("Target:"+trg_prefix+"/"+f_name_2)
                copy_object(table_name, i, src_bucket_name, src_prefix, trg_bucket_name, trg_prefix, sleep_check, f_name_2)
                logging.info("Completed copying data for source view "+table_name)
                logging.info(" ")
        run_count = run_count + 1
        print(" ")

        if source in ['facets_um', 'facets_rre', 'prime_reject','cca_source']:
            entity_dest_prefix = 'data-views/' + extract_loc + '/entity_load_log_landing/'+CONNECTOR+'/' + i + '/'
            entity_file_name = 'entitylog/entity_' + CONNECTOR +'_'+SOURCE_NAME +'_'+  extract_time + '_' + i + '_' + '.csv'
            run_entity = open(entity_file_name, "w")
            run_entity.write(CONNECTOR + "," + SOURCE_NAME + "," + i + "," + "NA,STARTED,"+str(date_sf)+"\n")
            run_entity.close()
            dest_entity_file_name = entity_dest_prefix + entity_file_name
            s3client.upload_file(entity_file_name, dest_bucket_name, dest_entity_file_name)
            conn_dest_prefix = 'data-views/' + extract_loc + '/connector_trigger/'+ENT_CONNECTOR+'/'+SRC_CONNECTOR+'/' + i + '/'
            connector_file_name = 'connector/connector_' + CONNECTOR +'_'+SOURCE_NAME +'_'+ extract_time + '_' + i +'_' + '.csv'
            run_conn = open(connector_file_name, "w")
            run_conn.write(CONNECTOR + "," + SOURCE_NAME + "," + i + "," + "NEW\n")
            run_conn.close()
            dest_conn_file_name = conn_dest_prefix + connector_file_name
            s3client.upload_file(connector_file_name, dest_bucket_name, dest_conn_file_name)

    if os.path.exists('manifest_source/'+source+'_manifest_'+extract_time+'_'+i+'.csv'):
        manifest = 'manifest_source/'+source+'_manifest_'+extract_time+'_'+i+'.csv'
        s3client.upload_file(manifest, trg_bucket_name, trg_prefix+"/manifest/"+i+"/"+manifest)
    else:
        print("No data for this Ingestion\n")

    logs_prefix = 'data-views/' + extract_loc + '/snowflake_logs/source_logs/'
    logging_file_name_s3 = source+"_"+ingestion_id+"_"+extract_time+".log"
    dest_log_file_name = logs_prefix + logging_file_name_s3
    s3client.upload_file(logging_file_name, dest_bucket_name, dest_log_file_name)

    copy_state = 'SUCCESS'
    update_job_status(CONNECTOR, extract_job_time, ingestion_id, copy_state)
    logging.info("Completed data copy for Ingesiton "+ str(ingestion_id))

    now1 = datetime.datetime.now()
    date_time1 = now1.strftime("%m/%d/%Y, %H:%M:%S")
    job_end_time_ts = date_time1

    run_cmp1 = open("job_report_source_data.txt", "a")
    run_cmp1.write(CONNECTOR +  "|" + ingestion_id + "|" + job_start_time_ts + "|" + job_end_time_ts + "\n")
    run_cmp1.close()
    s3client.upload_file('job_report_source_data.txt', CONFIG_BUCKET_NAME , REPORT_KEY)

s3client.upload_file('run_completed.txt', CONFIG_BUCKET_NAME , CONFIG_KEY)
s3client.upload_file('run_completed.txt', CONFIG_BUCKET_NAME , CONFIG_KEY+"_"+extract_time)

exit()
