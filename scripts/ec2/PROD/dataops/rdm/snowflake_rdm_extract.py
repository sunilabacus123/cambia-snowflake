import json
import datetime
import io
import time
import sys
import os
import boto3
from boto3 import client
import pyarrow.parquet as pq
from pytz import timezone
import pytz
from botocore.exceptions import ClientError

##### Change here ####
env = 'cambia'


if not os.path.exists('manifest'):
    os.makedirs('manifest')

src_bucket_name = 'abacus-sandboxes-' + env
dest_bucket_name = 'abacus-data-lake-' + env
database = env + '_data_ops'
extract_name = "cambia_extract"
#extract_loc = "cambia_transfer"
extract_loc = "cambia_extract"
workgroup = env + '-dataops'
#### Change here ####

s3_conn   = client('s3')
currentDT = datetime.datetime.now()
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')


ath = boto3.client('athena',region_name='us-east-1')
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
glue = boto3.client('glue',region_name='us-east-1')

src_bucket = s3.Bucket(src_bucket_name)

extract_time = currentDT.strftime("%Y%m%d%H%M")

currentdt = datetime.datetime.now()
run_date = currentdt.strftime("%m_%d_%Y")
run_month = currentdt.strftime("%m")
run_date1 = currentdt.strftime("%d")
run_year = currentdt.strftime("%Y")
run_date2 = currentdt.strftime("%m/%d/%Y")

codeset =sys.argv[1]

if codeset == 'icd_10':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'FACETS_CODESET_2020':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'PRIME_RX_CODESET_VF2020':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'VSP_CODESET_2020':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'CAMBIA_LOOKUPS':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'HCPCS_II':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'HCPCSII_MODIFIER':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'PROVIDER_TAXONOMY':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'PROVIDER_NPPES_SUB_CMS':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'PROVIDER_NPPES_CMS':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'brd':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
    brd_table_list = 'HCPCS_II,MS_DRG,POS_CMS,PROVIDER_TAXONOMY,HIPPS_CMS,HCPCSII_MODIFIER,CPT_AMA,icd_10'
elif codeset == 'LOINC_REGENSTRIEF':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'GRA_CMS':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/'
elif codeset == 'ZIP_CODE_CROSSWALK_DARTMOUTH_ATLAS':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/county/'
elif codeset == 'county':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/county/'
    county_table_list = 'COUNTY_GPCI_CMS,PROVIDER_NPPES_CMS,ZIP_CODE_CROSSWALK_DARTMOUTH_ATLAS,COUNTY_GAF_CMS,PROVIDER_NPPES_SUB_CMS'
elif codeset == 'medispan':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/medispan/'
    medispan_table_list = 'medispan_val_wk,medispan_sum_wk,medispan_cph_wk,medispan_rtdf_wk,medispan_ndc_wk,medispan_rte_wk,medispan_str_wk,medispan_ndcm_wk,medispan_sec_wk,medispan_prc_wk,medispan_lab_wk,medispan_mod_wk,medispan_frm_wk,medispan_stuom_wk,medispan_gppc_wk,medispan_idrg_wk,medispan_ings_wk,medispan_dfdrg_wk,medispan_dict_wk,medispan_name_ndc_wk,medispan_drg_ndc_wk,medispan_dfdrg_drgnm_wk'
#    medispan_table_list = 'medispan_val_wk'
elif codeset == 'rxnorm':
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/rxnorm/'
    rxnorm_table_list = 'rxnatomarchive_nlm,rxnconso_nlm,rxncui_nlm,rxncuichanges_nlm,rxndoc_nlm,rxnrel_nlm,rxnsab_nlm,rxnsat_nlm,rxnsty_nlm'
elif codeset == 'ncpdp':
    ingestionid =sys.argv[2]
    sql_path = 'data-ops/etl/' + extract_name + '/sql_2/rdm/ncpdp/'
    ncpdp_table_list = 'reference_data_ncpdp_mas,reference_data_ncpdp_mas_af,reference_data_ncpdp_mas_coo,reference_data_ncpdp_mas_erx,reference_data_ncpdp_mas_fwa,reference_data_ncpdp_mas_md,reference_data_ncpdp_mas_pc,reference_data_ncpdp_mas_pr,reference_data_ncpdp_mas_rec,reference_data_ncpdp_mas_rr,reference_data_ncpdp_mas_stl,reference_data_ncpdp_mas_svc,reference_data_ncpdp_mas_tx'
else:
    print("Enter correct rdm code set")



def create_table(view_name, run_date,sql_path,src_bucket_name):
    try:
        file_to_read = sql_path + view_name + '.sql'
        fileobj = s3client.get_object(Bucket=src_bucket_name, Key=file_to_read)
        filedata = fileobj['Body'].read()
        filecontents = filedata.decode("utf-8")

        if codeset == 'ncpdp':
            filecontents = filecontents.replace('replace_ingestionid',ingestionid)

        create_query = filecontents.replace('replace_date',run_date)

        print("\n============ Creating table for "+view_name+" ================\n")
        print("Athena Query :"+create_query)

        response = ath.start_query_execution(QueryString=create_query,
                                             QueryExecutionContext={'Database': database}, WorkGroup=workgroup)
        query_execution_id = response["QueryExecutionId"]
        return query_execution_id
    except ClientError as e:
        print('Query failed to submit' % e)
        return 'null'


def job_status():
    print("Checking job status")
    while len(create_query_ids) > 0:
        response = ath.batch_get_query_execution(QueryExecutionIds=create_query_ids)
        execution_ids = response['QueryExecutions']
        for i in execution_ids:
            query_id = i['QueryExecutionId']
            status = i['Status']['State']
            if status == 'SUCCEEDED':
                create_query_ids.remove(query_id)
            elif status == 'FAILED':
                create_query_ids.remove(query_id)
                failed_create_query_id.append(query_id)
            else:
                print(query_id, status)
        time.sleep(5)


def copy_object(state, src_bucket, src_prefix, view_name, dest_prefix,extract_time):
    if state:
        if state == 'SUCCEEDED':
            time.sleep(5)
            num = 1
            for s3object in src_bucket.objects.filter(Prefix=src_prefix):
                srckey = s3object.key
                print(srckey)
                if not srckey.endswith('/'):
                    file_num = str(num)
                    fileName = view_name + '_' + extract_time + '_' + file_num + '.gz'
                    print(fileName)
                    destfilekey = dest_prefix + fileName
                    copysource = src_bucket_name + '/' + srckey
                    print('destination_: s3://'+dest_bucket_name+"/" + destfilekey)
                    print('Source: s3://' + copysource)
                    s3.Object(dest_bucket_name, destfilekey).copy_from(CopySource=copysource)

                    num = num + 1
            print('Data copy is completed...')
            return 'success'
        else:

            print('Data is not copied...')
            return 'failed'
    else:
        print('Query is not started executing...')
        return 'failed'

def s3_object_rowcount(dest_bucket_name, prefix, view_name, run_date2):

    try:
        print("Getting details\n")
        print("dest_bucket_name\n")
        print("prefix\n")
        response = s3client.list_objects(Bucket=dest_bucket_name, Prefix=prefix, MaxKeys=100)
        print("got copy response\n")
        outputs = []
        print(response)
        objects = response["Contents"]
        print(objects)
        print("got copy response_1\n")
        for i in objects:
            file_name = i['Key']
            buffer = io.BytesIO()
            s3object = s3.Object(dest_bucket_name, file_name)
            s3object.download_fileobj(buffer)
            df = pq.ParquetFile(buffer)
            rowcount = df.metadata.num_rows
            print(file_name)
            f_name = file_name.split('/', 8)
            print(f_name)
            f_name1 = f_name[7]
            outputs.append( 'rdm'+ "," +run_date2 + ',' + view_name + ',' + 'refresh' + ',' + f_name1 + ',' + str(rowcount) )

    except KeyError:
        outputs.append('rdm'+ run_date2 + ',' + view_name + ',' + 'refresh' + ',' + 'No_File' + ',' + '0' )
        print('Cannot read s3 file')

    return outputs


def copy_files(view_name):
    if len(failed_create_query_id) > 0:
        print("Create table failed for some view, check athena history page")
        exit()
    else:
        manifest_view_list = []
        print("\ncopy files.. Started")
        print("copying\n")
        print(database+"\n")
        print(view_name+"_"+run_date+"\n")
        response = glue.get_table(DatabaseName=database, Name=view_name+"_"+run_date)
        print("Got database details from Glue\n")
        print(response['Table']['StorageDescriptor']['Location'])
        print("\n database details above \n")
        table_dest = response['Table']['StorageDescriptor']['Location']
        f_name = table_dest.split('/', 6)
        table_loc = f_name[6]
#        src_prefix = 'data-ops/athena-query-results/tables/' + table_loc + '/'
        src_prefix = 'data-ops/athena-query-results/tables/' + table_loc
        dest_prefix = 'data-views/' + extract_loc + '/'
        dest_extract_prefix = dest_prefix + 'rdm/' + view_name.lower() + '/' + run_year + '/' + run_month + '/' + run_date1 + '/'
        print("details to copy object\n")
#        print(src_bucket+"\n")
        print(src_prefix+"\n")
        print(view_name+"\n")
        print(dest_extract_prefix+"\n")
        print(extract_time+"\n")
        copy_state = copy_object('SUCCEEDED', src_bucket, src_prefix, view_name, dest_extract_prefix, extract_time)
        manifest_view_list.append(view_name)


    manifest = []
    if len(manifest_view_list) > 0:
        for view_name in manifest_view_list:
            dest_prefix = 'data-views/' + extract_loc + '/'
            dest_manifest_prefix = dest_prefix + 'rdm/manifest/' + run_year + '/' + run_month + '/' + run_date1 + '/'
            prefix = dest_prefix + 'rdm/' + view_name.lower() + '/' + run_year + '/' + run_month + '/' + run_date1 + '/'

            manifest.append(s3_object_rowcount(dest_bucket_name, prefix, view_name, run_date2))


        manifest1 = ['\n'.join(ele) for ele in manifest]
        manifest_file_name = 'manifest/manifest_' + extract_time + '.csv'
        with open(manifest_file_name, 'a') as filehandle:
            for listitem in manifest1:
                filehandle.write('%s\n' % listitem)


        dest_file_name = dest_manifest_prefix + manifest_file_name
        s3client.upload_file(manifest_file_name, dest_bucket_name, dest_file_name)

    else:
        print('Manifest file generation is not needed...')

reate_query_ids = []
failed_create_query_id = []

manifest_file_name = 'manifest/manifest_' + extract_time + '.csv'
with open(manifest_file_name, 'w') as filehandle:
#    filehandle.write("source,run_date,table_name,type,file_name,record_count\n")
    print("Starting Manifest generation")
filehandle.close()


if codeset == 'medispan':
    for view_name in medispan_table_list.split(','):
        create_query_ids = []
        failed_create_query_id = []
        create_query_ids.append(create_table(view_name,run_date,sql_path,src_bucket_name))
        job_status()
        copy_files(view_name)
elif codeset == 'county':
    for view_name in county_table_list.split(','):
        create_query_ids = []
        failed_create_query_id = []
        create_query_ids.append(create_table(view_name,run_date,sql_path,src_bucket_name))
        job_status()
        copy_files(view_name)
elif codeset == 'rxnorm':
    for view_name in rxnorm_table_list.split(','):
        create_query_ids = []
        failed_create_query_id = []
        create_query_ids.append(create_table(view_name,run_date,sql_path,src_bucket_name))
        job_status()
        copy_files(view_name)
elif codeset == 'ncpdp':
    for view_name in ncpdp_table_list.split(','):
        create_query_ids = []
        failed_create_query_id = []
        create_query_ids.append(create_table(view_name,run_date,sql_path,src_bucket_name))
        job_status()
        copy_files(view_name)
elif codeset == 'brd':
    for view_name in brd_table_list.split(','):
        create_query_ids = []
        failed_create_query_id = []
        create_query_ids.append(create_table(view_name,run_date,sql_path,src_bucket_name))
        job_status()
        copy_files(view_name)
elif codeset in ['LOINC_REGENSTRIEF','HCPCS_II','icd_10','ZIP_CODE_CROSSWALK_DARTMOUTH_ATLAS','GRA_CMS','FACETS_CODESET_2020','PRIME_RX_CODESET_VF2020','VSP_CODESET_2020','HCPCSII_MODIFIER','PROVIDER_TAXONOMY','PROVIDER_NPPES_SUB_CMS','PROVIDER_NPPES_CMS','CAMBIA_LOOKUPS']:
    view_name = codeset
    create_query_ids = []
    failed_create_query_id = []
    create_query_ids.append(create_table(view_name,run_date,sql_path,src_bucket_name))
    job_status()
    copy_files(view_name)

else:
    print("Invalid codeset, enter the correct codeset \n")
