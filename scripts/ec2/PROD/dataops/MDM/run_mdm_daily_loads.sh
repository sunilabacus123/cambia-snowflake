
# This Job will trigger MDM daily loads for Member and Person

if [ "$#" -ne 1 ]; then
    echo "Please pass the date parameter"
fi

echo ${1}
job_report=$(date '+%s')"_report.txt"

echo "Data copy started for MEMBER">> ${job_report}

python mdm_data_copy.py MEMBER ${1}


if [[ $? == 0 ]]
then
echo "Data copy Completed for MEMBER" >> ${job_report}
echo "" >> ${job_report}
echo "Data copy started for PERSON" >> ${job_report}
python mdm_data_copy.py PERSON ${1}
fi


if [[ $? == 0 ]]
then
echo "Data copy Completed for PERSON" >> ${job_report}
fi

