import json
import datetime
import io
import time
import sys  
import os
import boto3
import subprocess
from boto3 import client
import pyarrow.parquet as pq
s3_conn   = client('s3')

currentDT = datetime.datetime.now()
s3 = boto3.resource('s3',region_name='us-east-1')
s3client = boto3.client('s3',region_name='us-east-1')
ingestionid = list(sys.argv[2].split(",")) 
extract_time = currentDT.strftime("%Y%m%d%H%M")
run_count = 1


MANIFEST_CHECK = False
print(sys.argv[2])
print(type(sys.argv[2]))
if len(sys.argv) != 3:
    print("Usage: mdm_data_copy.py <source>  <DATE>")
    exit()
elif type(ingestionid) is not list:
    print("Please pass list for DATE\n")
    exit()
else:    
    env = sys.argv[1]


source = sys.argv[1]
if source == 'MEMBER':
    source_file = "manifest_mdm_member_data.txt"
elif source == 'PERSON':
    source_file = "manifest_mdm_person_data.txt"
else:
    print("please enter correct source details\n")
    exit()

# Sample command  python /home/ec2-user/dataops/MDM/mdm_data_copy.py MEMBER 2021/09/14,2021/09/15
CONFIG_BUCKET_NAME = 'abacus-sandboxes-cambia'
CONFIG_KEY = 'data-ops/etl/cambia_extract/config/mdm/'+source+'_manifest_MDM_run_completed.txt'

manifest = "mdm_"+source+"_manifest_"+extract_time+".csv"
mf_cmp1 = open(manifest, "a")
mf_cmp1.write("data,source,file_name,count\n")
mf_cmp1.close()

file = open(source_file, "r")
read = file.read()

for line in read.splitlines():
    if 'SOURCE_BUCKET=' in line:
        src_bucket_name = line.split('=',1)[1]
    elif 'TARGET_BUCKET=' in line:
        trg_bucket_name =  line.split('=',1)[1]
    elif 'TARGET_PREFIX=' in line:
        trg_prefix = line.split('=',1)[1]
    elif 'SOURCE_PREFIX=' in line:
        src_prefix = line.split('=',1)[1]


print("-------------------------------------------------------------------------------")
print("| Data Copy from Stage to distibution for source data is in progress          |")
print("| This sript copy data to s3 bucket abacus-data-lake-cambia                   |")
print("| s3 replication is set up on this bucket to copy data to dist bcuket         |")
print("| Data Copy in Progess for DATE's  "+sys.argv[2]+"                            |")
print("| For MDM "+sys.argv[1]+"                                                     |")
print("-------------------------------------------------------------------------------")

print(src_prefix)
print(trg_prefix)
print(trg_bucket_name)
print(src_bucket_name)
print(source)
print(source_file)
print(ingestionid)


try:
    s3.Bucket(CONFIG_BUCKET_NAME).download_file(CONFIG_KEY, source+'_manifest_MDM_run_completed.txt')
except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == "404":
        print("The object does not exist.")
    else:
        raise



def copy_object(table_name, ingest, src_bucket, src_prefix, dest_bucket, dest_prefix, sleep_check):
    bucketobj = s3.Bucket(src_bucket)
    num = 1
    num1 = 1
    trg_prefix_count = ""
    if sleep_check:
        time.sleep(5)
    print("-----------------------------------------------------------------------")
    print("Data copy is running for table "+table_name +" Ingestion ID "+ingest)
    print("-----------------------------------------------------------------------")
    print(table_name)
    src_prefix = src_prefix+"/"+ingest
    for s3object in bucketobj.objects.filter(Prefix=src_prefix):
        srckey = s3object.key
        if not srckey.endswith('/') and not srckey.endswith('SUCCESS'):
            file_num = str(num)

            copysource = src_bucket+"/"+srckey
            file_name_json = list(copysource.split("/"))
            destfilekey = dest_prefix+"/"+table_name.lower()+"/"+ingest+"/"+file_name_json[7]


            logfile = open(source+'_manifest_MDM_run_completed.txt', 'r')
            loglist = logfile.readlines()
            logfile.close()
            found = False

            table_file_name = table_name+":"+ingest+":"+file_name_json[7]

            for line1 in loglist:
                if str(table_file_name) in line1:            
                    found = True
    
            if not found:
                count_file = open(source+"_MDM_count_check.sh", "w")
                count_file.write("aws s3 cp s3://"+dest_bucket+"/"+destfilekey+" - | gunzip |  wc -l")
                count_file.close()
                print("Copy Started for :"+ srckey)
                s3.Object(dest_bucket, destfilekey).copy_from(CopySource=copysource)
                result = subprocess.run(['chmod', '+x', source+'_MDM_count_check.sh'], stdout=subprocess.PIPE)
                out = os.popen('./'+source+'_MDM_count_check.sh').read()
                manifest = "mdm_"+table_name+"_manifest_"+extract_time+".csv"
                mf_cmp = open(manifest, "a")
                mf_cmp.write("mdm_data,"+table_name+","+file_name_json[7]+","+out.strip()+"\n")
                mf_cmp.close()

                MANIFEST_CHECK = True
                num1 = num1 + 1
                num = num + 1
                run_cmp = open(source+"_manifest_MDM_run_completed.txt", "a")
                run_cmp.write(table_name+":"+ingest+":"+file_name_json[7]+"\n")
                run_cmp.close()
                print(MANIFEST_CHECK)
            else:
                print(" File already copied :"+file_name_json[7])

for i in ingestionid:
        print(" ")
        sleep_check = False
        if run_count > 1:
            sleep_check = True
            
        table_name = source
        manifest = "mdm_"+table_name+"_manifest_"+extract_time+".csv"

        copy_object(table_name, i, src_bucket_name, src_prefix, trg_bucket_name, trg_prefix, sleep_check)
              
manifest = source+"_mdm_manifest_"+table_name+"_manifest_"+extract_time+".csv"
s3client.upload_file(source+'_manifest_MDM_run_completed.txt', CONFIG_BUCKET_NAME , CONFIG_KEY)
s3client.upload_file(source+'_manifest_MDM_run_completed.txt', CONFIG_BUCKET_NAME , CONFIG_KEY+"_"+extract_time)

s3client.upload_file(manifest, trg_bucket_name, trg_prefix+"/manifest/"+manifest)

exit()
