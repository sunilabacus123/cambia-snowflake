#!/bin/bash
# This Job will trigger MDM daily loads for Member and Person

# get the current date and previous date

LOG_FILE="/home/ec2-user/dataops/MDM/complete_job_report.log"

logit()
{
    echo "${*}" >> ${LOG_FILE}
}


while :
do
        aws s3 cp s3://abacus-sandboxes-cambia/data-ops/etl/cambia_extract/config/mdm/complete_job_report.log .
	logit "---------------------------------------------------------------"
	logit "Job Triggered on  : "$(date '+%Y/%m/%d')
	logit "Running loads for : "$(date --date="1 day ago" '+%Y/%m/%d')","$(date '+%Y/%m/%d')
	logit "---------------------------------------------------------------"

	dates=$(date --date="1 day ago" '+%Y/%m/%d')","$(date '+%Y/%m/%d')
	current_date=$(date '+%Y/%m/%d:%H:%m')

	# Report files
	complet_job_report="complet_job_report.txt"

	# Download the report file

	# copy the reports
	logit "MEMBER--START--"${current_date} 

	# Call the Member Job
	python /home/ec2-user/dataops/MDM/mdm_data_copy.py MEMBER ${dates}


	if [[ $? == 0 ]]
	then

	current_date=$(date '+%Y/%m/%d:%H:%m')
	logit  "MEMBER--END--"${current_date}
	current_date=$(date '+%Y/%m/%d:%H:%m')
	logit  "PERSON--START--"${current_date}

	# Call the Person Job
	python /home/ec2-user/dataops/MDM/mdm_data_copy.py PERSON ${dates}
	fi


	if [[ $? == 0 ]]
	then
	current_date=$(date '+%Y/%m/%d:%H:%m')
	logit  "PERSON--END--"${current_date} 
	logit  "" 
	fi

	#Upload the report file
	aws s3 cp  ${LOG_FILE}  s3://abacus-sandboxes-cambia/data-ops/etl/cambia_extract/config/mdm/complete_job_report.log

	sleep 43200

done
